################################################################################
# Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# Install file for building open scenario api with Conan
################################################################################

from conan import ConanFile
from conan.tools.files import export_conandata_patches, patch
from conans import tools
import os

required_conan_version = ">=1.47.0"

class OpenScenarioApiConan(ConanFile):
    name = "openscenario_api"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}
    generators = "cmake"
    _repo_source = None
    short_paths = True
    _artifact_path = None
    no_copy_source = True

    def export_sources(self):
        export_conandata_patches(self)

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def _get_url_sha(self):
        if self.version in self.conan_data["sources"]:
            url = self.conan_data["sources"][self.version]["url"]
            sha256 = self.conan_data["sources"][self.version]["sha256"]
        else:
            url = self.conan_data["sources"]["default"]["url"]
            sha256 = self.version
        
        return url, sha256
        
    def _patch_sources(self):
        os.chdir(self.name)
        if self.settings.os == "Windows":
            os.system('git apply ../patches/v1.3.1.patch')
        else:
            os.system('git apply ../patches/rpath.patch')

    def source(self): 
        url, sha256 = self._get_url_sha()
        git = tools.Git(folder=self.name)
        git.clone(url)
        git.checkout(sha256)
        self._repo_source = os.path.join(self.source_folder, self.name)
        self._artifact_path = os.path.join(self._repo_source, "cpp", "buildArtifact")
        self._patch_sources()
    
    def build(self):
        if self.settings.os == "Windows":
            os.chdir(os.path.join(self._repo_source, "cpp"))
            os.system('cmake -Wno-dev --preset="MSYS-shared-release"')
            os.system('cmake --build --preset="Build-MSYS-shared-release"')
        else:
            os.chdir(self._artifact_path)
            os.system('chmod +x generateLinux.sh')
            os.system('./generateLinux.sh shared release make')

    def package(self):
        self.copy("*", src=os.path.join(self._repo_source, "cpp/common"), dst="include/common")  
        
        self.copy("*", src=os.path.join(self._repo_source, "cpp/expressionsLib/inc"), dst="include/expressionsLib/inc")     

        self.copy("*", src=os.path.join(self._repo_source, "cpp/externalLibs/Filesystem"), dst="include/externalLibs/Filesystem")        
        self.copy("*", src=os.path.join(self._repo_source, "cpp/externalLibs/TinyXML2"), dst="include/externalLibs/TinyXML2")   

        self.copy("*", src=os.path.join(self._repo_source, "cpp/openScenarioLib/src"), dst="include/openScenarioLib/src")      

        self.copy("*", src=os.path.join(self._repo_source, "cpp/openScenarioLib/generated"), dst="include/openScenarioLib/generated")    

        self.copy("*", src=os.path.join(self._repo_source, "cpp/build/cgReleaseMakeShared/antlr4_runtime/src/antlr4_runtime/runtime/Cpp/dist"), dst="lib")     
        self.copy("lib*", src=os.path.join(self._repo_source, "cpp/build/cgReleaseMakeShared/openScenarioLib"), dst="lib") 
        self.copy("lib*", src=os.path.join(self._repo_source, "cpp/build/cgReleaseMakeShared/expressionsLib"), dst="lib")     

        os.chdir(self.package_folder)
        os.system('find . -name "*.cpp" -exec rm {} \;')

