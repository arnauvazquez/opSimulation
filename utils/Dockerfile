################################################################################
# Copyright (c) 2020-2021 in-tech GmbH
#               2021 ITK-Engineering GmbH
#               2023 Deutsches Zentrum für Luft- und Raumfahrt e. V. (DLR)
#               2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

FROM ubuntu:22.04

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get -qq update && apt-get -qq dist-upgrade && apt-get install -qq -y --no-install-recommends \
    antlr4 \
    build-essential \
    ca-certificates \
    ccache \
    cmake \
    clang-format-15 \
    dos2unix \
    doxygen \
    g++ \
    gcc \
    git \
    google-mock \
    googletest \
    graphviz \
    lcov \
    libboost-filesystem-dev \
    libgmock-dev \
    libgtest-dev \
    libprotobuf-dev \
    libqt5xmlpatterns5-dev \
    libantlr4-runtime4.9 \
    libantlr4-runtime-java \
    libantlr4-runtime-dev \
    openjdk-17-jre \
    pkg-config \
    protobuf-compiler \
    python3 \
    python3-distutils \
    python3-pip \
    qtbase5-dev \
    uuid-dev \
    && apt-get -qq clean

# Packages required for qt build through conan
RUN apt-get install -qq -y --no-install-recommends \
    libx11-xcb-dev \
    libfontenc-dev \
    libice-dev \
    libsm-dev \
    libxaw7-dev \
    libxcomposite-dev \
    libxcursor-dev \
    libxdamage-dev \
    libxfixes-dev \
    libxi-dev \
    libxinerama-dev \
    libxkbfile-dev \
    libxmu-dev \
    libxmuu-dev \
    libxpm-dev \
    libxrandr-dev \
    libxrender-dev \
    libxres-dev \
    libxss-dev \
    libxt-dev \
    libxtst-dev \
    libxv-dev \
    libxvmc-dev \
    libxxf86vm-dev \
    libxcb-render0-dev \
    libxcb-render-util0-dev \
    libxcb-xkb-dev \
    libxcb-icccm4-dev \
    libxcb-image0-dev \
    libxcb-keysyms1-dev \
    libxcb-randr0-dev \
    libxcb-shape0-dev \
    libxcb-sync-dev \
    libxcb-xfixes0-dev \
    libxcb-xinerama0-dev \
    libxcb-dri3-dev \
    libxcb-cursor-dev \
    libxcb-util-dev \
    && apt-get -qq clean

RUN apt-get install -qq -y --no-install-recommends \
    texlive-base \
    texlive-latex-extra

RUN pip install \
    approvaltests==3.1.0 \
    empty-files \
    breathe \
    "conan>=1.0,<2.0" \
    exhale \
    junitparser \
    lxml \
    "pandas<1.5" \
    "pytest<7" \
    sphinx \
    sphinx-rtd-theme \
    sphinx-tabs \
    sphinxcontrib-spelling \
    watchdog

RUN apt-get install -qq -y --no-install-recommends \
    dvipng


# Create a folder
RUN mkdir -p /opsimulation/conan

# set the folder as a conan home
ENV CONAN_USER_HOME='/opsimulation/conan'

# copy the required contents from the repo to build conan packages
COPY repo/utils/ci/scripts/15_prepare_thirdParty.sh   repo/utils/ci/scripts/15_prepare_thirdParty.sh
COPY repo/utils/ci/conan                              repo/utils/ci/conan

# Create conan packages and make it available in /opsimulation/conan, allows not to build everytime
RUN repo/utils/ci/scripts/15_prepare_thirdParty.sh && \
    rm -r repo deps  && \
    conan remove "*" -s -b -f && \
    chmod -R 777 /opsimulation/conan
