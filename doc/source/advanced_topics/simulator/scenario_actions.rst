..
  *******************************************************************************
  Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _mapping_scenario_actions:

Mapping of OpenSCENARIO Actions
================================

.. table::
   :class: tight-table
   
   =================== =============
   Layer/Component	    API/Datatype
   =================== =============
                       OpenScenario
   OpenScenarioEngine  mantle_api
   opSimualtion	       openPASS
   FMU Interface	   osi3
   =================== =============

AcquirePositionAction
----------------------

.. table::
   :class: tight-table
   
   +--------------+-------------------------------------------------------------------------------------------------------+
   | API/Datatype | Used Methods/Classes/API                                                                              |
   +==============+=======================================================================================================+
   | OpenScenario | **AcquirePositionAction**                                                                             |
   +--------------+-------------------------------------------------------------------------------------------------------+
   | mantle_api   | Entity::AssignRoute(UniqueId, RouteDefinition)                                                        |
   |              | Entity::GetPosition()                                                                                 |
   |              | GeometryHelper::TranslateGlobalPositionLocally()                                                      |
   +--------------+-------------------------------------------------------------------------------------------------------+
   | openPASS	  | SpawnParameter                                                                                        |
   |              | AgentInterface::GetPositionX()                                                                        |
   |              | AgentInterface::GetPositionY()                                                                        |
   |              | AgentInterface::GetYaw()                                                                              |
   |              +-------------------------------------------------------------------------------------------------------+
   |              | RouteSamplerInterface::CalculateRouteFromWaypoints(std::vector<mantle_api::RouteWaypoint>)            |
   |              | EgoAgentInterface::SetRoadGraph(RoadGraph, RoadGraphVertex, RoadGraphVertex)                          |
   +--------------+-------------------------------------------------------------------------------------------------------+
   | osi3         | x -- not implemented                                                                                  |
   +--------------+-------------------------------------------------------------------------------------------------------+

AssignRouteAction
------------------

.. table::
   :class: tight-table
   
   +--------------+-------------------------------------------------------------------------------------------------------+
   | API/Datatype | Used Methods/Classes/API                                                                              |
   +==============+=======================================================================================================+
   | OpenScenario | **AssignRouteAction**                                                                                 |
   +--------------+-------------------------------------------------------------------------------------------------------+
   | mantle_api   | Entity::AssignRoute(UniqueId, RouteDefinition)                                                        |
   +--------------+-------------------------------------------------------------------------------------------------------+
   | openPASS	  | RouteSamplerInterface::CalculateRouteFromWaypoints(std::vector<mantle_api::RouteWaypoint>)            |
   |              | EgoAgentInterface::SetRoadGraph(RoadGraph, RoadGraphVertex, RoadGraphVertex)                          |
   +--------------+-------------------------------------------------------------------------------------------------------+
   | osi3         | x -- not relevant                                                                                     |
   +--------------+-------------------------------------------------------------------------------------------------------+

CustomCommandAction
--------------------

.. table::
   :class: tight-table
   
   +--------------+-------------------------------------------------------------------------------------------------------+
   | API/Datatype | Used Methods/Classes/API                                                                              |
   +==============+=======================================================================================================+
   | OpenScenario | **CustomCommandAction**                                                                               |
   +--------------+-------------------------------------------------------------------------------------------------------+
   | mantle_api   | ExecuteCustomCommand(actors, type, command)                                                           |
   +--------------+-------------------------------------------------------------------------------------------------------+
   | openPASS	  | AddCustomCommand(command)                                                                             |
   |              | GetCustomCommands()                                                                                   |
   |              | ComponentControllerImplementation::Trigger(time)                                                      |
   |              | TrajectoryFollowerCommonBase::Trigger(time)                                                           |
   +--------------+-------------------------------------------------------------------------------------------------------+
   | osi3         | osi3::TrafficAction::CustomAction                                                                     |
   |              | TrafficAction::mutable_custom_action()                                                                |
   |              | TrafficAction_CustomAction::set_command(command)                                                      |
   +--------------+-------------------------------------------------------------------------------------------------------+

FollowTrajectoryAction
-----------------------

.. table::
   :class: tight-table
   
   +--------------+-------------------------------------------------------------------------------------------------------+
   | API/Datatype | Used Methods/Classes/API                                                                              |
   +==============+=======================================================================================================+
   | OpenScenario | **FollowTrajectoryAction**                                                                            |
   +--------------+-------------------------------------------------------------------------------------------------------+
   | mantle_api   | UpdateControlStrategies(FollowTrajectoryControlStrategy)                                              |
   |              | HasControlStrategyGoalBeenReached(ControlStrategyType::kFollowTrajectory)                             |
   |              | GeometryHelper::TranslateGlobalPositionLocally(PolyLine)                                              |
   |              | MovementDomain::kBoth                                                                                 |
   +--------------+-------------------------------------------------------------------------------------------------------+
   | openPASS	  | SetStrategies(MovementDomain::kBoth, ControlStrategyType::kFollowTrajectory)                          |
   |              | Algorithm_Routecontrol_Implementation::ReadWayPointData()                                             |
   |              | DynamicsScenarioImplementation::ReadWayPointData()                                                    |
   |              | TrajectoryFollowerImplementation::Trigger(time)                                                       |
   |              | FmuHelper::AddTrafficCommandActionFromOpenScenarioTrajectory(TrafficCommand::add_action(), Trajectory)|
   +--------------+-------------------------------------------------------------------------------------------------------+
   | osi3         | **osi3::TrafficAction::FollowTrajectoryAction**                                                       |
   |              |                                                                                                       |
   |              | TrafficAction::mutable_follow_trajectory_action()                                                     |
   |              | TrafficAction_FollowTrajectoryAction::add_trajectory_point()                                          |
   |              +-------------------------------------------------------------------------------------------------------+
   |              | **osi3::TrafficAction::FollowPathAction**                                                             |
   |              |                                                                                                       |
   |              | TrafficAction::mutable_follow_path_action()                                                           |
   |              | TrafficAction_FollowPathAction::add_path_point()                                                      |
   +--------------+-------------------------------------------------------------------------------------------------------+

LaneChangeAction
----------------

.. table::
   :class: tight-table
   
   +--------------+-------------------------------------------------------------------------------------------------------+
   | API/Datatype | Used Methods/Classes/API                                                                              |
   +==============+=======================================================================================================+
   | OpenScenario | **LaneChangeAction**                                                                                  |
   |              |                                                                                                       |
   |              | LaneChangeActionImpl::GetLaneChangeTarget()                                                           |
   |              | ConvertScenarioLaneChangeTarget(ILaneChangeTarget) -> mantle_api::LaneId                              |
   |              +-------------------------------------------------------------------------------------------------------+
   |              | **RelativeTargetLane**                                                                                |
   |              |                                                                                                       |
   |              | LaneChangeTargetImpl::GetRelativeTargetLane()                                                         |
   |              | ConvertScenarioRelativeTargetLane(IRelativeTargetLane) -> mantle_api::LaneId                          |
   |              +-------------------------------------------------------------------------------------------------------+
   |              | **AbsoluteTargetLane**                                                                                |
   |              |                                                                                                       |
   |              | LaneChangeTargetImpl::GetAbsoluteTargetLane()                                                         |
   |              | ConvertScenarioAbsoluteTargetLane(IAbsoluteTargetLane) -> mantle_api::LaneId                          |
   +--------------+-------------------------------------------------------------------------------------------------------+
   | mantle_api   | UpdateControlStrategies(PerformLaneChangeControlStrategy)                                             |
   |              | (ControlStrategyType::kPerformLaneChange)                                                             |
   |              +-------------------------------------------------------------------------------------------------------+
   |              | **Relative**                                                                                          |
   |              |                                                                                                       |
   |              | LaneLocationQueryService::GetRelativeLaneId()                                                         |
   +--------------+-------------------------------------------------------------------------------------------------------+
   | openPASS	  | SetStrategies(MovementDomain::kLateral, ControlStrategyType::kPerformLaneChange)                      |
   |              | DynamicsScenarioImplementation::CalculateSinusiodalLaneChange(PerformLaneChangeControlStrategy)       |
   |              | DynamicsScenarioImplementation::ReadWayPointData()                                                    |
   +--------------+-------------------------------------------------------------------------------------------------------+
   | osi3         | x -- not implemented                                                                                  |
   +--------------+-------------------------------------------------------------------------------------------------------+

SpeedAction
---------------

.. table::
   :class: tight-table
   
   +--------------+-------------------------------------------------------------------------------------------------------+
   | API/Datatype | Used Methods/Classes/API                                                                              |
   +==============+=======================================================================================================+
   | OpenScenario | **SpeedAction**                                                                                       |
   |              | SetLinearVelocitySplineControlStrategy(std::string)                                                   |
   +--------------+-------------------------------------------------------------------------------------------------------+
   | mantle_api   | UpdateControlStrategies(FollowVelocitySplineControlStrategy)                                          |
   |              | HasControlStrategyGoalBeenReached(ControlStrategyType::kFollowVelocitySpline)                         |
   |              | SetSpeed(IEntity, units::velocity::meters_per_second_t)                                               |
   |              | MovementDomain::kLongitudinal                                                                         |
   +--------------+-------------------------------------------------------------------------------------------------------+
   | openPASS	  | SetStrategies(MovementDomain::kLongitudinal, ControlStrategyType::kFollowVelocitySpline)              |
   |              | ControlStrategies::HasNewLongitudinalStrategy()                                                       |
   |              | DynamicsScenarioImplementation::GetVelocityFromSplines()                                              |
   +--------------+-------------------------------------------------------------------------------------------------------+
   | osi3         | osi3::TrafficAction::SpeedAction                                                                      |
   |              | TrafficAction::mutable_speed_action()                                                                 |
   |              | TrafficAction_CustomAction::set_absolute_target_speed(targetSpeed)                                    |
   +--------------+-------------------------------------------------------------------------------------------------------+

TeleportAction
---------------

.. table::
   :class: tight-table
   
   +--------------+-------------------------------------------------------------------------------------------------------+
   | API/Datatype | Used Methods/Classes/API                                                                              |
   +==============+=======================================================================================================+
   | OpenScenario | **TeleportAction**                                                                                    |
   +--------------+-------------------------------------------------------------------------------------------------------+
   | mantle_api   | Entity::SetPosition(Vec3<units::length::meter_t>)                                                     |
   |              | Entity::SetOrientation(Orientation3<units::angle::radian_t>)                                          |
   |              | Entity::SetAssignedLaneIds(std::vector<uint64_t>)                                                     |
   |              | GeometryHelper::TranslateGlobalPositionLocally(...)                                                   |
   |              | LaneLocationQueryService::GetUpwardsShiftedLanePosition(...)                                          |
   |              | LaneLocationQueryService::GetLaneIdsAtPosition(…)                                                     |
   +--------------+-------------------------------------------------------------------------------------------------------+
   | openPASS	  | RouteSampler::Sample(mantle_api::Vec3<units::length::meter_t>, units::angle::radian_t)                |
   |              | AgentInterface::SetPositionX(units::length::meter_t)                                                  |
   |              | AgentInterface::SetPositionY(units::length::meter_t)                                                  |
   |              | AgentInterface:SetYaw(units::angle::radian_t)                                                         |
   +--------------+-------------------------------------------------------------------------------------------------------+
   | osi3         | x -- not relevant                                                                                     |
   +--------------+-------------------------------------------------------------------------------------------------------+

TrafficSignalAction
--------------------

.. table::
   :class: tight-table
   
   +--------------+-------------------------------------------------------------------------------------------------------+
   | API/Datatype | Used Methods/Classes/API                                                                              |
   +==============+=======================================================================================================+
   | OpenScenario | **TrafficSignalAction**                                                                               |
   +--------------+-------------------------------------------------------------------------------------------------------+
   | mantle_api   | SetTrafficSignalState(std::string name, std::string state)                                            |
   +--------------+-------------------------------------------------------------------------------------------------------+
   | openPASS	  | WorldData::GetTrafficSignIdMapping()                                                                  |
   |              | WorldData::GetSignalType(Id)                                                                          |
   |              | WorldData::GetTrafficLight(Id)                                                                        |
   |              | ThreeSignalsTrafficLight::SetState(CommonTrafficLight::State)                                         |
   |              | TwoSignalsTrafficLight::SetState(CommonTrafficLight::State)                                           |
   |              | OneSignalsTrafficLight::SetState(CommonTrafficLight::State)                                           |
   |              | DataBuffer::PutAcyclic                                                                                |
   +--------------+-------------------------------------------------------------------------------------------------------+
   | osi3         | TrafficLight_Classification::set_mode(::osi3::TrafficLight_Classification_Mode)                       |
   +--------------+-------------------------------------------------------------------------------------------------------+

TrafficSignalStateAction
-------------------------

.. table::
   :class: tight-table
   
   +--------------+-------------------------------------------------------------------------------------------------------+
   | API/Datatype | Used Methods/Classes/API                                                                              |
   +==============+=======================================================================================================+
   | OpenScenario | **TrafficSignalStateAction**                                                                          |
   +--------------+-------------------------------------------------------------------------------------------------------+
   | mantle_api   | SetTrafficSignalState(std::string name, std::string state)                                            |
   +--------------+-------------------------------------------------------------------------------------------------------+
   | openPASS	  | WorldData::GetTrafficSignIdMapping()                                                                  |
   |              | WorldData::GetSignalType(Id)                                                                          |
   |              | WorldData::GetTrafficLight(Id)                                                                        |
   |              | ThreeSignalsTrafficLight::SetState(CommonTrafficLight::State)                                         |
   |              | TwoSignalsTrafficLight::SetState(CommonTrafficLight::State)                                           |
   |              | OneSignalsTrafficLight::SetState(CommonTrafficLight::State)                                           |
   |              | DataBuffer::PutAcyclic                                                                                |
   +--------------+-------------------------------------------------------------------------------------------------------+
   | osi3         | TrafficLight_Classification::set_mode(::osi3::TrafficLight_Classification_Mode)                       |
   +--------------+-------------------------------------------------------------------------------------------------------+

TrafficSinkAction
--------------------

.. table::
   :class: tight-table
   
   +--------------+-------------------------------------------------------------------------------------------------------+
   | API/Datatype | Used Methods/Classes/API                                                                              |
   +==============+=======================================================================================================+
   | OpenScenario | **TrafficSinkAction**                                                                                 |
   |              |                                                                                                       |
   |              | ConvertScenarioTrafficDefinition(NET_ASAM_OPENSCENARIO::v1_1::ITrafficDefinition)                     |
   +--------------+-------------------------------------------------------------------------------------------------------+
   | mantle_api   | Entity::GetPosition()                                                                                 |
   |              | GeometryHelper::TranslateGlobalPositionLocally()                                                      |
   +--------------+-------------------------------------------------------------------------------------------------------+
   | openPASS	  | SpawnParameter                                                                                        |
   |              +-------------------------------------------------------------------------------------------------------+
   |              | AgentInterface::GetPositionX()                                                                        |
   |              | AgentInterface::GetPositionY()                                                                        |
   |              | AgentInterface:GetYaw()                                                                               |
   +--------------+-------------------------------------------------------------------------------------------------------+
   | osi3         | x -- not relevant                                                                                     |
   +--------------+-------------------------------------------------------------------------------------------------------+

VisibilityAction
--------------------

.. table::
   :class: tight-table
   
   +--------------+-------------------------------------------------------------------------------------------------------+
   | API/Datatype | Used Methods/Classes/API                                                                              |
   +==============+=======================================================================================================+
   | OpenScenario | **VisibilityAction**                                                                                  |
   +--------------+-------------------------------------------------------------------------------------------------------+
   | mantle_api   | SetVisibility(EntityVisibilityConfig)                                                                 |
   +--------------+-------------------------------------------------------------------------------------------------------+
   | openPASS	  | x -- not implemented                                                                                  |
   +--------------+-------------------------------------------------------------------------------------------------------+
   | osi3         | x -- not implemented                                                                                  |
   +--------------+-------------------------------------------------------------------------------------------------------+
