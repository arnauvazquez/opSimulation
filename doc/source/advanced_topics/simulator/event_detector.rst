..
  *******************************************************************************
  Copyright (c) 2021 in-tech GmbH
                2023 Mercedes-Benz Tech Innovation GmbH

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _eventdetector:

EventDetector
=============

EventDetectors are executed at the beginning of each timestep.
A detector uses its access to the world and the framework to detect specific events.
After detection, the event is usually forwarded to the EventNetwork.

There are multiple EventDetectors available, all of them sharing a common interface.

.. note:: Each instance of an EventDetector can have its own cycle time.


.. _eventdetector_collisiondetector:

CollisionDetector
-----------------

The CollisionDetector checks whether a collision has occurred.
Moving and stationary objects are taken into account.
Objects are considered as collided, if their two-dimensional bounding boxes intersect.

.. note:: The calculation of the bounding box itself considers a potential inclination by the roll angle of an object (projection onto the xy-plane).

In case of a collision, a CollisionEvent containing the participating object Ids is created.
This type of event is picked up by the CollisionManipulator, which updates the state of the collided objects accordingly.

The geometric parameters of the crash configuration in terms of collision angles are determined according to Wagstrom et. al. (https://www-esv.nhtsa.dot.gov/Proceedings/26/26ESV-000177.pdf). In addition, collision velocities are stored.

.. figure:: ./images/crash_config.svg

Furthermore, the CollisionDetector contains an impact evaluation which estimates based on momentum the changes in velocities due to the collision, considering vehicle mass ratio and initial crash configuration, e.h. `VelocityChange`. The impact model behind these calculations was developed and tested with full-overlap vehicle-to-vehicle collisions. 

The concept was described in detail at ESAR 2016 in Kolk et. al. (https://bast.opus.hbz-nrw.de/opus45-bast/frontdoor/deliver/index/docId/1825/file/F117_21.pdf). 


The parameters stored for each collision are listed in detail in the table below.

============================ ====================================================================
Parameter                    Description
============================ ====================================================================
CollisionVelocity            velocity of host prior to the collision
CollisionWithAgent           true (1) / false (0) if collision occured with another agent
HCPA                         host collision point angle in degree
HCPAo                        normalized HCPA
OCPA                         opponent collision point angle
OCPAo                        normalized OCPA
OYA                          opponent yaw angle
OpponentCollisionVelocity    velocity of opponent prior to the collision
OpponentPointOfContactLocalX X-coordinate of opponent point of contact in local coordinate system
OpponentPointOfContactLocalY Y-coordinate of opponent point of contact in local coordinate system
OpponentVelocity             velocity of opponent after the collision
OpponentVelocityChange       velocity change of opponent due to collision
OpponentVelocityDirection    velocity direction 
OpponentYawVelocity          opponent yaw velocity
PointOfContactLocalX         X-coordinate of  point of contact in local coordinate system
PointOfContactLocalY         Y-coordinate of opponent point of contact in local coordinate system
Velocity                     velocity of host after the collision
VelocityChange               velocity change of host due to collision
============================ ====================================================================

.. note:: Both geometric interpretation and impact calculation only work for collisions between two car agents. A more general application is work in progress.
