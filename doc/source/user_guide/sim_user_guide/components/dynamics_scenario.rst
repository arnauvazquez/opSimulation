.. _dynamics_scenario:

Dynamics_Scenario
=================

This module acts as a direct implementation of external scenario actions, requested through `ControlStrategies` defined in the `Mantle API <https://gitlab.eclipse.org/eclipse/openpass/scenario_api>`.
As the MantleAPI is not an official standard, the module currently follows a "standard conform" interpretation based on `openSCENARIO V1.1 <https://www.asam.net/standards/detail/openscenario/>`.

.. note::

   Due to the nature of |op| actions that are set by the scenario engine in one timestep, will be visible in the simulation output starting with the next timestep,
   because all states are only syncronized at the end of a timestep

In every timestep one strategy for each MovementDomain (longitudinal and lateral) is active.
If the scenario engine sets a new strategy for a MovementDomain the previous strategy for this MovementDomain is stopped.
A strategy can also apply to both MovementDomains (meaning it is the only active strategy).
If a strategy is finished (except KeepVelocity and KeepLaneOffset which are never finished), this is reported back to the scenario engine.

DefaultStrategies
-----------------

KeepVelocity - longitudinal
~~~~~~~~~~~~~~~~~~~~~~~~~~~

During this strategy the velocity is constant (same as at the time when this strategy is set).
This is the default longitudinal strategy, if no other longitudinal strategy is set.

KeepLaneOffset - lateral
~~~~~~~~~~~~~~~~~~~~~~~~

During this strategy the agent will drive along the lane with a constant offset (same as at the time when this strategy is set).
This is the default lateral strategy, if no other lateral strategy is set.

Strategies
----------

FollowVelocitySpline - longitudinal
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A velocity spline defines a function over time of the agent velocity.
This function is a piecewise polynomial of degree 3.
This strategy will be set for SpeedActions with DynamicsShape other than step.

PerformChangeLane - lateral
~~~~~~~~~~~~~~~~~~~~~~~~~~~

This strategy will have the agent switching lanes while keeping a constant longitudinal velocity (in s direction).
It will be set for LaneChangeActions where the target lane is not the current lane.
At the momement only DynamicsShape sinusoidal and DynamicsDimension distance or time is supported.

.. note::

   In the current implementation both movement domains are touched, even if a longitudinal movement controller is active.
   This means that the velocity of the longitudinal is ignored while the lane change is executed.
   This may result in a jump of the velocity at the end of the lane change.


FollowTrajectory - both
~~~~~~~~~~~~~~~~~~~~~~~~

A trajectory defines the position and orientation of the agent at defined times.
If a timestep lies between the trajectory points the position is linearly interpolated.
This strategy will be set for FollowTrajectoryActions.
TrajectoryFollowingMode is not supported and the agent will always strictly adhere to the trajectory.
