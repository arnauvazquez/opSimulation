..
  *******************************************************************************
  Copyright (c) 2021 ITK Engineering GmbH
                2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _building_with_conan:

Building with Conan
===================

.. image:: https://conan.io/img/logo-conan.svg

General
-------

Conan is an open source, decentralized and multi-platform package manager to create and share native binaries.

To learn more about Conan itself you can visit `Conan <https://conan.io/>`_.

Conan is used in the |op| project to create and manage the binaries of the used ThirdParty libraries.

Additional Prerequisites
--------------------------

To be able to work with Conan it is necessary to add additional packages.

.. tabs::
   
   .. tab:: Windows (MSYS2)

      .. code-block:: bash

         pip install "conan>=1.0,<2.0"

   .. tab:: Linux (Debian Bookworm)

      .. code-block:: bash

         apt install "conan>=1.0,<2.0"


Conan Recipes
-------------

A *recipe* is python file, which is used by conan to build and create a package. 
In this file there are several hooks available, which can be used for several tasks.
E.g. Retrieving the source files, building from source, configuring dependencies, packaging, etc.
Visit `Conan <https://conan.io/>`_. for detailed information about *recipes*.

Conan Create
------------

The `conan create` command is used to create Conan packages from source code. It involves building the source code, packaging the resulting binaries and artifacts, and generating the necessary metadata to publish the package.
The basic syntax of the command is as follows:

.. code-block:: bash

   $ conan create <path_to_recipe> <pkg/version@user/channel>

- `<path_to_source>`: The path to the conan recipe of that corresponding package.
- `<pkg/version@user/channel>`: The user and channel specify the namespace under which the package with the corresponding version will be built.

.. code-block:: bash

   $ conan create openscenario_engine/0.1@openpass/testing

This command will build the package `openscenario_engine` with version `1.0` under the user `openpass` and channel `testing`.

**Optional Settings**

The `-o` option allows you to pass optional settings to the `conan create` command, which can be used to customize the build process of the package. These settings are defined in the `conanfile.py` recipe. 
For example, you can specify build options, compiler flags, or any other configuration settings that influence the package creation process.

.. code-block:: bash

   $ conan create openscenario_engine/0.1@openpass/testing -o MantleAPI_version=0.1 -o Yase_version=0.1

In this example, the `-o` option is used to set the `MantleAPI_version` and `Yase_version` build options for the `openscenario_engine` package. These options will be considered during the package creation process.

.. note::

   For the packages MantleAPI, Yase, openscenario_api and openscenario_engine corresponding commit ids can be used instead of version when creating the package 

Conan Install
-------------

The `conan install` command is used to install Conan packages from a Conan recipe (a `conanfile.py` file) and its associated dependencies. It resolves and fetches the required packages from a remote Conan repository or local conan cache and installs them in the specified target location.
The basic syntax of the command is as follows:

.. code-block:: bash

   $ conan install <pkg/version@user/channel> -g <generator> -s <setting=value> --build <missing|outdated> --install-folder=<installation-path>

- `<pkg/version@user/channel>`: The user and channel specify the namespace under which the package with the corresponding version will be built.
- `-g <generator>`: Specifies the build system generator (e.g., `cmake`, `make`, `visual_studio`) for generating build files.
- `-s <setting=value>`: Overrides a Conan setting defined in the recipe with the given value.
- `--build <missing|outdated>`: Specifies whether to build missing or outdated packages.

**Generate Deployment Files**

The `-g deploy` option with the `conan install` command is used to generate deployment-related files and scripts for installing and running the package on a target system. 
These files can include packaging scripts, installation scripts, and other artifacts required for deploying the package to a specific environment.

.. code-block:: bash

   $ conan install openscenario_engine/0.1@openpass/testing -g deploy --install-folder="my/installation/path"

This command will deploy the package `openscenario_engine/0.1@openpass/testing` at the installation path.

.. note::

   There are several additional commands, arguments and fields for these commands available. 
   Please see `Conan Docs <https://docs.conan.io/en/latest/>`_ for additional information.

Build only ThirdParties
-----------------------

|op| requires number of third parties libraries and they are listed in ``conanfile.txt``. To install all the dependencies, the script ``15_prepare_thirdParty.sh`` can be used.

The following file shows an example which can be used. This example is also available in the conan folder in the opSimulation repository.

.. code-block:: 

   [requires]
      zlib/1.2.12@
      qt/5.15.3@
      gtest/1.11.0@openpass/testing
      FMILibrary/2.0.3@openpass/testing
      protobuf/3.20.0@
      units/2.3.3@openpass/testing
      open-simulation-interface/3.5.0@openpass/testing
      MantleAPI/0.1@openpass/testing
      Yase/0.1@openpass/testing
      openscenario_api/v1.3.1@openpass/testing
      openscenario_engine/0.1@openpass/testing

   [options]
      qt:with_pq=False
      qt:openssl=False
      qt:opengl=no
      msys2:no_kill=True
      qt:qtxmlpatterns=True
      b2:use_cxx_env=True
      b2:toolset=gcc
      protobuf:shared=True
      openscenario_engine:MantleAPI_version=515f156900f866e1004a53b352931547cf5a85f6
      openscenario_engine:Yase_version=d0c0e58d17358044cc9018c74308b45f6097ecfb

   [generators]
      deploy
      cmake_find_package

.. note::

   The exact list of dependency can be found in the source code. 
