..
  *******************************************************************************
  Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
                2021 in-tech GmbH

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _download_and_install_openpass:

Installing openPASS
===================

This section describes how compile and run |op|. Please make sure that all prerequisites have been properly installed according to section :ref:`Prerequisites`.
If you have strictly followed the instructions, the installed source packages should be located on your machine under 
``C:\openpass\thirdParty`` for **Windows** and ``~/openpass/thirdParty`` for **Linux**. If there is a path deviation, 
the following commands must be adjusted.

To keep the installation process of |op| as simple as possible, we again recommend a specific folder structure.
If you have strictly followed and will follow the recommended paths of this guide, no command modifications are necessary.

.. tabs::

   .. tab:: Notes for Windows

      - Recommended checkout path of |op| repository: ``C:\openpass``
      - Recommended dependency directory within the |op| repository: ``C:\openpass\deps\thirdParty``
      - Recommended installation directory of |op|: ``C:\openPASS\bin\core``

   .. tab:: Notes for Linux

      - Recommended checkout path of |op| repository: ``~/openpass``
      - Recommended dependency directory within the |op| repository: ``~/openpass/deps/thirdParty``
      - Recommended installation directory of |op|: ``/usr/local/openPASS/bin/core``
 
The above directory structure will be created by following the instructions of this guide. 

#. Clone |Op| repository

   As described above, the checkout path of the repository is assumed to be the default ``openpass``.

   .. tabs::

      .. tab:: Windows

         Start |mingw_shell|
         
         .. code-block:: 

            cd /C/
            git clone https://gitlab.eclipse.org/eclipse/openpass/opSimulation.git openpass

         .. note::

            As stated in :ref:`Building_under_windows`, the windows programming tools suffer from a `path length restriction`.
            This error manifests as strange **file not found** compile errors.
            It is therefore recommended to use a short path for source code checkout, e.g. a drive letter.
            This can also be done by the windows command `subst <https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/subst>`_.

      .. tab:: Linux

         Start ``Bash`` shell

         .. code-block:: 
         
            cd ~
            git clone https://gitlab.eclipse.org/eclipse/openpass/opSimulation.git openpass

#. Navigate into repository and checkout main branch

   .. code-block:: 

      cd openpass
      git checkout main

#. Create directory structure

   .. code-block:: 

      mkdir -p deps/thirdParty

#. Navigate into directory where installed prerequisites are located

   .. tabs::

      .. tab:: Windows

         .. code-block:: 

            cd /C/openpass/thirdParty

      .. tab:: Linux

         .. code-block:: 
      
               cd ~/openpass/thirdParty

#. Copy prerequisites into repository (c.f. :ref:`Prerequisites`)

   .. tabs::

      .. tab:: Windows

         .. code-block:: 

            cp -r osi /C/openpass/deps/thirdParty
            cp -r protobuf /C/openpass/deps/thirdParty
            cp -r protobuf-shared /C/openpass/deps/thirdParty
            cp -r FMILibrary /C/openpass/deps/thirdParty
            cp -r zlib/contrib/minizip /C/openpass/deps/thirdParty

      .. tab:: Linux

         .. code-block:: 
      
            cp -r osi ~/openpass/deps/thirdParty
            cp -r protobuf ~/openpass/deps/thirdParty
            cp -r protobuf-shared ~/openpass/deps/thirdParty
            cp -r FMILibrary ~/openpass/deps/thirdParty
            cp -r zlib/contrib/minizip /C/openpass/deps/thirdParty

   .. _ref_prerequisites:

   Once prerequisites are in place, all third party dependencies within ``openpass`` can be resolved by ``CMake``. 
   For ``CMake`` builds, each installed prerequisite, which is located in its own subdirectory under 

   - ``C:\openpass\deps\thirdParty`` for Windows and
   - ``~/openpass/deps/thirdParty`` for Linux,
  
   can be referenced by its path. 
   The reference is made via the :ref:`CMAKE_PREFIX_PATH` environmental variable, which holds a list of directories specifying the installed prerequisite. 
   This will be explained in more detail in the following.

#. Navigate back into repository

   .. tabs::

      .. tab:: Windows

         .. code-block:: 

            cd /C/openpass

      .. tab:: Linux

         .. code-block:: 
      
            cd ~/openpass

#. Create build directory and navigate into it

   .. code-block:: 
      
      mkdir build
      cd build

#. Prepare build

   |Op| links against shared libraries, which are located in the paths specified by ``CMAKE_PREFIX_PATH``. 
   To be able to install |Op| with resolved dependencies, all libraries found under the paths have to be copied right next to the executable during the installation step. 
   This is done by setting ``INSTALL_EXTRA_RUNTIME_DEPS=ON``. If you have followed the instructions strictly, no changes are necessary. 

   .. tabs::

      .. tab:: Windows

         .. code-block:: 

            cmake -G "MSYS Makefiles" \
            -D CMAKE_PREFIX_PATH="C:/openpass/deps/thirdParty/FMILibrary;C:/openpass/deps/thirdParty/osi;C:/openpass/deps/thirdParty/protobuf;C:/openpass/deps/thirdParty/protobuf-shared;C:/msys64/mingw64/bin;C:/openpass/deps/thirdParty/minizip" \
            -D CMAKE_INSTALL_PREFIX=C:/openPASS/bin/core \
            -D CMAKE_BUILD_TYPE=Release \
            -D USE_CCACHE=ON \
            -D WITH_DEBUG_POSTFIX=OFF \
            -D OPENPASS_ADJUST_OUTPUT=OFF \
            -D INSTALL_EXTRA_RUNTIME_DEPS=ON \
            ..

         .. note::

          - By specifying ``INSTALL_EXTRA_RUNTIME_DEPS=ON``, runtime dependencies will be copied to the installation directory when running ``make install``. This applies to all dependencies located in the paths specified in ``CMAKE_PREFIX_PATH``.
          - Make sure that the path ``C:/msys64/mingw64/bin`` is the last path in the CMAKE_PREFIX_PATH. Otherwise cmake might find and use local versions of required libraries instead of the ones listed in the thirdparties folder.

      .. tab:: Linux

         .. code-block:: 

            cmake -D CMAKE_PREFIX_PATH="$HOME/openpass/deps/thirdParty/FMILibrary;$HOME/openpass/deps/thirdParty/osi;$HOME/openpass/deps/thirdParty/protobuf;$HOME/openpass/deps/thirdParty/protobuf-shared;$HOME/openpass/deps/thirdParty/minizip" \
            -D CMAKE_INSTALL_PREFIX=/usr/local/openPASS/bin/core \
            -D CMAKE_BUILD_TYPE=Release \
            -D USE_CCACHE=ON \
            -D WITH_DEBUG_POSTFIX=OFF \
            -D OPENPASS_ADJUST_OUTPUT=OFF \
            -D INSTALL_EXTRA_RUNTIME_DEPS=ON \
            ..

         .. note: 
         
             - To install in the mentioned CMAKE_INSTALL_PREFIX path, create ``openPASS`` folder under ``/usr/local`` using ``sudo mkdir /usr/local/openPASS``
               And for the user to access the folder, modify the permissions using ``sudo chown <uesrname>:<username> /usr/local/openPASS``
             - If you need to adjust paths and options based on your system and needs, you need to pay attention on quotations ``" "``, when using it in console (see :ref:`Cmake_prefix_path`).
               And also you need to be careful with it when you configuring it under VSCode (see :ref:`vscode`).
   
   .. note:: For a build that goes beyond the default settings, see :ref:`Cmake` for more available variables and options that can be set.
             

#. Optional: Build and execute unit tests

   Starting from ``openpass/build``:

   .. code-block:: 

      make test ARGS="--output-on-failure -j3"

   .. note::

      ARGS is optional, but recommended.
      Adjust parallel build flag ``-j3`` based on your system.

#. Build documentation

   Starting from ``openpass/build``:

   .. code-block:: 

      make doc

   .. note:: Currently the documentation must be built before |op| can be successfully installed if the CMake variable `WITH_DOC=ON` (default).

#. Build simulation core

   Starting from ``openpass/build``:

   .. code-block:: 

      make -j3 install

   .. note:: Again, adjust parallel build flag ``-j3`` based on your system.
