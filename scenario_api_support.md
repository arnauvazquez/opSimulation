# IGeometryHelper

| Function                               | Status        |
| ---------------------------------------| --------------|
|TranslateGlobalPositionLocally          | ✔️ Implemented |
|TransformPolylinePointsFromWorldToLocal | ✔️ Implemented |
|TransformPositionFromWorldToLocal       | ✔️ Implemented |
| ---------------------------------------| --------------|

# IEnvironment

| Function                          | Status                                             |
| ----------------------------------| ---------------------------------------------------|
| CreateMap                         | ✔️ Implemented                                      |
| AddEntityToController             | ✔️ Implemented                                      |
| RemoveControllerFromEntity        | ❌ Not yet implemented                              |
| UpdateControlStrategies           | Currently ControlStrategies are always overwritten |
| HasControlStrategyGoalBeenReached | ❌ Not yet implemented (always returns true)        |
| GetQueryService                   | ✔️ Implemented                                      |
| GetConverter                      | ✔️ Implemented                                      |
| GetGeometryHelper                 | ✔️ Implemented                                      |
| GetEntityRepository               | ✔️ Implemented                                      |
| GetControllerRepository           | ✔️ Implemented                                      |
| SetDateTime                       | ❌ Not yet implemented                              |
| GetDateTime                       | ❌ Not yet implemented                              |
| GetSimulationTime                 | ✔️ Implemented                                      |
| SetWeather                        | ✔️ Implemented                                      |
| SetRoadCondition                  | ❌ Not yet implemented                              |
| SetTrafficSignalState             | ❌ Not yet implemented                              |
| ExecuteCustomCommand              | ✔️ Implemented                                      |
| SetUserDefinedValue               | ❌ Not yet implemented                              |
| GetUserDefinedValue               | ❌ Not yet implemented (always returns nullopt)     |
| ----------------------------------| ---------------------------------------------------|

# ICoordConverter

| Function | Status                                 |
| ---------| ---------------------------------------|
| Convert  | Implemented for OpenDrivePosition only |
| ---------| ---------------------------------------|

# ILaneLocationQueryService

| Function                                    | Status        |
| --------------------------------------------| --------------|
| GetLaneOrientation                          | ✔️ Implemented |
| GetUpwardsShiftedLanePosition               | ✔️ Implemented |
| IsPositionOnLane                            | ✔️ Implemented |
| GetLaneIdsAtPosition                        | ✔️ Implemented |
| FindLanePoseAtDistanceFrom                  | ✔️ Implemented |
| GetLongitudinalLaneDistanceBetweenPositions | ✔️ Implemented |
| FindRelativeLanePoseAtDistanceFrom          | ✔️ Implemented |
| GetRelativeLaneId                           | ✔️ Implemented |
| --------------------------------------------| --------------|

# IControllerRepository

| Function | Status                                                           |
| ---------| -----------------------------------------------------------------|
| Create   | Implemented for ExternalControllerConfig (openPASS AgentProfile) |
| Get      | ✔️ Implemented                                                    |
| Contains | ✔️ Implemented                                                    |
| Delete   | ❌ Not yet implemented                                            |
| ---------| -----------------------------------------------------------------|

# IEntity

| Function                   | Status                                          |
| ---------------------------| ------------------------------------------------|
| GetUniqueId                | ✔️ Implemented                                   |
| SetName                    | ✔️ Implemented                                   |
| GetName                    | ✔️ Implemented                                   |
| SetPosition                | ✔️ Implemented                                   |
| GetPosition                | ✔️ Implemented                                   |
| SetVelocity                | ✔️ Implemented                                   |
| GetVelocity                | ✔️ Implemented                                   |
| SetAcceleration            | not during Init, only absolute acceleration set |
| GetAcceleration            | ✔️ Implemented                                   |
| SetOrientation             | ✔️ Implemented                                   |
| GetOrientation             | ✔️ Implemented                                   |
| SetOrientationRate         | not during Init                                 |
| GetOrientationRate         | ✔️ Implemented                                   |
| SetOrientationAcceleration | not during Init                                 |
| GetOrientationAcceleration | ✔️ Implemented                                   |
| SetAssignedLaneIds         | ❌ Not yet implemented                           |
| GetAssignedLaneIds         | ❌ Not yet implemented                           |
| SetVisibility              | ❌ Not yet implemented                           |
| GetVisibility              | ❌ Not yet implemented                           |
| ---------------------------| ------------------------------------------------|

# IVehicle

| Function          | Status                |
| ------------------| ----------------------|
| SetProperties     | ❌ Not yet implemented |
| GetProperties     | ✔️ Implemented         |
| SetIndicatorState | ❌ Not yet implemented |
| GetIndicatorState | ❌ Not yet implemented |
| ------------------| ----------------------|

# IPedestrian

| Function          | Status                |
| ------------------| ----------------------|
| SetProperties     | ❌ Not yet implemented |
| GetProperties     | ✔️ Implemented         |
| ------------------| ----------------------|

# IStaticObject

❌ Not yet implemented

# IEntityRepository

| Function                      | Status                |
| ------------------------------| ----------------------|
| Create (Vehicle)              | ✔️ Implemented         |
| Create (Pedestrian)           | ✔️ Implemented         |
| Create (StaticObject)         | ❌ Not yet implemented |
| GetHost                       | ❌ Not yet implemented |
| Get (Name)                    | ✔️ Implemented         |
| Get (Id)                      | ✔️ Implemented         |
| Contains                      | ❌ Not yet implemented |
| Delete (Name)                 | ❌ Not yet implemented |
| Delete (Id)                   | ❌ Not yet implemented |
| GetEntities                   | ❌ Not yet implemented |
| RegisterEntityCreatedCallback | ❌ Not yet implemented |
| RegisterEntityDeletedCallback | ❌ Not yet implemented |
| ------------------------------| ----------------------|
