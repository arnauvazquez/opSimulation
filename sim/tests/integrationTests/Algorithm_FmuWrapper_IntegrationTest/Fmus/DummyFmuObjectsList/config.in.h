/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/*
Based on Modelica Reference FMU sources:

Copyright (c) 2023, Modelica Association Project "FMI".
All rights reserved.

The Reference FMUs and FMUSim are released under the 2-Clause BSD license:

--------------------------------------------------------------------------------
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIEDi
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--------------------------------------------------------------------------------

The Reference FMUs are a fork of the Test FMUs (https://github.com/CATIA-Systems/Test-FMUs)
by Dassault Systemes, which are a fork of the FMU SDK (https://github.com/qtronic/fmusdk)
by QTronic, both released under the 2-Clause BSD License.
*/

#pragma once

#include <stdbool.h>
#include <string.h>

// define class name and unique id
// clang-format off
#define MODEL_IDENTIFIER @DUMMY_FMU_NAME@
// clang-format on
#define INSTANTIATION_TOKEN "@FMUGUID@"

#define CO_SIMULATION

// define model size
#define NX 0
#define NZ 0

#define SET_FLOAT64
#define GET_INT32
#define SET_INT32

#define FIXED_SOLVER_STEP 1e-3
#define DEFAULT_STOP_TIME 3

typedef unsigned int ValueReference;

typedef struct
{
  double real[130];
  int integer[30];

} ModelData;
