/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/*
Based on Modelica Reference FMU sources:

Copyright (c) 2023, Modelica Association Project "FMI".
All rights reserved.

The Reference FMUs and FMUSim are released under the 2-Clause BSD license:

--------------------------------------------------------------------------------
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIEDi
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--------------------------------------------------------------------------------

The Reference FMUs are a fork of the Test FMUs (https://github.com/CATIA-Systems/Test-FMUs)
by Dassault Systemes, which are a fork of the FMU SDK (https://github.com/qtronic/fmusdk)
by QTronic, both released under the 2-Clause BSD License.
*/

#include <math.h>  // for fabs()
#include <float.h> // for DBL_MIN
#include <string.h>
#include "config.h"
#include "model.h"

#define V_MIN (0.1)
#define EVENT_EPSILON (1e-10)


void setStartValues(ModelInstance *comp) {
    UNUSED(comp);
}

Status calculateValues(ModelInstance *comp) {
    UNUSED(comp);
    // nothing to do
    return OK;
}

Status getFloat64(ModelInstance* comp, ValueReference vr, double values[], size_t nValues, size_t* index) {

    ASSERT_NVALUES(1);

    if (vr == 0)
    {
        values[(*index)++] = M(inputDouble);
        return OK;
    }
    else if (vr == 1)
    {
        values[(*index)++] = M(inputDouble) * M(scaleParameter) + M(inputInteger);
        return OK;
    }
    else if (vr == 2)
    {
        values[(*index)++] = M(scaleParameter);
        return OK;
    }
    else
    {
        logError(comp, "Unexpected value reference: %u.", vr);
        return Error;
    }
}

Status setFloat64(ModelInstance* comp, ValueReference vr, const double values[], size_t nValues, size_t* index) {

    ASSERT_NVALUES(1);

    if (vr == 0)
    {
         M(inputDouble) = values[(*index)++];
        return OK;
    }
    else if (vr == 1)
    {
        return OK;
    }
    else if (vr == 2)
    {
         M(scaleParameter) = values[(*index)++];
        return OK;
    }
    else
    {
        logError(comp, "Unexpected value reference: %u.", vr);
        return Error;
    }
}

Status getInt32(ModelInstance* comp, ValueReference vr, int32_t values[], size_t nValues, size_t* index) {

    ASSERT_NVALUES(1);

    if (vr == 0)
    {
        values[(*index)++] = M(inputInteger);
        return OK;
    }
    if (vr == 1)
    {
        return OK;
    }
    else
    {
        logError(comp, "Unexpected value reference: %u.", vr);
        return Error;
    }
}

Status setInt32(ModelInstance* comp, ValueReference vr, const int32_t values[], size_t nValues, size_t* index) {

    ASSERT_NVALUES(1);

    if (vr == 0)
    {
        M(inputInteger) = values[(*index)++];
        return OK;
    }
    if (vr == 1)
    {
        return OK;
    }
    else
    {
        logError(comp, "Unexpected value reference: %u.", vr);
        return Error;
    }
}

Status getBoolean(ModelInstance* comp, ValueReference vr, bool values[], size_t nValues, size_t *index) {

    ASSERT_NVALUES(1);

    if (vr == 0)
    {
        values[(*index)++] = M(inputBoolean);
        return OK;
    }
    else
    {
        logError(comp, "Unexpected value reference: %u.", vr);
        return Error;
    }
}

Status setBoolean(ModelInstance* comp, ValueReference vr, const bool values[], size_t nValues, size_t *index) {

    ASSERT_NVALUES(1);

    if (vr == 0)
    {
        M(inputBoolean) = values[(*index)++];
        return OK;
    }
    else
    {
        logError(comp, "Unexpected value reference: %u.", vr);
        return Error;
    }
}
