/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "agentUpdaterImpl.h"
#include "algo_longImpl.h"
#include "autonomousEmergencyBraking.h"
#include "fakeAgent.h"
#include "fakeCallback.h"
#include "fakeEgoAgent.h"
#include "fakeParameter.h"
#include "fakePublisher.h"
#include "fakeStochastics.h"
#include "fakeWorld.h"
#include "followingDriverModel.h"
#include "lateralImpl.h"
#include "parameters_vehicleImpl.h"
#include "regularDriving.h"
#include "sensorDataSignal.h"
#include "sensor_driverImpl.h"
#include "signalPrioritizerImpl.h"

using ::testing::_;
using ::testing::A;
using ::testing::AnyNumber;
using ::testing::Return;
using ::testing::ReturnRef;

TEST(DynamicsSignal_Controller, ActiveDriverAndAdas_WritesCorrectLonditudinalAndLateralController)
{
  auto stochastics = FakeStochastics{};
  auto world = FakeWorld{};
  auto parameters = FakeParameter{};
  auto publisher = FakePublisher{};
  auto callbacks = FakeCallback{};
  auto agent = FakeAgent{};
  auto egoAgent = FakeEgoAgent{};

  ON_CALL(agent, GetEgoAgent()).WillByDefault(ReturnRef(egoAgent));
  GlobalRoadPositions roadPosition;
  ON_CALL(agent, GetRoadPosition(_)).WillByDefault(ReturnRef(roadPosition));
  auto vehicleModel = std::make_shared<mantle_api::VehicleProperties>();
  vehicleModel->type = mantle_api::EntityType::kVehicle;
  vehicleModel->mass = 1_kg;
  vehicleModel->rear_axle.wheel_diameter = 1_m;
  vehicleModel->rear_axle.bb_center_to_axle_center.x = 0_m;
  vehicleModel->front_axle.bb_center_to_axle_center.x = 1_m;
  vehicleModel->properties["SteeringRatio"] = "1";
  vehicleModel->properties["NumberOfGears"] = "1";
  vehicleModel->properties["GearRatio1"] = "1";
  vehicleModel->properties["AxleRatio"] = "1";
  vehicleModel->properties["MaximumEngineTorque"] = "1000";
  vehicleModel->properties["MaximumEngineSpeed"] = "2";
  vehicleModel->properties["MinimumEngineSpeed"] = "1";
  vehicleModel->properties["FrictionCoefficient"] = "1";
  vehicleModel->properties["FrontSurface"] = "1";
  vehicleModel->properties["AirDragCoefficient"] = "1";
  ON_CALL(agent, GetVehicleModelParameters()).WillByDefault(Return(vehicleModel));

  osi3::SensorData sensorData;
  auto object = sensorData.add_moving_object();
  object->mutable_base()->mutable_dimension()->set_length(1);
  object->mutable_base()->mutable_dimension()->set_width(1);
  object->mutable_base()->mutable_position()->set_x(1);
  object->mutable_base()->mutable_position()->set_y(0);
  object->mutable_base()->mutable_velocity()->set_x(-1);
  object->mutable_base()->mutable_velocity()->set_y(0);
  object->mutable_header()->add_sensor_id()->set_value(0);

  auto sensorDriver = SensorDriverImplementation(
      "SensorDriver", false, 110, 0, 0, 100, &stochastics, &world, &parameters, &publisher, &callbacks, &agent);

  auto parametersVehicle = ParametersVehicleImplementation(
      "ParametersVehicle", false, 110, 0, 0, 100, &stochastics, &world, &parameters, &publisher, &callbacks, &agent);

  auto parametersDriver = FakeParameter{};
  std::map<std::string, double> parametersDriverDouble{};
  ON_CALL(parametersDriver, GetParametersDouble()).WillByDefault(ReturnRef(parametersDriverDouble));
  auto followingDriverModel = AlgorithmAgentFollowingDriverModelImplementation(
      "MyDriver", false, 101, 0, 0, 100, &stochastics, &world, &parametersDriver, &publisher, &callbacks, &agent);

  auto driver_longitudinal = AlgorithmLongitudinalImplementation(
      "DriverLongitudinal", false, 100, 0, 0, 100, &stochastics, &parameters, &publisher, &callbacks, &agent);

  auto driver_lateral = AlgorithmLateralImplementation(
      "DriverLateral", false, 100, 0, 0, 100, &stochastics, &parameters, &publisher, &callbacks, &agent);

  auto parametersAeb = FakeParameter{};
  std::map<std::string, double> parametersAebDouble{{"TTC", 10},
                                                    {"Acceleration", -10},
                                                    {"CollisionDetectionLongitudinalBoundary", 0},
                                                    {"CollisionDetectionLateralBoundary", 0}};
  ON_CALL(parametersAeb, GetParametersDouble()).WillByDefault(ReturnRef(parametersAebDouble));
  auto sensorLink = std::make_shared<FakeParameter>();
  std::map<std::string, const std::string> sensorLinkString{{"InputId", "Camera"}};
  ON_CALL(*sensorLink, GetParametersString()).WillByDefault(ReturnRef(sensorLinkString));
  std::map<std::string, int> sensorLinkInt{{"SensorId", 0}};
  ON_CALL(*sensorLink, GetParametersInt()).WillByDefault(ReturnRef(sensorLinkInt));
  std::map<std::string, ParameterInterface::ParameterLists> parametersListAeb{{"SensorLinks", {sensorLink}}};
  ON_CALL(parametersAeb, GetParameterLists()).WillByDefault(ReturnRef(parametersListAeb));
  auto aeb = AlgorithmAutonomousEmergencyBrakingImplementation(
      "MyAEB", false, 91, 0, 0, 100, &stochastics, &parametersAeb, &publisher, &callbacks, &agent);

  auto aeb_longitudinal = AlgorithmLongitudinalImplementation(
      "AebLongitudinal", false, 90, 0, 0, 100, &stochastics, &parameters, &publisher, &callbacks, &agent);

  auto parametersPrioritizer = FakeParameter{};
  std::map<std::string, int> parametersPrioritizerInt{{"0", 0}, {"1", 1}};
  ON_CALL(parametersPrioritizer, GetParametersInt()).WillByDefault(ReturnRef(parametersPrioritizerInt));
  auto prioritizer_longitudinal = SignalPrioritizerImplementation("PrioritizerLongitudinal",
                                                                  false,
                                                                  80,
                                                                  0,
                                                                  0,
                                                                  100,
                                                                  &stochastics,
                                                                  &parametersPrioritizer,
                                                                  &publisher,
                                                                  &callbacks,
                                                                  &agent);

  auto dynamicsRegularDriving = DynamicsRegularDrivingImplementation(
      "RegularDriving", false, 70, 0, 0, 100, &stochastics, &world, &parameters, &publisher, &callbacks, &agent);

  auto agentUpdater = AgentUpdaterImplementation(
      "AgentUpdater", false, 10, 0, 0, 100, &stochastics, &world, &parameters, &publisher, &callbacks, &agent);

  EXPECT_CALL(publisher, Publish(_, A<const openpass::databuffer::Value&>())).Times(AnyNumber());
  EXPECT_CALL(publisher, Publish("LongitudinalController", openpass::databuffer::Value{"MyAEB"}));
  EXPECT_CALL(publisher, Publish("LateralController", openpass::databuffer::Value{"MyDriver"}));

  std::shared_ptr<SignalInterface const> signal;

  sensorDriver.Trigger(0);
  sensorDriver.UpdateOutput(0, signal, 0);
  followingDriverModel.UpdateInput(0, signal, 0);
  driver_longitudinal.UpdateInput(3, signal, 0);
  aeb_longitudinal.UpdateInput(3, signal, 0);
  driver_lateral.UpdateInput(101, signal, 0);

  parametersVehicle.Trigger(0);
  parametersVehicle.UpdateOutput(1, signal, 0);
  driver_longitudinal.UpdateInput(1, signal, 0);
  aeb_longitudinal.UpdateInput(1, signal, 0);
  driver_lateral.UpdateInput(100, signal, 0);
  dynamicsRegularDriving.UpdateInput(100, signal, 0);

  followingDriverModel.Trigger(0);
  followingDriverModel.UpdateOutput(0, signal, 0);
  driver_lateral.UpdateInput(0, signal, 0);
  followingDriverModel.UpdateOutput(2, signal, 0);
  driver_longitudinal.UpdateInput(0, signal, 0);

  driver_lateral.Trigger(0);
  driver_lateral.UpdateOutput(0, signal, 0);
  dynamicsRegularDriving.UpdateInput(1, signal, 0);

  driver_longitudinal.Trigger(0);
  driver_longitudinal.UpdateOutput(0, signal, 0);
  prioritizer_longitudinal.UpdateInput(0, signal, 0);

  signal = std::make_shared<SensorDataSignal>(sensorData);
  aeb.UpdateInput(0, signal, 0);
  aeb.Trigger(0);
  aeb.UpdateOutput(0, signal, 0);
  aeb_longitudinal.UpdateInput(0, signal, 0);

  aeb_longitudinal.Trigger(0);
  aeb_longitudinal.UpdateOutput(0, signal, 0);
  prioritizer_longitudinal.UpdateInput(1, signal, 0);

  prioritizer_longitudinal.Trigger(0);
  prioritizer_longitudinal.UpdateOutput(0, signal, 0);
  dynamicsRegularDriving.UpdateInput(0, signal, 0);

  dynamicsRegularDriving.Trigger(0);
  dynamicsRegularDriving.UpdateOutput(0, signal, 0);
  agentUpdater.UpdateInput(0, signal, 0);

  agentUpdater.Trigger(0);
}