/********************************************************************************
 * Copyright (c) 2019 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <gmock/gmock.h>

#include "common/vector2d.h"
#include "include/worldObjectInterface.h"

class FakeWorldObject : public virtual WorldObjectInterface
{
public:
  MOCK_CONST_METHOD0(GetType, ObjectTypeOSI());
  MOCK_CONST_METHOD0(GetPositionX, units::length::meter_t());
  MOCK_CONST_METHOD0(GetPositionY, units::length::meter_t());
  MOCK_CONST_METHOD0(GetPositionZ, units::length::meter_t());
  MOCK_CONST_METHOD0(GetWidth, units::length::meter_t());
  MOCK_CONST_METHOD0(GetLength, units::length::meter_t());
  MOCK_CONST_METHOD1(GetRoadPosition, const GlobalRoadPositions&(const ObjectPoint& point));
  MOCK_CONST_METHOD0(GetTouchedRoads, const RoadIntervals&());
  MOCK_CONST_METHOD1(GetAbsolutePosition, Common::Vector2d<units::length::meter_t>(const ObjectPoint& objectPoint));
  MOCK_CONST_METHOD0(GetHeight, units::length::meter_t());
  MOCK_CONST_METHOD0(GetYaw, units::angle::radian_t());
  MOCK_CONST_METHOD0(GetRoll, units::angle::radian_t());
  MOCK_CONST_METHOD0(GetId, int());
  MOCK_CONST_METHOD0(GetBoundingBox2D, const polygon_t&());
  MOCK_CONST_METHOD0(GetDistanceReferencePointToLeadingEdge, units::length::meter_t());
  MOCK_CONST_METHOD0(GetVelocity, units::velocity::meters_per_second_t());
  MOCK_CONST_METHOD1(GetVelocity, Common::Vector2d<units::velocity::meters_per_second_t>(ObjectPoint));
  MOCK_CONST_METHOD1(GetAcceleration, Common::Vector2d<units::acceleration::meters_per_second_squared_t>(ObjectPoint));
  MOCK_METHOD0(Unlocate, void());
  MOCK_METHOD0(Locate, bool());
};
