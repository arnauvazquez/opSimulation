/********************************************************************************
 * Copyright (c) 2018 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <gtest/gtest.h>

#include "SensorGeometric2D.h"
#include "fakeAgent.h"
#include "fakeParameter.h"
#include "fakeStochastics.h"
#include "fakeWorld.h"
#include "fakeWorldObject.h"

class SensorGeometric2DWithObstruction_UnitTests : public ::testing::Test
{
public:
  SensorGeometric2DWithObstruction_UnitTests();
  virtual ~SensorGeometric2DWithObstruction_UnitTests() = 0;

  void PlaceEgo(units::length::meter_t x, units::length::meter_t y, units::angle::radian_t yaw);
  void PlaceAgent(units::length::meter_t x,
                  units::length::meter_t y,
                  units::length::meter_t length,
                  units::length::meter_t width,
                  units::angle::radian_t yaw);
  void InitSensor(units::length::meter_t range, double openingAngleH, bool obstruction, double minAreaPercentage);
  const std::vector<DetectedObject> DetectAndSortObjects();

  const polygon_t& GetBBByAgentId(size_t id);

  SensorGeometric2D* sensor = nullptr;

private:
  ::testing::NiceMock<FakeWorld> fakeWorldInterface;
  ::testing::NiceMock<TestStochasticsInterface> fakeStochastics;
  TestParameterInterface* fakeParameters;

  std::map<std::string, int> fakeIntegers = {{"SensorModelType", 1}};

  std::map<std::string, double> fakeDoubles = {{"FailureProbability", 0}, {"Latency", 0}};

  std::map<std::string, const std::string> fakeStrings = {{"SensorName", "TestSensor"}};

  std::map<std::string, bool> fakeBools;

  ::testing::NiceMock<FakeAgent> fakeEgoAgent;
  std::vector<const WorldObjectInterface*> fakeObjects;
  std::vector<const polygon_t*> boundingBoxes;
  int agentCount{1};  // account for ego
};
