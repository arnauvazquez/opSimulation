/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <stdlib.h>
#include <unistd.h>

#include "SsdToSspNetworkParser.h"
#include "dontCare.h"
#include "fakeAgent.h"
#include "fakeCallback.h"
#include "fakeParameter.h"
#include "fakeRadio.h"
#include "fakeScenarioControl.h"
#include "fakeStochastics.h"
#include "fakeWorld.h"

using ::testing::_;
using ::testing::AtLeast;
using ::testing::Eq;
using ::testing::HasSubstr;
using ::testing::NiceMock;
using ::testing::Return;
using ::testing::ThrowsMessage;

class SSPParserTests : public ::testing::Test
{
public:
  SSPParserTests() {}

protected:
  NiceMock<FakeWorld> fakeWorld;
  NiceMock<FakeAgent> fakeAgent;
  NiceMock<FakeParameter> fakeParameter;
  NiceMock<FakeCallback> fakeCallback;
  const std::shared_ptr<NiceMock<FakeScenarioControl>> fakeScenarioControl{
      std::make_shared<NiceMock<FakeScenarioControl>>()};
};

class TempFile
{
public:
  TempFile(char* filePath) : filePath(filePath) { close(mkstemp(filePath)); }
  ~TempFile() { unlink(filePath); }

private:
  char* filePath;
};

TEST_F(SSPParserTests, TwoComponentsCorrectInput)
{
  SsdToSspNetworkParser ssdToSspNetWorkParser{"SspWrapper",
                                              false,
                                              0,
                                              0,
                                              0,
                                              100,
                                              nullptr,
                                              &fakeWorld,
                                              &fakeParameter,
                                              nullptr,
                                              &fakeAgent,
                                              &fakeCallback,
                                              fakeScenarioControl};

  auto ssdSystem = std::make_shared<SsdSystem>("0");

  char filePath1[] = "tmp_XXXXXX";
  char filePath2[] = "tmp_XXXXXX";
  TempFile tempFile1(filePath1);
  TempFile tempFile2(filePath2);

  auto componentFirst = std::make_shared<SsdComponent>("component1", filePath1, SspComponentType::x_fmu_sharedlibrary);
  componentFirst->SetPriority(1);
  componentFirst->SetWriteMessageParameters({std::make_pair("output_osiType", "file_type")});
  componentFirst->EmplaceConnector("output", SspParserTypes::ScalarConnector{"connector1", {ConnectorType::Real, {}}});
  openpass::parameter::internal::ParameterSetLevel3 parameters;
  parameters.emplace_back("Logging", false);
  parameters.emplace_back("CsvOutput", false);
  componentFirst->SetParameters(parameters);

  auto componentSecond = std::make_shared<SsdComponent>("component2", filePath2, SspComponentType::x_fmu_sharedlibrary);
  componentSecond->SetPriority(0);
  componentSecond->EmplaceConnector("input", SspParserTypes::ScalarConnector{"connector2", {ConnectorType::Real, {}}});
  componentSecond->SetParameters(parameters);

  ssdSystem->AddComponent(std::move(componentFirst));
  ssdSystem->AddComponent(std::move(componentSecond));

  std::map<std::string, std::string> connection{{"startElement", "component1"},
                                                {"endElement", "component2"},
                                                {"startConnector", "connector1"},
                                                {"endConnector", "connector2"}};

  ssdSystem->AddConnection(std::make_shared<std::map<std::string, std::string>>(connection));
  std::vector<std::shared_ptr<SsdFile>> ssdFiles{std::make_shared<SsdFile>("SystemStructure.ssd", ssdSystem)};

  openpass::common::RuntimeInformation runtimeInformation{{openpass::common::framework}, {"", "outputDirectory", ""}};

  ON_CALL(fakeParameter, GetRuntimeInformation()).WillByDefault(testing::ReturnRef(runtimeInformation));
  ON_CALL(fakeAgent, GetId()).WillByDefault(Return(0));

  EXPECT_NO_THROW(ssdToSspNetWorkParser.GetRootSystem(ssdFiles));

  ASSERT_THAT(ssdToSspNetWorkParser.GetPriorities().size(), Eq(2));
  ASSERT_THAT(ssdToSspNetWorkParser.GetPriorities().at("component1"), Eq(1));
  ASSERT_THAT(ssdToSspNetWorkParser.GetPriorities().at("component2"), Eq(0));
  ASSERT_THAT(ssdToSspNetWorkParser.GetConnections().size(), Eq(1));
  ASSERT_THAT(std::get<SspParserTypes::ScalarConnector>(ssdToSspNetWorkParser.GetConnections()[0].first).first,
              Eq("connector1"));
  ASSERT_THAT(std::get<SspParserTypes::ScalarConnector>(ssdToSspNetWorkParser.GetConnections()[0].first).second,
              Eq(std::pair<ConnectorType, std::map<std::string, std::string>>{ConnectorType::Real, {}}));
  ASSERT_THAT(std::get<SspParserTypes::ScalarConnector>(ssdToSspNetWorkParser.GetConnections()[0].second).first,
              Eq("connector2"));
  ASSERT_THAT(std::get<SspParserTypes::ScalarConnector>(ssdToSspNetWorkParser.GetConnections()[0].second).second,
              Eq(std::pair<ConnectorType, std::map<std::string, std::string>>{ConnectorType::Real, {}}));
}

TEST_F(SSPParserTests, OneComponentCorrectInput)
{
  SsdToSspNetworkParser ssdToSspNetWorkParser{"SspWrapper",
                                              false,
                                              0,
                                              0,
                                              0,
                                              100,
                                              nullptr,
                                              &fakeWorld,
                                              &fakeParameter,
                                              nullptr,
                                              &fakeAgent,
                                              &fakeCallback,
                                              fakeScenarioControl};

  auto ssdSystem = std::make_shared<SsdSystem>("root system");
  auto subSystem = std::make_shared<SsdSystem>("TestSystem");
  auto componentFirst
      = std::make_shared<SsdComponent>("component1", "SubSystem.ssp#SubSystem.ssd", SspComponentType::x_ssp_definition);
  componentFirst->SetPriority(1);
  openpass::parameter::internal::ParameterSetLevel3 parameters;
  parameters.emplace_back("Logging", false);
  parameters.emplace_back("CsvOutput", false);
  componentFirst->SetParameters(parameters);
  ssdSystem->AddComponent(std::move(componentFirst));
  std::vector<std::shared_ptr<SsdFile>> ssdFiles{{std::make_shared<SsdFile>("SystemStructure.ssd", ssdSystem),
                                                  std::make_shared<SsdFile>("SubSystem.ssd", subSystem)}};

  openpass::common::RuntimeInformation runtimeInformation{{openpass::common::framework}, {"", "outputDirectory", ""}};

  ON_CALL(fakeParameter, GetRuntimeInformation()).WillByDefault(testing::ReturnRef(runtimeInformation));
  ON_CALL(fakeAgent, GetId()).WillByDefault(Return(0));

  EXPECT_NO_THROW(ssdToSspNetWorkParser.GetRootSystem(ssdFiles));

  ASSERT_THAT(ssdToSspNetWorkParser.GetPriorities().size(), Eq(1));
  ASSERT_THAT(ssdToSspNetWorkParser.GetPriorities().at("component1"), Eq(1));
  ASSERT_THAT(ssdToSspNetWorkParser.GetConnections().size(), Eq(0));
}

TEST_F(SSPParserTests, TwoComponentsIncorrectConnector)
{
  SsdToSspNetworkParser ssdToSspNetWorkParser{"SspWrapper",
                                              false,
                                              0,
                                              0,
                                              0,
                                              100,
                                              nullptr,
                                              &fakeWorld,
                                              &fakeParameter,
                                              nullptr,
                                              &fakeAgent,
                                              &fakeCallback,
                                              fakeScenarioControl};
  char filePath1[] = "tmp_XXXXXX";
  char filePath2[] = "tmp_XXXXXX";
  TempFile tempFile1(filePath1);
  TempFile tempFile2(filePath2);

  auto ssdSystem = std::make_shared<SsdSystem>("0");
  auto componentFirst = std::make_shared<SsdComponent>("component1", filePath1, SspComponentType::x_fmu_sharedlibrary);
  componentFirst->SetPriority(1);
  componentFirst->SetWriteMessageParameters({std::make_pair("output_osiType", "file_type")});
  componentFirst->EmplaceConnector("output", SspParserTypes::ScalarConnector{"connector1", {ConnectorType::Real, {}}});
  openpass::parameter::internal::ParameterSetLevel3 parameters;
  parameters.emplace_back("Logging", false);
  parameters.emplace_back("CsvOutput", false);
  componentFirst->SetParameters(parameters);

  auto componentSecond = std::make_shared<SsdComponent>("component2", filePath2, SspComponentType::x_fmu_sharedlibrary);
  componentSecond->SetPriority(0);
  componentSecond->EmplaceConnector("output", SspParserTypes::ScalarConnector{"connector2", {ConnectorType::Real, {}}});
  componentSecond->SetParameters(parameters);

  ssdSystem->AddComponent(std::move(componentFirst));
  ssdSystem->AddComponent(std::move(componentSecond));

  std::map<std::string, std::string> connection{{"startElement", "component1"},
                                                {"endElement", "component2"},
                                                {"startConnector", "connector1"},
                                                {"wrongConnector", "connector2"}};

  ssdSystem->AddConnection(std::make_shared<std::map<std::string, std::string>>(connection));
  std::vector<std::shared_ptr<SsdFile>> ssdFiles{std::make_shared<SsdFile>("SystemStructure.ssd", ssdSystem)};

  openpass::common::RuntimeInformation runtimeInformation{{openpass::common::framework}, {"", "outputDirectory", ""}};

  ON_CALL(fakeParameter, GetRuntimeInformation()).WillByDefault(testing::ReturnRef(runtimeInformation));
  ON_CALL(fakeAgent, GetId()).WillByDefault(Return(0));

  EXPECT_THAT([&]() { ssdToSspNetWorkParser.GetRootSystem(ssdFiles); },
              ThrowsMessage<std::runtime_error>(HasSubstr("wrongConnector")));
}

TEST_F(SSPParserTests, TwoComponentsMissingConnectionParameter)
{
  SsdToSspNetworkParser ssdToSspNetWorkParser{"SspWrapper",
                                              false,
                                              0,
                                              0,
                                              0,
                                              100,
                                              nullptr,
                                              &fakeWorld,
                                              &fakeParameter,
                                              nullptr,
                                              &fakeAgent,
                                              &fakeCallback,
                                              fakeScenarioControl};
  char filePath1[] = "tmp_XXXXXX";
  char filePath2[] = "tmp_XXXXXX";
  TempFile tempFile1(filePath1);
  TempFile tempFile2(filePath2);

  auto ssdSystem = std::make_shared<SsdSystem>("0");
  auto componentFirst = std::make_shared<SsdComponent>("component1", filePath1, SspComponentType::x_fmu_sharedlibrary);
  componentFirst->SetPriority(1);
  componentFirst->SetWriteMessageParameters({std::make_pair("output_osiType", "file_type")});
  componentFirst->EmplaceConnector("output", SspParserTypes::ScalarConnector{"connector1", {ConnectorType::Real, {}}});
  openpass::parameter::internal::ParameterSetLevel3 parameters;
  parameters.emplace_back("Logging", false);
  parameters.emplace_back("CsvOutput", false);
  componentFirst->SetParameters(parameters);

  auto componentSecond = std::make_shared<SsdComponent>("component2", filePath2, SspComponentType::x_fmu_sharedlibrary);
  componentSecond->SetPriority(0);
  componentSecond->EmplaceConnector("output", SspParserTypes::ScalarConnector{"connector2", {ConnectorType::Real, {}}});
  componentSecond->SetParameters(parameters);

  ssdSystem->AddComponent(std::move(componentFirst));
  ssdSystem->AddComponent(std::move(componentSecond));

  std::map<std::string, std::string> connection{
      {"startElement", "component1"}, {"endElement", "component2"}, {"startConnector", "connector1"}};

  ssdSystem->AddConnection(std::make_shared<std::map<std::string, std::string>>(connection));
  std::vector<std::shared_ptr<SsdFile>> ssdFiles{std::make_shared<SsdFile>("SystemStructure.ssd", ssdSystem)};

  openpass::common::RuntimeInformation runtimeInformation{{openpass::common::framework}, {"", "outputDirectory", ""}};

  ON_CALL(fakeParameter, GetRuntimeInformation()).WillByDefault(testing::ReturnRef(runtimeInformation));
  ON_CALL(fakeAgent, GetId()).WillByDefault(Return(0));

  EXPECT_THAT([&]() { ssdToSspNetWorkParser.GetRootSystem(ssdFiles); },
              ThrowsMessage<std::runtime_error>("SSP Parser: Not all connection parameters provided in"));
}

struct FakeLogger : public CallbackInterface
{
  void Log(CbkLogLevel logLevel, const char*, int, const std::string& message) const override
  {
    logMessages[logLevel].push_back(message);
  }

  // Retrieve all log messages for each log level
  std::vector<std::string> GetLogMessages(CbkLogLevel logLevel) const { return logMessages[logLevel]; }

private:
  mutable std::map<CbkLogLevel, std::vector<std::string>> logMessages;
};

TEST_F(SSPParserTests, TwoComponentsConnectionWithNonexistentConnector)
{
  FakeLogger fakeLogger;

  SsdToSspNetworkParser ssdToSspNetWorkParser{"SspWrapper",
                                              false,
                                              0,
                                              0,
                                              0,
                                              100,
                                              nullptr,
                                              &fakeWorld,
                                              &fakeParameter,
                                              nullptr,
                                              &fakeAgent,
                                              &fakeLogger,
                                              fakeScenarioControl};
  SspLogger::SetLogger(&fakeLogger, 0, "");

  char filePath1[] = "tmp_XXXXXX";
  char filePath2[] = "tmp_XXXXXX";
  TempFile tempFile1(filePath1);
  TempFile tempFile2(filePath2);

  auto ssdSystem = std::make_shared<SsdSystem>("0");
  auto componentFirst = std::make_shared<SsdComponent>("component1", filePath1, SspComponentType::x_fmu_sharedlibrary);
  componentFirst->SetPriority(1);
  componentFirst->SetWriteMessageParameters({std::make_pair("output_osiType", "file_type")});
  componentFirst->EmplaceConnector("output", SspParserTypes::ScalarConnector{"connector1", {ConnectorType::Real, {}}});
  openpass::parameter::internal::ParameterSetLevel3 parameters;
  parameters.emplace_back("Logging", false);
  parameters.emplace_back("CsvOutput", false);
  componentFirst->SetParameters(parameters);

  auto componentSecond = std::make_shared<SsdComponent>("component2", filePath2, SspComponentType::x_fmu_sharedlibrary);
  componentSecond->SetPriority(0);
  componentSecond->EmplaceConnector("output", SspParserTypes::ScalarConnector{"connector2", {ConnectorType::Real, {}}});
  componentSecond->SetParameters(parameters);

  ssdSystem->AddComponent(std::move(componentFirst));
  ssdSystem->AddComponent(std::move(componentSecond));

  std::map<std::string, std::string> connection{{"startElement", "component1"},
                                                {"endElement", "component2"},
                                                {"startConnector", "newConnector"},
                                                {"endConnector", "connector2"}};

  ssdSystem->AddConnection(std::make_shared<std::map<std::string, std::string>>(connection));
  std::vector<std::shared_ptr<SsdFile>> ssdFiles{std::make_shared<SsdFile>("SystemStructure.ssd", ssdSystem)};

  openpass::common::RuntimeInformation runtimeInformation{{openpass::common::framework}, {"", "outputDirectory", ""}};

  ON_CALL(fakeParameter, GetRuntimeInformation()).WillByDefault(testing::ReturnRef(runtimeInformation));
  ON_CALL(fakeAgent, GetId()).WillByDefault(Return(0));

  auto rootSystem = ssdToSspNetWorkParser.GetRootSystem(ssdFiles);
  ASSERT_THAT(fakeLogger.GetLogMessages(CbkLogLevel::Warning), Contains(HasSubstr("newConnector")));
}

TEST_F(SSPParserTests, StringTransformations)
{
  std::string correctnone = "OsmpConnectorName";
  std::string sizecorrect = "OsmpConnectorName.size";
  std::string locorrect = "OsmpConnectorName.base.lo";
  std::string hicorrect = "OsmpConnectorName.base.hi";

  std::string empty = "";
  std::string sizeWrong = "OsmpConnectorNamesize";
  std::string sizeWrongPos = ".size.OsmpConnectorName";
  std::string sizepointInName = "OsmpConnector.Name.size";
  std::string wrongCaps = "OsmpConnectorName.Size";

  std::string loWrong = "OsmpConnectorNamebase.lo";
  std::string loWrongPos = ".base.lo.OsmpConnectorName";
  std::string lopointInName = "OsmpConnector.Name.base.lo";
  std::string loWrongCaps = "OsmpConnectorName.base.Lo";

  std::string hiWrong = "OsmpConnectorNamebase.hi";
  std::string hiWrongPos = ".base.hi.OsmpConnectorName";
  std::string hipointInName = "OsmpConnector.Name.base.hi";
  std::string hiWrongCaps = "OsmpConnectorName.base.Hi";

  EXPECT_EQ(SSPParserHelper::RemoveOSMPRoleFromOSMPAnnotated(hicorrect), correctnone);
  EXPECT_EQ(SSPParserHelper::RemoveOSMPRoleFromOSMPAnnotated(locorrect), correctnone);
  EXPECT_EQ(SSPParserHelper::RemoveOSMPRoleFromOSMPAnnotated(sizecorrect), correctnone);

  EXPECT_EQ(SSPParserHelper::RemoveOSMPRoleFromOSMPAnnotated(correctnone), correctnone);

  EXPECT_EQ(SSPParserHelper::RemoveOSMPRoleFromOSMPAnnotated(sizeWrong), sizeWrong);
  EXPECT_EQ(SSPParserHelper::RemoveOSMPRoleFromOSMPAnnotated(sizeWrongPos), ".OsmpConnectorName");
  EXPECT_EQ(SSPParserHelper::RemoveOSMPRoleFromOSMPAnnotated(sizepointInName), "OsmpConnector.Name");
  EXPECT_EQ(SSPParserHelper::RemoveOSMPRoleFromOSMPAnnotated(wrongCaps), wrongCaps);

  EXPECT_EQ(SSPParserHelper::RemoveOSMPRoleFromOSMPAnnotated(loWrong), loWrong);
  EXPECT_EQ(SSPParserHelper::RemoveOSMPRoleFromOSMPAnnotated(loWrongPos), ".OsmpConnectorName");
  EXPECT_EQ(SSPParserHelper::RemoveOSMPRoleFromOSMPAnnotated(lopointInName), "OsmpConnector.Name");
  EXPECT_EQ(SSPParserHelper::RemoveOSMPRoleFromOSMPAnnotated(loWrongCaps), loWrongCaps);

  EXPECT_EQ(SSPParserHelper::RemoveOSMPRoleFromOSMPAnnotated(hiWrong), hiWrong);
  EXPECT_EQ(SSPParserHelper::RemoveOSMPRoleFromOSMPAnnotated(hiWrongPos), ".OsmpConnectorName");
  EXPECT_EQ(SSPParserHelper::RemoveOSMPRoleFromOSMPAnnotated(hipointInName), "OsmpConnector.Name");
  EXPECT_EQ(SSPParserHelper::RemoveOSMPRoleFromOSMPAnnotated(hiWrongCaps), hiWrongCaps);
}