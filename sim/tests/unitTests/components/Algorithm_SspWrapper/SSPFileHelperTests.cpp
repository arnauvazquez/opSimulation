/*******************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <filesystem>
#include <osi3/osi_sensordata.pb.h>
#include <sstream>
#include <stdlib.h>
#include <vector>

#include <QDir>
#include <QFile>

#include "SsdToSspNetworkParser.h"
#include "common/sensorDataSignal.h"
#include "fakeAgent.h"
#include "fakeCallback.h"
#include "fakeFmuWrapper.h"
#include "fakeParameter.h"
#include "fakeRadio.h"
#include "fakeScenarioControl.h"
#include "fakeWorld.h"
#include "include/fmuHandlerInterface.h"
#include "sim/src/components/Algorithm_SspWrapper/AlgorithmSspWrapper.h"
#include "sim/src/components/Algorithm_SspWrapper/SSPElements/Connector/ConnectorHelper.h"
#include "sim/src/components/Algorithm_SspWrapper/SSPElements/Connector/OSMPConnector.h"
#include "sim/src/components/Algorithm_SspWrapper/Visitors/Connector/UpdateInputSignalVisitor.h"
#include "sim/src/components/Algorithm_SspWrapper/Visitors/Connector/UpdateOutputSignalVisitor.h"

using ::testing::_;
using ::testing::Mock;
using ::testing::NiceMock;
using ::testing::Return;
using ::testing::ReturnArg;
using ::testing::ReturnRef;

using namespace ssp;

class TemporaryFolder
{
public:
  TemporaryFolder(const std::filesystem::path &folderPath)
      : folderPath(folderPath), isCreated(std::filesystem::create_directory(folderPath))
  {
  }

  ~TemporaryFolder() { std::filesystem::remove_all(folderPath); }

  bool hasFolderCreated() const { return isCreated; }

private:
  bool isCreated;
  std::filesystem::path folderPath;
};

class SSPFileHelperTests : public ::testing::Test
{
public:
  SSPFileHelperTests() {}

  std::vector<std::string> split(std::string string, std::string delimiter)
  {
    size_t pos_start = 0, pos_end, delim_len = delimiter.length();
    std::string token;
    std::vector<std::string> res;

    while ((pos_end = string.find(delimiter, pos_start)) != std::string::npos)
    {
      token = string.substr(pos_start, pos_end - pos_start);
      pos_start = pos_end + delim_len;
      res.push_back(token);
    }

    res.push_back(string.substr(pos_start));
    return res;
  }

protected:
  NiceMock<FakeWorld> fakeWorld;
  NiceMock<FakeAgent> fakeAgent;
  NiceMock<FakeParameter> fakeParameter;
  NiceMock<FakeCallback> fakeCallback;
  const std::shared_ptr<NiceMock<FakeScenarioControl>> fakeScenarioControl{
      std::make_shared<NiceMock<FakeScenarioControl>>()};
};

TEST_F(SSPFileHelperTests, WriteOutputJsonSensorData)
{
  FmuVariables fmuVariables;
  fmuVariables.emplace<FMI2>();
  std::get<FMI2>(fmuVariables)
      .emplace("SensorData.base.lo",
               *std::make_shared<FmuVariable2>(
                   0, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));
  std::get<FMI2>(fmuVariables)
      .emplace("SensorData.base.hi",
               *std::make_shared<FmuVariable2>(
                   1, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));
  std::get<FMI2>(fmuVariables)
      .emplace("SensorData.size",
               *std::make_shared<FmuVariable2>(
                   2, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));

  auto fakeFmuWrapper = std::make_shared<FakeFmuWrapper>();

  osi3::SensorData sensorData{};
  sensorData.mutable_version()->set_version_major(1);
  sensorData.mutable_version()->set_version_minor(2);
  sensorData.mutable_version()->set_version_patch(3);

  std::string serializedSensorData{};
  sensorData.SerializeToString(&serializedSensorData);
  fmi_integer_t lo{}, hi{}, size{};
  size.emplace<FMI2>(serializedSensorData.length());
  FmuHelper::encode_pointer_to_integer<FMI2>(serializedSensorData.data(), hi, lo);

  EXPECT_CALL(*fakeFmuWrapper, GetFmuVariables()).WillRepeatedly(ReturnRef(fmuVariables));
  auto baseLoValue = FmuValue{.intValue = std::get<FMI2>(lo)};
  EXPECT_CALL(*fakeFmuWrapper, GetValue(0, VariableType::Int)).WillRepeatedly(ReturnRef(baseLoValue));
  auto baseHiValue = FmuValue{.intValue = std::get<FMI2>(hi)};
  EXPECT_CALL(*fakeFmuWrapper, GetValue(1, VariableType::Int)).WillRepeatedly(ReturnRef(baseHiValue));
  auto sizeValue = FmuValue{.intValue = std::get<FMI2>(size)};
  EXPECT_CALL(*fakeFmuWrapper, GetValue(2, VariableType::Int)).WillRepeatedly(ReturnRef(sizeValue));

  EXPECT_CALL(*fakeFmuWrapper, SetValue(_, 0, VariableType::Int))
      .WillRepeatedly(
          [&baseLoValue](auto value, auto, auto)
          {
            //baseLoValue = value;
          });
  EXPECT_CALL(*fakeFmuWrapper, SetValue(_, 1, VariableType::Int))
      .WillRepeatedly(
          [&baseHiValue](auto value, auto, auto)
          {
            //baseHiValue = value;
          });
  EXPECT_CALL(*fakeFmuWrapper, SetValue(_, 2, VariableType::Int))
      .WillRepeatedly(
          [&sizeValue](auto value, auto, auto)
          {
            //sizeValue = value;
          });

  ON_CALL(*fakeFmuWrapper, GetFmuVariables()).WillByDefault(ReturnRef(fmuVariables));

  std::string componentName{"DummySensorData"};
  std::string systemName{"SspUnitTest"};

  auto tempPath = std::filesystem::temp_directory_path() / "TestOut" / FmuFileHelper::temporaryDirectoryName();
  TemporaryFolder tempFolder(tempPath);
  if (tempFolder.hasFolderCreated())
  {
    std::string outputDir = tempPath.string();
    std::string fileName = {"SensorData_0.json"};

    std::shared_ptr<OsmpConnector<osi3::SensorData, FMI2>> sensorViewConnector
        = std::make_shared<OsmpConnector<osi3::SensorData, FMI2>>("SensorData", "SensorData", fakeFmuWrapper, 10);
    sensorViewConnector->SetWriteJson(tempPath, "SensorData");

    auto sensorDataSignal = std::make_shared<SensorDataSignal const>(sensorData);
    UpdateInputSignalVisitor inVisitor{2, sensorDataSignal, 0, &fakeWorld, &fakeAgent, &fakeCallback};
    inVisitor.Visit(sensorViewConnector.get());

    ON_CALL(*fakeFmuWrapper, UpdateOutput)
        .WillByDefault(
            [sensorViewConnector](int localLinkId, std::shared_ptr<const SignalInterface> &data, int time) mutable
            {
              const std::shared_ptr<const osi3::SensorData> sensorData
                  = std::dynamic_pointer_cast<const osi3::SensorData>(sensorViewConnector->GetMessage());
              data = std::make_shared<const SensorDataSignal>(*sensorData);
            });
    Mock::AllowLeak(fakeFmuWrapper.get());

    std::shared_ptr<SignalInterface const> outSignal{};
    UpdateOutputSignalVisitor outVisitor{6, outSignal, 0, &fakeWorld, &fakeAgent, &fakeCallback};
    outVisitor.Visit(sensorViewConnector.get());
    auto sensorDataSignalOut = std::dynamic_pointer_cast<SensorDataSignal const>(outVisitor.data);

    QString dataS{};
    QFile file{QString::fromStdString(tempPath.string()) + QDir::separator() + "SensorData" + QDir::separator()
               + "JsonFiles" + QDir::separator() + QString::fromStdString(fileName)};
    file.open(QIODevice::ReadOnly);
    osi3::SensorData sensorDataOut{};
    auto data = QString(file.readAll());
    google::protobuf::util::JsonStringToMessage(data.toStdString(), &sensorDataOut);
    file.close();
    auto deletedFilesCount = std::filesystem::remove_all(tempPath);
    ASSERT_NE(deletedFilesCount, 0);
    ASSERT_TRUE(sensorDataSignalOut->sensorData.version().has_version_major());
    ASSERT_TRUE(sensorDataSignalOut->sensorData.version().has_version_minor());
    ASSERT_TRUE(sensorDataSignalOut->sensorData.version().has_version_patch());
    ASSERT_EQ(1, sensorDataSignalOut->sensorData.version().version_major());
    ASSERT_EQ(2, sensorDataSignalOut->sensorData.version().version_minor());
    ASSERT_EQ(3, sensorDataSignalOut->sensorData.version().version_patch());

    ASSERT_TRUE(sensorDataOut.version().has_version_major());
    ASSERT_TRUE(sensorDataOut.version().has_version_minor());
    ASSERT_TRUE(sensorDataOut.version().has_version_patch());
    ASSERT_EQ(1, sensorDataOut.version().version_major());
    ASSERT_EQ(2, sensorDataOut.version().version_minor());
    ASSERT_EQ(3, sensorDataOut.version().version_patch());
  }
}

TEST_F(SSPFileHelperTests, WriteTraceSensorData)
{
  FmuVariables fmuVariables;
  fmuVariables.emplace<FMI2>();
  std::get<FMI2>(fmuVariables)
      .emplace("SensorData.base.lo",
               *std::make_shared<FmuVariable2>(
                   0, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));
  std::get<FMI2>(fmuVariables)
      .emplace("SensorData.base.hi",
               *std::make_shared<FmuVariable2>(
                   1, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));
  std::get<FMI2>(fmuVariables)
      .emplace("SensorData.size",
               *std::make_shared<FmuVariable2>(
                   2, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));

  auto fakeFmuWrapper = std::make_shared<FakeFmuWrapper>();

  osi3::SensorData sensorData{};
  sensorData.mutable_version()->set_version_major(1);
  sensorData.mutable_version()->set_version_minor(2);
  sensorData.mutable_version()->set_version_patch(3);

  std::string serializedSensorData{};
  sensorData.SerializeToString(&serializedSensorData);
  fmi_integer_t lo{}, hi{}, size;
  size.emplace<FMI2>(serializedSensorData.length());
  FmuHelper::encode_pointer_to_integer<FMI2>(serializedSensorData.data(), hi, lo);

  EXPECT_CALL(*fakeFmuWrapper, GetFmuVariables()).WillRepeatedly(ReturnRef(fmuVariables));
  auto baseLoValue = FmuValue{.intValue = std::get<FMI2>(lo)};
  EXPECT_CALL(*fakeFmuWrapper, GetValue(0, VariableType::Int)).WillRepeatedly(ReturnRef(baseLoValue));
  auto baseHiValue = FmuValue{.intValue = std::get<FMI2>(hi)};
  EXPECT_CALL(*fakeFmuWrapper, GetValue(1, VariableType::Int)).WillRepeatedly(ReturnRef(baseHiValue));
  auto sizeValue = FmuValue{.intValue = std::get<FMI2>(size)};
  EXPECT_CALL(*fakeFmuWrapper, GetValue(2, VariableType::Int)).WillRepeatedly(ReturnRef(sizeValue));

  EXPECT_CALL(*fakeFmuWrapper, SetValue(_, 0, VariableType::Int))
      .WillRepeatedly(
          [&baseLoValue](auto value, auto, auto)
          {
            //baseLoValue = value;
          });
  EXPECT_CALL(*fakeFmuWrapper, SetValue(_, 1, VariableType::Int))
      .WillRepeatedly(
          [&baseHiValue](auto value, auto, auto)
          {
            //baseHiValue = value;
          });
  EXPECT_CALL(*fakeFmuWrapper, SetValue(_, 2, VariableType::Int))
      .WillRepeatedly(
          [&sizeValue](auto value, auto, auto)
          {
            //sizeValue = value;
          });

  ON_CALL(*fakeFmuWrapper, GetFmuVariables()).WillByDefault(ReturnRef(fmuVariables));

  std::shared_ptr<std::map<std::string, FmuFileHelper::TraceEntry>> targetOutputTracesMap
      = std::make_shared<std::map<std::string, FmuFileHelper::TraceEntry>>();

  std::shared_ptr<OsmpConnector<osi3::SensorData, FMI2>> sensorViewConnector
      = std::make_shared<OsmpConnector<osi3::SensorData, FMI2>>("SensorData", "SensorData", fakeFmuWrapper, 10);
  sensorViewConnector->SetWriteBinaryTrace(".", targetOutputTracesMap, "SensorData");

  auto sensorDataSignal = std::make_shared<SensorDataSignal const>(sensorData);
  UpdateInputSignalVisitor inVisitor{2, sensorDataSignal, 0, &fakeWorld, &fakeAgent, &fakeCallback};
  inVisitor.Visit(sensorViewConnector.get());

  ON_CALL(*fakeFmuWrapper, UpdateOutput)
      .WillByDefault(
          [sensorViewConnector](int localLinkId, std::shared_ptr<const SignalInterface> &data, int time) mutable
          {
            const std::shared_ptr<const osi3::SensorData> sensorData
                = std::dynamic_pointer_cast<const osi3::SensorData>(sensorViewConnector->GetMessage());
            data = std::make_shared<const SensorDataSignal>(*sensorData);
          });
  Mock::AllowLeak(fakeFmuWrapper.get());

  std::shared_ptr<SignalInterface const> outSignal{};
  UpdateOutputSignalVisitor outVisitor{6, outSignal, 0, &fakeWorld, &fakeAgent, &fakeCallback};
  outVisitor.Visit(sensorViewConnector.get());
  auto sensorDataSignalOut = std::dynamic_pointer_cast<SensorDataSignal const>(outVisitor.data);

  ASSERT_TRUE(sensorDataSignalOut->sensorData.version().has_version_major());
  ASSERT_TRUE(sensorDataSignalOut->sensorData.version().has_version_minor());
  ASSERT_TRUE(sensorDataSignalOut->sensorData.version().has_version_patch());
  ASSERT_EQ(1, sensorDataSignalOut->sensorData.version().version_major());
  ASSERT_EQ(2, sensorDataSignalOut->sensorData.version().version_minor());
  ASSERT_EQ(3, sensorDataSignalOut->sensorData.version().version_patch());

  ASSERT_TRUE(targetOutputTracesMap->size() == 1);  // eigendlich 2 aber filename nicht ideal und überschreibt
  auto traceEntry = targetOutputTracesMap->at("SensorData_SensorData");
  ASSERT_EQ(traceEntry.time, 0);
  ASSERT_EQ(traceEntry.message, serializedSensorData);
}

TEST_F(SSPFileHelperTests, TestGenerateFilename)
{
  FmuFileHelper::TraceEntry traceEntry;
  traceEntry.time = 0;
  traceEntry.osiType = "sv";

  const std::pair<const std::string, FmuFileHelper::TraceEntry> fileToOutputTrace("test", traceEntry);
  std::string outputType = "TestOutputType";

  auto fileName = FmuFileHelper::GenerateTraceFileName(outputType, fileToOutputTrace);

  std::string delimiter = "_";
  std::vector<std::string> fileNameSplits = split(fileName, delimiter);

  ASSERT_EQ(fileNameSplits.size(), 6);
  ASSERT_EQ(fileNameSplits[1], "sv");

  const auto currentInterfaceVersion
      = osi3::InterfaceVersion::descriptor()->file()->options().GetExtension(osi3::current_interface_version);
  std::stringstream osiVersion;
  osiVersion << std::to_string(currentInterfaceVersion.version_major());
  osiVersion << currentInterfaceVersion.version_minor();
  osiVersion << currentInterfaceVersion.version_patch();
  ASSERT_EQ(fileNameSplits[2], osiVersion.str());

  ASSERT_EQ(fileNameSplits[3], std::to_string(GOOGLE_PROTOBUF_VERSION));
  ASSERT_EQ(fileNameSplits[4], std::to_string(0));

  delimiter = ".";
  std::vector<std::string> fileEndingSplit = split(fileNameSplits[5], delimiter);

  ASSERT_EQ(fileEndingSplit.size(), 2);
  ASSERT_EQ(fileEndingSplit[0], outputType);
  ASSERT_EQ(fileEndingSplit[1], "osi");
}

TEST_F(SSPFileHelperTests, WriteBinaryTraceFileNameCompleteTest)
{
  FmuVariables fmuVariables;
  fmuVariables.emplace<FMI2>();
  std::get<FMI2>(fmuVariables)
      .emplace("SensorData.base.lo",
               *std::make_shared<FmuVariable2>(
                   0, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));
  std::get<FMI2>(fmuVariables)
      .emplace("SensorData.base.hi",
               *std::make_shared<FmuVariable2>(
                   1, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));
  std::get<FMI2>(fmuVariables)
      .emplace("SensorData.size",
               *std::make_shared<FmuVariable2>(
                   2, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));

  auto fakeFmuWrapper = std::make_shared<FakeFmuWrapper>();

  osi3::SensorData sensorData{};
  sensorData.mutable_version()->set_version_major(1);
  sensorData.mutable_version()->set_version_minor(2);
  sensorData.mutable_version()->set_version_patch(3);

  std::string serializedSensorData{};
  sensorData.SerializeToString(&serializedSensorData);
  fmi_integer_t lo{}, hi{}, size;
  size.emplace<FMI2>(serializedSensorData.length());
  FmuHelper::encode_pointer_to_integer<FMI2>(serializedSensorData.data(), hi, lo);

  EXPECT_CALL(*fakeFmuWrapper, GetFmuVariables()).WillRepeatedly(ReturnRef(fmuVariables));
  auto baseLoValue = FmuValue{.intValue = std::get<FMI2>(lo)};
  EXPECT_CALL(*fakeFmuWrapper, GetValue(0, VariableType::Int)).WillRepeatedly(ReturnRef(baseLoValue));
  auto baseHiValue = FmuValue{.intValue = std::get<FMI2>(hi)};
  EXPECT_CALL(*fakeFmuWrapper, GetValue(1, VariableType::Int)).WillRepeatedly(ReturnRef(baseHiValue));
  auto sizeValue = FmuValue{.intValue = std::get<FMI2>(size)};
  EXPECT_CALL(*fakeFmuWrapper, GetValue(2, VariableType::Int)).WillRepeatedly(ReturnRef(sizeValue));

  EXPECT_CALL(*fakeFmuWrapper, SetValue(_, 0, VariableType::Int))
      .WillRepeatedly(
          [&baseLoValue](auto value, auto, auto)
          {
            // baseLoValue = value;
          });
  EXPECT_CALL(*fakeFmuWrapper, SetValue(_, 1, VariableType::Int))
      .WillRepeatedly(
          [&baseHiValue](auto value, auto, auto)
          {
            // baseHiValue = value;
          });
  EXPECT_CALL(*fakeFmuWrapper, SetValue(_, 2, VariableType::Int))
      .WillRepeatedly(
          [&sizeValue](auto value, auto, auto)
          {
            // sizeValue = value;
          });

  ON_CALL(*fakeFmuWrapper, GetFmuVariables()).WillByDefault(ReturnRef(fmuVariables));

  std::shared_ptr<std::map<std::string, FmuFileHelper::TraceEntry>> targetOutputTracesMap
      = std::make_shared<std::map<std::string, FmuFileHelper::TraceEntry>>();

  std::shared_ptr<OsmpConnector<osi3::SensorData, FMI2>> sensorViewConnector
      = std::make_shared<OsmpConnector<osi3::SensorData, FMI2>>("SensorData", "SensorData", fakeFmuWrapper, 10);
  sensorViewConnector->SetWriteBinaryTrace(".", targetOutputTracesMap, "SensorData");

  auto sensorDataSignal = std::make_shared<SensorDataSignal const>(sensorData);
  UpdateInputSignalVisitor inVisitor{2, sensorDataSignal, 0, &fakeWorld, &fakeAgent, &fakeCallback};
  inVisitor.Visit(sensorViewConnector.get());

  ON_CALL(*fakeFmuWrapper, UpdateOutput)
      .WillByDefault(
          [sensorViewConnector](int localLinkId, std::shared_ptr<const SignalInterface> &data, int time) mutable
          {
            const std::shared_ptr<const osi3::SensorData> sensorData
                = std::dynamic_pointer_cast<const osi3::SensorData>(sensorViewConnector->GetMessage());
            data = std::make_shared<const SensorDataSignal>(*sensorData);
          });
  Mock::AllowLeak(fakeFmuWrapper.get());

  std::shared_ptr<SignalInterface const> outSignal{};
  UpdateOutputSignalVisitor outVisitor{6, outSignal, 0, &fakeWorld, &fakeAgent, &fakeCallback};
  outVisitor.Visit(sensorViewConnector.get());
  auto sensorDataOut = std::dynamic_pointer_cast<SensorDataSignal const>(outVisitor.data)->sensorData;

  std::string componentName{"DummySensorData"};
  std::string systemName{"SspUnitTest"};

  std::map<std::string, FmuFileHelper::TraceEntry> fileToOutputTracesMap
      = *(sensorViewConnector.get()->GetTracesMap().value());

  for (const auto &file_trace_element : fileToOutputTracesMap)
  {
    std::string filename = GenerateTraceFileName(componentName, file_trace_element);

    std::string delimiter = "_";
    std::vector<std::string> fileNameSplits = split(filename, delimiter);

    ASSERT_EQ(fileNameSplits.size(), 6);
    ASSERT_EQ(fileNameSplits[1], "sd");

    const auto currentInterfaceVersion
        = osi3::InterfaceVersion::descriptor()->file()->options().GetExtension(osi3::current_interface_version);
    std::stringstream osiVersion;
    osiVersion << std::to_string(currentInterfaceVersion.version_major());
    osiVersion << currentInterfaceVersion.version_minor();
    osiVersion << currentInterfaceVersion.version_patch();
    ASSERT_EQ(fileNameSplits[2], osiVersion.str());

    ASSERT_EQ(fileNameSplits[3], std::to_string(GOOGLE_PROTOBUF_VERSION));
    ASSERT_EQ(fileNameSplits[4], std::to_string(0));

    delimiter = ".";
    std::vector<std::string> fileEndingSplit = split(fileNameSplits[5], delimiter);

    ASSERT_EQ(fileEndingSplit.size(), 2);
    ASSERT_EQ(fileEndingSplit[0], componentName);
    ASSERT_EQ(fileEndingSplit[1], "osi");
  }
}

TEST_F(SSPFileHelperTests, TestOutputDirectory)
{
  SsdToSspNetworkParser ssdToSspNetWorkParser{"SspWrapper",
                                              false,
                                              0,
                                              0,
                                              0,
                                              100,
                                              nullptr,
                                              &fakeWorld,
                                              &fakeParameter,
                                              nullptr,
                                              &fakeAgent,
                                              &fakeCallback,
                                              fakeScenarioControl};

  std::string systemName = "TestSystem";

  auto ssdSystem = std::make_shared<SsdSystem>("root system");
  openpass::parameter::internal::ParameterSetLevel3 parameters;
  parameters.emplace_back("Logging", false);
  parameters.emplace_back("CsvOutput", false);

  auto subSystemComponent
      = std::make_shared<SsdComponent>("subsystem", "SubSystem.ssp#SubSystem.ssd", SspComponentType::x_ssp_definition);
  ssdSystem->AddComponent(std::move(subSystemComponent));

  auto subSystem = std::make_shared<SsdSystem>(systemName);
  openpass::parameter::internal::ParameterSetLevel3 subSystemParameters;
  subSystemParameters.emplace_back("Logging", false);
  subSystemParameters.emplace_back("CsvOutput", false);

  std::vector<std::shared_ptr<SsdFile>> ssdFiles{{std::make_shared<SsdFile>("SystemStructure.ssd", ssdSystem),
                                                  std::make_shared<SsdFile>("SubSystem.ssd", subSystem)}};

  openpass::common::RuntimeInformation runtimeInformation{{openpass::common::framework}, {"", "outputDirectory", ""}};

  ON_CALL(fakeParameter, GetRuntimeInformation()).WillByDefault(testing::ReturnRef(runtimeInformation));
  ON_CALL(fakeAgent, GetId()).WillByDefault(Return(0));

  auto rootSystem = ssdToSspNetWorkParser.GetRootSystem(ssdFiles);

  std::filesystem::path outDirectoryActual = rootSystem->GetOutputDir();
  std::filesystem::path outDirectoryExpected = std::filesystem::path("outputDirectory") / "ssp" / "Agent0000";

  ASSERT_EQ(outDirectoryActual, outDirectoryExpected);

  auto *subSystemUnderTest = dynamic_cast<ssp::System *>(rootSystem->GetElements().at(0).get());

  outDirectoryActual = subSystemUnderTest->GetOutputDir();
  outDirectoryExpected = std::filesystem::path("outputDirectory") / "ssp" / "Agent0000" / systemName;

  ASSERT_EQ(outDirectoryActual, outDirectoryExpected);
}
