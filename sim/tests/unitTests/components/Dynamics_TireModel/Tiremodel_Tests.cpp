/********************************************************************************
 * Copyright (c) 2023 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "fakeAgent.h"
#include "fakeParameter.h"
#include "tiremodel.h"

using ::testing::_;
using ::testing::NiceMock;
using ::testing::Return;
using ::testing::ReturnRef;

TEST(TMEASYTireModel, CalculationOfTireForces)
{
  const units::force::newton_t fakeFRef = 1000_N;
  const Common::Vector2d<double> fakeMuTireMax(1.1, 1.1);
  const Common::Vector2d<double> fakeMuTireSlide(1.0, 1.0);
  const Common::Vector2d<double> fakeSMax(0.5, 0.5);
  const Common::Vector2d<double> fakeMuTireMax2FRef(1.1, 1.1);
  const Common::Vector2d<double> fakeMuTireSlide2FRef(1.0, 1.0);
  const Common::Vector2d<double> fakeSMax2FRef(0.5, 0.5);
  const units::length::meter_t fakeR = 0.5_m;
  const double fakeMuScale = 1;
  const Common::Vector2d<double> fakeSSlide(0.6, 0.6);
  const Common::Vector2d<double> fakeSSlide2fRef(0.6, 0.6);
  const Common::Vector2d<units::force::newton_t> fakeF0p(1000_N, 1000_N);
  const Common::Vector2d<units::force::newton_t> fakeF0p2FRef(1000_N, 1000_N);
  const Common::Vector2d<units::length::meter_t> fakePositionTire(0_m, 0_m);
  const units::frequency::hertz_t fakeRotationVelocity = 1.0_Hz;
  const double fakeInertia = 2;
  const units::length::meter_t fakePneumaticTrail = 0.2_m;
  const double fakeVelocityLimit = 0.27;
  Tire *fakeTire = new Tire(fakeFRef,
                            fakeMuTireMax,
                            fakeMuTireSlide,
                            fakeSMax,
                            fakeMuTireMax2FRef,
                            fakeMuTireSlide2FRef,
                            fakeSMax2FRef,
                            fakeR,
                            fakeMuScale,
                            fakeSSlide,
                            fakeSSlide2fRef,
                            fakeF0p,
                            fakeF0p2FRef,
                            fakePositionTire,
                            fakeRotationVelocity,
                            fakeInertia,
                            fakePneumaticTrail);

  double fakeYawVelocity = 0.0;
  Common::Vector2d<double> fakeVelocityCar;
  fakeVelocityCar.x = 0.0;
  fakeVelocityCar.y = 0.0;

  fakeTire->SetTireAngle(0.0_rad);
  fakeTire->Rescale(fakeFRef);

  // Check Longitudinal Tire Force
  fakeVelocityCar.x = fakeSMax.x * (fakeR.value() * std::fabs(fakeRotationVelocity.value()) + fakeVelocityLimit)
                    + fakeR.value() * fakeRotationVelocity.value();
  fakeVelocityCar.y = 0.0;

  fakeTire->CalcVelTire(fakeYawVelocity * 1_rad_per_s, {fakeVelocityCar.x * 1_mps, fakeVelocityCar.y * 1_mps});
  fakeTire->CalcTireForce();

  // Check Roll Friction
  ASSERT_DOUBLE_EQ(fakeTire->GetRollFriction().value(), 0.01 * fakeFRef.value());

  ASSERT_DOUBLE_EQ(fakeTire->GetLongitudinalForce().value(), -fakeFRef.value() * fakeMuScale * fakeMuTireMax.x);

  fakeVelocityCar.x = fakeSSlide.x * (fakeR.value() * std::fabs(fakeRotationVelocity.value()) + fakeVelocityLimit)
                    + fakeR.value() * fakeRotationVelocity.value();

  fakeTire->CalcVelTire(fakeYawVelocity * 1_rad_per_s, {fakeVelocityCar.x * 1_mps, fakeVelocityCar.y * 1_mps});
  fakeTire->CalcTireForce();

  ASSERT_DOUBLE_EQ(fakeTire->GetLongitudinalForce().value(), -fakeFRef.value() * fakeMuScale * fakeMuTireSlide.x);

  // Check Lateral Tire Force

  fakeVelocityCar.x = fakeR.value() * fakeRotationVelocity.value();
  fakeVelocityCar.y = fakeSMax.y * (fakeR.value() * std::fabs(fakeRotationVelocity.value()) + fakeVelocityLimit);
  fakeTire->CalcVelTire(fakeYawVelocity * 1_rad_per_s, {fakeVelocityCar.x * 1_mps, fakeVelocityCar.y * 1_mps});
  fakeTire->CalcTireForce();

  ASSERT_DOUBLE_EQ(fakeTire->GetLateralForce().value(), -fakeFRef.value() * fakeMuScale * fakeMuTireMax.y);

  fakeVelocityCar.y = fakeSSlide.y * (fakeR.value() * std::fabs(fakeRotationVelocity.value()) + fakeVelocityLimit);
  fakeTire->CalcVelTire(fakeYawVelocity * 1_rad_per_s, {fakeVelocityCar.x * 1_mps, fakeVelocityCar.y * 1_mps});
  fakeTire->CalcTireForce();

  ASSERT_DOUBLE_EQ(fakeTire->GetLateralForce().value(), -fakeFRef.value() * fakeMuScale * fakeMuTireSlide.y);

  // Check combination Lateral & Longitudinal Tire Forces

  fakeVelocityCar.x = fakeSMax.x * (fakeR.value() * std::fabs(fakeRotationVelocity.value()) + fakeVelocityLimit)
                    + fakeR.value() * fakeRotationVelocity.value();
  fakeVelocityCar.y = fakeSMax.y * (fakeR.value() * std::fabs(fakeRotationVelocity.value()) + fakeVelocityLimit);
  fakeTire->CalcVelTire(fakeYawVelocity * 1_rad_per_s, {fakeVelocityCar.x * 1_mps, fakeVelocityCar.y * 1_mps});
  fakeTire->CalcTireForce();

  ASSERT_DOUBLE_EQ(fakeTire->GetLateralForce().value(), fakeTire->GetLongitudinalForce().value());

  fakeVelocityCar.x = fakeSMax.x * (fakeR.value() * std::fabs(fakeRotationVelocity.value()) + fakeVelocityLimit)
                    + fakeR.value() * fakeRotationVelocity.value();
  fakeVelocityCar.y = 0.5 * fakeSMax.y * (fakeR.value() * std::fabs(fakeRotationVelocity.value()) + fakeVelocityLimit);
  fakeTire->CalcVelTire(fakeYawVelocity * 1_rad_per_s, {fakeVelocityCar.x * 1_mps, fakeVelocityCar.y * 1_mps});
  fakeTire->CalcTireForce();

  ASSERT_DOUBLE_EQ(fakeTire->GetLateralForce().value(), 0.5 * fakeTire->GetLongitudinalForce().value());

  // Check Self Aligning Torque
  fakeTire->CalcSelfAligningTorque();
  ASSERT_DOUBLE_EQ(fakeTire->GetSelfAligningTorque().value(),
                   -fakeTire->GetLateralForce().value() * fakePneumaticTrail.value());
}

TEST(TMEASYTireModel, CalculationOfTireForcesWithVerticalForceScaling)
{
  const units::force::newton_t fakeFRef = 1000_N;
  const Common::Vector2d<double> fakeMuTireMax(1.1, 1.1);
  const Common::Vector2d<double> fakeMuTireSlide(1.0, 1.0);
  const Common::Vector2d<double> fakeSMax(0.5, 0.5);
  const Common::Vector2d<double> fakeMuTireMax2FRef(1.0, 1.1);
  const Common::Vector2d<double> fakeMuTireSlide2FRef(0.9, 1.0);
  const Common::Vector2d<double> fakeSMax2FRef(0.6, 0.5);
  const units::length::meter_t fakeR = 0.5_m;
  const double fakeMuScale = 1;
  const Common::Vector2d<double> fakeSSlide(0.6, 0.6);
  const Common::Vector2d<double> fakeSSlide2fRef(0.7, 0.6);
  const Common::Vector2d<units::force::newton_t> fakeF0p(1000_N, 1000_N);
  const Common::Vector2d<units::force::newton_t> fakeF0p2FRef(1000_N, 1000_N);
  const Common::Vector2d<units::length::meter_t> fakePositionTire(0_m, 0_m);
  const units::frequency::hertz_t fakeRotationVelocity = 1.0_Hz;
  const double fakeInertia = 2;
  const units::length::meter_t fakePneumaticTrail = 0.2_m;
  const double fakeVelocityLimit = 0.27;
  Tire *fakeTire = new Tire(fakeFRef,
                            fakeMuTireMax,
                            fakeMuTireSlide,
                            fakeSMax,
                            fakeMuTireMax2FRef,
                            fakeMuTireSlide2FRef,
                            fakeSMax2FRef,
                            fakeR,
                            fakeMuScale,
                            fakeSSlide,
                            fakeSSlide2fRef,
                            fakeF0p,
                            fakeF0p2FRef,
                            fakePositionTire,
                            fakeRotationVelocity,
                            fakeInertia,
                            fakePneumaticTrail);

  double fakeYawVelocity = 0.0;
  Common::Vector2d<double> fakeVelocityCar;

  fakeTire->SetTireAngle(0.0_rad);
  fakeTire->Rescale(fakeFRef);

  // Check Longitudinal Tire Force with different vertical forces

  units::force::newton_t fakeFReftemp = fakeFRef;
  fakeVelocityCar.y = 0.0;
  while (fakeFReftemp <= fakeFRef)
  {
    fakeTire->Rescale(fakeFReftemp);

    fakeVelocityCar.x = (fakeSMax.x + (fakeSMax2FRef.x - fakeSMax.x) * (fakeFReftemp.value() / fakeFRef.value() - 1))
                          * (fakeR.value() * std::fabs(fakeRotationVelocity.value()) + fakeVelocityLimit)
                      + fakeR.value() * fakeRotationVelocity.value();
    fakeTire->CalcVelTire(fakeYawVelocity * 1_rad_per_s, {fakeVelocityCar.x * 1_mps, fakeVelocityCar.y * 1_mps});
    fakeTire->CalcTireForce();

    double expectedLongitudinalForce = -(fakeFReftemp.value() / fakeFRef.value())
                                     * (2 * fakeFRef.value() * fakeMuScale * fakeMuTireMax.x
                                        - 0.5 * 2 * fakeFRef.value() * fakeMuScale * fakeMuTireMax2FRef.x
                                        - (fakeFRef.value() * fakeMuScale * fakeMuTireMax.x
                                           - 0.5 * 2 * fakeFRef.value() * fakeMuScale * fakeMuTireMax2FRef.x)
                                              * (fakeFReftemp / fakeFRef));
    ASSERT_DOUBLE_EQ(fakeTire->GetLongitudinalForce().value(), expectedLongitudinalForce);

    fakeFReftemp += 50.0_N;
  }
}

TEST(TMEASYTireModel, CalculationOfTireForcesWithDriveTorque)
{
  const units::force::newton_t fakeFRef = 1000_N;
  const Common::Vector2d<double> fakeMuTireMax(1.1, 1.1);
  const Common::Vector2d<double> fakeMuTireSlide(1.0, 1.0);
  const Common::Vector2d<double> fakeSMax(0.5, 0.5);
  const Common::Vector2d<double> fakeMuTireMax2FRef(1.0, 1.1);
  const Common::Vector2d<double> fakeMuTireSlide2FRef(0.9, 1.0);
  const Common::Vector2d<double> fakeSMax2FRef(0.6, 0.5);
  const units::length::meter_t fakeR = 0.5_m;
  const double fakeMuScale = 1;
  const Common::Vector2d<double> fakeSSlide(0.6, 0.6);
  const Common::Vector2d<double> fakeSSlide2FRef(0.7, 0.6);
  const Common::Vector2d<units::force::newton_t> fakeF0p(1000_N, 1000_N);
  const Common::Vector2d<units::force::newton_t> fakeF0p2FRef(1000_N, 1000_N);
  const Common::Vector2d<units::length::meter_t> fakePositionTire(0_m, 0_m);
  const units::frequency::hertz_t fakeRotationVelocity = 0.0_Hz;
  const double fakeInertia = 2;
  const units::length::meter_t fakePneumaticTrail = 0.2_m;
  const double fakeVelocityLimit = 0.27;

  Tire *fakeTire = new Tire(fakeFRef,
                            fakeMuTireMax,
                            fakeMuTireSlide,
                            fakeSMax,
                            fakeMuTireMax2FRef,
                            fakeMuTireSlide2FRef,
                            fakeSMax2FRef,
                            fakeR,
                            fakeMuScale,
                            fakeSSlide,
                            fakeSSlide2FRef,
                            fakeF0p,
                            fakeF0p2FRef,
                            fakePositionTire,
                            fakeRotationVelocity,
                            fakeInertia,
                            fakePneumaticTrail);

  double fakeYawVelocity = 0.0;
  Common::Vector2d<double> fakeVelocityCar;
  double fakeTorque = 500.0;

  fakeTire->SetTireAngle(0.0_rad);
  fakeTire->Rescale(fakeFRef);

  fakeTire->SetTorque(fakeTorque * 1_Nm);
  fakeTire->CalcRotAcc();
  ASSERT_DOUBLE_EQ(fakeTorque, fakeTire->GetRotAcceleration() * fakeInertia);
  fakeTire->CalcRotVel(1.0);
  ASSERT_DOUBLE_EQ(fakeTire->GetRotationVelocity().value(),
                   fakeRotationVelocity.value() + fakeTire->GetRotAcceleration());
}

TEST(TMEASYTireModel, CheckTireModelImplementation)
{
  NiceMock<FakeParameter> fakeParameters;
  std::map<std::string, const std::vector<double>> fakeParametersDoubleVector;
  std::map<std::string, double> fakeParametersDouble;
  std::map<std::string, const std::vector<bool>> fakeParametersBoolVector;

  int fakeCycleTimeMs = 10;

  fakeParametersDoubleVector.insert(std::pair<std::string, std::vector<double>>("MuTireMaxXFRef", {1.1, 1.1}));
  fakeParametersDoubleVector.insert(std::pair<std::string, std::vector<double>>("MuTireMaxX2FRef", {1.1, 1.1}));
  fakeParametersDoubleVector.insert(std::pair<std::string, std::vector<double>>("MuTireSlideXFRef", {1.0, 1.0}));
  fakeParametersDoubleVector.insert(std::pair<std::string, std::vector<double>>("MuTireSlideX2FRef", {1.0, 1.0}));
  fakeParametersDoubleVector.insert(std::pair<std::string, std::vector<double>>("SlipTireMaxXFRef", {0.6, 0.6}));
  fakeParametersDoubleVector.insert(std::pair<std::string, std::vector<double>>("SlipTireMaxX2FRef", {0.6, 0.6}));
  fakeParametersDoubleVector.insert(std::pair<std::string, std::vector<double>>("SlipTireSlideXFRef", {0.8, 0.8}));
  fakeParametersDoubleVector.insert(std::pair<std::string, std::vector<double>>("SlipTireSlideX2FRef", {0.8, 0.8}));
  fakeParametersDoubleVector.insert(std::pair<std::string, std::vector<double>>("F0pXFRef", {15.0, 15.0}));
  fakeParametersDoubleVector.insert(std::pair<std::string, std::vector<double>>("F0pX2FRef", {15.0, 15.0}));
  fakeParametersDoubleVector.insert(std::pair<std::string, std::vector<double>>("MuTireMaxYFRef", {1.1, 1.1}));
  fakeParametersDoubleVector.insert(std::pair<std::string, std::vector<double>>("MuTireMaxY2FRef", {1.1, 1.1}));
  fakeParametersDoubleVector.insert(std::pair<std::string, std::vector<double>>("MuTireSlideYFRef", {1.0, 1.0}));
  fakeParametersDoubleVector.insert(std::pair<std::string, std::vector<double>>("MuTireSlideY2FRef", {1.0, 1.0}));
  fakeParametersDoubleVector.insert(std::pair<std::string, std::vector<double>>("SlipTireMaxYFRef", {0.6, 0.6}));
  fakeParametersDoubleVector.insert(std::pair<std::string, std::vector<double>>("SlipTireMaxY2FRef", {0.6, 0.6}));
  fakeParametersDoubleVector.insert(std::pair<std::string, std::vector<double>>("SlipTireSlideYFRef", {0.8, 0.8}));
  fakeParametersDoubleVector.insert(std::pair<std::string, std::vector<double>>("SlipTireSlideY2FRef", {0.8, 0.8}));
  fakeParametersDoubleVector.insert(std::pair<std::string, std::vector<double>>("F0pYFRef", {15.0, 15.0}));
  fakeParametersDoubleVector.insert(std::pair<std::string, std::vector<double>>("F0pY2FRef", {15.0, 15.0}));
  fakeParametersDoubleVector.insert(std::pair<std::string, std::vector<double>>("FRef", {1000.0, 1000.0}));
  fakeParametersDoubleVector.insert(std::pair<std::string, std::vector<double>>("Inertia", {2.0, 2.0}));
  fakeParametersDoubleVector.insert(std::pair<std::string, std::vector<double>>("PneumaticTrail", {0.2, 0.2}));

  fakeParametersBoolVector.insert(std::pair<std::string, std::vector<bool>>("FRefNormalized", {true, true}));

  ON_CALL(fakeParameters, GetParametersDoubleVector()).WillByDefault(ReturnRef(fakeParametersDoubleVector));
  ON_CALL(fakeParameters, GetParametersDouble()).WillByDefault(ReturnRef(fakeParametersDouble));
  ON_CALL(fakeParameters, GetParametersBoolVector()).WillByDefault(ReturnRef(fakeParametersBoolVector));

  NiceMock<FakeAgent> fakeAgent;

  auto fakeVehicleModelParameters = std::make_shared<mantle_api::VehicleProperties>();
  fakeVehicleModelParameters->performance.max_deceleration = 10.0_mps_sq;
  fakeVehicleModelParameters->mass = 1500_kg;
  fakeVehicleModelParameters->properties.insert(std::pair<std::string, std::string>("XPositionCOG", std::to_string(2)));
  fakeVehicleModelParameters->properties.insert(std::pair<std::string, std::string>("YPositionCOG", std::to_string(0)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("FrictionCoefficient", std::to_string(1.0)));

  fakeVehicleModelParameters->front_axle.wheel_diameter = 0.5_m;
  fakeVehicleModelParameters->front_axle.track_width = 2.0_m;
  fakeVehicleModelParameters->front_axle.bb_center_to_axle_center.x = 2.0_m;
  fakeVehicleModelParameters->rear_axle.wheel_diameter = 0.5_m;
  fakeVehicleModelParameters->rear_axle.track_width = 2.0_m;
  fakeVehicleModelParameters->rear_axle.bb_center_to_axle_center.x = -2.0_m;

  ON_CALL(fakeAgent, GetVehicleModelParameters()).WillByDefault(Return(fakeVehicleModelParameters));

  DynamicsTireModel implementation(
      "", false, 0, 0, 0, fakeCycleTimeMs, nullptr, nullptr, &fakeParameters, nullptr, nullptr, &fakeAgent);

  SignalVectorDouble fakeDriveTorque({1000.0, 1000.0, 1000.0, 1000.0});
  SignalVectorDouble fakeBrakeTorque({0, 0, 0, 0});
  SignalVectorDouble fakeWheelAngle({0, 0, 0, 0});
  SignalVectorDouble fakeVerticalForce({1000.0, 1000.0, 1000.0, 1000.0});

  implementation.UpdateInput(0, std::make_shared<SignalVectorDouble const>(fakeDriveTorque), 0);
  implementation.UpdateInput(1, std::make_shared<SignalVectorDouble const>(fakeBrakeTorque), 0);
  implementation.UpdateInput(2, std::make_shared<SignalVectorDouble const>(fakeWheelAngle), 0);
  implementation.UpdateInput(3, std::make_shared<SignalVectorDouble const>(fakeVerticalForce), 0);

  Common::Vector2d fakeVelocity = {1.0, 0.0};
  ON_CALL(fakeAgent, GetYawRate()).WillByDefault(Return(0.0_rad_per_s));
  ON_CALL(fakeAgent, GetYaw()).WillByDefault(Return(0.0_rad));
  ON_CALL(fakeAgent, GetVelocity(_))
      .WillByDefault(Return(Common::Vector2d{fakeVelocity.x * 1_mps, fakeVelocity.y * 1_mps}));
  ON_CALL(fakeAgent, GetId()).WillByDefault(Return(0));

  implementation.Trigger(0);

  std::shared_ptr<SignalInterface const> outputSignal;
  implementation.UpdateOutput(0, outputSignal, 0);
  ASSERT_DOUBLE_EQ(std::static_pointer_cast<SignalVectorDouble const>(outputSignal)->value[0],
                   std::static_pointer_cast<SignalVectorDouble const>(outputSignal)->value[1]);
  ASSERT_DOUBLE_EQ(std::static_pointer_cast<SignalVectorDouble const>(outputSignal)->value[1],
                   std::static_pointer_cast<SignalVectorDouble const>(outputSignal)->value[2]);

  implementation.UpdateOutput(1, outputSignal, 0);
  ASSERT_DOUBLE_EQ(std::static_pointer_cast<SignalVectorDouble const>(outputSignal)->value[0],
                   std::static_pointer_cast<SignalVectorDouble const>(outputSignal)->value[1]);
  ASSERT_DOUBLE_EQ(std::static_pointer_cast<SignalVectorDouble const>(outputSignal)->value[1],
                   std::static_pointer_cast<SignalVectorDouble const>(outputSignal)->value[2]);

  implementation.UpdateOutput(2, outputSignal, 0);
  ASSERT_DOUBLE_EQ(std::static_pointer_cast<SignalVectorDouble const>(outputSignal)->value[0],
                   std::static_pointer_cast<SignalVectorDouble const>(outputSignal)->value[1]);
  ASSERT_DOUBLE_EQ(std::static_pointer_cast<SignalVectorDouble const>(outputSignal)->value[1],
                   std::static_pointer_cast<SignalVectorDouble const>(outputSignal)->value[2]);

  implementation.UpdateOutput(3, outputSignal, 0);
  ASSERT_DOUBLE_EQ(std::static_pointer_cast<SignalVectorDouble const>(outputSignal)->value[0],
                   std::static_pointer_cast<SignalVectorDouble const>(outputSignal)->value[1]);
  ASSERT_DOUBLE_EQ(std::static_pointer_cast<SignalVectorDouble const>(outputSignal)->value[1],
                   std::static_pointer_cast<SignalVectorDouble const>(outputSignal)->value[2]);
}