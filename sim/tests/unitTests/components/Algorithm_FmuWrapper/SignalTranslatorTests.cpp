/*******************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <cstring>
#include <functional>
#include <osi3/osi_sensordata.pb.h>
#include <osi3/osi_trafficupdate.pb.h>
#include <variant>

#include "ChannelDefinitionParser.h"
#include "SignalInterface/SignalTranslator.h"
#include "common/dynamicsSignal.h"
#include "common/primitiveSignals.h"
#include "fakeAgent.h"
#include "include/fmuHandlerInterface.h"
#include "include/signalInterface.h"

using ::testing::Each;
using ::testing::ElementsAre;
using ::testing::Eq;
using ::testing::Field;
using ::testing::Return;
using ::testing::WhenDynamicCastTo;

inline void PrintTo(DynamicsInformation d, ::std::ostream* os)
{
  *os << "acceleration: " << d.acceleration << '\n';
  *os << "velocityX: " << d.velocityX << '\n';
  *os << "velocityY: " << d.velocityY << '\n';
  *os << "positionX: " << d.positionX << '\n';
  *os << "positionY: " << d.positionY << '\n';
  *os << "yaw: " << d.yaw << '\n';
  *os << "yawRate: " << d.yawRate << '\n';
  *os << "yawAcceleration: " << d.yawAcceleration << '\n';
  *os << "roll: " << d.roll << '\n';
  *os << "steeringWheelAngle: " << d.steeringWheelAngle << '\n';
  *os << "centripetalAcceleration: " << d.centripetalAcceleration << '\n';
  *os << "travelDistance: " << d.travelDistance << '\n';
}

struct FmuValueWrapper
{
  FmuValueWrapper(double realValue) : raw{} { raw.realValue = realValue; }
  FmuValueWrapper(int intValue) : raw{} { raw.intValue = intValue; }
  FmuValueWrapper(bool boolValue) : raw{} { raw.boolValue = boolValue; }

  FmuValue raw;
};

template <typename T>
bool operator==(const T& lhs, const T& rhs)
{
  return std::memcmp(&lhs, &rhs, sizeof(T)) == 0;
}

TEST(SignalParser, GivenOutputSignalForDynamicsSignal_WhenSignalNotEmpty_ThenGeneratesDynamicsSignal)
{
  std::set<SignalType> outputSignals{SignalType::DynamicsSignal};
  std::map<SignalValue, FmuValueWrapper> fakeValues{{SignalValue::DynamicsSignal_Acceleration, 1.0},
                                                    {SignalValue::DynamicsSignal_Velocity, 2.0},
                                                    {SignalValue::DynamicsSignal_Velocity, 3.0},
                                                    {SignalValue::DynamicsSignal_PositionX, 4.0},
                                                    {SignalValue::DynamicsSignal_PositionY, 5.0},
                                                    {SignalValue::DynamicsSignal_Yaw, 6.0},
                                                    {SignalValue::DynamicsSignal_YawRate, 7.0},
                                                    {SignalValue::DynamicsSignal_YawAcceleration, 8.0},
                                                    {SignalValue::DynamicsSignal_SteeringWheelAngle, 9.0},
                                                    {SignalValue::DynamicsSignal_CentripetalAcceleration, 10.0},
                                                    {SignalValue::DynamicsSignal_TravelDistance, 11.0}};

  std::vector<VariableType> requested_types;

  std::function<FmuValue&(SignalValue, VariableType)> getFmuSignalValue
      = [&](SignalValue signal_value, VariableType var_type) -> FmuValue&
  {
    requested_types.push_back(var_type);
    return fakeValues.at(signal_value).raw;
  };

  FakeAgent fakeAgent;

  auto data = std::visit(  //
      OutputSignalVisitor(SignalType::DynamicsSignal, outputSignals, {}, {}, {}, &fakeAgent, FMI1, getFmuSignalValue),
      OsiSource{std::monostate{}});

  ASSERT_THAT(requested_types, Each(VariableType::Double));
  EXPECT_THAT(data.get(),
              WhenDynamicCastTo<DynamicsSignal const*>(Field("dynamicsInformation",
                                                             &DynamicsSignal::dynamicsInformation,
                                                             Eq(DynamicsInformation{1.0_mps_sq,
                                                                                    2.0_mps,
                                                                                    0.0_mps,  // sinus-component
                                                                                    4.0_m,
                                                                                    5.0_m,
                                                                                    6.0_rad,
                                                                                    7.0_rad_per_s,
                                                                                    8.0_rad_per_s_sq,
                                                                                    0.0_rad,  // always zero
                                                                                    9.0_rad,
                                                                                    10.0_mps_sq,
                                                                                    11.0_m}))));
}

TEST(SignalParser, GivenOutputSignalForTrafficUpdate_WhenSignalNotEmpty_ThenGeneratesDynamicsSignal)
{
  std::set<SignalType> outputSignals{};
  osi3::TrafficUpdate trafficUpdate;
  FakeAgent fakeAgent;

  std::shared_ptr<mantle_api::VehicleProperties> vehicleModelParameters
      = std::make_shared<mantle_api::VehicleProperties>();
  vehicleModelParameters->bounding_box.geometric_center.x = 0.5_m;
  ON_CALL(fakeAgent, GetVehicleModelParameters()).WillByDefault(Return(vehicleModelParameters));
  ON_CALL(fakeAgent, GetPositionX()).WillByDefault(Return(0.0_m));
  ON_CALL(fakeAgent, GetPositionY()).WillByDefault(Return(1.0_m));

  const auto& movingObject = trafficUpdate.add_update();
  movingObject->mutable_base()->mutable_orientation()->set_yaw(0);
  movingObject->mutable_base()->mutable_position()->set_x(4.5);
  movingObject->mutable_base()->mutable_position()->set_y(4);
  movingObject->mutable_base()->mutable_velocity()->set_x(10);
  movingObject->mutable_base()->mutable_velocity()->set_y(20);
  movingObject->mutable_base()->mutable_acceleration()->set_x(30);
  movingObject->mutable_base()->mutable_acceleration()->set_y(40);
  movingObject->mutable_base()->mutable_orientation()->set_roll(3);
  movingObject->mutable_base()->mutable_orientation_rate()->set_yaw(4);
  movingObject->mutable_base()->mutable_orientation_acceleration()->set_yaw(5);

  auto data = std::visit(  //
      OutputSignalVisitor(SignalType::DynamicsSignal, outputSignals, {}, {}, {}, &fakeAgent, FMI1, {}),
      OsiSource{&trafficUpdate});

  EXPECT_THAT(data.get(),
              WhenDynamicCastTo<DynamicsSignal const*>(Field("dynamicsInformation",
                                                             &DynamicsSignal::dynamicsInformation,
                                                             Eq(DynamicsInformation{30.0_mps_sq,
                                                                                    10.0_mps,
                                                                                    20.0_mps,
                                                                                    4.0_m,
                                                                                    4.0_m,
                                                                                    0.0_rad,
                                                                                    4.0_rad_per_s,
                                                                                    5.0_rad_per_s_sq,
                                                                                    3.0_rad,
                                                                                    0.0_rad,
                                                                                    40.0_mps_sq,
                                                                                    5.0_m}))));
}

TEST(SignalParser, GivenOutputSignalForAccelerationSignal_WhenSignalNotEmpty_ThenGeneratesAccelerationSignal)
{
  std::set<SignalType> outputSignals{SignalType::AccelerationSignal};
  std::map<SignalValue, FmuValueWrapper> fakeValue{{SignalValue::AccelerationSignal_Acceleration, 20.0}};

  VariableType requested_type;

  std::function<FmuValue&(SignalValue, VariableType)> getFmuSignalValue
      = [&](SignalValue signal_value, VariableType var_type) -> FmuValue&
  {
    requested_type = var_type;
    return fakeValue.at(signal_value).raw;
  };

  FakeAgent fakeAgent;

  auto data = std::visit(  //
      OutputSignalVisitor(
          SignalType::AccelerationSignal, outputSignals, {}, {}, {}, &fakeAgent, FMI1, getFmuSignalValue),
      OsiSource{std::monostate{}});

  ASSERT_THAT(requested_type, VariableType::Double);
  EXPECT_THAT(data.get(),
              WhenDynamicCastTo<AccelerationSignal const*>(
                  Field("acceleration", &AccelerationSignal::acceleration, Eq(20.0_mps_sq))));
}

TEST(SignalParser, GivenOutputSignalForLongitudinalSignal_WhenSignalNotEmpty_ThenGeneratesLongitudinalSignal)
{
  std::set<SignalType> outputSignals{SignalType::LongitudinalSignal};
  std::map<SignalValue, FmuValueWrapper> fakeValues{{SignalValue::LongitudinalSignal_AccPedalPos, 10.0},
                                                    {SignalValue::LongitudinalSignal_BrakePedalPos, 20.0},
                                                    {SignalValue::LongitudinalSignal_Gear, 30}};

  std::vector<VariableType> requested_types;

  std::function<FmuValue&(SignalValue, VariableType)> getFmuSignalValue
      = [&](SignalValue signal_value, VariableType var_type) -> FmuValue&
  {
    requested_types.push_back(var_type);
    return fakeValues.at(signal_value).raw;
  };

  FakeAgent fakeAgent;

  auto data = std::visit(  //
      OutputSignalVisitor(
          SignalType::LongitudinalSignal, outputSignals, {}, {}, {}, &fakeAgent, FMI1, getFmuSignalValue),
      OsiSource{std::monostate{}});

  ASSERT_THAT(requested_types, ElementsAre(VariableType::Double, VariableType::Double, VariableType::Int));
  EXPECT_THAT(
      data.get(),
      WhenDynamicCastTo<LongitudinalSignal const*>(Field("accPedalPos", &LongitudinalSignal::accPedalPos, Eq(10.0))));
  EXPECT_THAT(data.get(),
              WhenDynamicCastTo<LongitudinalSignal const*>(
                  Field("brakePedalPos", &LongitudinalSignal::brakePedalPos, Eq(20.0))));
  EXPECT_THAT(data.get(),
              WhenDynamicCastTo<LongitudinalSignal const*>(Field("gear", &LongitudinalSignal::gear, Eq(30))));
}

TEST(SignalParser, GivenOutputSignalForSteeringSignal_WhenSignalNotEmpty_ThenGeneratesSteeringSignal)
{
  std::set<SignalType> outputSignals{SignalType::SteeringSignal};
  std::map<SignalValue, FmuValueWrapper> fakeValue{{SignalValue::SteeringSignal_SteeringWheelAngle, 90.0}};

  VariableType requested_type;

  std::function<FmuValue&(SignalValue, VariableType)> getFmuSignalValue
      = [&](SignalValue signal_value, VariableType var_type) -> FmuValue&
  {
    requested_type = var_type;
    return fakeValue.at(signal_value).raw;
  };

  FakeAgent fakeAgent;

  auto data = std::visit(  //
      OutputSignalVisitor(SignalType::SteeringSignal, outputSignals, {}, {}, {}, &fakeAgent, FMI1, getFmuSignalValue),
      OsiSource{std::monostate{}});

  ASSERT_THAT(requested_type, VariableType::Double);
  EXPECT_THAT(
      data.get(),
      WhenDynamicCastTo<SteeringSignal const*>(Field("acceleration", &SteeringSignal::steeringWheelAngle, Eq(90_rad))));
}

TEST(SignalParser, GivenOutputSignalForCompCtrlSignal_WhenSignalNotEmpty_ThenGeneratesCompCtrlSignal)
{
  std::set<SignalType> outputSignals{SignalType::CompCtrlSignal, SignalType::CompCtrlSignalWarningDirection};
  std::map<SignalValue, FmuValueWrapper> fakeValues{{SignalValue::CompCtrlSignal_MovementDomain, 1},
                                                    {SignalValue::CompCtrlSignal_WarningActivity, false},
                                                    {SignalValue::CompCtrlSignal_WarningLevel, 1},
                                                    {SignalValue::CompCtrlSignal_WarningType, 1},
                                                    {SignalValue::CompCtrlSignal_WarningIntensity, 1},
                                                    {SignalValue::CompCtrlSignal_WarningDirection, 7}};

  FmuEnumerations fmi1Enumerations;
  fmi1Enumerations.movementDomains = {{1, MovementDomain::Undefined},
                                      {2, MovementDomain::Lateral},
                                      {3, MovementDomain::Longitudinal},
                                      {4, MovementDomain::Both}};

  fmi1Enumerations.warningLevels = {{1, ComponentWarningLevel::INFO}, {2, ComponentWarningLevel::WARNING}};

  fmi1Enumerations.warningTypes
      = {{1, ComponentWarningType::OPTIC}, {2, ComponentWarningType::ACOUSTIC}, {3, ComponentWarningType::HAPTIC}};

  fmi1Enumerations.warningIntensities = {{1, ComponentWarningIntensity::LOW},
                                         {2, ComponentWarningIntensity::MEDIUM},
                                         {3, ComponentWarningIntensity::HIGH}};

  fmi1Enumerations.warningDirections = {{1, AreaOfInterest::LEFT_FRONT},
                                        {2, AreaOfInterest::LEFT_FRONT_FAR},
                                        {3, AreaOfInterest::RIGHT_FRONT},
                                        {4, AreaOfInterest::RIGHT_FRONT_FAR},
                                        {5, AreaOfInterest::LEFT_REAR},
                                        {6, AreaOfInterest::RIGHT_REAR},
                                        {7, AreaOfInterest::EGO_FRONT},
                                        {8, AreaOfInterest::EGO_FRONT_FAR}};

  std::vector<VariableType> requested_types;

  std::function<FmuValue&(SignalValue, VariableType)> getFmuSignalValue
      = [&](SignalValue signal_value, VariableType var_type) -> FmuValue&
  {
    requested_types.push_back(var_type);
    return fakeValues.at(signal_value).raw;
  };

  FakeAgent fakeAgent;

  auto data0 = std::visit(  //
      OutputSignalVisitor(
          SignalType::CompCtrlSignal, outputSignals, fmi1Enumerations, {}, {}, &fakeAgent, FMI1, getFmuSignalValue),
      OsiSource{std::monostate{}});

  auto compCtrlSignal0 = std::dynamic_pointer_cast<const VehicleCompToCompCtrlSignal>(data0);
  EXPECT_THAT(compCtrlSignal0->GetMovementDomain(), Eq(MovementDomain::Undefined));
  auto warnings0 = compCtrlSignal0->GetComponentWarnings();
  EXPECT_THAT(warnings0.front().activity, Eq(false));
  EXPECT_THAT(warnings0.front().level, Eq(ComponentWarningLevel::INFO));
  EXPECT_THAT(warnings0.front().type, Eq(ComponentWarningType::OPTIC));
  EXPECT_THAT(warnings0.front().intensity, Eq(ComponentWarningIntensity::LOW));
  EXPECT_THAT(warnings0.front().direction, Eq(AreaOfInterest::EGO_FRONT));

  ASSERT_THAT(requested_types,
              ElementsAre(VariableType::Enum,
                          VariableType::Bool,
                          VariableType::Enum,
                          VariableType::Enum,
                          VariableType::Enum,
                          VariableType::Enum));

  FmuEnumerations fmi2Enumerations;
  fmi2Enumerations.movementDomains = {{0, MovementDomain::Undefined},
                                      {1, MovementDomain::Lateral},
                                      {2, MovementDomain::Longitudinal},
                                      {3, MovementDomain::Both}};

  fmi2Enumerations.warningLevels = {{0, ComponentWarningLevel::INFO}, {1, ComponentWarningLevel::WARNING}};

  fmi2Enumerations.warningTypes
      = {{0, ComponentWarningType::OPTIC}, {1, ComponentWarningType::ACOUSTIC}, {2, ComponentWarningType::HAPTIC}};

  fmi2Enumerations.warningIntensities = {{0, ComponentWarningIntensity::LOW},
                                         {1, ComponentWarningIntensity::MEDIUM},
                                         {2, ComponentWarningIntensity::HIGH}};

  fmi2Enumerations.warningDirections = {{0, AreaOfInterest::LEFT_FRONT},
                                        {1, AreaOfInterest::LEFT_FRONT_FAR},
                                        {2, AreaOfInterest::RIGHT_FRONT},
                                        {3, AreaOfInterest::RIGHT_FRONT_FAR},
                                        {4, AreaOfInterest::LEFT_REAR},
                                        {5, AreaOfInterest::RIGHT_REAR},
                                        {6, AreaOfInterest::EGO_FRONT},
                                        {7, AreaOfInterest::EGO_FRONT_FAR}};

  auto data1 = std::visit(  //
      OutputSignalVisitor(
          SignalType::CompCtrlSignal, outputSignals, fmi2Enumerations, {}, {}, &fakeAgent, FMI2, getFmuSignalValue),
      OsiSource{std::monostate{}});

  auto compCtrlSignal1 = std::dynamic_pointer_cast<const VehicleCompToCompCtrlSignal>(data1);
  EXPECT_THAT(compCtrlSignal1->GetMovementDomain(), Eq(MovementDomain::Lateral));
  auto warnings1 = compCtrlSignal1->GetComponentWarnings();
  EXPECT_THAT(warnings1.front().activity, Eq(false));
  EXPECT_THAT(warnings1.front().level, Eq(ComponentWarningLevel::WARNING));
  EXPECT_THAT(warnings1.front().type, Eq(ComponentWarningType::ACOUSTIC));
  EXPECT_THAT(warnings1.front().intensity, Eq(ComponentWarningIntensity::MEDIUM));
  EXPECT_THAT(warnings1.front().direction, Eq(AreaOfInterest::EGO_FRONT_FAR));
}