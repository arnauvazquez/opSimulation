/********************************************************************************
 * Copyright (c) 2020-2021 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "egoAgent.h"
#include "fakeAgent.h"
#include "fakeRadio.h"
#include "fakeWorld.h"

using ::testing::_;
using ::testing::ElementsAre;
using ::testing::Eq;
using ::testing::NiceMock;
using ::testing::Return;
using ::testing::ReturnRef;
using ::testing::SizeIs;
using ::testing::VariantWith;

TEST(EgoAgent_Test, GetDistanceToEndOfLane)
{
  NiceMock<FakeAgent> fakeAgent;
  NiceMock<FakeWorld> fakeWorld;

  GlobalRoadPositions agentPosition{{"Road1", GlobalRoadPosition{"Road1", -2, 12_m, 0_m, 0_rad}}};
  ON_CALL(fakeAgent, GetRoadPosition(VariantWith<ObjectPointPredefined>(ObjectPointPredefined::FrontCenter)))
      .WillByDefault(ReturnRef(agentPosition));
  std::vector<std::string> roads{"Road1"};
  ON_CALL(fakeAgent, GetRoads(_)).WillByDefault(Return(roads));

  RoadGraph roadGraph;
  RoadGraphVertex root = add_vertex(RouteElement{"Road1", true}, roadGraph);
  RoadGraphVertex target = add_vertex(RouteElement{"Road2", true}, roadGraph);
  add_edge(root, target, roadGraph);

  RouteQueryResult<units::length::meter_t> distances{{0, 123_m}};
  ON_CALL(fakeWorld, GetDistanceToEndOfLane(_, _, -1, 12_m, 100_m)).WillByDefault(Return(distances));

  EgoAgent egoAgent{&fakeAgent, &fakeWorld};
  egoAgent.SetRoadGraph(std::move(roadGraph), root, target);

  const auto result = egoAgent.GetDistanceToEndOfLane(100_m, 1);
  ASSERT_THAT(result.value(), Eq(123));
}

TEST(EgoAgent_Test, GetObjectsInRange)
{
  NiceMock<FakeAgent> fakeAgent;
  NiceMock<FakeWorld> fakeWorld;

  GlobalRoadPositions agentPosition{{"Road1", GlobalRoadPosition{"Road1", -2, 12_m, 0_m, 0_rad}}};
  ON_CALL(fakeAgent, GetRoadPosition(VariantWith<ObjectPointPredefined>(ObjectPointPredefined::FrontCenter)))
      .WillByDefault(ReturnRef(agentPosition));
  std::vector<std::string> roads{"Road1"};
  ON_CALL(fakeAgent, GetRoads(_)).WillByDefault(Return(roads));

  RoadGraph roadGraph;
  RoadGraphVertex root = add_vertex(RouteElement{"Road1", true}, roadGraph);
  RoadGraphVertex target = add_vertex(RouteElement{"Road2", true}, roadGraph);
  add_edge(root, target, roadGraph);

  FakeAgent otherAgent;
  RouteQueryResult<std::vector<const WorldObjectInterface *>> objects{{0, {&otherAgent}}};
  ON_CALL(fakeWorld, GetObjectsInRange(_, _, -1, 12_m, 100_m, 100_m)).WillByDefault(Return(objects));

  EgoAgent egoAgent{&fakeAgent, &fakeWorld};
  egoAgent.SetRoadGraph(std::move(roadGraph), root, target);

  const auto result = egoAgent.GetObjectsInRange(100_m, 100_m, 1);
  ASSERT_THAT(result, ElementsAre(&otherAgent));
}

TEST(EgoAgent_Test, GetAgentsInRange)
{
  NiceMock<FakeAgent> fakeAgent;
  NiceMock<FakeWorld> fakeWorld;

  GlobalRoadPositions agentPosition{{"Road1", GlobalRoadPosition{"Road1", -2, 12_m, 0_m, 0_rad}}};
  ON_CALL(fakeAgent, GetRoadPosition(VariantWith<ObjectPointPredefined>(ObjectPointPredefined::FrontCenter)))
      .WillByDefault(ReturnRef(agentPosition));
  std::vector<std::string> roads{"Road1"};
  ON_CALL(fakeAgent, GetRoads(_)).WillByDefault(Return(roads));

  RoadGraph roadGraph;
  RoadGraphVertex root = add_vertex(RouteElement{"Road1", true}, roadGraph);
  RoadGraphVertex target = add_vertex(RouteElement{"Road2", true}, roadGraph);
  add_edge(root, target, roadGraph);

  FakeAgent otherAgent;
  RouteQueryResult<AgentInterfaces> objects{{0, {&otherAgent}}};
  ON_CALL(fakeWorld, GetAgentsInRange(_, _, -1, 12_m, 100_m, 100_m)).WillByDefault(Return(objects));

  EgoAgent egoAgent{&fakeAgent, &fakeWorld};
  egoAgent.SetRoadGraph(std::move(roadGraph), root, target);

  const auto result = egoAgent.GetAgentsInRange(100_m, 100_m, 1);
  ASSERT_THAT(result, ElementsAre(&otherAgent));
}

TEST(EgoAgent_Test, GetTrafficSignsInRange)
{
  NiceMock<FakeAgent> fakeAgent;
  NiceMock<FakeWorld> fakeWorld;

  GlobalRoadPositions agentPosition{{"Road1", GlobalRoadPosition{"Road1", -2, 12_m, 0_m, 0_rad}}};
  ON_CALL(fakeAgent, GetRoadPosition(VariantWith<ObjectPointPredefined>(ObjectPointPredefined::FrontCenter)))
      .WillByDefault(ReturnRef(agentPosition));
  std::vector<std::string> roads{"Road1"};
  ON_CALL(fakeAgent, GetRoads(_)).WillByDefault(Return(roads));

  RoadGraph roadGraph;
  RoadGraphVertex root = add_vertex(RouteElement{"Road1", true}, roadGraph);
  RoadGraphVertex target = add_vertex(RouteElement{"Road2", true}, roadGraph);
  add_edge(root, target, roadGraph);

  RouteQueryResult<std::vector<CommonTrafficSign::Entity>> objects{{0, {CommonTrafficSign::Entity{}}}};
  ON_CALL(fakeWorld, GetTrafficSignsInRange(_, _, -1, 12_m, 100_m)).WillByDefault(Return(objects));

  EgoAgent egoAgent{&fakeAgent, &fakeWorld};
  egoAgent.SetRoadGraph(std::move(roadGraph), root, target);

  const auto result = egoAgent.GetTrafficSignsInRange(100_m, 1);
  ASSERT_THAT(result, SizeIs(1));
}

TEST(EgoAgent_Test, GetReferencePointPosition_FirstRoad)
{
  NiceMock<FakeAgent> fakeAgent;
  NiceMock<FakeWorld> fakeWorld;

  RoadGraph roadGraph;
  RoadGraphVertex root = add_vertex(RouteElement{"Road1", true}, roadGraph);
  RoadGraphVertex target = add_vertex(RouteElement{"Road2", true}, roadGraph);
  add_edge(root, target, roadGraph);

  GlobalRoadPositions referencePoint{{"Road1", {"Road1", -2, 2.0_m, 3.0_m, 0.1_rad}}};
  GlobalRoadPositions frontPoint{{"Road1", {"Road1", -2, 12.0_m, 3.0_m, 0.1_rad}}};
  ON_CALL(fakeAgent, GetRoadPosition(VariantWith<ObjectPointPredefined>(ObjectPointPredefined::Reference)))
      .WillByDefault(ReturnRef(referencePoint));
  ON_CALL(fakeAgent, GetRoadPosition(VariantWith<ObjectPointPredefined>(ObjectPointPredefined::FrontCenter)))
      .WillByDefault(ReturnRef(frontPoint));
  std::vector<std::string> roads{"Road1"};
  ON_CALL(fakeAgent, GetRoads(_)).WillByDefault(Return(roads));

  EgoAgent egoAgent{&fakeAgent, &fakeWorld};
  egoAgent.SetRoadGraph(std::move(roadGraph), root, target);

  const auto result = egoAgent.GetReferencePointPosition();

  ASSERT_THAT(result.has_value(), Eq(true));
  ASSERT_THAT(result.value().laneId, Eq(-2));
  ASSERT_THAT(result.value().roadPosition.s.value(), Eq(2.0));
  ASSERT_THAT(result.value().roadPosition.t.value(), Eq(3.0));
  ASSERT_THAT(result.value().roadPosition.hdg.value(), Eq(0.1));
}

TEST(EgoAgent_Test, GetReferencePointPosition_SecondRoad)
{
  NiceMock<FakeAgent> fakeAgent;
  NiceMock<FakeWorld> fakeWorld;

  RoadGraph roadGraph;
  RoadGraphVertex root = add_vertex(RouteElement{"Road1", true}, roadGraph);
  RoadGraphVertex target = add_vertex(RouteElement{"Road2", true}, roadGraph);
  add_edge(root, target, roadGraph);

  GlobalRoadPositions referencePoint{{"Road2", {"Road2", -2, 2.0_m, 3.0_m, 0.1_rad}}};
  GlobalRoadPositions frontPoint{{"Road2", {"Road2", -2, 12.0_m, 3.0_m, 0.1_rad}}};
  ON_CALL(fakeAgent, GetRoadPosition(VariantWith<ObjectPointPredefined>(ObjectPointPredefined::Reference)))
      .WillByDefault(ReturnRef(referencePoint));
  ON_CALL(fakeAgent, GetRoadPosition(VariantWith<ObjectPointPredefined>(ObjectPointPredefined::FrontCenter)))
      .WillByDefault(ReturnRef(frontPoint));
  std::vector<std::string> roads{"Road2"};
  ON_CALL(fakeAgent, GetRoads(VariantWith<ObjectPointPredefined>(ObjectPointPredefined::FrontCenter)))
      .WillByDefault(Return(roads));

  EgoAgent egoAgent{&fakeAgent, &fakeWorld};
  egoAgent.SetRoadGraph(std::move(roadGraph), root, target);
  egoAgent.Update();

  const auto result = egoAgent.GetReferencePointPosition();

  ASSERT_THAT(result.has_value(), Eq(true));
  ASSERT_THAT(result.value().laneId, Eq(-2));
  ASSERT_THAT(result.value().roadPosition.s.value(), Eq(2.0));
  ASSERT_THAT(result.value().roadPosition.t.value(), Eq(3.0));
  ASSERT_THAT(result.value().roadPosition.hdg.value(), Eq(0.1));
}

TEST(EgoAgent_Test, GetReferencePointPosition_NotOnRoute)
{
  NiceMock<FakeAgent> fakeAgent;
  NiceMock<FakeWorld> fakeWorld;

  RoadGraph roadGraph;
  RoadGraphVertex root = add_vertex(RouteElement{"Road1", true}, roadGraph);
  RoadGraphVertex target = add_vertex(RouteElement{"Road2", true}, roadGraph);
  add_edge(root, target, roadGraph);

  GlobalRoadPositions referencePoint{{"Offroad", {"Offroad", -2, 2.0_m, 3.0_m, 0.1_rad}}};
  GlobalRoadPositions frontPoint{{"Offroad", {"Offroad", -2, 12.0_m, 3.0_m, 0.1_rad}}};
  ON_CALL(fakeAgent, GetRoadPosition(VariantWith<ObjectPointPredefined>(ObjectPointPredefined::Reference)))
      .WillByDefault(ReturnRef(referencePoint));
  ON_CALL(fakeAgent, GetRoadPosition(VariantWith<ObjectPointPredefined>(ObjectPointPredefined::FrontCenter)))
      .WillByDefault(ReturnRef(frontPoint));

  EgoAgent egoAgent{&fakeAgent, &fakeWorld};
  egoAgent.SetRoadGraph(std::move(roadGraph), root, target);

  const auto result = egoAgent.GetReferencePointPosition();

  ASSERT_THAT(result.has_value(), Eq(false));
}

class EgoAgent_GetAlternatives_Test : public ::testing::Test
{
public:
  EgoAgent_GetAlternatives_Test()
  {
    add_edge(root, node1, roadGraph);
    add_edge(root, node2, roadGraph);
    add_edge(node2, node21, roadGraph);
    add_edge(node2, node22, roadGraph);
    add_edge(root, node3, roadGraph);
    ON_CALL(fakeAgent, GetRoadPosition(VariantWith<ObjectPointPredefined>(ObjectPointPredefined::FrontCenter)))
        .WillByDefault(ReturnRef(agentPosition));
    ON_CALL(fakeAgent, GetRoads(_)).WillByDefault(Return(roads));
    RouteQueryResult<units::length::meter_t> distances{{node1, 100_m}, {node21, 150_m}, {node22, 200_m}, {node3, 50_m}};
    ON_CALL(fakeWorld, GetDistanceToEndOfLane(_, _, -3, 12_m, 500_m)).WillByDefault(Return(distances));

    RouteQueryResult<std::vector<const WorldObjectInterface *>> objects{
        {node1, {&opponent1, &opponent2}}, {node21, {}}, {node22, {}}, {node3, {&opponent1}}};
    ON_CALL(fakeWorld, GetObjectsInRange(_, _, -3, 12_m, 100_m, 500_m)).WillByDefault(Return(objects));

    egoAgent.SetRoadGraph(std::move(roadGraph), root, node21);
  }

  NiceMock<FakeAgent> fakeAgent;
  NiceMock<FakeWorld> fakeWorld;
  EgoAgent egoAgent{&fakeAgent, &fakeWorld};
  GlobalRoadPositions agentPosition{{"Road1", GlobalRoadPosition{"Road1", -2, 12_m, 0_m, 0_rad}}};
  std::vector<std::string> roads{"Road1"};
  RoadGraph roadGraph;
  RoadGraphVertex root = add_vertex(RouteElement{"Road1", true}, roadGraph);
  RoadGraphVertex node1 = add_vertex(roadGraph);
  RoadGraphVertex node2 = add_vertex(roadGraph);
  RoadGraphVertex node21 = add_vertex(roadGraph);
  RoadGraphVertex node22 = add_vertex(roadGraph);
  RoadGraphVertex node3 = add_vertex(roadGraph);
  FakeAgent opponent1;
  FakeAgent opponent2;
};

TEST_F(EgoAgent_GetAlternatives_Test, GetAlternativesFilterByDistance)
{
  const auto alternatives = egoAgent.GetAlternatives(
      Predicate<DistanceToEndOfLane>{[](const units::length::meter_t &distance) { return distance < 160_m; }},
      DistanceToEndOfLaneParameter{500_m, -1});

  ASSERT_THAT(alternatives, ElementsAre(0, 1, 3));
}

TEST_F(EgoAgent_GetAlternatives_Test, GetAlternativesFilterByDistanceAndObjects)
{
  const auto alternatives = egoAgent.GetAlternatives(
      Predicate<DistanceToEndOfLane, ObjectsInRange>{[](const units::length::meter_t &distance, const auto &objects)
                                                     { return distance < 160_m && objects.value.empty(); }},
      DistanceToEndOfLaneParameter{500_m, -1},
      ObjectsInRangeParameter{100_m, 500_m, -1});

  ASSERT_THAT(alternatives, ElementsAre(1));
}

TEST_F(EgoAgent_GetAlternatives_Test, GetAlternativesFilterAndSortByDistance)
{
  const auto alternatives = egoAgent.GetAlternatives(
      Predicate<DistanceToEndOfLane>{[](const units::length::meter_t &distance) { return distance < 160_m; }},
      Compare<DistanceToEndOfLane>{[](const auto &distances) { return distances.first < distances.second; }},
      DistanceToEndOfLaneParameter{500_m, -1});

  ASSERT_THAT(alternatives, ElementsAre(3, 0, 1));
}

TEST_F(EgoAgent_GetAlternatives_Test, GetAlternativesFilterAndSortByDistanceAndObjects)
{
  const auto alternatives = egoAgent.GetAlternatives(
      Predicate<DistanceToEndOfLane, ObjectsInRange>{[](const units::length::meter_t &distance, const auto &objects)
                                                     { return distance < 160_m && !objects.value.empty(); }},
      Compare<DistanceToEndOfLane, ObjectsInRange>{[](const auto &distances, const auto &objects) {
        return objects.first.value.size() < objects.second.value.size();
      }},
      DistanceToEndOfLaneParameter{500_m, -1},
      ObjectsInRangeParameter{100_m, 500_m, -1});

  ASSERT_THAT(alternatives, ElementsAre(3, 0));
}

struct GetLaneIdFromRelative_Data
{
  bool inOdDirection;
  int ownLaneId;
  int relativeLane;
  int expectedLaneId;
};

class GetLaneIdFromRelativeTest : public ::testing::TestWithParam<GetLaneIdFromRelative_Data>
{
};

TEST_P(GetLaneIdFromRelativeTest, CalculatesCorrectLaneId)
{
  const auto &data = GetParam();

  NiceMock<FakeAgent> fakeAgent;
  NiceMock<FakeWorld> fakeWorld;

  GlobalRoadPositions agentPosition{{"Road1", GlobalRoadPosition{"Road1", data.ownLaneId, 0_m, 0_m, 0_rad}}};
  ON_CALL(fakeAgent, GetRoadPosition(VariantWith<ObjectPointPredefined>(ObjectPointPredefined::FrontCenter)))
      .WillByDefault(ReturnRef(agentPosition));
  std::vector<std::string> roads{"Road1"};
  ON_CALL(fakeAgent, GetRoads(VariantWith<ObjectPointPredefined>(ObjectPointPredefined::FrontCenter)))
      .WillByDefault(Return(roads));
  RoadGraph roadGraph;
  RoadGraphVertex root = add_vertex(RouteElement{"Road1", data.inOdDirection}, roadGraph);

  EgoAgent egoAgent{&fakeAgent, &fakeWorld};
  egoAgent.SetRoadGraph(std::move(roadGraph), root, root);

  auto result = egoAgent.GetLaneIdFromRelative(data.relativeLane);

  ASSERT_THAT(result, Eq(data.expectedLaneId));
}

INSTANTIATE_TEST_SUITE_P(InOdDirection,
                         GetLaneIdFromRelativeTest,
                         ::testing::Values(GetLaneIdFromRelative_Data{true, -1, 0, -1},
                                           GetLaneIdFromRelative_Data{true, -1, 1, 1},
                                           GetLaneIdFromRelative_Data{true, -1, 2, 2},
                                           GetLaneIdFromRelative_Data{true, -1, -1, -2},
                                           GetLaneIdFromRelative_Data{true, -2, 1, -1},
                                           GetLaneIdFromRelative_Data{true, -2, 2, 1},
                                           GetLaneIdFromRelative_Data{true, 1, 0, 1},
                                           GetLaneIdFromRelative_Data{true, 1, 1, 2},
                                           GetLaneIdFromRelative_Data{true, 1, -1, -1},
                                           GetLaneIdFromRelative_Data{true, 2, -3, -2}));

INSTANTIATE_TEST_SUITE_P(AgainstOdDirection,
                         GetLaneIdFromRelativeTest,
                         ::testing::Values(GetLaneIdFromRelative_Data{false, -1, 0, -1},
                                           GetLaneIdFromRelative_Data{false, -1, 1, -2},
                                           GetLaneIdFromRelative_Data{false, -1, -1, 1},
                                           GetLaneIdFromRelative_Data{false, -1, -2, 2},
                                           GetLaneIdFromRelative_Data{false, -2, -1, -1},
                                           GetLaneIdFromRelative_Data{false, -2, -2, 1},
                                           GetLaneIdFromRelative_Data{false, 1, 0, 1},
                                           GetLaneIdFromRelative_Data{false, 1, 1, -1},
                                           GetLaneIdFromRelative_Data{false, 1, -1, 2},
                                           GetLaneIdFromRelative_Data{false, 2, 3, -2}));
