/********************************************************************************
 * Copyright (c) 2018-2019 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <cmath>

#include "Generators/laneGeometryElementGenerator.h"
#include "OWL/DataTypes.h"
#include "WorldToRoadCoordinateConverter.h"

using ::testing::DoubleNear;
using ::testing::Eq;

using namespace World::Localization;

struct WorldToRoadCoordinateConverterRectangular_Data
{
  // do not change order of items
  // unless you also change it in INSTANTIATE_TEST_CASE_P
  // (see bottom of file)

  // data for generator
  Common::Vector2d<units::length::meter_t> origin;
  units::length::meter_t width;
  units::length::meter_t length;
  units::angle::radian_t hdg;

  // test point
  Common::Vector2d<units::length::meter_t> point;
  units::angle::radian_t pointHdg;

  // expected values
  OWL::Primitive::RoadCoordinate expected;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const WorldToRoadCoordinateConverterRectangular_Data& obj)
  {
    return os << "origin " << obj.origin << ", "
              << "width " << obj.width << ", "
              << "length " << obj.length << ", "
              << "hdg " << obj.hdg << ", "
              << "point (" << obj.point.x << ", " << obj.point.y << "), "
              << "pointHdg " << obj.pointHdg << ", "
              << "expected (s " << obj.expected.s << ", t " << obj.expected.t << ", hdg " << obj.expected.yaw << ")";
  }
};

/// \see https://github.com/google/googletest/blob/master/googletest/docs/AdvancedGuide.md
class WorldToRoadCoordinateConverterRectangular_Test
    : public ::testing::Test,
      public ::testing::WithParamInterface<WorldToRoadCoordinateConverterRectangular_Data>
{
};

struct WorldToRoadCoordinateConverterCurved_Data
{
  // do not change order of items
  // unless you also change it in INSTANTIATE_TEST_CASE_P
  // (see bottom of file)

  // data for generator
  Common::Vector2d<units::length::meter_t> origin;
  units::length::meter_t width;
  units::length::meter_t length;
  units::length::meter_t sDistance;
  units::length::meter_t radius;

  // test point
  Common::Vector2d<units::length::meter_t> point;
  units::angle::radian_t pointHdg;

  // expected values
  OWL::Primitive::RoadCoordinate expected;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const WorldToRoadCoordinateConverterCurved_Data& obj)
  {
    return os << "origin " << obj.origin << ", "
              << "width " << obj.width << ", "
              << "length " << obj.length << ", "
              << "sDistance " << obj.sDistance << ", "
              << "radius " << obj.radius << ", "
              << "point (" << obj.point.x << ", " << obj.point.y << "), "
              << "pointHdg " << obj.pointHdg << ", "
              << "expected (s " << obj.expected.s << ", t " << obj.expected.t << ", hdg " << obj.expected.yaw << ")";
  }
};

class WorldToRoadCoordinateConverterCurved_Test
    : public ::testing::Test,
      public ::testing::WithParamInterface<WorldToRoadCoordinateConverterCurved_Data>
{
};

/// \test Checks if a point is matched if it is inside the geometric element
/// \sa PointQuery
TEST(WorldToRoadCoordinateConverter_Test, Point_IsOnlyMatchedIfInsideElement)
{
  Common::Vector2d<units::length::meter_t> origin{-5_m, 0_m};
  units::length::meter_t width = 5_m;
  units::length::meter_t length = 10_m;
  units::angle::radian_t hdg = 0_rad;

  auto laneGeometryElement
      = OWL::Testing::LaneGeometryElementGenerator::RectangularLaneGeometryElement(origin, width, length, hdg);
  LocalizationElement localizationElement{laneGeometryElement};
  WorldToRoadCoordinateConverter converter(localizationElement);

  Common::Vector2d<units::length::meter_t> pointOutsideElement{1000_m, 1000_m};
  ASSERT_THAT(converter.IsConvertible(pointOutsideElement), Eq(false));

  Common::Vector2d<units::length::meter_t> pointWithinElement{0.1_m, -0.1_m};
  ASSERT_THAT(converter.IsConvertible(pointWithinElement), Eq(true));
}

/// \test Checks if a positive t coordinate is calculated, even if the first points of the
///       geometric element fall together, and so the projection vector for t collapses
TEST(WorldToRoadCoordinateConverter_Test, CurrentPointTrippleIsSingular_CalculatesPositiveTCoordinate)
{
  Common::Vector2d<units::length::meter_t> origin{-5_m, 0_m};
  units::length::meter_t width = 5_m;
  units::length::meter_t length = 10_m;
  units::angle::radian_t hdg = 0_rad;

  auto laneGeometryElement
      = OWL::Testing::LaneGeometryElementGenerator::TriangularLaneGeometryElement(origin, width, length, hdg);
  LocalizationElement localizationElement{laneGeometryElement};
  WorldToRoadCoordinateConverter converter(localizationElement);

  Common::Vector2d<units::length::meter_t> pointWithinElement{0.0_m, 2.0_m};
  auto roadCoordinate = converter.GetRoadCoordinate(pointWithinElement, 0.0_rad);

  EXPECT_THAT(roadCoordinate.s.value(), DoubleNear(5, 1e-2));
  EXPECT_THAT(roadCoordinate.t.value(), DoubleNear(2, 1e-2));
  EXPECT_THAT(roadCoordinate.hdg.value(), DoubleNear(0, 1e-2));
}

/// \test Checks if a negative t coordinate is calculated, even if the first points of the
///       geometric element fall together, and so the projection vector for t collapses
TEST(WorldToRoadCoordinateConverter_Test, CurrentPointTrippleIsSingular_CalculatesNegativeTCoordinate)
{
  Common::Vector2d<units::length::meter_t> origin{-5_m, 0_m};
  units::length::meter_t width = 5_m;
  units::length::meter_t length = 10_m;
  units::angle::radian_t hdg = 0_rad;

  auto laneGeometryElement
      = OWL::Testing::LaneGeometryElementGenerator::TriangularLaneGeometryElement(origin, width, length, hdg);
  LocalizationElement localizationElement{laneGeometryElement};
  WorldToRoadCoordinateConverter converter(localizationElement);

  Common::Vector2d<units::length::meter_t> pointWithinElement{0.0_m, -2.0_m};
  auto roadCoordinate = converter.GetRoadCoordinate(pointWithinElement, 0.0_rad);

  EXPECT_THAT(roadCoordinate.s.value(), DoubleNear(5, 1e-2));
  EXPECT_THAT(roadCoordinate.t.value(), DoubleNear(-2, 1e-2));
  EXPECT_THAT(roadCoordinate.hdg.value(), DoubleNear(0, 1e-2));
}

/// \test Checks if the road coordinates are calculated right
///
TEST_P(WorldToRoadCoordinateConverterRectangular_Test, PointWithinElement_ReturnsExpectedRoadCoordinates)
{
  auto data = GetParam();
  auto laneGeometryElement = OWL::Testing::LaneGeometryElementGenerator::RectangularLaneGeometryElement(
      data.origin, data.width, data.length, data.hdg);
  LocalizationElement localizationElement{laneGeometryElement};
  WorldToRoadCoordinateConverter converter(localizationElement);
  auto roadCoordinate = converter.GetRoadCoordinate(data.point, data.pointHdg);

  EXPECT_THAT(roadCoordinate.s.value(), DoubleNear(data.expected.s.value(), 1e-2));
  EXPECT_THAT(roadCoordinate.t.value(), DoubleNear(data.expected.t.value(), 1e-2));
  EXPECT_THAT(roadCoordinate.hdg.value(), DoubleNear(data.expected.yaw.value(), 1e-2));
}

INSTANTIATE_TEST_CASE_P(
    sCoordinateTestSet,
    WorldToRoadCoordinateConverterRectangular_Test,
    testing::Values(
        /*  origin       width length     hdg              point         pointHdg              expected              */
        /*  x     y        /     /         |             x       y           |           s         t         yaw     */
        WorldToRoadCoordinateConverterRectangular_Data{
            {-5_m, 0_m}, 5_m, 10_m, 0.00_rad, {1.00_m, 0.00_m}, 0.00_rad, {6.00_m, 0.00_m, 0.00_rad}},
        WorldToRoadCoordinateConverterRectangular_Data{
            {-4_m, 0_m}, 5_m, 10_m, 0.00_rad, {1.00_m, 0.00_m}, 0.00_rad, {5.00_m, 0.00_m, 0.00_rad}},
        WorldToRoadCoordinateConverterRectangular_Data{
            {-5_m, 0_m}, 5_m, 10_m, 0.00_rad, {-1.00_m, 0.00_m}, 0.00_rad, {4.00_m, 0.00_m, 0.00_rad}},
        // 180°
        WorldToRoadCoordinateConverterRectangular_Data{
            {5_m, 0_m}, 5_m, 10_m, 180_deg, {-1.00_m, 0.00_m}, 180_deg, {6.00_m, 0.00_m, 0.00_rad}},
        WorldToRoadCoordinateConverterRectangular_Data{
            {4_m, 0_m}, 5_m, 10_m, 180_deg, {-1.00_m, 0.00_m}, 180_deg, {5.00_m, 0.00_m, 0.00_rad}},
        WorldToRoadCoordinateConverterRectangular_Data{
            {5_m, 0_m}, 5_m, 10_m, 180_deg, {1.00_m, 0.00_m}, 180_deg, {4.00_m, 0.00_m, 0.00_rad}},
        // +/-45°
        WorldToRoadCoordinateConverterRectangular_Data{
            {0_m, 0_m}, 5_m, 10_m, 45_deg, {1.41_m, 1.41_m}, 45_deg, {2.00_m, 0.00_m, 0.00_rad}},
        WorldToRoadCoordinateConverterRectangular_Data{
            {0_m, 0_m}, 5_m, 10_m, -45_deg, {1.41_m, -1.41_m}, -45_deg, {2.00_m, 0.00_m, 0.00_rad}}));

INSTANTIATE_TEST_CASE_P(
    tCoordinateTestSet,
    WorldToRoadCoordinateConverterRectangular_Test,
    testing::Values(
        /*  origin           width length     hdg              point         pointHdg              expected */
        /*   x         y       /     /         |             x       y           |           s         t         yaw */
        WorldToRoadCoordinateConverterRectangular_Data{
            {-5_m, 0_m}, 5_m, 10_m, 0.00_rad, {0.00_m, 1.00_m}, 0.00_rad, {5.00_m, 1.00_m, 0.00_rad}},
        WorldToRoadCoordinateConverterRectangular_Data{
            {-5_m, 0_m}, 5_m, 10_m, 0.00_rad, {0.00_m, 0.00_m}, 0.00_rad, {5.00_m, 0.00_m, 0.00_rad}},
        WorldToRoadCoordinateConverterRectangular_Data{
            {-5_m, 0_m}, 5_m, 10_m, 0.00_rad, {0.00_m, -1.00_m}, 0.00_rad, {5.00_m, -1.00_m, 0.00_rad}},
        // 180°
        WorldToRoadCoordinateConverterRectangular_Data{
            {5_m, 0_m}, 5_m, 10_m, 180_deg, {0.00_m, -1.00_m}, 180_deg, {5.00_m, 1.00_m, 0.00_rad}},
        WorldToRoadCoordinateConverterRectangular_Data{
            {5_m, 0_m}, 5_m, 10_m, 180_deg, {0.00_m, 0.00_m}, 180_deg, {5.00_m, 0.00_m, 0.00_rad}},
        WorldToRoadCoordinateConverterRectangular_Data{
            {5_m, 0_m}, 5_m, 10_m, 180_deg, {0.00_m, 1.00_m}, 180_deg, {5.00_m, -1.00_m, 0.00_rad}},
        // +/-45°
        WorldToRoadCoordinateConverterRectangular_Data{
            {-1.41_m, -1.41_m}, 5_m, 10_m, 45_deg, {-1.41_m, 1.41_m}, 45_deg, {2.00_m, 2.00_m, 0.00_rad}},
        WorldToRoadCoordinateConverterRectangular_Data{
            {-1.41_m, -1.41_m}, 5_m, 10_m, 45_deg, {1.41_m, -1.41_m}, 45_deg, {2.00_m, -2.00_m, 0.00_rad}},
        WorldToRoadCoordinateConverterRectangular_Data{
            {-1.41_m, 1.41_m}, 5_m, 10_m, -45_deg, {1.41_m, 1.41_m}, -45_deg, {2.00_m, 2.00_m, 0.00_rad}},
        WorldToRoadCoordinateConverterRectangular_Data{
            {-1.41_m, 1.41_m}, 5_m, 10_m, -45_deg, {-1.41_m, -1.41_m}, -45_deg, {2.00_m, -2.00_m, 0.00_rad}}));

INSTANTIATE_TEST_CASE_P(
    yawTestSet,
    WorldToRoadCoordinateConverterRectangular_Test,
    testing::Values(
        /*                                                  origin       width length     hdg             point pointHdg
           expected        */
        /*                                                  x     y        |     |         |            x        y | s
           t          yaw */
        WorldToRoadCoordinateConverterRectangular_Data{
            {-5_m, 0_m}, 5_m, 10_m, 0.00_rad, {0.00_m, 0.00_m}, 0.10_rad, {5.00_m, 0.00_m, 0.10_rad}},
        WorldToRoadCoordinateConverterRectangular_Data{
            {-5_m, 0_m}, 5_m, 10_m, 0.00_rad, {0.00_m, 0.00_m}, -0.10_rad, {5.00_m, 0.00_m, -0.10_rad}},
        // 180°
        WorldToRoadCoordinateConverterRectangular_Data{
            {5_m, 0_m}, 5_m, 10_m, 180_deg, {0.00_m, 0.00_m}, 180_deg + .1_rad, {5.00_m, 0.00_m, 0.10_rad}},
        WorldToRoadCoordinateConverterRectangular_Data{
            {5_m, 0_m}, 5_m, 10_m, 180_deg, {0.00_m, 0.00_m}, 180_deg - .1_rad, {5.00_m, 0.00_m, -0.10_rad}}));

INSTANTIATE_TEST_CASE_P(yawWithinPiTestSet,
                        WorldToRoadCoordinateConverterRectangular_Test,
                        testing::Values(
                            /*                                                   origin       width length     hdg point
                               pointHdg                        expected          */
                            /*                                                  x     y        |     |         | x y |
                               s        t              yaw */
                            WorldToRoadCoordinateConverterRectangular_Data{{-5_m, 0_m},
                                                                           5_m,
                                                                           10_m,
                                                                           0.00_deg,
                                                                           {0.00_m, 0.00_m},
                                                                           180_deg + 1.0_rad,
                                                                           {5.00_m, 0.00_m, -180_deg + 1.00_rad}},
                            WorldToRoadCoordinateConverterRectangular_Data{{-5_m, 0_m},
                                                                           5_m,
                                                                           10_m,
                                                                           0.00_deg,
                                                                           {0.00_m, 0.00_m},
                                                                           -180_deg - 1.0_rad,
                                                                           {5.00_m, 0.00_m, 180_deg - 1.00_rad}}));

TEST_P(WorldToRoadCoordinateConverterCurved_Test, PointWithinElement_ReturnsExpectedRoadCoordinates)
{
  auto data = GetParam();
  auto laneGeometryElement = OWL::Testing::LaneGeometryElementGenerator::CurvedLaneGeometryElement(
      data.origin, data.width, data.length, data.sDistance, data.radius);
  LocalizationElement localizationElement{laneGeometryElement};
  WorldToRoadCoordinateConverter converter(localizationElement);
  auto roadCoordinate = converter.GetRoadCoordinate(data.point, data.pointHdg);

  EXPECT_THAT(roadCoordinate.s.value(), DoubleNear(data.expected.s.value(), 1e-2));
  EXPECT_THAT(roadCoordinate.t.value(), DoubleNear(data.expected.t.value(), 1e-2));
  EXPECT_THAT(roadCoordinate.hdg.value(), DoubleNear(data.expected.yaw.value(), 1e-2));
}

INSTANTIATE_TEST_CASE_P(
    curvedTestSet,
    WorldToRoadCoordinateConverterCurved_Test,
    testing::Values(
        /*                                            origin     width length  sDistance radius            point
           pointHdg           expected       */
        /*                                            x    y       |     |        |       |             x        y | s
           t       yaw */
        WorldToRoadCoordinateConverterCurved_Data{
            {-5_m, 5_m}, 4_m, 3_m, 3_m, 20.0_m, {-5.0_m, 5.0_m}, 0.0_rad, {0.0_m, 0.0_m, 0.0_rad}},
        WorldToRoadCoordinateConverterCurved_Data{
            {-5_m, 5_m}, 4_m, 3_m, 3_m, 20.0_m, {-2.0112_m, 5.225_m}, 0.0_rad, {3.0_m, 0.0_m, 0.0_rad}},
        WorldToRoadCoordinateConverterCurved_Data{
            {-5_m, 5_m}, 4_m, 3_m, 5_m, 20.0_m, {-2.0112_m, 5.225_m}, 0.0_rad, {5.0_m, 0.0_m, 0.0_rad}},
        WorldToRoadCoordinateConverterCurved_Data{
            {-5_m, 5_m}, 4_m, 3_m, 3_m, 20.0_m, {-5_m, 3.0_m}, 0.0_rad, {0.0_m, -2.0_m, 0.0_rad}},
        WorldToRoadCoordinateConverterCurved_Data{
            {-5_m, 5_m}, 4_m, 3_m, 5_m, 20.0_m, {-2.3101_m, 7.2021_m}, 0.0_rad, {5.0_m, 2.0_m, 0.0_rad}},
        WorldToRoadCoordinateConverterCurved_Data{
            {-5_m, 5_m}, 4_m, 3_m, 5_m, 20.0_m, {-3.6551_m, 7.1011_m}, 0.0_rad, {2.5_m, 2.0_m, 0.0_rad}},
        WorldToRoadCoordinateConverterCurved_Data{
            {-5_m, 5_m}, 4_m, 3_m, 5_m, 8.0_m, {-2.0698_m, 5.5559_m}, 0.0_rad, {5.0_m, 0.0_m, 0.0_rad}}));
