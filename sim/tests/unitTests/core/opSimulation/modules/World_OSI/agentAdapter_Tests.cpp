/********************************************************************************
 * Copyright (c) 2017-2020 in-tech GmbH
 *               2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2023 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <optional>

#include "AgentAdapter.h"
#include "Localization.h"
#include "fakeCallback.h"
#include "fakeLocalizer.h"
#include "fakeMovingObject.h"
#include "fakeRadio.h"
#include "fakeRoad.h"
#include "fakeWorld.h"
#include "fakeWorldData.h"

using ::testing::_;
using ::testing::AllOf;
using ::testing::DoubleEq;
using ::testing::ElementsAreArray;
using ::testing::Eq;
using ::testing::Ge;
using ::testing::IsNull;
using ::testing::Le;
using ::testing::Lt;
using ::testing::Return;
using ::testing::ReturnRef;

class AgentAdapterTest
{
public:
  explicit AgentAdapterTest(OWL::Implementation::MovingObject &obj,
                            const mantle_api::EntityType type,
                            const mantle_api::VehicleClass classification = mantle_api::VehicleClass::kInvalid)
      : fakeLocalizer(fakeWorldData), fakeWorld(), fakeCallbacks()
  {
    RoadGraph graph = RoadGraph(2);
    graph.added_vertex(0);

    Route route;
    route.root = 0;
    route.target = 0;
    route.roadGraph = graph;

    agentBuildInstructions.spawnParameter.position.x = 0_m;
    agentBuildInstructions.spawnParameter.position.y = 0_m;
    agentBuildInstructions.spawnParameter.orientation.yaw = 0_rad;
    agentBuildInstructions.spawnParameter.velocity = 0_mps;
    agentBuildInstructions.spawnParameter.acceleration = 0_mps_sq;
    agentBuildInstructions.spawnParameter.gear = 1;
    agentBuildInstructions.spawnParameter.route = route;

    ON_CALL(fakeWorldData, AddMovingObject(321)).WillByDefault(ReturnRef(obj));
    agentBuildInstructions.system.sensorParameters = sensorParameter;
    World::Localization::Result localizerResult;
    std::unordered_map<std::string, OWL::Interfaces::Road *> roads = {{"TestRoadId", &fakeRoad}};
    ON_CALL(fakeWorldData, GetRoads()).WillByDefault(ReturnRef(roads));
    ON_CALL(fakeLocalizer, Locate(_, _)).WillByDefault(Return(localizerResult));

    agentAdapter = std::make_shared<AgentAdapter>(obj, fakeWorld, fakeCallbacks, fakeLocalizer);

    switch (type)
    {
      case mantle_api::EntityType::kOther:
        agentBuildInstructions.entityProperties = returnOther();
        break;
      case mantle_api::EntityType::kPedestrian:
        agentBuildInstructions.entityProperties = returnPedestrian();
        break;
      case mantle_api::EntityType::kAnimal:
        agentBuildInstructions.entityProperties = returnAnimal();
        break;
      case mantle_api::EntityType::kStatic:
        agentBuildInstructions.entityProperties = returnStaticObject();
        break;
      case mantle_api::EntityType::kVehicle:
        switch (classification)
        {
          case mantle_api::VehicleClass::kMedium_car:
            agentBuildInstructions.entityProperties = returnCar();
            break;
          case mantle_api::VehicleClass::kMotorbike:
            agentBuildInstructions.entityProperties = returnMotorbike();
            break;
          case mantle_api::VehicleClass::kBicycle:
            agentBuildInstructions.entityProperties = returnBicycle();
            break;
          case mantle_api::VehicleClass::kHeavy_truck:
            agentBuildInstructions.entityProperties = returnHeavyTruck();
            break;
        }
        break;
    }

    agentAdapter->InitParameter(agentBuildInstructions);
  }

  static std::shared_ptr<mantle_api::EntityProperties> returnOther()
  {
    auto defaultOther = std::make_shared<mantle_api::EntityProperties>();
    defaultOther->type = mantle_api::EntityType::kOther;
    return defaultOther;
  }

  static std::shared_ptr<mantle_api::VehicleProperties> returnBicycle()
  {
    auto params = std::make_shared<mantle_api::VehicleProperties>();
    params = returnMotorbike();
    params->type = mantle_api::EntityType::kVehicle;
    params->classification = mantle_api::VehicleClass::kBicycle;
    return params;
  }

  static std::shared_ptr<mantle_api::VehicleProperties> returnHeavyTruck()
  {
    auto params = std::make_shared<mantle_api::VehicleProperties>();
    params = returnCar();
    params->type = mantle_api::EntityType::kVehicle;
    params->classification = mantle_api::VehicleClass::kHeavy_truck;
    return params;
  }

  static std::shared_ptr<mantle_api::EntityProperties> returnPedestrian()
  {
    auto defaultPedestrian = std::make_shared<mantle_api::EntityProperties>();
    defaultPedestrian->type = mantle_api::EntityType::kPedestrian;
    return defaultPedestrian;
  }

  static std::shared_ptr<mantle_api::EntityProperties> returnAnimal()
  {
    auto defaultAnimal = std::make_shared<mantle_api::EntityProperties>();
    defaultAnimal->type = mantle_api::EntityType::kAnimal;
    return defaultAnimal;
  }

  static std::shared_ptr<mantle_api::EntityProperties> returnStaticObject()
  {
    auto defaultStaticObject = std::make_shared<mantle_api::EntityProperties>();
    defaultStaticObject->type = mantle_api::EntityType::kStatic;
    return defaultStaticObject;
  }

  static std::shared_ptr<mantle_api::VehicleProperties> returnCar()
  {
    auto defaultCar = std::make_shared<mantle_api::VehicleProperties>();
    defaultCar->type = mantle_api::EntityType::kVehicle;
    defaultCar->classification = mantle_api::VehicleClass::kMedium_car;
    defaultCar->front_axle.bb_center_to_axle_center.x = 0.3_m;
    defaultCar->front_axle.bb_center_to_axle_center.z = 0.1_m;
    defaultCar->front_axle.wheel_diameter = 2.0_m;
    defaultCar->front_axle.track_width = 2.0_m;
    defaultCar->rear_axle.track_width = 2.0_m;
    defaultCar->rear_axle.wheel_diameter = 2.0_m;
    defaultCar->rear_axle.bb_center_to_axle_center.x = -0.3_m;
    defaultCar->rear_axle.bb_center_to_axle_center.z = 0.1_m;
    defaultCar->bounding_box.geometric_center.x = defaultCar->bounding_box.geometric_center.y = 0_m;
    defaultCar->bounding_box.geometric_center.z = 0.2_m;
    defaultCar->bounding_box.dimension.height = 0.5_m;
    defaultCar->bounding_box.dimension.width = 0.5_m;
    defaultCar->bounding_box.dimension.length = 0.5_m;
    defaultCar->properties.emplace("SteeringRatio", "10.0");

    return defaultCar;
  }

  static std::shared_ptr<mantle_api::VehicleProperties> returnMotorbike()
  {
    auto defaultBicycle = std::make_shared<mantle_api::VehicleProperties>();
    defaultBicycle->type = mantle_api::EntityType::kVehicle;
    defaultBicycle->classification = mantle_api::VehicleClass::kMotorbike;
    defaultBicycle->front_axle.bb_center_to_axle_center.x = 0.3_m;
    defaultBicycle->front_axle.bb_center_to_axle_center.z = 0.1_m;
    defaultBicycle->rear_axle.bb_center_to_axle_center.x = -0.3_m;
    defaultBicycle->rear_axle.bb_center_to_axle_center.z = 0.1_m;
    defaultBicycle->front_axle.wheel_diameter = 1.5_m;
    defaultBicycle->rear_axle.wheel_diameter = 1.5_m;
    defaultBicycle->front_axle.track_width = 2.0_m;
    defaultBicycle->rear_axle.track_width = 2.0_m;
    defaultBicycle->bounding_box.geometric_center.x = defaultBicycle->bounding_box.geometric_center.y = 0_m;
    defaultBicycle->bounding_box.geometric_center.z = 0.2_m;
    defaultBicycle->bounding_box.dimension.height = 0.5_m;
    defaultBicycle->bounding_box.dimension.width = 0.25_m;
    defaultBicycle->bounding_box.dimension.length = 0.5_m;
    defaultBicycle->properties.emplace("SteeringRatio", "5.0");

    return defaultBicycle;
  }

  void SetWheelRotation(units::angle::radian_t angle,
                        units::velocity::meters_per_second_t velocity,
                        units::acceleration::meters_per_second_squared_t acceleration)
  {
    agentAdapter->SetWheelsRotationRateAndOrientation(angle, velocity, acceleration);
  }

  void SetWheelRotation(int axleIndex, int wheelIndex, double rotationRate)
  {
    agentAdapter->SetWheelRotationRate(axleIndex, wheelIndex, rotationRate);
  }

  double GetWheelRotation(int axleIndex, int wheelIndex)
  {
    return agentAdapter->GetWheelRotationRate(axleIndex, wheelIndex);
  }

  std::shared_ptr<AgentAdapter> agentAdapter;

private:
  testing::NiceMock<OWL::Fakes::WorldData> fakeWorldData;
  WorldInterface *fakeWorld;
  const CallbackInterface *fakeCallbacks;
  testing::NiceMock<OWL::Fakes::Road> fakeRoad;

  const testing::NiceMock<World::Localization::FakeLocalizer> fakeLocalizer;
  openpass::sensors::Parameters sensorParameter;
  AgentBuildInstructions agentBuildInstructions;
};

TEST(DISABLED_AgentAdapter_Test, GenerateCarWheels)
{
  osi3::MovingObject osi_car;
  osi3::MovingObject *osi_p = &osi_car;
  OWL::Implementation::MovingObject movingObject(&osi_car);
  AgentAdapterTest cartest(movingObject, mantle_api::EntityType::kVehicle, mantle_api::VehicleClass::kMedium_car);

  std::shared_ptr<const mantle_api::VehicleProperties> input_car = AgentAdapterTest::returnCar();

  ASSERT_FLOAT_EQ(
      input_car->front_axle.bb_center_to_axle_center.x.value() - input_car->bounding_box.geometric_center.x.value(),
      osi_car.vehicle_attributes().bbcenter_to_front().x());
  ASSERT_FLOAT_EQ(0, osi_car.vehicle_attributes().bbcenter_to_front().y());
  ASSERT_FLOAT_EQ(
      input_car->front_axle.bb_center_to_axle_center.z.value() - input_car->bounding_box.geometric_center.z.value(),
      osi_car.vehicle_attributes().bbcenter_to_front().z());

  ASSERT_EQ(osi_car.type(), osi3::MovingObject_Type_TYPE_VEHICLE);
  ASSERT_EQ(osi_car.vehicle_attributes().wheel_data_size(), 4);
  ASSERT_EQ(osi_car.vehicle_attributes().number_wheels(), 4);

  OWL::WheelData w = movingObject.GetWheelData(0, 0).value();

  ASSERT_EQ(w.index, 0);
  ASSERT_EQ(movingObject.GetWheelData(0, 0)->axle, 0);
  ASSERT_EQ(movingObject.GetWheelData(0, 1)->index, 1);
  ASSERT_EQ(movingObject.GetWheelData(0, 1)->axle, 0);
  ASSERT_EQ(movingObject.GetWheelData(1, 0)->index, 0);
  ASSERT_EQ(movingObject.GetWheelData(1, 0)->axle, 1);
  ASSERT_EQ(movingObject.GetWheelData(1, 1)->index, 1);
  ASSERT_EQ(movingObject.GetWheelData(1, 1)->axle, 1);

  ASSERT_EQ(movingObject.GetWheelData(0, 2), std::nullopt);
  ASSERT_EQ(movingObject.GetWheelData(0, 2), std::nullopt);

  ASSERT_FLOAT_EQ(movingObject.GetWheelData(0, 0)->wheelRadius.value(),
                  input_car->front_axle.wheel_diameter.value() / 2.0);
  ASSERT_FLOAT_EQ(movingObject.GetWheelData(0, 1)->wheelRadius.value(),
                  input_car->front_axle.wheel_diameter.value() / 2.0);
  ASSERT_FLOAT_EQ(movingObject.GetWheelData(1, 0)->wheelRadius.value(),
                  input_car->rear_axle.wheel_diameter.value() / 2.0);
  ASSERT_FLOAT_EQ(movingObject.GetWheelData(1, 1)->wheelRadius.value(),
                  input_car->rear_axle.wheel_diameter.value() / 2.0);

  ASSERT_FLOAT_EQ(
      movingObject.GetWheelData(0, 0)->position.x.value(),
      input_car->front_axle.bb_center_to_axle_center.x.value() - input_car->bounding_box.geometric_center.x.value());
  ASSERT_FLOAT_EQ(osi_car.vehicle_attributes().bbcenter_to_front().x(),
                  movingObject.GetWheelData(0, 0)->position.x.value());
  ASSERT_FLOAT_EQ(
      movingObject.GetWheelData(0, 1)->position.x.value(),
      input_car->front_axle.bb_center_to_axle_center.x.value() - input_car->bounding_box.geometric_center.x.value());
  ASSERT_FLOAT_EQ(osi_car.vehicle_attributes().bbcenter_to_front().x(),
                  movingObject.GetWheelData(0, 1)->position.x.value());

  ASSERT_FLOAT_EQ(
      movingObject.GetWheelData(1, 0)->position.x.value(),
      input_car->rear_axle.bb_center_to_axle_center.x.value() - input_car->bounding_box.geometric_center.x.value());
  ASSERT_FLOAT_EQ(osi_car.vehicle_attributes().bbcenter_to_rear().x(),
                  movingObject.GetWheelData(1, 0)->position.x.value());
  ASSERT_FLOAT_EQ(
      movingObject.GetWheelData(1, 1)->position.x.value(),
      input_car->rear_axle.bb_center_to_axle_center.x.value() - input_car->bounding_box.geometric_center.x.value());
  ASSERT_FLOAT_EQ(osi_car.vehicle_attributes().bbcenter_to_rear().x(),
                  movingObject.GetWheelData(1, 1)->position.x.value());

  ASSERT_GE(movingObject.GetWheelData(0, 0)->position.x.value(), 0);
  ASSERT_GE(movingObject.GetWheelData(0, 1)->position.x.value(), 0);
  ASSERT_LE(movingObject.GetWheelData(1, 0)->position.x.value(), 0);
  ASSERT_LE(movingObject.GetWheelData(1, 1)->position.x.value(), 0);

  ASSERT_FLOAT_EQ(
      movingObject.GetWheelData(0, 0)->position.y.value(),
      -(input_car->front_axle.track_width.value() / 2.0f - input_car->bounding_box.geometric_center.y.value()));
  ASSERT_FLOAT_EQ(
      movingObject.GetWheelData(0, 1)->position.y.value(),
      (input_car->front_axle.track_width.value() / 2.0f - input_car->bounding_box.geometric_center.y.value()));
  ASSERT_FLOAT_EQ(
      movingObject.GetWheelData(1, 0)->position.y.value(),
      -(input_car->front_axle.track_width.value() / 2.0f - input_car->bounding_box.geometric_center.y.value()));
  ASSERT_FLOAT_EQ(
      movingObject.GetWheelData(1, 1)->position.y.value(),
      (input_car->front_axle.track_width.value() / 2.0f - input_car->bounding_box.geometric_center.y.value()));

  ASSERT_LE(movingObject.GetWheelData(0, 0)->position.y.value(), 0);
  ASSERT_GE(movingObject.GetWheelData(0, 1)->position.y.value(), 0);
  ASSERT_LE(movingObject.GetWheelData(1, 0)->position.y.value(), 0);
  ASSERT_GE(movingObject.GetWheelData(1, 1)->position.y.value(), 0);

  ASSERT_FLOAT_EQ(movingObject.GetWheelData(0, 0)->position.z.value(),
                  osi_car.vehicle_attributes().bbcenter_to_front().z());
  ASSERT_FLOAT_EQ(movingObject.GetWheelData(0, 1)->position.z.value(),
                  osi_car.vehicle_attributes().bbcenter_to_front().z());
  ASSERT_FLOAT_EQ(movingObject.GetWheelData(1, 0)->position.z.value(),
                  osi_car.vehicle_attributes().bbcenter_to_rear().z());
  ASSERT_FLOAT_EQ(movingObject.GetWheelData(1, 1)->position.z.value(),
                  osi_car.vehicle_attributes().bbcenter_to_rear().z());

  cartest.SetWheelRotation(0.3_rad, 2.0_mps, 0.41_mps_sq);

  auto rotation_rate_front = 2.0 / (input_car->front_axle.wheel_diameter.value() / 2.0);
  auto rotation_rate_rear = 2.0 / (input_car->rear_axle.wheel_diameter.value() / 2.0);
  auto dTime = (2.0 - 0.0) / 0.41;
  auto angle = rotation_rate_front * dTime;

  ASSERT_FLOAT_EQ(movingObject.GetWheelData(0, 0)->rotation_rate.value(), rotation_rate_front);
  ASSERT_FLOAT_EQ(movingObject.GetWheelData(0, 1)->rotation_rate.value(), rotation_rate_front);
  ASSERT_FLOAT_EQ(movingObject.GetWheelData(1, 0)->rotation_rate.value(), rotation_rate_rear);
  ASSERT_FLOAT_EQ(movingObject.GetWheelData(1, 1)->rotation_rate.value(), rotation_rate_rear);

  ASSERT_FLOAT_EQ(movingObject.GetWheelData(0, 0)->orientation.pitch.value(),
                  CommonHelper::SetAngleToValidRange(units::angle::radian_t{angle}).value());
  ASSERT_FLOAT_EQ(movingObject.GetWheelData(0, 1)->orientation.pitch.value(),
                  CommonHelper::SetAngleToValidRange(units::angle::radian_t{angle}).value());
  ASSERT_FLOAT_EQ(movingObject.GetWheelData(1, 0)->orientation.pitch.value(),
                  CommonHelper::SetAngleToValidRange(units::angle::radian_t{angle}).value());
  ASSERT_FLOAT_EQ(movingObject.GetWheelData(1, 1)->orientation.pitch.value(),
                  CommonHelper::SetAngleToValidRange(units::angle::radian_t{angle}).value());

  ASSERT_FLOAT_EQ(movingObject.GetWheelData(0, 0)->orientation.roll.value(), 0.0);
  ASSERT_FLOAT_EQ(movingObject.GetWheelData(0, 1)->orientation.roll.value(), 0.0);
  ASSERT_FLOAT_EQ(movingObject.GetWheelData(1, 0)->orientation.roll.value(), 0.0);
  ASSERT_FLOAT_EQ(movingObject.GetWheelData(1, 1)->orientation.roll.value(), 0.0);

  ASSERT_FLOAT_EQ(movingObject.GetWheelData(0, 0)->orientation.yaw.value(),
                  0.3 * std::stod(input_car->properties.at("SteeringRatio")));
  ASSERT_FLOAT_EQ(movingObject.GetWheelData(0, 1)->orientation.yaw.value(),
                  0.3 * std::stod(input_car->properties.at("SteeringRatio")));
  ASSERT_FLOAT_EQ(movingObject.GetWheelData(1, 0)->orientation.yaw.value(), 0.0);
  ASSERT_FLOAT_EQ(movingObject.GetWheelData(1, 1)->orientation.yaw.value(), 0.0);

  rotation_rate_front += 1.0;
  rotation_rate_rear += 2.0;

  cartest.SetWheelRotation(0, 0, rotation_rate_front);
  cartest.SetWheelRotation(1, 0, rotation_rate_rear);

  ASSERT_FLOAT_EQ(cartest.GetWheelRotation(0, 0), rotation_rate_front);
}

TEST(DISABLED_AgentAdapter_Test, GenerateMotorbikeWheels)
{
  osi3::MovingObject osi_bike;
  OWL::Implementation::MovingObject movingObject(&osi_bike);
  AgentAdapterTest biketest(movingObject, mantle_api::EntityType::kVehicle, mantle_api::VehicleClass::kMotorbike);

  std::shared_ptr<const mantle_api::VehicleProperties> input_bike = AgentAdapterTest::returnMotorbike();

  ASSERT_FLOAT_EQ(
      input_bike->front_axle.bb_center_to_axle_center.x.value() - input_bike->bounding_box.geometric_center.x.value(),
      osi_bike.vehicle_attributes().bbcenter_to_front().x());
  ASSERT_FLOAT_EQ(0, osi_bike.vehicle_attributes().bbcenter_to_front().y());
  ASSERT_FLOAT_EQ(
      input_bike->front_axle.bb_center_to_axle_center.z.value() - input_bike->bounding_box.geometric_center.z.value(),
      osi_bike.vehicle_attributes().bbcenter_to_front().z());

  ASSERT_EQ(osi_bike.type(), osi3::MovingObject_Type_TYPE_VEHICLE);
  ASSERT_EQ(osi_bike.vehicle_attributes().wheel_data_size(), 2);
  ASSERT_EQ(osi_bike.vehicle_attributes().number_wheels(), 2);

  ASSERT_EQ(movingObject.GetWheelData(0, 1), std::nullopt);
  ASSERT_EQ(movingObject.GetWheelData(1, 1), std::nullopt);

  ASSERT_EQ(movingObject.GetWheelData(0, 0)->index, 0);
  ASSERT_EQ(movingObject.GetWheelData(0, 0)->axle, 0);
  ASSERT_EQ(movingObject.GetWheelData(1, 0)->index, 0);
  ASSERT_EQ(movingObject.GetWheelData(1, 0)->axle, 1);

  ASSERT_FLOAT_EQ(movingObject.GetWheelData(0, 0)->wheelRadius.value(),
                  input_bike->front_axle.wheel_diameter.value() / 2.0);
  ASSERT_FLOAT_EQ(movingObject.GetWheelData(1, 0)->wheelRadius.value(),
                  input_bike->rear_axle.wheel_diameter.value() / 2.0);

  ASSERT_FLOAT_EQ(
      movingObject.GetWheelData(0, 0)->position.x.value(),
      input_bike->front_axle.bb_center_to_axle_center.x.value() - input_bike->bounding_box.geometric_center.x.value());
  ASSERT_FLOAT_EQ(osi_bike.vehicle_attributes().bbcenter_to_front().x(),
                  movingObject.GetWheelData(0, 0)->position.x.value());

  ASSERT_FLOAT_EQ(
      movingObject.GetWheelData(1, 0)->position.x.value(),
      input_bike->rear_axle.bb_center_to_axle_center.x.value() - input_bike->bounding_box.geometric_center.x.value());
  ASSERT_FLOAT_EQ(osi_bike.vehicle_attributes().bbcenter_to_rear().x(),
                  movingObject.GetWheelData(1, 0)->position.x.value());

  ASSERT_FLOAT_EQ(movingObject.GetWheelData(0, 0)->position.y.value(), 0);
  ASSERT_FLOAT_EQ(movingObject.GetWheelData(1, 0)->position.y.value(), 0);

  ASSERT_FLOAT_EQ(movingObject.GetWheelData(0, 0)->position.z.value(),
                  osi_bike.vehicle_attributes().bbcenter_to_front().z());
  ASSERT_FLOAT_EQ(movingObject.GetWheelData(1, 0)->position.z.value(),
                  osi_bike.vehicle_attributes().bbcenter_to_rear().z());

  biketest.SetWheelRotation(0.3_rad, 2.0_mps, 0.41_mps_sq);

  auto rotation_rate_front = 2.0 / (input_bike->front_axle.wheel_diameter.value() / 2.0);
  auto rotation_rate_rear = 2.0 / (input_bike->rear_axle.wheel_diameter.value() / 2.0);
  auto dTime = (2.0 - 0.0) / 0.41;
  auto angle = rotation_rate_front * dTime;

  ASSERT_FLOAT_EQ(movingObject.GetWheelData(0, 0)->rotation_rate.value(), rotation_rate_front);
  ASSERT_FLOAT_EQ(movingObject.GetWheelData(1, 0)->rotation_rate.value(), rotation_rate_rear);

  ASSERT_FLOAT_EQ(movingObject.GetWheelData(0, 0)->orientation.pitch.value(),
                  CommonHelper::SetAngleToValidRange(units::angle::radian_t{angle}).value());
  ASSERT_FLOAT_EQ(movingObject.GetWheelData(1, 0)->orientation.pitch.value(),
                  CommonHelper::SetAngleToValidRange(units::angle::radian_t{angle}).value());

  ASSERT_FLOAT_EQ(movingObject.GetWheelData(0, 0)->orientation.roll.value(), 0.0);
  ASSERT_FLOAT_EQ(movingObject.GetWheelData(1, 0)->orientation.roll.value(), 0.0);

  ASSERT_FLOAT_EQ(movingObject.GetWheelData(0, 0)->orientation.yaw.value(),
                  0.3 * std::stod(input_bike->properties.at("SteeringRatio")));
  ASSERT_FLOAT_EQ(movingObject.GetWheelData(1, 0)->orientation.yaw.value(), 0.0);
}

TEST(AgentAdapter_Test, GeneratePedestrianWheels)
{
  osi3::MovingObject osi_person;
  OWL::Implementation::MovingObject movingObject(&osi_person);
  AgentAdapterTest pedestrianTest(movingObject, mantle_api::EntityType::kPedestrian);

  ASSERT_EQ(osi_person.type(), osi3::MovingObject_Type_TYPE_PEDESTRIAN);
  ASSERT_EQ(osi_person.vehicle_attributes().wheel_data_size(), 0);
  ASSERT_EQ(osi_person.vehicle_attributes().number_wheels(), 0);

  ASSERT_EQ(movingObject.GetWheelData(0, 0), std::nullopt);

  pedestrianTest.SetWheelRotation(0, 0, 1.0);
  ASSERT_FLOAT_EQ(pedestrianTest.GetWheelRotation(0, 0), 0.0);
}

TEST(AgentAdapter_Test, SetWheelData) {}

TEST(MovingObject_Tests, SetWheelRotationRate)
{
  double rotationrate[4] = {1.0, 2.0, 3.0, 4.0};

  osi3::MovingObject osiObject;
  OWL::Implementation::MovingObject movingObject(&osiObject);
  movingObject.SetType(mantle_api::EntityType::kVehicle);

  OWL::WheelData wheeldata{};
  wheeldata.axle = 0;
  wheeldata.index = 0;
  movingObject.AddWheel(wheeldata);
  wheeldata.index = 1;
  movingObject.AddWheel(wheeldata);
  wheeldata.index = 0;
  wheeldata.axle = 1;
  movingObject.AddWheel(wheeldata);
  wheeldata.index = 1;
  wheeldata.axle = 1;
  movingObject.AddWheel(wheeldata);

  movingObject.SetWheelRotationRate(0, 0, rotationrate[0]);
  movingObject.SetWheelRotationRate(0, 1, rotationrate[1]);
  movingObject.SetWheelRotationRate(1, 0, rotationrate[2]);
  movingObject.SetWheelRotationRate(1, 1, rotationrate[3]);

  std::optional<const OWL::WheelData> FrontWheel = movingObject.GetWheelData(0, 0);
  std::optional<const OWL::WheelData> FrontWheel2 = movingObject.GetWheelData(0, 1);
  std::optional<const OWL::WheelData> RearWheel = movingObject.GetWheelData(1, 0);
  std::optional<const OWL::WheelData> RearWheel2 = movingObject.GetWheelData(1, 1);

  ASSERT_FLOAT_EQ(FrontWheel.value().rotation_rate.value(), rotationrate[0]);
  ASSERT_FLOAT_EQ(RearWheel.value().rotation_rate.value(), rotationrate[2]);
  ASSERT_FLOAT_EQ(FrontWheel2.value().rotation_rate.value(), rotationrate[1]);
  ASSERT_FLOAT_EQ(RearWheel2.value().rotation_rate.value(), rotationrate[3]);
}

TEST(MovingObject_Tests, SetAndGetReferencePointPosition_ReturnsCorrectPosition)
{
  mantle_api::Vec3<units::length::meter_t> position;
  position.x = 100.0_m;
  position.y = 150.0_m;
  position.z = 10.0_m;
  osi3::MovingObject osiObject;
  OWL::Implementation::MovingObject movingObject(&osiObject);
  movingObject.SetLength(8.0_m);
  movingObject.SetBoundingBoxCenterToRear(7.0_m, 0.0_m, 0.0_m);
  movingObject.SetYaw(0.5_rad);
  movingObject.SetReferencePointPosition(position);
  const auto resultPosition = movingObject.GetReferencePointPosition();
  ASSERT_THAT(resultPosition.x.value(), DoubleEq(position.x.value()));
  ASSERT_THAT(resultPosition.y.value(), DoubleEq(position.y.value()));
  ASSERT_THAT(resultPosition.z.value(), DoubleEq(position.z.value()));
}

TEST(MovingObject_Tests, SetAndGetReferencePointPositionWithYawChangeInBetween_ReturnsCorrectPosition)
{
  mantle_api::Vec3<units::length::meter_t> position;
  position.x = 100.0_m;
  position.y = 150.0_m;
  position.z = 10.0_m;
  osi3::MovingObject osiObject;
  OWL::Implementation::MovingObject movingObject(&osiObject);
  movingObject.SetLength(8.0_m);
  movingObject.SetBoundingBoxCenterToRear(7.0_m, 0.0_m, 0.0_m);
  movingObject.SetYaw(0.5_rad);
  movingObject.SetReferencePointPosition(position);
  movingObject.SetYaw(0.7_rad);
  auto resultPosition = movingObject.GetReferencePointPosition();
  ASSERT_THAT(resultPosition.x.value(), DoubleEq(position.x.value()));
  ASSERT_THAT(resultPosition.y.value(), DoubleEq(position.y.value()));
  ASSERT_THAT(resultPosition.z.value(), DoubleEq(position.z.value()));
}

TEST(MovingObject_Tests, SetReferencePointPosition_SetsCorrectPositionOnOSIObject)
{
  mantle_api::Vec3<units::length::meter_t> position;
  position.x = 100.0_m;
  position.y = 150.0_m;
  position.z = 10.0_m;
  osi3::MovingObject osiObject;
  OWL::Implementation::MovingObject movingObject(&osiObject);
  movingObject.SetLength(8.0_m);
  movingObject.SetBoundingBoxCenterToRear(-2.0_m, 0.0_m, 0.0_m);
  movingObject.SetYaw(M_PI * 0.25_rad);
  movingObject.SetReferencePointPosition(position);
  auto resultPosition = osiObject.base().position();
  ASSERT_THAT(resultPosition.x(), DoubleEq(position.x.value() + std::sqrt(2)));
  ASSERT_THAT(resultPosition.y(), DoubleEq(position.y.value() + std::sqrt(2)));
  ASSERT_THAT(resultPosition.z(), DoubleEq(position.z.value()));
}

TEST(MovingObject_Tests, SetWheelsRotationRateAndOrientation)
{
  units::length::meter_t wheelDiameter = 1.0_m;

  osi3::MovingObject osiObject;
  OWL::Implementation::MovingObject movingObject(&osiObject);
  movingObject.SetType(mantle_api::EntityType::kVehicle);

  OWL::WheelData wheeldata{};
  wheeldata.wheelRadius = wheelDiameter / 2.0;
  wheeldata.axle = 0;
  wheeldata.index = 0;

  movingObject.AddWheel(wheeldata);
  wheeldata.index = 1;
  movingObject.AddWheel(wheeldata);
  wheeldata.index = 0;
  wheeldata.axle = 1;
  movingObject.AddWheel(wheeldata);
  wheeldata.index = 1;
  wheeldata.axle = 1;
  movingObject.AddWheel(wheeldata);

  movingObject.SetWheelsRotationRateAndOrientation(0.1_mps, wheelDiameter / 2.0, wheelDiameter / 2.0, 2.0_s);
  movingObject.SetFrontAxleSteeringYaw(0.4_rad);

  auto rotationrate = 0.1 / (wheelDiameter / 2.0);

  std::optional<const OWL::WheelData> FrontWheel = movingObject.GetWheelData(0, 0);
  std::optional<const OWL::WheelData> FrontWheel2 = movingObject.GetWheelData(0, 1);
  std::optional<const OWL::WheelData> RearWheel = movingObject.GetWheelData(1, 0);
  std::optional<const OWL::WheelData> RearWheel2 = movingObject.GetWheelData(1, 1);

  ASSERT_FLOAT_EQ(FrontWheel.value().orientation.yaw.value(), 0.4);
  ASSERT_FLOAT_EQ(RearWheel.value().orientation.yaw.value(), 0.0);
  ASSERT_FLOAT_EQ(FrontWheel2.value().orientation.yaw.value(), 0.4);
  ASSERT_FLOAT_EQ(RearWheel2.value().orientation.yaw.value(), 0.0);

  ASSERT_FLOAT_EQ(FrontWheel.value().orientation.pitch.value(), 0.4);
  ASSERT_FLOAT_EQ(RearWheel.value().orientation.pitch.value(), 0.4);
  ASSERT_FLOAT_EQ(FrontWheel2.value().orientation.pitch.value(), 0.4);
  ASSERT_FLOAT_EQ(RearWheel2.value().orientation.pitch.value(), 0.4);

  ASSERT_FLOAT_EQ(FrontWheel.value().orientation.roll.value(), 0.0);
  ASSERT_FLOAT_EQ(RearWheel.value().orientation.roll.value(), 0.0);
  ASSERT_FLOAT_EQ(FrontWheel2.value().orientation.roll.value(), 0.0);
  ASSERT_FLOAT_EQ(RearWheel2.value().orientation.roll.value(), 0.0);

  ASSERT_FLOAT_EQ(FrontWheel.value().rotation_rate.value(), rotationrate.value());
  ASSERT_FLOAT_EQ(RearWheel.value().rotation_rate.value(), rotationrate.value());
  ASSERT_FLOAT_EQ(FrontWheel2.value().rotation_rate.value(), rotationrate.value());
  ASSERT_FLOAT_EQ(RearWheel2.value().rotation_rate.value(), rotationrate.value());
}

TEST(MovingObject_Tests, SetAgentType_MapsCorrectOSIType)
{
  osi3::MovingObject osiObject;
  OWL::Implementation::MovingObject movingObject(&osiObject);

  const std::vector<std::pair<mantle_api::VehicleClass, osi3::MovingObject_VehicleClassification_Type>>
      expectedVehicleTypes
      = {{mantle_api::VehicleClass::kMedium_car,
          osi3::MovingObject_VehicleClassification_Type::MovingObject_VehicleClassification_Type_TYPE_MEDIUM_CAR},
         {mantle_api::VehicleClass::kMotorbike,
          osi3::MovingObject_VehicleClassification_Type::MovingObject_VehicleClassification_Type_TYPE_MOTORBIKE},
         {mantle_api::VehicleClass::kBicycle,
          osi3::MovingObject_VehicleClassification_Type::MovingObject_VehicleClassification_Type_TYPE_BICYCLE},
         {mantle_api::VehicleClass::kHeavy_truck,
          osi3::MovingObject_VehicleClassification_Type::MovingObject_VehicleClassification_Type_TYPE_HEAVY_TRUCK}};

  for (const auto &[agentVehicleType, expectedOsiVehicleType] : expectedVehicleTypes)
  {
    movingObject.SetType(mantle_api::EntityType::kVehicle);
    movingObject.SetVehicleClassification(agentVehicleType);

    ASSERT_THAT(osiObject.type(), osi3::MovingObject_Type::MovingObject_Type_TYPE_VEHICLE);
    ASSERT_THAT(osiObject.vehicle_classification().type(), expectedOsiVehicleType);
  }

  movingObject.SetType(mantle_api::EntityType::kPedestrian);
  ASSERT_THAT(osiObject.type(), osi3::MovingObject_Type::MovingObject_Type_TYPE_PEDESTRIAN);
}

struct CalculateBoundingBoxData
{
  units::angle::radian_t yaw;
  units::angle::radian_t roll;
  std::vector<std::pair<double, double>> expectedResult;
};

class CalculateBoundingBox_Tests : public testing::Test, public ::testing::WithParamInterface<CalculateBoundingBoxData>
{
};

class TestWorldObject : public WorldObjectAdapter
{
public:
  TestWorldObject(OWL::Interfaces::WorldObject &baseTrafficObject) : WorldObjectAdapter(baseTrafficObject) {}

  virtual units::length::meter_t GetLaneRemainder(const std::string &roadId, Side) const {};
  virtual ObjectTypeOSI GetType() const {};
  virtual const RoadIntervals &GetTouchedRoads() const {};
  virtual Common::Vector2d<units::length::meter_t> GetAbsolutePosition(const ObjectPoint &objectPoint) const
  {
    return {0_m, 0_m};
  }
  virtual const GlobalRoadPositions &GetRoadPosition(const ObjectPoint &point) const { return {}; }
  virtual Common::Vector2d<units::velocity::meters_per_second_t> GetVelocity(ObjectPoint point) const
  {
    return {0_mps, 0_mps};
  }
  virtual Common::Vector2d<units::acceleration::meters_per_second_squared_t> GetAcceleration(ObjectPoint point) const
  {
    return {0_mps_sq, 0_mps_sq};
  }
  virtual bool Locate() { return false; }
  virtual void Unlocate(){};
};

TEST_P(CalculateBoundingBox_Tests, CalculateBoundingBox_ReturnCorrectPoints)
{
  const auto &data = GetParam();
  OWL::Fakes::MovingObject movingObject;
  mantle_api::Vec3<units::length::meter_t> position{10_m, 20_m, 0_m};
  ON_CALL(movingObject, GetReferencePointPosition()).WillByDefault(Return(position));
  mantle_api::Dimension3 dimension{6.0_m, 2.0_m, 1.6_m};
  ON_CALL(movingObject, GetDimension()).WillByDefault(Return(dimension));
  mantle_api::Orientation3<units::angle::radian_t> orientation{data.yaw, 0_rad, data.roll};
  ON_CALL(movingObject, GetAbsOrientation()).WillByDefault(Return(orientation));

  TestWorldObject object(movingObject);

  auto result = object.GetBoundingBox2D();

  std::vector<std::pair<double, double>> resultPoints;
  for (const point_t point : result.outer())
  {
    resultPoints.emplace_back(bg::get<0>(point), bg::get<1>(point));
  }
  resultPoints.pop_back();  //in boost the last point is equal to the first

  ASSERT_THAT(resultPoints, ElementsAreArray(data.expectedResult));
}

INSTANTIATE_TEST_SUITE_P(
    BoundingBoxTest,
    CalculateBoundingBox_Tests,
    testing::Values(
        //! yaw     roll   expectedResult
        CalculateBoundingBoxData{0.0_rad, 0.0_rad, {{7.0, 19.0}, {7.0, 21.0}, {13.0, 21.0}, {13.0, 19.0}}},
        CalculateBoundingBoxData{
            units::angle::radian_t(M_PI_2), 0.0_rad, {{11.0, 17.0}, {9.0, 17.0}, {9.0, 23.0}, {11.0, 23.0}}},
        CalculateBoundingBoxData{0.0_rad,
                                 units::angle::radian_t(M_PI_4),
                                 {{7.0, 20 - 2.6 * M_SQRT1_2},
                                  {7.0, 20.0 + M_SQRT1_2},
                                  {13.0, 20.0 + M_SQRT1_2},
                                  {13.0, 20.0 - 2.6 * M_SQRT1_2}}},
        CalculateBoundingBoxData{0.0_rad,
                                 units::angle::radian_t(-M_PI_4),
                                 {{7.0, 20 - M_SQRT1_2},
                                  {7.0, 20.0 + 2.6 * M_SQRT1_2},
                                  {13.0, 20.0 + 2.6 * M_SQRT1_2},
                                  {13.0, 20.0 - M_SQRT1_2}}}));
