/********************************************************************************
 * Copyright (c) 2019 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2019 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <cmath>

#include "common/commonTools.h"
#include "common/gtest/dontCare.h"
#include "fakeAgent.h"
#include "fakeParameter.h"
#include "fakeRadio.h"
#include "fakeWorld.h"
#include "fakeWorldObject.h"

using ::testing::_;
using ::testing::DoubleNear;
using ::testing::NiceMock;
using ::testing::Return;
using ::testing::TestWithParam;
using ::testing::Values;

using namespace units::literals;

struct TtcCalculationTest_Data
{
  units::length::meter_t egoX;
  units::length::meter_t egoY;
  units::velocity::meters_per_second_t egoVelocityX;
  units::velocity::meters_per_second_t egoVelocityY;
  units::acceleration::meters_per_second_squared_t egoAcceleration;
  units::angle::radian_t egoYaw;
  units::angular_velocity::radians_per_second_t egoYawRate;
  units::angular_acceleration::radians_per_second_squared_t egoYawAcceleration;
  units::length::meter_t opponentX;
  units::length::meter_t opponentY;
  units::velocity::meters_per_second_t opponentVelocityX;
  units::velocity::meters_per_second_t opponentVelocityY;
  units::acceleration::meters_per_second_squared_t opponentAcceleration;
  units::angle::radian_t opponentYaw;
  units::angular_velocity::radians_per_second_t opponentYawRate;
  units::angular_acceleration::radians_per_second_squared_t opponentYawAcceleration;
  units::length::meter_t longitudinalBoundary;
  units::length::meter_t lateralBoundary;

  units::time::second_t expectedTtc;
};

class TtcCalcualtionTest : public ::TestWithParam<TtcCalculationTest_Data>
{
};

TEST_P(TtcCalcualtionTest, CalculateObjectTTC_ReturnsCorrectTtc)
{
  auto data = GetParam();
  double fakeCycleTime = 10;
  units::time::second_t fakeMaxTtc = 10.0_s;

  NiceMock<FakeAgent> ego;
  ON_CALL(ego, GetPositionX()).WillByDefault(Return(data.egoX));
  ON_CALL(ego, GetPositionY()).WillByDefault(Return(data.egoY));
  ON_CALL(ego, GetYaw()).WillByDefault(Return(data.egoYaw));
  ON_CALL(ego, GetRoll()).WillByDefault(Return(0.0_rad));
  ON_CALL(ego, GetYawRate()).WillByDefault(Return(data.egoYawRate));
  ON_CALL(ego, GetYawAcceleration()).WillByDefault(Return(data.egoYawAcceleration));
  ON_CALL(ego, GetVelocity(_)).WillByDefault(Return(Common::Vector2d{data.egoVelocityX, data.egoVelocityY}));
  ON_CALL(ego, GetAcceleration(_))
      .WillByDefault(Return(Common::Vector2d{data.egoAcceleration * units::math::cos(data.egoYaw),
                                             data.egoAcceleration * units::math::sin(data.egoYaw)}));
  ON_CALL(ego, GetLength()).WillByDefault(Return(2.0_m));
  ON_CALL(ego, GetWidth()).WillByDefault(Return(1.0_m));
  ON_CALL(ego, GetDistanceReferencePointToLeadingEdge()).WillByDefault(Return(1.0_m));

  NiceMock<FakeAgent> opponent;
  ON_CALL(opponent, GetPositionX()).WillByDefault(Return(data.opponentX));
  ON_CALL(opponent, GetPositionY()).WillByDefault(Return(data.opponentY));
  ON_CALL(opponent, GetYaw()).WillByDefault(Return(data.opponentYaw));
  ON_CALL(opponent, GetRoll()).WillByDefault(Return(0.0_rad));
  ON_CALL(opponent, GetYawRate()).WillByDefault(Return(data.opponentYawRate));
  ON_CALL(opponent, GetYawAcceleration()).WillByDefault(Return(data.opponentYawAcceleration));
  ON_CALL(opponent, GetVelocity(_))
      .WillByDefault(Return(Common::Vector2d{data.opponentVelocityX, data.opponentVelocityY}));
  ON_CALL(opponent, GetAcceleration(_))
      .WillByDefault(Return(Common::Vector2d{data.opponentAcceleration * units::math::cos(data.opponentYaw),
                                             data.opponentAcceleration * units::math::sin(data.opponentYaw)}));
  ON_CALL(opponent, GetLength()).WillByDefault(Return(2.0_m));
  ON_CALL(opponent, GetWidth()).WillByDefault(Return(1.0_m));
  ON_CALL(opponent, GetDistanceReferencePointToLeadingEdge()).WillByDefault(Return(1.0_m));

  auto result = TtcCalculations::CalculateObjectTTC(
      ego, opponent, fakeMaxTtc, data.longitudinalBoundary, data.lateralBoundary, fakeCycleTime);
  ASSERT_THAT(result.value(), DoubleNear(data.expectedTtc.value(), 0.001));
}

INSTANTIATE_TEST_CASE_P(
    TtcCalculationTestCase,
    TtcCalcualtionTest,
    ::testing::Values(
        // Ego opponent
        //       x,       y,      v_x,     v_y,          a,     yaw,       yawRate,           yawAcc,     x,         y,
        //       v_x,      v_y,          a,     yaw,        yawRate,           yawAcc,   long,   lat,         ttc
        TtcCalculationTest_Data{-20.0_m,
                                0.0_m,
                                10.0_mps,
                                0.0_mps,
                                0.0_mps_sq,
                                0.0_rad,
                                0.0_rad_per_s,
                                0.0_rad_per_s_sq,
                                0.0_m,
                                0.0_m,
                                12.0_mps,
                                0.0_mps,
                                0.0_mps_sq,
                                0.0_rad,
                                0.0_rad_per_s,
                                0.0_rad_per_s_sq,
                                0.0_m,
                                0.0_m,
                                units::time::second_t(std::numeric_limits<double>::max())},
        TtcCalculationTest_Data{0.0_m,
                                0.0_m,
                                12.0_mps,
                                0.0_mps,
                                0.0_mps_sq,
                                0.0_rad,
                                0.0_rad_per_s,
                                0.0_rad_per_s_sq,
                                -20.0_m,
                                0.0_m,
                                10.0_mps,
                                0.0_mps,
                                0.0_mps_sq,
                                0.0_rad,
                                0.0_rad_per_s,
                                0.0_rad_per_s_sq,
                                0.0_m,
                                0.0_m,
                                units::time::second_t(std::numeric_limits<double>::max())},
        TtcCalculationTest_Data{0.0_m,
                                10.0_m,
                                10.0_mps,
                                0.0_mps,
                                0.0_mps_sq,
                                0.0_rad,
                                0.0_rad_per_s,
                                0.0_rad_per_s_sq,
                                40.5_m,
                                10.0_m,
                                2.0_mps,
                                0.0_mps,
                                0.0_mps_sq,
                                0.0_rad,
                                0.0_rad_per_s,
                                0.0_rad_per_s_sq,
                                0.0_m,
                                0.0_m,
                                4.82_s},
        TtcCalculationTest_Data{20.0_m,
                                -10.0_m,
                                10.0_mps,
                                0.0_mps,
                                0.0_mps_sq,
                                0.0_rad,
                                0.0_rad_per_s,
                                0.0_rad_per_s_sq,
                                5.0_m,
                                -10.0_m,
                                40.0_mps,
                                0.0_mps,
                                0.0_mps_sq,
                                0.0_rad,
                                0.0_rad_per_s,
                                0.0_rad_per_s_sq,
                                0.0_m,
                                0.0_m,
                                0.44_s},
        TtcCalculationTest_Data{0.0_m,
                                0.0_m,
                                0.0_mps,
                                0.0_mps,
                                0.0_mps_sq,
                                0.0_rad,
                                0.0_rad_per_s,
                                0.0_rad_per_s_sq,
                                0.0_m,
                                14.0_m,
                                0.0_mps,
                                -5.0_mps,
                                0.0_mps_sq,
                                -90_deg,
                                0.0_rad_per_s,
                                0.0_rad_per_s_sq,
                                10.0_m,
                                5.0_m,
                                1.0_s},
        TtcCalculationTest_Data{0.0_m,
                                0.0_m,
                                0.0_mps,
                                0.0_mps,
                                0.0_mps_sq,
                                0.0_rad,
                                0.0_rad_per_s,
                                0.0_rad_per_s_sq,
                                -50.0_m,
                                0.0_m,
                                5.0_mps,
                                0.0_mps,
                                2.0_mps_sq,
                                0.0_rad,
                                0.0_rad_per_s,
                                0.0_rad_per_s_sq,
                                0.0_m,
                                0.0_m,
                                4.87_s},
        TtcCalculationTest_Data{0.0_m,
                                0.0_m,
                                0.0_mps,
                                0.0_mps,
                                0.0_mps_sq,
                                0.0_rad,
                                0.0_rad_per_s,
                                0.0_rad_per_s_sq,
                                -10.0_m,
                                -10.0_m,
                                0.0_mps,
                                10.0_mps,
                                0.0_mps_sq,
                                90_deg,
                                -1.0_rad_per_s,
                                0.0_rad_per_s_sq,
                                0.0_m,
                                0.0_m,
                                1.38_s}));
