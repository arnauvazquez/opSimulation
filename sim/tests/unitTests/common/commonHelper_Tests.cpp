/********************************************************************************
 * Copyright (c) 2020-2021 in-tech GmbH
 *               2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "common/commonTools.h"
#include "fakeRadio.h"
#include "fakeWorld.h"

using ::testing::_;
using ::testing::DoubleNear;
using ::testing::Eq;
using ::testing::Return;
using ::testing::TestWithParam;
using ::testing::UnorderedElementsAreArray;
using ::testing::Values;

struct CartesianNetDistance_Data
{
  polygon_t ownBoundingBox;
  polygon_t otherBoundingBox;
  double expectedNetX;
  double expectedNetY;
};

class CartesianNetDistanceTest : public ::TestWithParam<CartesianNetDistance_Data>
{
};

TEST_P(CartesianNetDistanceTest, GetCartesianNetDistance_ReturnsCorrectDistances)
{
  auto data = GetParam();
  auto [netX, netY] = CommonHelper::GetCartesianNetDistance(data.ownBoundingBox, data.otherBoundingBox);

  ASSERT_THAT(netX, DoubleNear(data.expectedNetX, 1e-3));
  ASSERT_THAT(netY, DoubleNear(data.expectedNetY, 1e-3));
}

INSTANTIATE_TEST_SUITE_P(
    CartesianNetDistanceTestCase,
    CartesianNetDistanceTest,
    ::testing::Values(
        // ownBoundingBox                          otherBoundingBox                                 x y
        CartesianNetDistance_Data{
            polygon_t{{{0, 0}, {1, 0}, {1, 1}, {0, 1}}}, polygon_t{{{1, 0}, {2, 0}, {2, 1}, {1, 1}}}, 0, 0},
        CartesianNetDistance_Data{
            polygon_t{{{-1, 0}, {0, -1}, {1, 0}, {0, 1}}}, polygon_t{{{1, 1}, {2, 1}, {2, 2}, {1, 2}}}, 0, 0},
        CartesianNetDistance_Data{
            polygon_t{{{-1, 0}, {0, -1}, {1, 0}, {0, 1}}}, polygon_t{{{3, 4}, {4, 4}, {4, 6}, {3, 6}}}, 2, 3},
        CartesianNetDistance_Data{polygon_t{{{-1, 0}, {0, -1}, {1, 0}, {0, 1}}},
                                  polygon_t{{{-10, -10}, {-8, -10}, {-8, -9}, {-10, -9}}},
                                  -7,
                                  -8}));

struct GetRoadWithLowestHeading_Data
{
  const GlobalRoadPositions roadPositions;
  RouteElement expectedResult;
};

class GetRoadWithLowestHeading_Test : public ::TestWithParam<GetRoadWithLowestHeading_Data>
{
};

TEST_P(GetRoadWithLowestHeading_Test, GetRoadWithLowestHeading)
{
  auto data = GetParam();
  FakeWorld world;
  ON_CALL(world, IsDirectionalRoadExisting(_, _)).WillByDefault(Return(true));
  auto result = CommonHelper::GetRoadWithLowestHeading(data.roadPositions, world);

  ASSERT_THAT(result, Eq(data.expectedResult));
}

INSTANTIATE_TEST_SUITE_P(
    GetRoadWithLowestHeadingTestCase,
    GetRoadWithLowestHeading_Test,
    ::testing::Values(
        // roadPositions                                                        expectedResult
        GetRoadWithLowestHeading_Data{{{"RoadA", {"RoadA", -1, 0_m, 0_m, 0.1_rad}}}, {"RoadA", true}},
        GetRoadWithLowestHeading_Data{{{"RoadA", {"RoadA", -1, 0_m, 0_m, 0.1_rad + units::angle::radian_t(M_PI)}}},
                                      {"RoadA", false}},
        GetRoadWithLowestHeading_Data{
            {{"RoadA", {"RoadA", -1, 0_m, 0_m, -0.1_rad}}, {"RoadB", {"RoadB", -1, 0_m, 0_m, 0.2_rad}}},
            {"RoadA", true}},
        GetRoadWithLowestHeading_Data{
            {{"RoadA", {"RoadA", -1, 0_m, 0_m, -0.2_rad}}, {"RoadB", {"RoadB", -1, 0_m, 0_m, 0.1_rad}}},
            {"RoadB", true}},
        GetRoadWithLowestHeading_Data{{{"RoadA", {"RoadA", -1, 0_m, 0_m, -0.1_rad + units::angle::radian_t(M_PI)}},
                                       {"RoadB", {"RoadB", -1, 0_m, 0_m, 0.2_rad}}},
                                      {"RoadA", false}},
        GetRoadWithLowestHeading_Data{{{"RoadA", {"RoadA", -1, 0_m, 0_m, -0.2_rad}},
                                       {"RoadB", {"RoadB", -1, 0_m, 0_m, units::angle::radian_t(M_PI) + 0.1_rad}}},
                                      {"RoadB", false}}));

class SetAngleToValidRange_Data
{
public:
  // do not change order of items
  // unless you also change it in INSTANTIATE_TEST_CASE_P
  units::angle::radian_t angle;
  units::angle::radian_t normalizedAngle;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const SetAngleToValidRange_Data& data)
  {
    return os << "angle: " << data.angle << ", normalizedAngle: " << data.normalizedAngle;
  }
};

class SetAngleToValidRange : public ::testing::TestWithParam<SetAngleToValidRange_Data>
{
};

TEST_P(SetAngleToValidRange, ReturnsAngleWithinPlusMinusPi)
{
  auto data = GetParam();

  auto normalizedAngle = CommonHelper::SetAngleToValidRange(data.angle);

  ASSERT_THAT(normalizedAngle.value(), DoubleNear(data.normalizedAngle.value(), 1e-3));
}

INSTANTIATE_TEST_SUITE_P(
    AngleList,
    SetAngleToValidRange,
    /*                             angle   expectedNormalizedAngle  */
    testing::Values(
        SetAngleToValidRange_Data{units::angle::radian_t(0.0), units::angle::radian_t(0.0)},
        SetAngleToValidRange_Data{units::angle::radian_t(M_PI_4), units::angle::radian_t(M_PI_4)},
        SetAngleToValidRange_Data{units::angle::radian_t(-M_PI_4), units::angle::radian_t(-M_PI_4)},
        SetAngleToValidRange_Data{units::angle::radian_t(M_PI_2), units::angle::radian_t(M_PI_2)},
        SetAngleToValidRange_Data{units::angle::radian_t(-M_PI_2), units::angle::radian_t(-M_PI_2)},
        SetAngleToValidRange_Data{units::angle::radian_t(3.0 * M_PI_4), units::angle::radian_t(3.0 * M_PI_4)},
        SetAngleToValidRange_Data{units::angle::radian_t(-3.0 * M_PI_4), units::angle::radian_t(-3.0 * M_PI_4)},
        SetAngleToValidRange_Data{units::angle::radian_t(2.0 * M_PI + M_PI_4), units::angle::radian_t(M_PI_4)},
        SetAngleToValidRange_Data{units::angle::radian_t(-2.0 * M_PI - M_PI_4), units::angle::radian_t(-M_PI_4)},
        SetAngleToValidRange_Data{units::angle::radian_t(2.0 * M_PI + M_PI_2), units::angle::radian_t(M_PI_2)},
        SetAngleToValidRange_Data{units::angle::radian_t(-2.0 * M_PI - M_PI_2), units::angle::radian_t(-M_PI_2)},
        SetAngleToValidRange_Data{units::angle::radian_t(4.0 * M_PI + M_PI_2), units::angle::radian_t(M_PI_2)},
        SetAngleToValidRange_Data{units::angle::radian_t(-4.0 * M_PI - M_PI_2), units::angle::radian_t(-M_PI_2)}));

struct GetIntersectionPoints_Data
{
  std::vector<Common::Vector2d<units::length::meter_t>> firstQuadrangle;
  std::vector<Common::Vector2d<units::length::meter_t>> secondQuadrangle;
  bool firstIsRectangular;
  std::vector<Common::Vector2d<units::length::meter_t>> expectedIntersection;
};

class GetIntersectionPoints_Test : public testing::Test,
                                   public ::testing::WithParamInterface<GetIntersectionPoints_Data>
{
};

TEST_P(GetIntersectionPoints_Test, CorrectIntersectionPoints)
{
  auto data = GetParam();
  const auto result = CommonHelper::IntersectionCalculation::GetIntersectionPoints(
      data.firstQuadrangle, data.secondQuadrangle, data.firstIsRectangular);
  EXPECT_THAT(result, UnorderedElementsAreArray(data.expectedIntersection));
}

INSTANTIATE_TEST_CASE_P(
    BothRectangular,
    GetIntersectionPoints_Test,
    testing::Values(
        //Element corners                  Object corners                             Intersection points
        GetIntersectionPoints_Data{{{1_m, 1_m}, {1_m, 3_m}, {3_m, 3_m}, {3_m, 1_m}},
                                   {{2_m, 4_m}, {2_m, 6_m}, {4_m, 6_m}, {4_m, 4_m}},
                                   true,
                                   {}},
        GetIntersectionPoints_Data{{{1_m, 1_m}, {1_m, 3_m}, {3_m, 3_m}, {3_m, 1_m}},
                                   {{2_m, 2_m}, {2_m, 6_m}, {4_m, 6_m}, {4_m, 2_m}},
                                   true,
                                   {{2_m, 2_m}, {2_m, 3_m}, {3_m, 2_m}, {3_m, 3_m}}},
        GetIntersectionPoints_Data{{{1_m, 1_m}, {1_m, 3_m}, {3_m, 3_m}, {3_m, 1_m}},
                                   {{0_m, 0_m}, {0_m, 4_m}, {4_m, 4_m}, {4_m, 0_m}},
                                   true,
                                   {{1_m, 1_m}, {1_m, 3_m}, {3_m, 3_m}, {3_m, 1_m}}},
        GetIntersectionPoints_Data{{{1_m, 1_m}, {1_m, 3_m}, {3_m, 3_m}, {3_m, 1_m}},
                                   {{2_m, 2_m}, {2_m, 2.5_m}, {2.5_m, 2.5_m}, {2.5_m, 2_m}},
                                   true,
                                   {{2_m, 2_m}, {2_m, 2.5_m}, {2.5_m, 2.5_m}, {2.5_m, 2_m}}},
        GetIntersectionPoints_Data{{{1_m, 1_m}, {1_m, 3_m}, {3_m, 3_m}, {3_m, 1_m}},
                                   {{1_m, 4_m}, {3_m, 6_m}, {6_m, 3_m}, {4_m, 1_m}},
                                   true,
                                   {{3_m, 2_m}, {2_m, 3_m}, {3_m, 3_m}}},
        GetIntersectionPoints_Data{{{1_m, 1_m}, {1_m, 3_m}, {3_m, 3_m}, {3_m, 1_m}},
                                   {{1_m, 1_m}, {1_m, 3_m}, {3_m, 3_m}, {3_m, 1_m}},
                                   true,
                                   {{1_m, 1_m}, {1_m, 3_m}, {3_m, 3_m}, {3_m, 1_m}}},
        GetIntersectionPoints_Data{{{-1_m, 0_m}, {0_m, 1_m}, {1_m, 0_m}, {0_m, -1_m}},
                                   {{0_m, 0_m}, {1_m, 1_m}, {2_m, 0_m}, {1_m, -1_m}},
                                   true,
                                   {{0_m, 0_m}, {0.5_m, 0.5_m}, {1_m, 0_m}, {0.5_m, -0.5_m}}}));

INSTANTIATE_TEST_CASE_P(
    SecondNotRectangular,
    GetIntersectionPoints_Test,
    testing::Values(
        //Element corners                  Object corners                              Intersection points
        GetIntersectionPoints_Data{{{1_m, 1_m}, {1_m, 2_m}, {3_m, 4_m}, {3_m, 1_m}},
                                   {{2_m, 5_m}, {2_m, 6_m}, {4_m, 6_m}, {4_m, 5_m}},
                                   false,
                                   {}},
        GetIntersectionPoints_Data{{{1_m, 1_m}, {1_m, 2_m}, {3_m, 4_m}, {3_m, 1_m}},
                                   {{2_m, 2_m}, {2_m, 6_m}, {4_m, 6_m}, {4_m, 2_m}},
                                   false,
                                   {{2_m, 2_m}, {2_m, 3_m}, {3_m, 2_m}, {3_m, 4_m}}},
        GetIntersectionPoints_Data{{{1_m, 1_m}, {1_m, 2_m}, {3_m, 4_m}, {3_m, 1_m}},
                                   {{0_m, 0_m}, {0_m, 5_m}, {4_m, 5_m}, {4_m, 0_m}},
                                   false,
                                   {{1_m, 1_m}, {1_m, 2_m}, {3_m, 4_m}, {3_m, 1_m}}},
        GetIntersectionPoints_Data{{{1_m, 1_m}, {1_m, 2_m}, {3_m, 4_m}, {3_m, 1_m}},
                                   {{2_m, 2_m}, {2_m, 2.5_m}, {2.5_m, 2.5_m}, {2.5_m, 2_m}},
                                   false,
                                   {{2_m, 2_m}, {2_m, 2.5_m}, {2.5_m, 2.5_m}, {2.5_m, 2_m}}},
        GetIntersectionPoints_Data{{{1_m, 1_m}, {1_m, 2_m}, {3_m, 4_m}, {3_m, 1_m}},
                                   {{-2_m, 0_m}, {-2_m, 2_m}, {0_m, 2_m}, {0_m, 0_m}},
                                   false,
                                   {}},
        GetIntersectionPoints_Data{{{1_m, 1_m}, {1_m, 2_m}, {3_m, 4_m}, {3_m, 1_m}},
                                   {{1_m, 1_m}, {1_m, 2_m}, {3_m, 4_m}, {3_m, 1_m}},
                                   false,
                                   {{1_m, 1_m}, {1_m, 2_m}, {3_m, 4_m}, {3_m, 1_m}}}));

/// Data table with the basic Informations for situations
/// \see PointQuery
struct WithinPolygon_Data
{
  // do not change order of items
  // unless you also change it in INSTANTIATE_TEST_CASE_P
  // (see bottom of file)
  units::length::meter_t Ax;
  units::length::meter_t Ay;
  units::length::meter_t Bx;
  units::length::meter_t By;
  units::length::meter_t Dx;
  units::length::meter_t Dy;
  units::length::meter_t Cx;
  units::length::meter_t Cy;
  units::length::meter_t Px;
  units::length::meter_t Py;
  bool withinPolygon;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const WithinPolygon_Data& obj)
  {
    return os << "A (" << obj.Ax << ", " << obj.Ay << "), "
              << "B (" << obj.Bx << ", " << obj.By << "), "
              << "D (" << obj.Dx << ", " << obj.Dy << "), "
              << "C (" << obj.Cx << ", " << obj.Cy << "), "
              << "P (" << obj.Px << ", " << obj.Py << "), "
              << "withinPolygon: " << obj.withinPolygon;
  }
};

/// \see https://github.com/google/googletest/blob/master/googletest/docs/AdvancedGuide.md
class WithinPolygon : public ::testing::Test, public ::testing::WithParamInterface<WithinPolygon_Data>
{
};

/// Tests if point is within a given geometry element
TEST_P(WithinPolygon, ParameterTest)
{
  auto data = GetParam();
  Common::Vector2d<units::length::meter_t> point = {data.Px, data.Py};
  ASSERT_THAT(CommonHelper::IntersectionCalculation::IsWithin(
                  {data.Ax, data.Ay}, {data.Bx, data.By}, {data.Cx, data.Cy}, {data.Dx, data.Dy}, point),
              Eq(data.withinPolygon));
}

INSTANTIATE_TEST_CASE_P(
    OutsideBoundarySet,
    WithinPolygon,
    testing::Values(
        // /*                    Ax     Ay     Bx     By     Dx     Dy     Cx     Cy     Px     Py   withinPolygon */
        WithinPolygon_Data{-10.0_m, 10.0_m, 10.0_m, 10.0_m, 10.0_m, -10.0_m, -10.0_m, -10.0_m, -10.1_m, 0.0_m, false},
        WithinPolygon_Data{-10.0_m, 10.0_m, 10.0_m, 10.0_m, 10.0_m, -10.0_m, -10.0_m, -10.0_m, 10.1_m, 0.0_m, false},
        WithinPolygon_Data{-10.0_m, 10.0_m, 10.0_m, 10.0_m, 10.0_m, -10.0_m, -10.0_m, -10.0_m, 0.0_m, -10.1_m, false},
        WithinPolygon_Data{-10.0_m, 10.0_m, 10.0_m, 10.0_m, 10.0_m, -10.0_m, -10.0_m, -10.0_m, 0.0_m, 10.1_m, false},
        // 180° rotated
        WithinPolygon_Data{10.0_m, -10.0_m, -10.0_m, -10.0_m, -10.0_m, 10.0_m, 10.0_m, 10.0_m, -10.1_m, 0.0_m, false},
        WithinPolygon_Data{10.0_m, -10.0_m, -10.0_m, -10.0_m, -10.0_m, 10.0_m, 10.0_m, 10.0_m, 10.1_m, 0.0_m, false},
        WithinPolygon_Data{10.0_m, -10.0_m, -10.0_m, -10.0_m, -10.0_m, 10.0_m, 10.0_m, 10.0_m, 0.0_m, -10.1_m, false},
        WithinPolygon_Data{10.0_m, -10.0_m, -10.0_m, -10.0_m, -10.0_m, 10.0_m, 10.0_m, 10.0_m, 0.0_m, 10.1_m, false},
        // 45° rotated
        WithinPolygon_Data{0.0_m, 10.0_m, 10.0_m, 0.0_m, 0.0_m, -10.0_m, -10.0_m, 0.0_m, -7.0_m, -7.0_m, false},
        WithinPolygon_Data{0.0_m, 10.0_m, 10.0_m, 0.0_m, 0.0_m, -10.0_m, -10.0_m, 0.0_m, 7.0_m, -7.0_m, false},
        WithinPolygon_Data{0.0_m, 10.0_m, 10.0_m, 0.0_m, 0.0_m, -10.0_m, -10.0_m, 0.0_m, 7.0_m, 7.0_m, false},
        WithinPolygon_Data{0.0_m, 10.0_m, 10.0_m, 0.0_m, 0.0_m, -10.0_m, -10.0_m, 0.0_m, -7.0_m, 7.0_m, false},
        // other direction
        WithinPolygon_Data{10.0_m, -10.0_m, 10.0_m, 10.0_m, -10.0_m, 10.0_m, -10.0_m, -10.0_m, -10.1_m, 0.0_m, false},
        WithinPolygon_Data{10.0_m, -10.0_m, 10.0_m, 10.0_m, -10.0_m, 10.0_m, -10.0_m, -10.0_m, 10.1_m, 0.0_m, false},
        WithinPolygon_Data{10.0_m, -10.0_m, 10.0_m, 10.0_m, -10.0_m, 10.0_m, -10.0_m, -10.0_m, 0.0_m, -10.1_m, false},
        WithinPolygon_Data{10.0_m, -10.0_m, 10.0_m, 10.0_m, -10.0_m, 10.0_m, -10.0_m, -10.0_m, 0.0_m, 10.1_m, false}));

INSTANTIATE_TEST_CASE_P(
    InsideBoundarySet,
    WithinPolygon,
    testing::Values(
        /*                    Ax     Ay     Bx     By     Dx     Dy     Cx     Cy     Px     Py   withinPolygon */
        WithinPolygon_Data{-12.3_m, 13.4_m, 15.6_m, 17.8_m, 19.2_m, -10.1_m, -12.3_m, -14.5_m, 0.0_m, 0.0_m, true},
        WithinPolygon_Data{-12.3_m, 13.4_m, 15.6_m, 17.8_m, 19.2_m, -10.1_m, -12.3_m, -14.5_m, -10.0_m, -10.0_m, true},
        WithinPolygon_Data{-12.3_m, 13.4_m, 15.6_m, 17.8_m, 19.2_m, -10.1_m, -12.3_m, -14.5_m, 10.0_m, -10.0_m, true},
        WithinPolygon_Data{-12.3_m, 13.4_m, 15.6_m, 17.8_m, 19.2_m, -10.1_m, -12.3_m, -14.5_m, -10.0_m, 10.0_m, true},
        WithinPolygon_Data{-12.3_m, 13.4_m, 15.6_m, 17.8_m, 19.2_m, -10.1_m, -12.3_m, -14.5_m, 10.0_m, 10.0_m, true},
        // 45° rotated
        WithinPolygon_Data{0.0_m, 10.0_m, 10.0_m, 0.0_m, 0.0_m, -10.0_m, -10.0_m, 0.0_m, -3.0_m, -3.0_m, true},
        WithinPolygon_Data{0.0_m, 10.0_m, 10.0_m, 0.0_m, 0.0_m, -10.0_m, -10.0_m, 0.0_m, -3.0_m, 3.0_m, true},
        WithinPolygon_Data{0.0_m, 10.0_m, 10.0_m, 0.0_m, 0.0_m, -10.0_m, -10.0_m, 0.0_m, 3.0_m, -3.0_m, true},
        WithinPolygon_Data{0.0_m, 10.0_m, 10.0_m, 0.0_m, 0.0_m, -10.0_m, -10.0_m, 0.0_m, -3.0_m, 3.0_m, true},
        // something in between
        WithinPolygon_Data{-12.3_m, 13.4_m, 15.6_m, 17.8_m, 19.0_m, -12.0_m, -12.1_m, -12.2_m, 0.0_m, 0.0_m, true},
        WithinPolygon_Data{
            -12.3_m, 13.4_m, 15.6_m, 17.8_m, 19.0_m, -12.0_m, -12.1_m, -12.2_m, -10.0_m, -10.0_m, true}));

INSTANTIATE_TEST_CASE_P(
    OnEdgeSet,
    WithinPolygon,
    testing::Values(
        /*                    Ax     Ay     Bx     By     Dx     Dy     Cx     Cy     Px     Py   withinPolygon */
        WithinPolygon_Data{-10.0_m, 10.0_m, 10.0_m, 10.0_m, 10.0_m, -10.0_m, -10.0_m, -10.0_m, 10.0_m, 0.0_m, true},
        WithinPolygon_Data{-10.0_m, 10.0_m, 10.0_m, 10.0_m, 10.0_m, -10.0_m, -10.0_m, -10.0_m, 0.0_m, 10.0_m, true},
        WithinPolygon_Data{-10.0_m, 10.0_m, 10.0_m, 10.0_m, 10.0_m, -10.0_m, -10.0_m, -10.0_m, 0.0_m, -10.0_m, true},
        WithinPolygon_Data{-10.0_m, 10.0_m, 10.0_m, 10.0_m, 10.0_m, -10.0_m, -10.0_m, -10.0_m, -10.0_m, 0.0_m, true},
        // diagonal in between
        WithinPolygon_Data{-10.0_m, 10.0_m, 10.0_m, 10.0_m, 10.0_m, -10.0_m, -10.0_m, -10.0_m, 2.0_m, 2.0_m, true},
        WithinPolygon_Data{-10.0_m, 10.0_m, 10.0_m, 10.0_m, 10.0_m, -10.0_m, -10.0_m, -10.0_m, 2.0_m, -2.0_m, true},
        WithinPolygon_Data{-10.0_m, 10.0_m, 10.0_m, 10.0_m, 10.0_m, -10.0_m, -10.0_m, -10.0_m, -2.0_m, -2.0_m, true},
        WithinPolygon_Data{-10.0_m, 10.0_m, 10.0_m, 10.0_m, 10.0_m, -10.0_m, -10.0_m, -10.0_m, -2.0_m, 2.0_m, true},
        // 45° rotated deges
        WithinPolygon_Data{0.0_m, 10.0_m, 10.0_m, 0.0_m, 0.0_m, -10.0_m, -10.0_m, 0.0_m, 5.0_m, 5.0_m, true},
        WithinPolygon_Data{0.0_m, 10.0_m, 10.0_m, 0.0_m, 0.0_m, -10.0_m, -10.0_m, 0.0_m, -5.0_m, 5.0_m, true},
        WithinPolygon_Data{0.0_m, 10.0_m, 10.0_m, 0.0_m, 0.0_m, -10.0_m, -10.0_m, 0.0_m, 5.0_m, 5.0_m, true},
        WithinPolygon_Data{0.0_m, 10.0_m, 10.0_m, 0.0_m, 0.0_m, -10.0_m, -10.0_m, 0.0_m, 5.0_m, -5.0_m, true},
        // 45° rotated diagonal in between
        WithinPolygon_Data{0.0_m, 10.0_m, 10.0_m, 0.0_m, 0.0_m, -10.0_m, -10.0_m, 0.0_m, 0.0_m, -7.0_m, true},
        WithinPolygon_Data{0.0_m, 10.0_m, 10.0_m, 0.0_m, 0.0_m, -10.0_m, -10.0_m, 0.0_m, 0.0_m, -7.0_m, true},
        WithinPolygon_Data{0.0_m, 10.0_m, 10.0_m, 0.0_m, 0.0_m, -10.0_m, -10.0_m, 0.0_m, 7.0_m, 0.0_m, true},
        WithinPolygon_Data{0.0_m, 10.0_m, 10.0_m, 0.0_m, 0.0_m, -10.0_m, -10.0_m, 0.0_m, -7.0_m, 0.0_m, true}));
