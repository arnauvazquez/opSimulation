/********************************************************************************
 * Copyright (c) 2019 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  spawnItemParameterInterface.h
//! @brief This file contains the interface of the spawn item parameter to interact
//!        with the framework.
//-----------------------------------------------------------------------------

#pragma once

#include <string>

#include "include/agentBlueprintInterface.h"

class WorldInterface;
//-----------------------------------------------------------------------------
//! This class represents the functionality of a spawn item (agent) which will
//! be spawned by a spawn point
//-----------------------------------------------------------------------------
class SpawnItemParameterInterface
{
public:
  SpawnItemParameterInterface() = default;
  SpawnItemParameterInterface(const SpawnItemParameterInterface&) = delete;
  SpawnItemParameterInterface(SpawnItemParameterInterface&&) = delete;
  SpawnItemParameterInterface& operator=(const SpawnItemParameterInterface&) = delete;
  SpawnItemParameterInterface& operator=(SpawnItemParameterInterface&&) = delete;
  virtual ~SpawnItemParameterInterface() = default;

  //-----------------------------------------------------------------------------
  //! Sets the x-coordinate of the agent to be spawned
  //!
  //! @param[in]     positionX    X-coordinate
  //-----------------------------------------------------------------------------
  virtual void SetPositionX(units::length::meter_t positionX) = 0;

  //-----------------------------------------------------------------------------
  //! Sets the y-coordinate of the agent to be spawned
  //!
  //! @param[in]     positionY    Y-coordinate
  //-----------------------------------------------------------------------------
  virtual void SetPositionY(units::length::meter_t positionY) = 0;

  //-----------------------------------------------------------------------------
  //! Sets the forward velocity of the agent to be spawned
  //!
  //! @param[in]     velocity    Forward velocity
  //-----------------------------------------------------------------------------
  virtual void SetVelocity(units::velocity::meters_per_second_t velocity) = 0;

  //-----------------------------------------------------------------------------
  //! Sets the forward acceleration of the agent to be spawned
  //!
  //! @param[in]     acceleration    Forward acceleration
  //-----------------------------------------------------------------------------
  virtual void SetAcceleration(units::acceleration::meters_per_second_squared_t acceleration) = 0;

  //-----------------------------------------------------------------------------
  //! Sets the gear of the agent to be spawned
  //!
  //! @param[in]     gear    current/calculated gear
  //-----------------------------------------------------------------------------
  virtual void SetGear(double gear) = 0;

  //-----------------------------------------------------------------------------
  //! Sets the yaw angle of the agent to be spawned
  //!
  //! @param[in]     yawAngle    Agent orientation (0 points to east)
  //-----------------------------------------------------------------------------
  virtual void SetYaw(units::angle::radian_t yawAngle) = 0;

  //-----------------------------------------------------------------------------
  //! Sets the next time when the agent will be spawned
  //!
  //! @param[in]     nextTimeOffset    Time offset counted from the current
  //!                                  scheduling time
  //-----------------------------------------------------------------------------
  virtual void SetNextTimeOffset(int nextTimeOffset) = 0;

  //-----------------------------------------------------------------------------
  //! Selectes the agent to be spawned next within the configured agent array
  //!
  //! @param[in]     index    Index within configured agent array
  //-----------------------------------------------------------------------------
  virtual void SetIndex(int index) = 0;

  //-----------------------------------------------------------------------------
  //! Sets the vehicle type of the agent to be spawned
  //!
  //! @param[in]     vehicleModel    vehicleModel of agent
  //-----------------------------------------------------------------------------
  virtual void SetVehicleModel(std::string vehicleModel) = 0;

  /// @return X-coordinate of the agent
  virtual double GetPositionX() const = 0;

  /// @return Y-coordinate of the agent
  virtual double GetPositionY() const = 0;

  /// @return velocity of the agent
  virtual units::velocity::meters_per_second_t GetVelocity() const = 0;

  /// @return acceleration of the agent
  virtual units::acceleration::meters_per_second_squared_t GetAcceleration() const = 0;

  /// @return yaw angle of the agent
  virtual units::angle::radian_t GetYaw() const = 0;

  /// @return model of the vehicle
  virtual std::string GetVehicleModel() const = 0;
};
