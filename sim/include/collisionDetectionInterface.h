/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** \file  collisionDetectionInterface.h
 *	\brief This file provides the interface for the CollisionDetector
 */
//-----------------------------------------------------------------------------

#pragma once

#include "include/runResultInterface.h"

namespace core
{

//! Interface for collision detection
class CollisionDetectionInterface
{
public:
  virtual ~CollisionDetectionInterface() = default;

  //! @brief Function is called by framework when the scheduler calls the trigger task of this component
  //!
  //! @param[in] time Current scheduling time
  virtual void Trigger(int time) = 0;

  //! \brief Set parameters when class is initialized
  //! \param [in] runResult   Pointer to run result
  virtual void Initialize(RunResultInterface *runResult) = 0;
};

}  //namespace core