/********************************************************************************
 * Copyright (c) 2016 ITK Engineering GmbH
 *               2017-2020 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  agentBlueprintInterface.h
//! @brief This file contains the interface of the agent blueprint to interact
//!        with the framework.
//-----------------------------------------------------------------------------

#include <MantleAPI/Common/route_definition.h>
#include <MantleAPI/Traffic/entity_properties.h>
#include <string>
#include <unordered_map>

#include "common/globalDefinitions.h"
#include "common/worldDefinitions.h"
#include "include/agentTypeInterface.h"
#include "include/profilesInterface.h"
#include "include/scenarioControlInterface.h"

#pragma once

//! Container for vehicle components and their possible profiles
//! The key is the VehicleComponent type and the value is the name of the component profile
using VehicleComponentProfileNames = std::unordered_map<std::string, std::string>;

//! Route from OpenSCENARIO converted into graph
struct Route
{
  RoadGraph roadGraph;     //!< Directed graph representing the road network
  RoadGraphVertex root;    //!< Root of the roadGraph
  RoadGraphVertex target;  //!< Target in the roadGraph
};

/// struct represents paramters of the spawn object
struct SpawnParameter
{
  /// Position
  mantle_api::Vec3<units::length::meter_t> position{0.0_m, 0.0_m, 0.0_m};
  /// Velocity
  units::velocity::meters_per_second_t velocity{0.0_mps};
  /// Acceleration
  units::acceleration::meters_per_second_squared_t acceleration{0.0_mps_sq};
  /// Orientation
  mantle_api::Orientation3<units::angle::radian_t> orientation{0.0_rad, 0.0_rad, 0.0_rad};
  /// Current Gear: double for compatiblity - use integer only!
  double gear = 0.0;
  /// Route of the spawned object
  Route route{};
  /// Route in MantleAPI definition
  mantle_api::RouteDefinition routeDefinition{};
};

/// systems of an agent
struct System
{
  /// type of the agent
  std::shared_ptr<core::AgentTypeInterface> agentType;
  /// agent profile name
  std::string agentProfileName;
  /// driver profile name
  std::string driverProfileName;
  /// sensor parameters
  openpass::sensors::Parameters sensorParameters;
  /// component profiles
  VehicleComponentProfileNames vehicleComponentProfileNames;
};

/// components of an agent
struct AgentBuildInstructions
{
  /// name
  std::string name;
  /// category
  AgentCategory agentCategory;
  /// properties
  std::shared_ptr<const mantle_api::EntityProperties> entityProperties;
  /// system
  System system;
  /// spawn parameter
  SpawnParameter spawnParameter;
  /// scenario control
  std::shared_ptr<ScenarioControlInterface> scenarioControl;
};
