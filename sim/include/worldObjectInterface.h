/********************************************************************************
 * Copyright (c) 2017-2019 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <unordered_map>

#include "common/boostGeometryCommon.h"
#include "common/globalDefinitions.h"
#include "common/vector2d.h"
#include "common/worldDefinitions.h"

class WorldObjectInterface;

/// Interface representing world object
class WorldObjectInterface
{
public:
  /// \brief Retrieves OSI object type
  /// \return type of OSI object
  virtual ObjectTypeOSI GetType() const = 0;

  /// \brief Retrieves position x of the boundary box center
  /// \return x coordinate
  virtual units::length::meter_t GetPositionX() const = 0;

  /// \brief Retrieves position y of the boundary box center
  /// \return y coordinate
  virtual units::length::meter_t GetPositionY() const = 0;

  /// \brief Retrieves position z of the boundary box center
  /// \return z coordinate
  virtual units::length::meter_t GetPositionZ() const = 0;

  /// \brief Retrieves width of the boundary box
  /// \return width
  virtual units::length::meter_t GetWidth() const = 0;

  /// \brief Retrieves length of the boundary box
  /// \return length
  virtual units::length::meter_t GetLength() const = 0;

  /// \brief Retrieves height of the boundary box
  /// \return height
  virtual units::length::meter_t GetHeight() const = 0;

  /// \brief Retrieves yaw angle w.r.t. x-axis
  /// \return yaw
  virtual units::angle::radian_t GetYaw() const = 0;

  /// \brief Retrieves roll angle
  /// \return roll
  virtual units::angle::radian_t GetRoll() const = 0;

  /// \brief  Get unique id of an object
  /// \return id
  virtual int GetId() const = 0;

  /// \brief Retrieves boundary box information
  /// \return 2d bounding box information
  virtual const polygon_t& GetBoundingBox2D() const = 0;

  /// \brief  Get roads on which the object is on
  /// \return touched road intervals
  virtual const RoadIntervals& GetTouchedRoads() const = 0;

  /// \brief  Returns the world position of the given point
  /// \param  point point to locate (must not be a of type ObjectPointRelative)
  /// \return world position
  virtual Common::Vector2d<units::length::meter_t> GetAbsolutePosition(const ObjectPoint& point) const = 0;

  /// \brief  Returns the road position(s) of the given point
  /// \param  point point to locate (must not be a of type ObjectPointRelative)
  /// \return road positions
  virtual const GlobalRoadPositions& GetRoadPosition(const ObjectPoint& point) const = 0;

  /// \brief Retrieves the distance from the reference point to the leading edge
  /// \return distance
  virtual units::length::meter_t GetDistanceReferencePointToLeadingEdge() const = 0;

  /// \brief  Returns the objects velocity at the given point
  /// \param  point point where to get the velocity (must not be a of type ObjectPointRelative)
  /// \return velocity
  virtual Common::Vector2d<units::velocity::meters_per_second_t> GetVelocity(ObjectPoint point
                                                                             = ObjectPointPredefined::Reference) const
      = 0;

  /// \brief  Returns the objects acceleration at the given point
  /// \param  point point where to get the acceleration (must not be a of type ObjectPointRelative)
  /// \return acceleration
  virtual Common::Vector2d<units::acceleration::meters_per_second_squared_t> GetAcceleration(
      ObjectPoint point = ObjectPointPredefined::Reference) const
      = 0;

  /**
   * Locate the object on the road network
   *
   * @return true if the objects' reference point is locatable, false otherwise
   */
  virtual bool Locate() = 0;

  /// Clears the localization data of an object (e.g. lane assignments)
  virtual void Unlocate() = 0;

  // object is not inteded to be copied or assigned
  WorldObjectInterface() = default;
  WorldObjectInterface(const WorldObjectInterface&) = delete;
  WorldObjectInterface(WorldObjectInterface&&) = delete;
  WorldObjectInterface& operator=(const WorldObjectInterface&) = delete;
  WorldObjectInterface& operator=(WorldObjectInterface&&) = delete;
  virtual ~WorldObjectInterface() = default;
};
