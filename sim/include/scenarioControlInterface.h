/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  scenarioControlInterface.h
//! @brief This interfaces provies access to the control strategies of a single entity
//-----------------------------------------------------------------------------

#pragma once

#include <limits>
#include <memory>

#include "MantleAPI/Common/route_definition.h"
#include "MantleAPI/Common/trajectory.h"
#include "MantleAPI/Traffic/control_strategy.h"

namespace ScenarioCommand
{

static constexpr uint64_t ID_UNSET{0};
struct FollowTrajectory  ///< Struct representing follow trajectory acction
{
  mantle_api::Trajectory value;  ///< value of the trajectory defined as per mantle_api
};

struct AssignRoute  ///< Struct representing Assign route action
{
  mantle_api::RouteDefinition value;  ///< value of the route definition defined as per mantle_api
  bool constrain_orientation{false};  ///< constrain orientation
  enum class FollowingMode            ///< enum for supported following modes
  {
    FOLLOWING_MODE_POSITION = 0,
    FOLLOWING_MODE_FOLLOW = 1
  } followingMode  ///< value of the following mode
      = FollowingMode::FOLLOWING_MODE_POSITION;
  uint64_t id{ID_UNSET};  ///< Id to increment when AddCommand is called
};

/// Type def Any is used to represent either AssignRoute or FollowTrajectory action
using Any = std::variant<ScenarioCommand::AssignRoute, ScenarioCommand::FollowTrajectory>;
}  //namespace ScenarioCommand

/// Type def ScenarioCommands represents the list of ScenarioCommand
using ScenarioCommands = std::vector<ScenarioCommand::Any>;

class ScenarioControlInterface  ///< Class representing control strategy Interface
{
public:
  ScenarioControlInterface() = default;
  virtual ~ScenarioControlInterface() = default;
  /// @brief Function to get strategies
  /// @param type Control strategy type
  /// @return list of pointers to the control strategy
  virtual std::vector<std::shared_ptr<mantle_api::ControlStrategy>> GetStrategies(mantle_api::ControlStrategyType type)
      = 0;

  /// @brief Function to get strategies
  /// @param domain Movement domain
  /// @return list of pointers to the control strategy
  virtual std::vector<std::shared_ptr<mantle_api::ControlStrategy>> GetStrategies(mantle_api::MovementDomain domain)
      = 0;

  /// @brief Function to set strategies
  /// @param strategies list of pointers of control strategies to set
  virtual void SetStrategies(std::vector<std::shared_ptr<mantle_api::ControlStrategy>> strategies) = 0;

  /// @return Returns true, if control strategy has a new longitudinal strategy
  virtual bool HasNewLongitudinalStrategy() const = 0;

  /// @return Returns true, if control strategy has a new lateral strategy
  virtual bool HasNewLateralStrategy() const = 0;

  /// @brief Setter function to set control strategy goal
  /// @param type Type of control strategy
  virtual void SetControlStrategyGoalReached(mantle_api::ControlStrategyType type) = 0;

  /// @brief Function to check if the given control strategy goal has been reached
  /// @param type Type of control strategy
  /// @return Returns true, if the control strategy goal has been reached
  virtual bool HasControlStrategyGoalBeenReached(mantle_api::ControlStrategyType type) const = 0;

  /// @return Returns true, if custom controller is used
  virtual bool UseCustomController() const = 0;

  /// @brief Setter function to use custom controller
  /// @param useCustomController boolean, if customcontroller has to be used
  virtual void SetCustomController(bool useCustomController) = 0;

  //! @brief Method which retrieves the custom commands
  //!
  //! @return Custom commands
  virtual const std::vector<std::string> &GetCustomCommands() = 0;

  //! @brief Method which adds a custom command to the bottom of the list of custom commands
  //!
  //! @param[in] command  Custom command to add
  virtual void AddCustomCommand(const std::string &command) = 0;

  /// @brief Function to update for the next time step
  virtual void UpdateForNextTimestep() = 0;

  /// @return Getter function to return scenario commands
  virtual const ScenarioCommands &GetCommands() = 0;
};