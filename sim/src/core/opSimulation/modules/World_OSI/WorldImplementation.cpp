/********************************************************************************
 * Copyright (c) 2016-2017 ITK Engineering GmbH
 *               2017-2021 in-tech GmbH
 *               2020 HLRS, University of Stuttgart
 *               2020-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "WorldImplementation.h"

#include <osi3/osi_sensorview.pb.h>
#include <osi3/osi_sensorviewconfiguration.pb.h>

#include "EntityRepository.h"
#include "WorldData.h"
#include "WorldEntities.h"
#include "common/RoutePlanning/RouteCalculation.h"

namespace
{
template <typename T>
static std::vector<const T *> get_transformed(const std::vector<const OWL::Interfaces::WorldObject *> &worldObjects)
{
  std::vector<const T *> transformedContainer;
  std::transform(worldObjects.begin(),
                 worldObjects.end(),
                 std::back_inserter(transformedContainer),
                 [](const OWL::Interfaces::WorldObject *object) { return object->GetLink<T>(); });
  return transformedContainer;
}
}  // namespace

WorldImplementation::WorldImplementation(const CallbackInterface *callbacks,
                                         StochasticsInterface *stochastics,
                                         DataBufferWriteInterface *dataBuffer,
                                         OWL::Interfaces::WorldData &worldData)
    : agentNetwork(this, callbacks),
      callbacks(callbacks),
      dataBuffer(dataBuffer),
      repository(dataBuffer),
      worldData(worldData)
{
}

WorldImplementation::~WorldImplementation() {}

AgentInterface *WorldImplementation::GetAgent(int id) const
{
  return agentNetwork.GetAgent(id);
}

const std::vector<const WorldObjectInterface *> &WorldImplementation::GetWorldObjects() const
{
  return worldObjects;
}

std::map<int, AgentInterface *> WorldImplementation::GetAgents()
{
  std::map<int, AgentInterface *> agents;
  for (auto &agent : agentNetwork.GetAgents())
  {
    agents.insert({agent.GetId(), &agent});
  }
  return agents;
}

const std::vector<int> WorldImplementation::GetRemovedAgentsInPreviousTimestep()
{
  return agentNetwork.GetRemovedAgentsInPreviousTimestep();
}

const std::vector<const TrafficObjectInterface *> &WorldImplementation::GetTrafficObjects() const
{
  return trafficObjects;
}

const TrafficRules &WorldImplementation::GetTrafficRules() const
{
  return worldParameter.trafficRules;
}

void WorldImplementation::ExtractParameter(ParameterInterface *parameters)
{
  auto intParameter = parameters->GetParametersInt();
  auto doubleParameter = parameters->GetParametersDouble();
  auto stringParameter = parameters->GetParametersString();
  auto boolParameter = parameters->GetParametersBool();

  worldParameter.timeOfDay = stringParameter.at("TimeOfDay");
  worldParameter.visibilityDistance = units::length::meter_t(intParameter.at("VisibilityDistance"));
  worldParameter.friction = doubleParameter.at("Friction");
  worldParameter.weather = stringParameter.at("Weather");

  auto openSpeedLimit = helper::map::query(doubleParameter, "OpenSpeedLimit");
  THROWIFFALSE(openSpeedLimit.has_value(), "Missing traffic rule OpenSpeedLimit")
  worldParameter.trafficRules.openSpeedLimit = openSpeedLimit.value();

  auto openSpeedLimitTrucks = helper::map::query(doubleParameter, "OpenSpeedLimitTrucks");
  THROWIFFALSE(openSpeedLimitTrucks.has_value(), "Missing traffic rule OpenSpeedLimitTrucks")
  worldParameter.trafficRules.openSpeedLimitTrucks = openSpeedLimitTrucks.value();

  auto openSpeedLimitBuses = helper::map::query(doubleParameter, "OpenSpeedLimitBuses");
  THROWIFFALSE(openSpeedLimitBuses.has_value(), "Missing traffic rule OpenSpeedLimitBuses")
  worldParameter.trafficRules.openSpeedLimitBuses = openSpeedLimitBuses.value();

  auto keepToOuterLanes = helper::map::query(boolParameter, "KeepToOuterLanes");
  THROWIFFALSE(keepToOuterLanes.has_value(), "Missing traffic rule KeepToOuterLanes")
  worldParameter.trafficRules.keepToOuterLanes = keepToOuterLanes.value();

  auto dontOvertakeOnOuterLanes = helper::map::query(boolParameter, "DontOvertakeOnOuterLanes");
  THROWIFFALSE(dontOvertakeOnOuterLanes.has_value(), "Missing traffic rule DontOvertakeOnOuterLanes")
  worldParameter.trafficRules.dontOvertakeOnOuterLanes = dontOvertakeOnOuterLanes.value();

  auto formRescueLane = helper::map::query(boolParameter, "FormRescueLane");
  THROWIFFALSE(formRescueLane.has_value(), "Missing traffic rule FormRescueLane")
  worldParameter.trafficRules.formRescueLane = formRescueLane.value();

  auto zipperMerge = helper::map::query(boolParameter, "ZipperMerge");
  THROWIFFALSE(zipperMerge.has_value(), "Missing traffic rule ZipperMerge")
  worldParameter.trafficRules.zipperMerge = zipperMerge.value();
}

void WorldImplementation::Reset()
{
  for (auto &[id, lane] : worldData.GetLanes())
  {
    lane->ClearMovingObjects();
  }
  worldData.Reset();
  worldParameter.Reset();
  agentNetwork.Clear();
  worldObjects.clear();
  repository.Reset();
  worldObjects.insert(worldObjects.end(), trafficObjects.begin(), trafficObjects.end());
}

void WorldImplementation::Clear()
{
  for (auto worldObject : worldObjects)
  {
    delete worldObject;
  }
}

void *WorldImplementation::GetWorldData()
{
  return &worldData;
}

void *WorldImplementation::GetOsiGroundTruth() const
{
  // return &groundTruth;
  return nullptr;
}

void WorldImplementation::QueueAgentUpdate(std::function<void()> func)
{
  agentNetwork.QueueAgentUpdate(func);
}

void WorldImplementation::QueueAgentRemove(const AgentInterface *agent)
{
  agentNetwork.QueueAgentRemove(agent);
}

void WorldImplementation::RemoveAgents()
{
  agentNetwork.RemoveAgents();
}

void WorldImplementation::RemoveAgent(const AgentInterface *agent)
{
  worldData.RemoveMovingObjectById(agent->GetId());
  auto it = std::find(worldObjects.begin(), worldObjects.end(), agent);
  if (it != worldObjects.end())
  {
    worldObjects.erase(it);
  }
}

void WorldImplementation::PublishGlobalData(int timestamp)
{
  agentNetwork.PublishGlobalData(
      [&](openpass::type::EntityId id, openpass::type::FlatParameterKey key, openpass::type::FlatParameterValue value)
      { dataBuffer->PutCyclic(id, key, value); });
}

void WorldImplementation::SyncGlobalData(int timestamp)
{
  for (auto &[id, lane] : worldData.GetLanes())
  {
    lane->ClearMovingObjects();
  }
  agentNetwork.SyncGlobalData();

  worldData.ResetTemporaryMemory();
  radio.Reset();
}

bool WorldImplementation::CreateScenery(const SceneryInterface *scenery, const TurningRates &turningRates)
{
  this->scenery = scenery;
  sceneryConverter = std::make_unique<SceneryConverter>(scenery, repository, worldData, localizer, callbacks);

  THROWIFFALSE(sceneryConverter->ConvertRoads(), "Unable to finish conversion process.")
  localizer.Init();
  sceneryConverter->ConvertObjects();
  InitTrafficObjects();

  RoadNetworkBuilder networkBuilder(*scenery);
  auto [roadGraph, vertexMapping] = networkBuilder.Build();
  worldData.SetRoadGraph(std::move(roadGraph), std::move(vertexMapping));
  worldData.SetTurningRates(turningRates);
  return true;
}

void WorldImplementation::SetWeather(const mantle_api::Weather &weather)
{
  worldData.SetEnvironment(weather);
}

const std::map<std::string, CommonTrafficLight::State> stateConversionMap{
    {"off", CommonTrafficLight::State::Off},
    {"red", CommonTrafficLight::State::Red},
    {"yellow", CommonTrafficLight::State::Yellow},
    {"green", CommonTrafficLight::State::Green},
    {"red yellow", CommonTrafficLight::State::RedYellow},
    {"yellow flashing", CommonTrafficLight::State::YellowFlashing}};

void WorldImplementation::SetTrafficSignalState(const std::string &traffic_signal_name,
                                                const std::string &traffic_signal_state)
{
  const auto owlId = helper::map::query(worldData.GetTrafficSignIdMapping(), traffic_signal_name);
  if (!owlId.has_value())
  {
    throw std::runtime_error(R"(No signal with id "traffic_signal_name" defined in Scenery)");
  }
  if (worldData.GetSignalType(owlId.value()) != OWL::SignalType::TrafficLight)
  {
    throw std::runtime_error(R"(Signal with id "traffic_signal_name" is not a traffic light)");
  }
  auto &trafficLight = worldData.GetTrafficLight(owlId.value());
  const auto state = helper::map::query(stateConversionMap, traffic_signal_state);
  if (!state.has_value())
  {
    throw std::runtime_error(R"(Unknown state "traffic_signal_state")");
  }
  trafficLight.SetState(state.value());

  dataBuffer->PutAcyclic(
      owlId.value(),
      "TrafficLightController",
      Acyclic{"TrafficLight",
              {{static_cast<int>(owlId.value())}},
              {{static_cast<int>(owlId.value())}},
              {{"traffic_light_state", traffic_signal_state}, {"opendrive_id", traffic_signal_name}}});
}

AgentInterface &WorldImplementation::CreateAgentAdapter(const AgentBuildInstructions &agentBuildInstructions)
{
  const auto id = repository.Register(openpass::entity::EntityType::MovingObject,
                                      openpass::utils::GetEntityInfo(agentBuildInstructions));
  auto &movingObject = worldData.AddMovingObject(id);
  auto &agent = agentNetwork.CreateAgent(movingObject, this, callbacks, localizer);
  movingObject.SetLink(&agent);
  agent.InitParameter(agentBuildInstructions);
  worldObjects.push_back(&agent);
  return agent;
}

std::string WorldImplementation::GetTimeOfDay() const
{
  return worldParameter.timeOfDay;
}

units::length::meter_t WorldImplementation::GetVisibilityDistance() const
{
  return worldParameter.visibilityDistance;
}

double WorldImplementation::GetFriction() const
{
  return worldParameter.friction;
}

RouteQueryResult<std::vector<CommonTrafficSign::Entity>> WorldImplementation::GetTrafficSignsInRange(
    const RoadGraph &roadGraph,
    RoadGraphVertex startNode,
    int laneId,
    units::length::meter_t startDistance,
    units::length::meter_t searchRange) const
{
  const auto laneMultiStream = worldDataQuery.CreateLaneMultiStream(roadGraph, startNode, laneId, startDistance);
  const auto startDistanceOnStream = laneMultiStream->GetPositionByVertexAndS(startNode, startDistance);
  return worldDataQuery.GetTrafficSignsInRange(*laneMultiStream, startDistanceOnStream, searchRange);
}

RouteQueryResult<std::vector<CommonTrafficSign::Entity>> WorldImplementation::GetRoadMarkingsInRange(
    const RoadGraph &roadGraph,
    RoadGraphVertex startNode,
    int laneId,
    units::length::meter_t startDistance,
    units::length::meter_t searchRange) const
{
  const auto laneMultiStream = worldDataQuery.CreateLaneMultiStream(roadGraph, startNode, laneId, startDistance);
  const auto startDistanceOnStream = laneMultiStream->GetPositionByVertexAndS(startNode, startDistance);
  return worldDataQuery.GetRoadMarkingsInRange(*laneMultiStream, startDistanceOnStream, searchRange);
}

RouteQueryResult<std::vector<CommonTrafficLight::Entity>> WorldImplementation::GetTrafficLightsInRange(
    const RoadGraph &roadGraph,
    RoadGraphVertex startNode,
    int laneId,
    units::length::meter_t startDistance,
    units::length::meter_t searchRange) const
{
  const auto laneMultiStream = worldDataQuery.CreateLaneMultiStream(roadGraph, startNode, laneId, startDistance);
  auto startDistanceOnStream = laneMultiStream->GetPositionByVertexAndS(startNode, startDistance);
  return worldDataQuery.GetTrafficLightsInRange(*laneMultiStream, startDistanceOnStream, searchRange);
}

RouteQueryResult<std::vector<LaneMarking::Entity>> WorldImplementation::GetLaneMarkings(
    const RoadGraph &roadGraph,
    RoadGraphVertex startNode,
    int laneId,
    units::length::meter_t startDistance,
    units::length::meter_t range,
    Side side) const
{
  const auto laneMultiStream = worldDataQuery.CreateLaneMultiStream(roadGraph, startNode, laneId, startDistance);
  const auto startDistanceOnStream = laneMultiStream->GetPositionByVertexAndS(startNode, startDistance);
  return worldDataQuery.GetLaneMarkings(*laneMultiStream, startDistanceOnStream, range, side);
}

RouteQueryResult<RelativeWorldView::Roads> WorldImplementation::GetRelativeJunctions(
    const RoadGraph &roadGraph,
    RoadGraphVertex startNode,
    units::length::meter_t startDistance,
    units::length::meter_t range) const
{
  const auto roadMultiStream = worldDataQuery.CreateRoadMultiStream(roadGraph, startNode);
  const auto startDistanceOnStream = roadMultiStream->GetPositionByVertexAndS(startNode, startDistance);
  return worldDataQuery.GetRelativeJunctions(*roadMultiStream, startDistanceOnStream, range);
}

RouteQueryResult<RelativeWorldView::Roads> WorldImplementation::GetRelativeRoads(const RoadGraph &roadGraph,
                                                                                 RoadGraphVertex startNode,
                                                                                 units::length::meter_t startDistance,
                                                                                 units::length::meter_t range) const
{
  const auto roadMultiStream = worldDataQuery.CreateRoadMultiStream(roadGraph, startNode);
  const auto startDistanceOnStream = roadMultiStream->GetPositionByVertexAndS(startNode, startDistance);
  return worldDataQuery.GetRelativeRoads(*roadMultiStream, startDistanceOnStream, range);
}

std::vector<JunctionConnection> WorldImplementation::GetConnectionsOnJunction(std::string junctionId,
                                                                              std::string incomingRoadId) const
{
  return worldDataQuery.GetConnectionsOnJunction(junctionId, incomingRoadId);
}

std::vector<IntersectingConnection> WorldImplementation::GetIntersectingConnections(std::string connectingRoadId) const
{
  return worldDataQuery.GetIntersectingConnections(connectingRoadId);
}

std::vector<JunctionConnectorPriority> WorldImplementation::GetPrioritiesOnJunction(std::string junctionId) const
{
  return worldDataQuery.GetPrioritiesOnJunction(junctionId);
}

RoadNetworkElement WorldImplementation::GetRoadSuccessor(std::string roadId) const
{
  return worldDataQuery.GetRoadSuccessor(roadId);
}

RoadNetworkElement WorldImplementation::GetRoadPredecessor(std::string roadId) const
{
  return worldDataQuery.GetRoadPredecessor(roadId);
}

std::pair<RoadGraph, RoadGraphVertex> WorldImplementation::GetRoadGraph(const RouteElement &start,
                                                                        int maxDepth,
                                                                        bool inDrivingDirection) const
{
  auto startVertex = worldData.GetRoadGraphVertexMapping().at(start);
  return RouteCalculation::FilterRoadGraphByStartPosition(
      worldData.GetRoadGraph(), startVertex, maxDepth, inDrivingDirection);
}

std::map<RoadGraphEdge, double> WorldImplementation::GetEdgeWeights(const RoadGraph &roadGraph) const
{
  return worldDataQuery.GetEdgeWeights(roadGraph);
}

std::unique_ptr<RoadStreamInterface> WorldImplementation::GetRoadStream(const std::vector<RouteElement> &route) const
{
  return worldDataQuery.CreateRoadStream(route);
}

AgentInterface *WorldImplementation::GetEgoAgent()
{
  for (auto &agent : agentNetwork.GetAgents())
  {
    if (agent.IsEgoAgent())
    {
      return &agent;
    }
  }

  return nullptr;
}

AgentInterface *WorldImplementation::GetAgentByName(const std::string &scenarioName)
{
  for (auto &agent : agentNetwork.GetAgents())
  {
    if (agent.GetScenarioName() == (scenarioName))
    {
      return &agent;
    }
  }

  return nullptr;
}

RouteQueryResult<std::optional<GlobalRoadPosition>> WorldImplementation::ResolveRelativePoint(
    const RoadGraph &roadGraph,
    RoadGraphVertex startNode,
    ObjectPointRelative relativePoint,
    const WorldObjectInterface &object) const
{
  const auto roadMultiStream = worldDataQuery.CreateRoadMultiStream(roadGraph, startNode);
  return worldDataQuery.ResolveRelativePoint(*roadMultiStream, relativePoint, object.GetTouchedRoads());
}

RouteQueryResult<Obstruction> WorldImplementation::GetObstruction(
    const RoadGraph &roadGraph,
    RoadGraphVertex startNode,
    const GlobalRoadPosition &ownPosition,
    const std::map<ObjectPoint, Common::Vector2d<units::length::meter_t>> &points,
    const RoadIntervals &touchedRoads) const
{
  const auto laneMultiStream
      = worldDataQuery.CreateLaneMultiStream(roadGraph, startNode, ownPosition.laneId, ownPosition.roadPosition.s);
  return worldDataQuery.GetObstruction(*laneMultiStream, ownPosition.roadPosition.t, points, touchedRoads);
}

RouteQueryResult<RelativeWorldView::Lanes> WorldImplementation::GetRelativeLanes(const RoadGraph &roadGraph,
                                                                                 RoadGraphVertex startNode,
                                                                                 int laneId,
                                                                                 units::length::meter_t distance,
                                                                                 units::length::meter_t range,
                                                                                 bool includeOncoming) const
{
  const auto roadMultiStream = worldDataQuery.CreateRoadMultiStream(roadGraph, startNode);
  const auto startDistanceOnStream = roadMultiStream->GetPositionByVertexAndS(startNode, distance);

  return worldDataQuery.GetRelativeLanes(*roadMultiStream, startDistanceOnStream, laneId, range, includeOncoming);
}

RouteQueryResult<std::optional<int>> WorldImplementation::GetRelativeLaneId(const RoadGraph &roadGraph,
                                                                            RoadGraphVertex startNode,
                                                                            int laneId,
                                                                            units::length::meter_t distance,
                                                                            GlobalRoadPositions targetPosition) const
{
  const auto roadMultiStream = worldDataQuery.CreateRoadMultiStream(roadGraph, startNode);
  const auto startDistanceOnStream = roadMultiStream->GetPositionByVertexAndS(startNode, distance);

  return worldDataQuery.GetRelativeLaneId(*roadMultiStream, startDistanceOnStream, laneId, targetPosition);
}

RouteQueryResult<AgentInterfaces> WorldImplementation::GetAgentsInRange(const RoadGraph &roadGraph,
                                                                        RoadGraphVertex startNode,
                                                                        int laneId,
                                                                        units::length::meter_t startDistance,
                                                                        units::length::meter_t backwardRange,
                                                                        units::length::meter_t forwardRange) const
{
  const auto laneMultiStream = worldDataQuery.CreateLaneMultiStream(roadGraph, startNode, laneId, startDistance);
  const auto startDistanceOnStream = laneMultiStream->GetPositionByVertexAndS(startNode, startDistance);
  const auto queryResult = worldDataQuery.GetObjectsOfTypeInRange<OWL::Interfaces::MovingObject>(
      *laneMultiStream, startDistanceOnStream - backwardRange, startDistanceOnStream + forwardRange);
  RouteQueryResult<AgentInterfaces> result;
  for (const auto &[node, objects] : queryResult)
  {
    result[node] = get_transformed<AgentInterface>(objects);
  }
  return result;
}

RouteQueryResult<std::vector<const WorldObjectInterface *>> WorldImplementation::GetObjectsInRange(
    const RoadGraph &roadGraph,
    RoadGraphVertex startNode,
    int laneId,
    units::length::meter_t startDistance,
    units::length::meter_t backwardRange,
    units::length::meter_t forwardRange) const
{
  const auto laneMultiStream = worldDataQuery.CreateLaneMultiStream(roadGraph, startNode, laneId, startDistance);
  const auto startDistanceOnStream = laneMultiStream->GetPositionByVertexAndS(startNode, startDistance);
  const auto queryResult = worldDataQuery.GetObjectsOfTypeInRange<OWL::Interfaces::WorldObject>(
      *laneMultiStream, startDistanceOnStream - backwardRange, startDistanceOnStream + forwardRange);
  RouteQueryResult<std::vector<const WorldObjectInterface *>> result;
  for (const auto &[node, objects] : queryResult)
  {
    result[node] = get_transformed<WorldObjectInterface>(objects);
  }
  return result;
}

std::vector<const AgentInterface *> WorldImplementation::GetAgentsInRangeOfJunctionConnection(
    std::string connectingRoadId, units::length::meter_t range) const
{
  auto movingObjects = worldDataQuery.GetMovingObjectsInRangeOfJunctionConnection(connectingRoadId, range);
  return get_transformed<AgentInterface>(movingObjects);
}

units::length::meter_t WorldImplementation::GetDistanceToConnectorEntrance(
    /*const ObjectPosition position,*/ std::string intersectingConnectorId,
    int intersectingLaneId,
    std::string ownConnectorId) const
{
  return worldDataQuery.GetDistanceUntilObjectEntersConnector(
      /*position,*/ intersectingConnectorId, intersectingLaneId, ownConnectorId);
}

units::length::meter_t WorldImplementation::GetDistanceToConnectorDeparture(
    /*const ObjectPosition position,*/ std::string intersectingConnectorId,
    int intersectingLaneId,
    std::string ownConnectorId) const
{
  return worldDataQuery.GetDistanceUntilObjectLeavesConnector(
      /*position,*/ intersectingConnectorId, intersectingLaneId, ownConnectorId);
}

std::optional<Position> WorldImplementation::LaneCoord2WorldCoord(units::length::meter_t distanceOnLane,
                                                                  units::length::meter_t offset,
                                                                  std::string roadId,
                                                                  int laneId) const
{
  OWL::CLane &lane = worldDataQuery.GetLaneByOdId(roadId, laneId, distanceOnLane);
  if (!lane.Exists())
  {
    return std::nullopt;
  }
  return worldDataQuery.GetPositionByDistanceAndLane(lane, distanceOnLane, offset);
}

GlobalRoadPositions WorldImplementation::WorldCoord2LaneCoord(units::length::meter_t x,
                                                              units::length::meter_t y,
                                                              units::angle::radian_t heading) const
{
  return localizer.Locate({x, y}, heading);
}

bool WorldImplementation::IsSValidOnLane(
    std::string roadId,
    int laneId,
    units::length::meter_t distance)  //when necessary optional parameter with reference to get point
{
  return worldDataQuery.IsSValidOnLane(roadId, laneId, distance);
}

bool WorldImplementation::IsDirectionalRoadExisting(const std::string &roadId, bool inOdDirection) const
{
  return worldData.GetRoadGraphVertexMapping().find(RouteElement{roadId, inOdDirection})
      != worldData.GetRoadGraphVertexMapping().end();
}

bool WorldImplementation::IsLaneTypeValid(const std::string &roadId,
                                          const int laneId,
                                          const units::length::meter_t distanceOnLane,
                                          const LaneTypes &validLaneTypes)
{
  const auto &laneType = worldDataQuery.GetLaneByOdId(roadId, laneId, distanceOnLane).GetLaneType();

  if (std::find(validLaneTypes.begin(), validLaneTypes.end(), laneType) == validLaneTypes.end())
  {
    return false;
  }

  return true;
}

units::curvature::inverse_meter_t WorldImplementation::GetLaneCurvature(std::string roadId,
                                                                        int laneId,
                                                                        units::length::meter_t position) const
{
  auto &lane = worldDataQuery.GetLaneByOdId(roadId, laneId, position);
  if (!lane.Exists())
  {
    return 0.0_i_m;
  }
  return lane.GetCurvature(position);
}

RouteQueryResult<std::optional<units::curvature::inverse_meter_t>> WorldImplementation::GetLaneCurvature(
    const RoadGraph &roadGraph,
    RoadGraphVertex startNode,
    int laneId,
    units::length::meter_t position,
    units::length::meter_t distance) const
{
  const auto laneMultiStream = worldDataQuery.CreateLaneMultiStream(roadGraph, startNode, laneId, position);
  const auto positionOnStream = laneMultiStream->GetPositionByVertexAndS(startNode, position);
  return worldDataQuery.GetLaneCurvature(*laneMultiStream, positionOnStream + distance);
}

units::length::meter_t WorldImplementation::GetLaneWidth(std::string roadId,
                                                         int laneId,
                                                         units::length::meter_t position) const
{
  auto &lane = worldDataQuery.GetLaneByOdId(roadId, laneId, position);
  if (!lane.Exists())
  {
    return 0.0_m;
  }
  return lane.GetWidth(position);
}

RouteQueryResult<std::optional<units::length::meter_t>> WorldImplementation::GetLaneWidth(
    const RoadGraph &roadGraph,
    RoadGraphVertex startNode,
    int laneId,
    units::length::meter_t position,
    units::length::meter_t distance) const
{
  const auto laneMultiStream = worldDataQuery.CreateLaneMultiStream(roadGraph, startNode, laneId, position);
  const auto positionOnStream = laneMultiStream->GetPositionByVertexAndS(startNode, position);
  return worldDataQuery.GetLaneWidth(*laneMultiStream, positionOnStream + distance);
}

units::angle::radian_t WorldImplementation::GetLaneDirection(std::string roadId,
                                                             int laneId,
                                                             units::length::meter_t position) const
{
  auto &lane = worldDataQuery.GetLaneByOdId(roadId, laneId, position);
  if (!lane.Exists())
  {
    return 0.0_rad;
  }
  return lane.GetDirection(position);
}

RouteQueryResult<std::optional<units::angle::radian_t>> WorldImplementation::GetLaneDirection(
    const RoadGraph &roadGraph,
    RoadGraphVertex startNode,
    int laneId,
    units::length::meter_t position,
    units::length::meter_t distance) const
{
  const auto laneMultiStream = worldDataQuery.CreateLaneMultiStream(roadGraph, startNode, laneId, position);
  const auto positionOnStream = laneMultiStream->GetPositionByVertexAndS(startNode, position);
  return worldDataQuery.GetLaneDirection(*laneMultiStream, positionOnStream + distance);
}

RouteQueryResult<units::length::meter_t> WorldImplementation::GetDistanceToEndOfLane(
    const RoadGraph &roadGraph,
    RoadGraphVertex startNode,
    int laneId,
    units::length::meter_t initialSearchDistance,
    units::length::meter_t maximumSearchLength) const
{
  return GetDistanceToEndOfLane(
      roadGraph,
      startNode,
      laneId,
      initialSearchDistance,
      maximumSearchLength,
      {LaneType::Driving, LaneType::Exit, LaneType::OnRamp, LaneType::OffRamp, LaneType::Stop});
}

RouteQueryResult<units::length::meter_t> WorldImplementation::GetDistanceToEndOfLane(
    const RoadGraph &roadGraph,
    RoadGraphVertex startNode,
    int laneId,
    units::length::meter_t initialSearchDistance,
    units::length::meter_t maximumSearchLength,
    const LaneTypes &laneTypes) const
{
  auto laneMultiStream = worldDataQuery.CreateLaneMultiStream(roadGraph, startNode, laneId, initialSearchDistance);
  auto initialPositionOnStream = laneMultiStream->GetPositionByVertexAndS(startNode, initialSearchDistance);
  return worldDataQuery.GetDistanceToEndOfLane(
      *laneMultiStream, initialPositionOnStream, maximumSearchLength, laneTypes);
}

LaneSections WorldImplementation::GetLaneSections(const std::string &roadId) const
{
  LaneSections result;

  const auto &road = worldDataQuery.GetRoadByOdId(roadId);
  for (const auto &section : road->GetSections())
  {
    LaneSection laneSection;
    laneSection.startS = section->GetSOffset();
    laneSection.endS = laneSection.startS + section->GetLength();

    for (const auto &lane : section->GetLanes())
    {
      laneSection.laneIds.push_back(lane->GetOdId());
    }

    result.push_back(laneSection);
  }

  return result;
}

bool WorldImplementation::IntersectsWithAgent(units::length::meter_t x,
                                              units::length::meter_t y,
                                              units::angle::radian_t rotation,
                                              units::length::meter_t length,
                                              units::length::meter_t width,
                                              units::length::meter_t center)
{
  polygon_t polyNewAgent = World::Localization::GetBoundingBox(x, y, length, width, rotation, center);

  for (std::pair<int, AgentInterface *> agent : GetAgents())
  {
    polygon_t polyAgent = agent.second->GetBoundingBox2D();

    bool intersects = bg::intersects(polyNewAgent, polyAgent);
    if (intersects)
    {
      return true;
    }
  }

  return false;
}

std::optional<Position> WorldImplementation::RoadCoord2WorldCoord(RoadPosition roadCoord, std::string roadID) const
{
  const auto &[lane, tOnLane] = worldDataQuery.GetLaneByOffset(roadID, roadCoord.t, roadCoord.s);
  if (!lane.Exists())
  {
    return std::nullopt;
  }
  return worldDataQuery.GetPositionByDistanceAndLane(lane, roadCoord.s, tOnLane);
}

units::length::meter_t WorldImplementation::GetRoadLength(const std::string &roadId) const
{
  return worldDataQuery.GetRoadByOdId(roadId)->GetLength();
}

void WorldImplementation::InitTrafficObjects()
{
  assert(trafficObjects.size() == 0);

  for (auto &baseTrafficObject : worldData.GetStationaryObjects())
  {
    TrafficObjectInterface *trafficObject = baseTrafficObject.second->GetLink<TrafficObjectInterface>();
    trafficObjects.push_back(trafficObject);
    worldObjects.push_back(trafficObject);
  }
}

RouteQueryResult<std::optional<units::length::meter_t>> WorldImplementation::GetDistanceBetweenObjects(
    const RoadGraph &roadGraph,
    RoadGraphVertex startNode,
    units::length::meter_t ownPosition,
    const GlobalRoadPositions &target) const
{
  const auto roadStream = worldDataQuery.CreateRoadMultiStream(roadGraph, startNode);
  const auto ownStreamPosition = roadStream->GetPositionByVertexAndS(startNode, ownPosition);
  return worldDataQuery.GetDistanceBetweenObjects(*roadStream, ownStreamPosition, target);
}

RadioInterface &WorldImplementation::GetRadio()
{
  return radio;
}

uint64_t WorldImplementation::GetUniqueLaneId(std::string roadId, int laneId, units::length::meter_t position) const
{
  auto &lane = worldDataQuery.GetLaneByOdId(roadId, laneId, position);
  return lane.GetId();
}
