/********************************************************************************
 * Copyright (c) 2018 AMFD GmbH
 *               2016-2021 ITK Engineering GmbH
 *               2017-2020 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2023 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  AgentAdapter.h
//! @brief This file implements the wrapper for the agent so it can
//!        interact with the world.
//!        It permits a simple implementation of delegation concepts
//!        (composition vs. inheritance).
//-----------------------------------------------------------------------------

#pragma once

#include <functional>

#include <QtGlobal>

#include "Localization.h"
#include "WorldData.h"
#include "WorldDataQuery.h"
#include "WorldObjectAdapter.h"
#include "egoAgent.h"
#include "include/agentInterface.h"
#include "include/callbackInterface.h"
#include "include/stochasticsInterface.h"
#include "include/trafficObjectInterface.h"
#include "include/worldInterface.h"

constexpr double zeroBaseline = 1e-9;  ///< values smaller than this are considered zero

/*!
 * \brief The AgentAdapter class
 * This class is a adapter for the communication between the framework and world.
 */
class AgentAdapter final : public WorldObjectAdapter, public AgentInterface
{
public:
  /// Name of the current module
  const std::string MODULENAME = "AGENTADAPTER";

  /// @brief AgentAdapter constructor
  /// @param mo           Refernce to the moving object
  /// @param world        Pointer to the world interface
  /// @param callbacks    Pointer to the call back interface
  /// @param localizer    Reference to the localizer
  AgentAdapter(OWL::Interfaces::MovingObject &mo,
               WorldInterface *world,
               const CallbackInterface *callbacks,
               const World::Localization::Localizer &localizer);

  ~AgentAdapter() override;

  void InitParameter(const AgentBuildInstructions &agentBuildInstructions) override;

  ObjectTypeOSI GetType() const override { return ObjectTypeOSI::Vehicle; }

  int GetId() const override
  {
    return static_cast<int>(GetBaseTrafficObject().GetId());
    ;
  }

  EgoAgentInterface &GetEgoAgent() override { return egoAgent; }

  std::string GetVehicleModelType() const override { return vehicleModelType; }

  std::string GetScenarioName() const override { return objectName; }

  std::string GetDriverProfileName() const override { return driverProfileName; }

  units::velocity::meters_per_second_t GetSpeedGoalMin() const override { return speedGoalMin; }

  units::angular_velocity::revolutions_per_minute_t GetEngineSpeed() const override { return engineSpeed; }

  units::length::meter_t GetDistanceReferencePointToLeadingEdge() const override
  {
    return GetBaseTrafficObject().GetDistanceReferencePointToLeadingEdge();
  }

  int GetGear() const override { return currentGear; }

  double GetEffAccelPedal() const override { return accelPedal; }

  double GetEffBrakePedal() const override { return brakePedal; }

  units::angle::radian_t GetSteeringWheelAngle() const override { return steeringWheelAngle; }

  units::acceleration::meters_per_second_squared_t GetMaxAcceleration() const override { return maxAcceleration; }

  units::acceleration::meters_per_second_squared_t GetMaxDeceleration() const override { return maxDeceleration; }

  bool GetHeadLight() const override;

  bool GetHighBeamLight() const override;

  bool GetHorn() const override { return hornSwitch; }

  bool GetFlasher() const override { return flasherSwitch; }

  std::vector<std::string> GetRoads(ObjectPoint point) const override;

  LightState GetLightState() const override;

  const RoadIntervals &GetTouchedRoads() const override { return locateResult.touchedRoads; }

  Common::Vector2d<units::length::meter_t> GetAbsolutePosition(const ObjectPoint &objectPoint) const override;

  const GlobalRoadPositions &GetRoadPosition(const ObjectPoint &point) const override;

  void UpdateCollision(std::pair<ObjectTypeOSI, int> collisionPartner) override;

  std::vector<std::pair<ObjectTypeOSI, int>> GetCollisionPartners() const override { return collisionPartners; }

  PostCrashVelocity GetPostCrashVelocity() const override { return postCrashVelocity; }

  std::shared_ptr<const mantle_api::EntityProperties> GetVehicleModelParameters() const override
  {
    return vehicleModelParameters;
  }

  units::length::meter_t GetWheelbase() const override
  {
    if (std::dynamic_pointer_cast<const mantle_api::VehicleProperties>(vehicleModelParameters) != nullptr)
    {
      auto vehicleProperties = std::dynamic_pointer_cast<const mantle_api::VehicleProperties>(vehicleModelParameters);
      return vehicleProperties->front_axle.bb_center_to_axle_center.x
           - vehicleProperties->rear_axle.bb_center_to_axle_center.x;
    }
    else
    {
      // Currently, AgentAdapter and Dynamics cannot handle pedestrians properly
      // setting the required default value here for now for compatibility with cars
      return 1_m;
    }
  }

  void SetPostCrashVelocity(PostCrashVelocity postCrashVelocity) override
  {
    this->postCrashVelocity = postCrashVelocity;
  }

  void SetPositionX(units::length::meter_t value) override
  {
    world->QueueAgentUpdate([this, value]() { GetBaseTrafficObject().SetX(value); });
  }

  void SetPositionY(units::length::meter_t value) override
  {
    world->QueueAgentUpdate([this, value]() { GetBaseTrafficObject().SetY(value); });
  }

  void SetVelocity(units::velocity::meters_per_second_t value) override
  {
    world->QueueAgentUpdate([this, value]() { GetBaseTrafficObject().SetAbsVelocity(value); });
  }

  void SetVelocityVector(const mantle_api::Vec3<units::velocity::meters_per_second_t> &velocity) override
  {
    world->QueueAgentUpdate(
        [this, velocity]()
        {
          units::length::meter_t longitudinal = GetLongitudinal(ObjectPointPredefined::Center);
          units::length::meter_t lateral = GetLateral(ObjectPointPredefined::Center);
          Common::Vector2d<units::velocity::meters_per_second_t> rotation{
              0.0_mps, 0.0_mps};  //! velocity from rotation of vehicle around reference point
          if (longitudinal != 0.0_m || lateral != 0.0_m)
          {
            rotation.x = -lateral * GetYawRate()/1_rad;
            rotation.y = longitudinal * GetYawRate()/1_rad;
          }

          GetBaseTrafficObject().SetAbsVelocity({velocity.x + rotation.x, velocity.y + rotation.y, velocity.z});
        });
  }

  void SetAcceleration(units::acceleration::meters_per_second_squared_t value) override
  {
    world->QueueAgentUpdate([this, value]() { GetBaseTrafficObject().SetAbsAcceleration(value); });
  }

  void SetAccelerationVector(
      const mantle_api::Vec3<units::acceleration::meters_per_second_squared_t> &acceleration) override
  {
    world->QueueAgentUpdate(
        [this, acceleration]()
        {
          units::length::meter_t longitudinal = GetLongitudinal(ObjectPointPredefined::Center);
          units::length::meter_t lateral = GetLateral(ObjectPointPredefined::Center);
          Common::Vector2d<units::acceleration::meters_per_second_squared_t> rotation{
              0.0_mps_sq, 0.0_mps_sq};  //! acceleration from rotation of vehicle around reference point
          if (longitudinal != 0.0_m || lateral != 0.0_m)
          {
            rotation.x
                = (-lateral * GetYawAcceleration() - units::math::pow<2>(GetYawRate()) * longitudinal / 1_rad) / 1_rad;
            rotation.y
                = (longitudinal * GetYawAcceleration() - units::math::pow<2>(GetYawRate()) * lateral / 1_rad) / 1_rad;
          }
          GetBaseTrafficObject().SetAbsAcceleration(
              {acceleration.x + rotation.x, acceleration.y + rotation.y, acceleration.z});
        });
  }

  void SetYaw(units::angle::radian_t value) override
  {
    world->QueueAgentUpdate([this, value]() { UpdateYaw(value); });
  }

  void SetRoll(units::angle::radian_t value) override
  {
    world->QueueAgentUpdate([this, value]() { UpdateRoll(value); });
  }

  void SetYawRate(units::angular_velocity::radians_per_second_t value) override
  {
    world->QueueAgentUpdate(
        [this, value]()
        {
          auto orientationRate = GetBaseTrafficObject().GetAbsOrientationRate();
          orientationRate.yaw = value;
          GetBaseTrafficObject().SetAbsOrientationRate(orientationRate);
        });
  }
  void SetYawAcceleration(units::angular_acceleration::radians_per_second_squared_t value) override
  {
    world->QueueAgentUpdate(
        [this, value]()
        {
          OWL::Primitive::AbsOrientationAcceleration orientationAcceleration
              = GetBaseTrafficObject().GetAbsOrientationAcceleration();
          orientationAcceleration.yawAcceleration = value;
          GetBaseTrafficObject().SetAbsOrientationAcceleration(orientationAcceleration);
        });
  }

  void SetCentripetalAcceleration(units::acceleration::meters_per_second_squared_t value) override
  {
    world->QueueAgentUpdate(
        [this, value]()
        {
          centripetalAcceleration = value;

          // Code seems to work incorrectly here, further investigation needed
          // OWL::Primitive::AbsAcceleration absAcceleration = GetBaseTrafficObject().GetAbsAcceleration();
          // OWL::Primitive::AbsOrientation absOrientation = GetBaseTrafficObject().GetAbsOrientation();

          // Common::Vector2d<units::velocity::meters_per_second_t vec(absAcceleration.ax, absAcceleration.ay);

          // // Rotate reference frame from groundTruth to car and back
          // vec.Rotate(-absOrientation.yaw);
          // vec.y = value;
          // vec.Rotate(absOrientation.yaw);

          // absAcceleration.ax = vec.x;
          // absAcceleration.ay = vec.y;

          // GetBaseTrafficObject().SetAbsAcceleration(absAcceleration);
        });
  }

  void SetTangentialAcceleration(units::acceleration::meters_per_second_squared_t value) override
  {
    world->QueueAgentUpdate(
        [this, value]()
        {
          tangentialAcceleration = value;

          // Code seems to work incorrectly here, further investigation needed
          // OWL::Primitive::AbsAcceleration absAcceleration = GetBaseTrafficObject().GetAbsAcceleration();
          // OWL::Primitive::AbsOrientation absOrientation = GetBaseTrafficObject().GetAbsOrientation();

          // Common::Vector2d<units::velocity::meters_per_second_t vec(absAcceleration.ax, absAcceleration.ay);

          // // Rotate reference frame from groundTruth to car and back
          // vec.Rotate(-absOrientation.yaw);
          // vec.x = value;
          // vec.Rotate(absOrientation.yaw);

          // absAcceleration.ax = vec.x;
          // absAcceleration.ay = vec.y;

          // GetBaseTrafficObject().SetAbsAcceleration(absAcceleration);
        });
  }

  void SetWheelsRotationRateAndOrientation(units::angle::radian_t steeringWheelAngle,
                                           units::velocity::meters_per_second_t velocity,
                                           units::acceleration::meters_per_second_squared_t acceleration) override
  {
    //TODO Move this to Dynamics Modules
    if (vehicleModelParameters->type == mantle_api::EntityType::kPedestrian) return;

    auto dTime = (velocity - previousVelocity) / acceleration;
    previousVelocity = velocity;
    auto wheelRadiusFront = std::dynamic_pointer_cast<const mantle_api::VehicleProperties>(vehicleModelParameters)
                                ->front_axle.wheel_diameter
                          / 2.0;
    auto wheelRadiusRear = std::dynamic_pointer_cast<const mantle_api::VehicleProperties>(vehicleModelParameters)
                               ->rear_axle.wheel_diameter
                         / 2.0;
    try
    {
      auto steeringToWheelYawRatio
          = std::stod(std::dynamic_pointer_cast<const mantle_api::VehicleProperties>(vehicleModelParameters)
                          ->properties.at("SteeringRatio"));
      GetBaseTrafficObject().SetWheelsRotationRateAndOrientation(velocity, wheelRadiusFront, wheelRadiusRear, dTime);
      GetBaseTrafficObject().SetFrontAxleSteeringYaw(steeringWheelAngle * steeringToWheelYawRatio);
    }
    catch (const std::out_of_range &e)
    {
      Log(CbkLogLevel::Error, __FILE__, __LINE__, "Agent does not have SteeringRatio");
    }
  }

  void SetWheelRotationRate(int axleIndex, int wheelIndex, double rotationRate) override
  {
    if (vehicleModelParameters->type == mantle_api::EntityType::kPedestrian) return;

    try
    {
      GetBaseTrafficObject().SetWheelRotationRate(axleIndex, wheelIndex, rotationRate);
    }
    catch (const std::out_of_range &e)
    {
      Log(CbkLogLevel::Error, __FILE__, __LINE__, "Agent does not have selected Wheel");
    }
  }

  double GetWheelRotationRate(int axleIndex, int wheelIndex) override
  {
    if (vehicleModelParameters->type == mantle_api::EntityType::kPedestrian) return 0.0;

    return GetBaseTrafficObject().GetWheelData(axleIndex, wheelIndex)->rotation_rate.value();
  }

  void SetDistanceTraveled(units::length::meter_t value) override
  {
    world->QueueAgentUpdate([this, value]() { distanceTraveled = value; });
  }

  void SetVehicleModelParameter(const std::shared_ptr<const mantle_api::EntityProperties> parameter) override
  {
    world->QueueAgentUpdate([this, parameter]() { UpdateEntityModelParameter(parameter); });
  }

  void SetMaxAcceleration(units::acceleration::meters_per_second_squared_t value) override
  {
    world->QueueAgentUpdate([this, value]() { maxAcceleration = value; });
  }

  void SetEngineSpeed(units::angular_velocity::revolutions_per_minute_t value) override
  {
    world->QueueAgentUpdate([this, value]() { engineSpeed = value; });
  }

  void SetMaxDeceleration(units::acceleration::meters_per_second_squared_t maxDeceleration) override
  {
    world->QueueAgentUpdate([this, maxDeceleration]() { this->maxDeceleration = maxDeceleration; });
  }

  void SetGear(int gear) override
  {
    world->QueueAgentUpdate([this, gear]() { currentGear = gear; });
  }

  void SetEffAccelPedal(double percent) override
  {
    world->QueueAgentUpdate([this, percent]() { accelPedal = percent; });
  }

  void SetEffBrakePedal(double percent) override
  {
    world->QueueAgentUpdate([this, percent]() { brakePedal = percent; });
  }

  void SetSteeringWheelAngle(units::angle::radian_t steeringWheelAngle) override
  {
    world->QueueAgentUpdate(
        [this, steeringWheelAngle]()
        {
          this->steeringWheelAngle = steeringWheelAngle;
          GetBaseTrafficObject().SetSteeringWheelAngle(steeringWheelAngle);
        });
  }

  void SetHeadLight(bool headLight) override
  {
    world->QueueAgentUpdate([this, headLight]() { GetBaseTrafficObject().SetHeadLight(headLight); });
  }

  void SetHighBeamLight(bool highBeam) override
  {
    world->QueueAgentUpdate([this, highBeam]() { GetBaseTrafficObject().SetHighBeamLight(highBeam); });
  }

  void SetHorn(bool horn) override
  {
    world->QueueAgentUpdate([this, horn]() { hornSwitch = horn; });
  }

  void SetFlasher(bool flasher) override
  {
    world->QueueAgentUpdate([this, flasher]() { flasherSwitch = flasher; });
  }

  units::angular_velocity::radians_per_second_t GetYawRate() const override
  {
    return GetBaseTrafficObject().GetAbsOrientationRate().yaw;
  }

  units::angular_acceleration::radians_per_second_squared_t GetYawAcceleration() const override
  {
    return GetBaseTrafficObject().GetAbsOrientationAcceleration().yawAcceleration;
  }

  units::acceleration::meters_per_second_squared_t GetCentripetalAcceleration() const override
  {
    return centripetalAcceleration;

    // Code seems to work incorrectly here, further investigation needed
    // OWL::Primitive::AbsAcceleration absAcceleration = GetBaseTrafficObject().GetAbsAcceleration();
    // OWL::Primitive::AbsOrientation absOrientation = GetBaseTrafficObject().GetAbsOrientation();

    // Common::Vector2d<units::velocity::meters_per_second_t vec(absAcceleration.ax, absAcceleration.ay);

    // // Rotate reference frame from groundTruth to car
    // vec.Rotate(-absOrientation.yaw);

    // return vec.y;
  }

  units::acceleration::meters_per_second_squared_t GetTangentialAcceleration() const override
  {
    return tangentialAcceleration;

    // Code seems to work incorrectly here, further investigation needed
    // OWL::Primitive::AbsAcceleration absAcceleration = GetBaseTrafficObject().GetAbsAcceleration();
    // OWL::Primitive::AbsOrientation absOrientation = GetBaseTrafficObject().GetAbsOrientation();

    // Common::Vector2d<units::velocity::meters_per_second_t vec(absAcceleration.ax, absAcceleration.ay);

    // // Rotate reference frame from groundTruth to car
    // vec.Rotate(-absOrientation.yaw);

    // return vec.x;
  }

  bool Locate() override;

  void Unlocate() override;

  bool Update() override;

  void SetBrakeLight(bool brakeLightStatus) override;

  bool GetBrakeLight() const override;

  AgentCategory GetAgentCategory() const override;

  std::string GetAgentTypeName() const override;

  void SetIndicatorState(IndicatorState indicatorState) override;

  IndicatorState GetIndicatorState() const override;

  bool IsLeavingWorld() const override;

  bool IsAgentInWorld() const override;

  void SetPosition(Position pos) override;

  units::length::meter_t GetDistanceTraveled() const override { return distanceTraveled; }

  bool IsEgoAgent() const override;

  /// @brief If the agent is on given road
  /// @param road Reference to the road
  /// @return True, if present
  bool OnRoad(const OWL::Interfaces::Road &road) const;

  /// @brief If the agent is on given lane
  /// @param lane Reference to the lane
  /// @return True, if present
  bool OnLane(const OWL::Interfaces::Lane &lane) const;

  Common::Vector2d<units::velocity::meters_per_second_t> GetVelocity(ObjectPoint point
                                                                     = ObjectPointPredefined::Reference) const override;

  Common::Vector2d<units::acceleration::meters_per_second_squared_t> GetAcceleration(
      ObjectPoint point = ObjectPointPredefined::Reference) const override;

  const openpass::sensors::Parameters &GetSensorParameters() const override { return sensorParameters; }

  void SetSensorParameters(openpass::sensors::Parameters sensorParameters) override
  {
    this->sensorParameters = sensorParameters;
  }

  units::length::meter_t GetDistanceToConnectorEntrance(std::string intersectingConnectorId,
                                                        int intersectingLaneId,
                                                        std::string ownConnectorId) const override;

  units::length::meter_t GetDistanceToConnectorDeparture(std::string intersectingConnectorId,
                                                         int intersectingLaneId,
                                                         std::string ownConnectorId) const override;

protected:
  //-----------------------------------------------------------------------------
  //! Provides callback to LOG() macro
  //!
  //! @param[in]     logLevel    Importance of log
  //! @param[in]     file        Name of file where log is called
  //! @param[in]     line        Line within file where log is called
  //! @param[in]     message     Message to log
  //-----------------------------------------------------------------------------
  void Log(CbkLogLevel logLevel, const char *file, int line, const std::string &message) const
  {
    if (callbacks)
    {
      callbacks->Log(logLevel, file, line, message);
    }
  }

private:
  WorldInterface *world;
  const CallbackInterface *callbacks;
  const World::Localization::Localizer &localizer;
  EgoAgent egoAgent;

  OWL::Interfaces::MovingObject &GetBaseTrafficObject()
  {
    return *(static_cast<OWL::Interfaces::MovingObject *>(&baseTrafficObject));
  }

  OWL::Interfaces::MovingObject &GetBaseTrafficObject() const
  {
    return *(static_cast<OWL::Interfaces::MovingObject *>(&baseTrafficObject));
  }

  void UpdateEntityModelParameter(const std::shared_ptr<const mantle_api::EntityProperties> properties)
  {
    mantle_api::Dimension3 dimension;
    dimension.width = properties->bounding_box.dimension.width;
    dimension.length = properties->bounding_box.dimension.length;
    dimension.height = properties->bounding_box.dimension.height;

    GetBaseTrafficObject().SetDimension(dimension);
    GetBaseTrafficObject().SetType(properties->type);

    if (properties->type == mantle_api::EntityType::kVehicle)
    {
      auto vehicleProperties = std::dynamic_pointer_cast<const mantle_api::VehicleProperties>(properties);
      GetBaseTrafficObject().SetBoundingBoxCenterToRear(vehicleProperties->rear_axle.bb_center_to_axle_center.x,
                                                        0.0_m,
                                                        vehicleProperties->rear_axle.bb_center_to_axle_center.z);
      GetBaseTrafficObject().SetBoundingBoxCenterToFront(vehicleProperties->front_axle.bb_center_to_axle_center.x,
                                                         0.0_m,
                                                         vehicleProperties->front_axle.bb_center_to_axle_center.z);
      GetBaseTrafficObject().SetVehicleClassification(vehicleProperties->classification);
    }
    else
    {
      GetBaseTrafficObject().SetBoundingBoxCenterToRear(
          -properties->bounding_box.geometric_center.x, 0.0_m, -properties->bounding_box.geometric_center.z);
    }
    GenerateWheels(properties);
    vehicleModelParameters = properties;
  }

  void GenerateWheels(const std::shared_ptr<const mantle_api::EntityProperties> properties)
  {
    if (properties->type == mantle_api::EntityType::kPedestrian)
    {
      return;
    }

    auto vehicleProperties = std::dynamic_pointer_cast<const mantle_api::VehicleProperties>(properties);
    std::vector<mantle_api::Axle> axles{vehicleProperties->front_axle, vehicleProperties->rear_axle};
    for (int axle = 0; axle < 2; axle++)
    {
      OWL::WheelData newWheel;
      newWheel.axle = axle;
      newWheel.wheelRadius = axles[axle].wheel_diameter / 2.0;
      newWheel.rim_radius = units::length::meter_t(std::numeric_limits<double>::signaling_NaN());
      newWheel.width = units::length::meter_t(std::numeric_limits<double>::signaling_NaN());
      newWheel.rotation_rate
          = units::angular_velocity::revolutions_per_minute_t(std::numeric_limits<double>::signaling_NaN());

      //Assuming that the center of the wheels are located at the axles
      newWheel.position.x
          = axle == 0
              ? (vehicleProperties->front_axle.bb_center_to_axle_center.x - properties->bounding_box.geometric_center.x)
              : (vehicleProperties->rear_axle.bb_center_to_axle_center.x - properties->bounding_box.geometric_center.x);

      newWheel.position.z
          = axle == 0
              ? (vehicleProperties->front_axle.bb_center_to_axle_center.z - properties->bounding_box.geometric_center.z)
              : (vehicleProperties->rear_axle.bb_center_to_axle_center.z - properties->bounding_box.geometric_center.z);

      if (properties->type == mantle_api::EntityType::kVehicle)
      {
        if (vehicleProperties->classification == mantle_api::VehicleClass::kMedium_car)
        {
          newWheel.position.y
              = -vehicleProperties->front_axle.track_width / 2.0f
              - properties->bounding_box.geometric_center.y;  //assume that the wheels are at the end of the axle
          newWheel.index = 0;
          GetBaseTrafficObject().AddWheel(newWheel);
          newWheel.position.y
              = vehicleProperties->front_axle.track_width / 2.0f
              - properties->bounding_box.geometric_center.y;  //assume that the wheels are at the end of the axle
          newWheel.index = 1;
          GetBaseTrafficObject().AddWheel(newWheel);
        }
        else if (vehicleProperties->classification == mantle_api::VehicleClass::kBicycle
                 || vehicleProperties->classification == mantle_api::VehicleClass::kMotorbike)
        {
          //assuming that the positionY is in the middle of the two wheel vehicle
          newWheel.position.y = 0.0_m;
          newWheel.index = 0;
          GetBaseTrafficObject().AddWheel(newWheel);
        }
      }
    }

    vehicleModelParameters = properties;
  }

  void UpdateYaw(units::angle::radian_t yawAngle)
  {
    auto orientation = baseTrafficObject.GetAbsOrientation();
    orientation.yaw = yawAngle;
    GetBaseTrafficObject().SetAbsOrientation(orientation);
  }

  void UpdateRoll(units::angle::radian_t rollAngle)
  {
    auto orientation = baseTrafficObject.GetAbsOrientation();
    orientation.roll = rollAngle;
    GetBaseTrafficObject().SetAbsOrientation(orientation);
  }

  //-----------------------------------------------------------------------------
  //! Initialize the ego vehicle object inside the drivingView.
  //-----------------------------------------------------------------------------
  void InitEgoVehicle();

  //-----------------------------------------------------------------------------
  //! Update the ego vehicle object inside the drivingView.
  //-----------------------------------------------------------------------------
  void UpdateEgoVehicle();

  //! Returns the longitudinal position of the ObjectPoint (must not be of type ObjectPointRelative)
  units::length::meter_t GetLongitudinal(const ObjectPoint &objectPoint) const;

  //! Returns the lateral position of the ObjectPoint (must not be of type ObjectPointRelative)
  units::length::meter_t GetLateral(const ObjectPoint &objectPoint) const;

  struct LaneObjParameters
  {
    units::length::meter_t distance;
    units::angle::radian_t relAngle;
    units::length::meter_t latPosition;
    Common::Vector2d<units::length::meter_t> upperLeftCoord;
    Common::Vector2d<units::length::meter_t> upperRightCoord;
    Common::Vector2d<units::length::meter_t> lowerLeftCoord;
    Common::Vector2d<units::length::meter_t> lowerRightCoord;
  };

  bool hornSwitch = false;
  bool flasherSwitch = false;
  int currentGear = 0;
  units::acceleration::meters_per_second_squared_t maxAcceleration{0.0};
  units::acceleration::meters_per_second_squared_t maxDeceleration{0.0};
  double accelPedal = 0.;
  double brakePedal = 0.;
  units::angle::radian_t steeringWheelAngle{0.0};
  units::acceleration::meters_per_second_squared_t centripetalAcceleration{0.0};
  units::acceleration::meters_per_second_squared_t tangentialAcceleration{0.0};
  units::angular_acceleration::radians_per_second_squared_t yawAcceleration{0.0};
  units::angular_velocity::revolutions_per_minute_t engineSpeed{0.0};
  units::length::meter_t distanceTraveled{0.0};
  units::velocity::meters_per_second_t previousVelocity{0.0};

  mutable World::Localization::Result locateResult;
  mutable std::vector<GlobalRoadPosition> boundaryPoints;

  std::vector<std::pair<ObjectTypeOSI, int>> collisionPartners;
  PostCrashVelocity postCrashVelocity{};
  bool isValid = true;

  AgentCategory agentCategory;
  std::string agentTypeName;
  std::string vehicleModelType;
  std::string driverProfileName;
  std::string objectName;
  std::shared_ptr<const mantle_api::EntityProperties> vehicleModelParameters;

  units::velocity::meters_per_second_t speedGoalMin;

  bool completlyInWorld = false;

  openpass::sensors::Parameters sensorParameters;
};
