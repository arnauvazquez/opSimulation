/********************************************************************************
 * Copyright (c) 2018-2021 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <gmock/gmock.h>

#include "WorldData.h"

namespace osi3
{
class SensorView;
class SensorViewConfiguration;
}  // namespace osi3

namespace OWL::Fakes
{
class WorldData : public OWL::Interfaces::WorldData
{
public:
  MOCK_METHOD0(Clear, void());
  MOCK_METHOD0(Reset, void());
  MOCK_CONST_METHOD0(GetOsiGroundTruth, const osi3::GroundTruth &());
  MOCK_CONST_METHOD0(GetRoads, const std::unordered_map<std::string, OWL::Interfaces::Road *> &());
  MOCK_METHOD1(AddMovingObject, OWL::Interfaces::MovingObject &(const Id));
  MOCK_METHOD2(AddStationaryObject, OWL::Interfaces::StationaryObject &(const Id, void *linkedObject));
  MOCK_METHOD1(RemoveMovingObjectById, void(OWL::Id id));
  MOCK_CONST_METHOD0(GetLaneIdMapping, const std::unordered_map<OWL::Id, OWL::OdId> &());
  MOCK_CONST_METHOD0(GetLanes, const IdMapping<OWL::Interfaces::Lane> &());
  MOCK_CONST_METHOD1(GetLane, const Lane &(Id id));
  MOCK_METHOD6(AddLane,
               void(const Id,
                    const Id logicalLaneId,
                    RoadLaneSectionInterface &odSection,
                    const RoadLaneInterface &odLane,
                    const std::vector<OWL::Id> laneBoundaries,
                    const std::vector<Id> logicalLaneBoundaries));
  MOCK_METHOD4(
      AddLaneBoundary,
      OWL::Id(const Id, const RoadLaneRoadMark &odLaneRoadMark, double sectionStart, OWL::LaneMarkingSide side));
  MOCK_METHOD3(AddLogicalLaneBoundary,
               Id(const Id id, const RoadLaneRoadMark &odLaneRoadMark, units::length::meter_t sectionStart));
  MOCK_METHOD1(AddReferenceLine, Id(const Id id));
  MOCK_METHOD2(AddSection, void(const RoadInterface &, const RoadLaneSectionInterface &));
  MOCK_METHOD1(AddRoad, void(const RoadInterface &));
  MOCK_METHOD1(AddJunction, void(const JunctionInterface *odJunction));
  MOCK_METHOD2(AddJunctionConnection, void(const JunctionInterface *odJunction, const RoadInterface &odRoad));
  MOCK_METHOD9(AddLaneGeometryPoint,
               void(const RoadLaneInterface &,
                    const Common::Vector2d<units::length::meter_t> &,
                    const Common::Vector2d<units::length::meter_t> &,
                    const Common::Vector2d<units::length::meter_t> &,
                    const units::length::meter_t,
                    const units::length::meter_t,
                    const units::length::meter_t,
                    const units::curvature::inverse_meter_t,
                    const units::angle::radian_t));
  MOCK_METHOD4(AddCenterLinePoint,
               void(const RoadLaneSectionInterface &odSection,
                    const Common::Vector2d<units::length::meter_t> &pointCenter,
                    const units::length::meter_t sOffset,
                    units::angle::radian_t heading));
  MOCK_METHOD3(AddLaneSuccessor,
               void(/* const */ RoadLaneInterface &, /* const */ RoadLaneInterface &, bool atBeginOfOtherLane));
  MOCK_METHOD3(AddLanePredecessor,
               void(/* const */ RoadLaneInterface &, /* const */ RoadLaneInterface &, bool atBeginOfOtherLane));
  MOCK_METHOD2(AddIntersectionInfo, void(IntersectionInfo intersectionInfo, IntersectionInfo crossIntersectionInfo));
  MOCK_METHOD2(SetRoadSuccessor, void(const RoadInterface &, const RoadInterface &));
  MOCK_METHOD2(SetRoadPredecessor, void(const RoadInterface &, const RoadInterface &));
  MOCK_METHOD2(SetRoadSuccessorJunction, void(const RoadInterface &, const JunctionInterface *));
  MOCK_METHOD2(SetRoadPredecessorJunction, void(const RoadInterface &, const JunctionInterface *));
  MOCK_METHOD2(SetSectionSuccessor, void(const RoadLaneSectionInterface &, const RoadLaneSectionInterface &));
  MOCK_METHOD2(SetSectionPredecessor, void(const RoadLaneSectionInterface &, const RoadLaneSectionInterface &));
  MOCK_METHOD4(ConnectLanes,
               void(/* const */ RoadLaneSectionInterface &,
                    /* const */ RoadLaneSectionInterface &,
                    const std::map<int, int> &,
                    bool));
  MOCK_METHOD1(SetEnvironment, void(const mantle_api::Weather &weather));
  MOCK_METHOD2(AddTrafficSign, OWL::Interfaces::TrafficSign &(const Id, const std::string odId));
  MOCK_METHOD3(AddTrafficLight,
               OWL::Interfaces::TrafficLight &(const std::vector<Id> trafficLightIds,
                                               const std::string odId,
                                               const std::string &type));

  MOCK_CONST_METHOD0(GetTrafficSignIdMapping, const std::unordered_map<std::string, OWL::Id> &());
  MOCK_CONST_METHOD1(GetSignalType, SignalType(Id id));
  MOCK_METHOD1(AddRoadMarking, OWL::Interfaces::RoadMarking &(const Id));
  MOCK_CONST_METHOD0(GetStationaryObjects, const IdMapping<OWL::Interfaces::StationaryObject> &());
  MOCK_CONST_METHOD1(GetStationaryObject, const OWL::Interfaces::StationaryObject &(Id id));
  MOCK_CONST_METHOD1(GetMovingObject, OWL::Interfaces::MovingObject &(Id id));
  MOCK_METHOD1(GetTrafficSign, OWL::Interfaces::TrafficSign &(Id id));
  MOCK_METHOD1(GetTrafficLight, OWL::Interfaces::TrafficLight &(Id id));

  MOCK_CONST_METHOD0(GetRoadGraph, const RoadGraph &());
  MOCK_METHOD2(SetRoadGraph, void(const RoadGraph &&roadGraph, const RoadGraphVertexMapping &&vertexMapping));
  MOCK_CONST_METHOD0(GetRoadGraphVertexMapping, const RoadGraphVertexMapping &());
  MOCK_METHOD1(SetTurningRates, void(const TurningRates &turningRates));
  MOCK_CONST_METHOD0(GetTurningRates, const TurningRates &());
  MOCK_METHOD3(GetSensorView, SensorView_ptr(osi3::SensorViewConfiguration &, int, int));
  MOCK_METHOD0(ResetTemporaryMemory, void());
  MOCK_CONST_METHOD1(GetLaneBoundary, const OWL::Interfaces::LaneBoundary &(Id id));
  MOCK_CONST_METHOD1(GetLogicalLaneBoundary, const OWL::Interfaces::LogicalLaneBoundary &(Id id));
  MOCK_METHOD1(GetLogicalLaneBoundary, OWL::Interfaces::LogicalLaneBoundary &(Id id));
  MOCK_METHOD4(AddLaneBoundary,
               OWL::Id(const Id,
                       const RoadLaneRoadMark &odLaneRoadMark,
                       units::length::meter_t sectionStart,
                       OWL::LaneMarkingSide side));
  MOCK_METHOD4(SetCenterLaneBoundary,
               void(const RoadLaneSectionInterface &odSection,
                    std::vector<OWL::Id> laneBoundaryIds,
                    std::vector<Id> logicalLaneBoundaryIds,
                    Id referenceLine));
  MOCK_METHOD3(AddCenterLinePoint,
               void(const RoadLaneSectionInterface &odSection,
                    const Common::Vector2d<units::length::meter_t> &pointCenter,
                    const units::length::meter_t sOffset));
  MOCK_CONST_METHOD0(GetJunctions, const std::map<std::string, OWL::Junction *> &());
  MOCK_METHOD3(AssignTrafficSignToLane,
               void(OWL::Id laneId, Interfaces::TrafficSign &trafficSign, const RoadSignalInterface &specification));
  MOCK_METHOD3(AssignRoadMarkingToLane,
               void(OWL::Id laneId,
                    OWL::Interfaces::RoadMarking &roadMarking,
                    const RoadSignalInterface &specification));
  MOCK_METHOD3(AssignRoadMarkingToLane,
               void(OWL::Id laneId,
                    OWL::Interfaces::RoadMarking &roadMarking,
                    const RoadObjectInterface &specification));
  MOCK_METHOD3(AssignTrafficLightToLane,
               void(OWL::Id laneId,
                    OWL::Interfaces::TrafficLight &trafficLight,
                    const RoadSignalInterface &specification));
  MOCK_METHOD3(AddJunctionPriority,
               void(const JunctionInterface *odJunction, const std::string &high, const std::string &low));
  MOCK_CONST_METHOD1(GetOwlId, OWL::Id(int));
  MOCK_CONST_METHOD1(GetAgentId, int(const OWL::Id));
  const OWL::Implementation::InvalidLane &GetInvalidLane() const override { return invalidLane; }
  const OWL::Implementation::InvalidLane invalidLane;
};
}  // namespace OWL::Fakes
