/********************************************************************************
 * Copyright (c) 2018-2019 in-tech GmbH
 *               2023 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <gmock/gmock.h>

#include "OWL/DataTypes.h"
#include "common/globalDefinitions.h"

namespace osi3
{
class GroundTruth;
}

namespace OWL::Fakes
{
class StationaryObject : public OWL::Interfaces::StationaryObject
{
public:
  void SetLinkedObjectForTesting(void* linkedObject) { this->linkedObject = linkedObject; }

  MOCK_CONST_METHOD0(GetId, OWL::Id());
  MOCK_CONST_METHOD0(GetDimension, mantle_api::Dimension3());

  MOCK_CONST_METHOD0(GetReferencePointPosition, mantle_api::Vec3<units::length::meter_t>());
  MOCK_CONST_METHOD0(GetAbsOrientation, mantle_api::Orientation3<units::angle::radian_t>());
  MOCK_CONST_METHOD0(GetAbsVelocityDouble, units::velocity::meters_per_second_t());
  MOCK_CONST_METHOD0(GetAbsAccelerationDouble, units::acceleration::meters_per_second_squared_t());

  MOCK_METHOD2(AddLaneAssignment,
               void(const OWL::Interfaces::Lane& lane, const std::optional<RoadPosition>& referencePoint));
  MOCK_CONST_METHOD0(GetLaneAssignments, const OWL::Interfaces::Lanes&());
  MOCK_METHOD0(ClearLaneAssignments, void());

  MOCK_METHOD1(SetAbsOrientation, void(const mantle_api::Orientation3<units::angle::radian_t>& newOrientation));
  MOCK_METHOD1(SetDimension, void(const mantle_api::Dimension3& newDimension));
  MOCK_CONST_METHOD0(GetTouchedRoads, const RoadIntervals&());
  MOCK_METHOD1(SetTouchedRoads, void(const RoadIntervals& touchedRoads));
  MOCK_METHOD1(SetReferencePointPosition, void(const mantle_api::Vec3<units::length::meter_t>& newPosition));
  MOCK_METHOD1(SetSourceReference, void(const std::string& odId));

  MOCK_CONST_METHOD1(CopyToGroundTruth, void(osi3::GroundTruth&));
};

class MovingObject : public OWL::Interfaces::MovingObject
{
public:
  void SetLinkedObjectForTesting(void* linkedObject) { this->linkedObject = linkedObject; }

  MOCK_CONST_METHOD0(GetId, OWL::Id());
  MOCK_CONST_METHOD0(GetDimension, mantle_api::Dimension3());

  MOCK_CONST_METHOD0(GetLane, const OWL::Interfaces::Lane&());
  MOCK_CONST_METHOD0(GetSection, const OWL::Interfaces::Section&());
  MOCK_CONST_METHOD0(GetRoad, const OWL::Interfaces::Road&());

  MOCK_CONST_METHOD0(GetReferencePointPosition, mantle_api::Vec3<units::length::meter_t>());
  MOCK_CONST_METHOD0(GetTouchedRoads, const RoadIntervals&());

  MOCK_CONST_METHOD0(GetAbsOrientation, mantle_api::Orientation3<units::angle::radian_t>());
  MOCK_CONST_METHOD0(GetLaneOrientation, OWL::Primitive::LaneOrientation());

  MOCK_CONST_METHOD0(GetAbsVelocity, mantle_api::Vec3<units::velocity::meters_per_second_t>());
  MOCK_CONST_METHOD0(GetAbsVelocityDouble, units::velocity::meters_per_second_t());

  MOCK_CONST_METHOD0(GetAbsAcceleration, OWL::Primitive::AbsAcceleration());
  MOCK_CONST_METHOD0(GetAbsAccelerationDouble, units::acceleration::meters_per_second_squared_t());

  MOCK_CONST_METHOD0(GetAbsOrientationRate, mantle_api::Orientation3<units::angular_velocity::radians_per_second_t>());
  MOCK_CONST_METHOD0(GetAbsOrientationAcceleration, OWL::Primitive::AbsOrientationAcceleration());

  MOCK_METHOD1(SetDimension, void(const mantle_api::Dimension3& newDimension));
  MOCK_METHOD1(SetLength, void(const units::length::meter_t newLength));
  MOCK_METHOD1(SetWidth, void(const units::length::meter_t newWidth));
  MOCK_METHOD1(SetHeight, void(const units::length::meter_t newHeight));
  MOCK_METHOD3(SetBoundingBoxCenterToRear,
               void(const units::length::meter_t distanceX,
                    const units::length::meter_t distanceY,
                    const units::length::meter_t distanceZ));
  MOCK_METHOD3(SetBoundingBoxCenterToFront,
               void(const units::length::meter_t distanceX,
                    const units::length::meter_t distanceY,
                    const units::length::meter_t distanceZ));

  MOCK_METHOD1(SetReferencePointPosition, void(const mantle_api::Vec3<units::length::meter_t>& newPosition));
  MOCK_METHOD1(SetX, void(const units::length::meter_t newX));
  MOCK_METHOD1(SetY, void(const units::length::meter_t newY));
  MOCK_METHOD1(SetZ, void(const units::length::meter_t newZ));

  MOCK_METHOD1(SetTouchedRoads, void(const RoadIntervals& touchedRoads));

  MOCK_METHOD1(SetAbsOrientation, void(const mantle_api::Orientation3<units::angle::radian_t>& newOrientation));
  MOCK_METHOD1(SetYaw, void(const units::angle::radian_t newYaw));
  MOCK_METHOD1(SetPitch, void(const units::angle::radian_t newPitch));
  MOCK_METHOD1(SetRoll, void(const units::angle::radian_t newRoll));

  MOCK_METHOD1(SetAbsVelocity, void(const mantle_api::Vec3<units::velocity::meters_per_second_t>& newVelocity));
  MOCK_METHOD1(SetAbsVelocity, void(const units::velocity::meters_per_second_t newVelocity));

  MOCK_METHOD1(SetAbsAcceleration, void(const OWL::Primitive::AbsAcceleration& newAcceleration));
  MOCK_METHOD1(SetAbsAcceleration, void(const units::acceleration::meters_per_second_squared_t newAcceleration));

  MOCK_METHOD1(SetAbsOrientationRate,
               void(const mantle_api::Orientation3<units::angular_velocity::radians_per_second_t>& newOrientationRate));
  MOCK_METHOD1(SetAbsOrientationAcceleration,
               void(const OWL::Primitive::AbsOrientationAcceleration& newOrientationAcceleration));

  MOCK_METHOD2(AddLaneAssignment,
               void(const OWL::Interfaces::Lane& lane, const std::optional<RoadPosition>& referencePoint));
  MOCK_CONST_METHOD0(GetLaneAssignments, const OWL::Interfaces::Lanes&());
  MOCK_METHOD0(ClearLaneAssignments, void());

  MOCK_CONST_METHOD0(GetDistanceReferencePointToLeadingEdge, units::length::meter_t());

  MOCK_METHOD1(SetIndicatorState, void(IndicatorState indicatorState));
  MOCK_CONST_METHOD0(GetIndicatorState, IndicatorState());
  MOCK_METHOD1(SetBrakeLightState, void(bool brakeLightState));
  MOCK_CONST_METHOD0(GetBrakeLightState, bool());
  MOCK_METHOD1(SetHeadLight, void(bool headLight));
  MOCK_CONST_METHOD0(GetHeadLight, bool());
  MOCK_METHOD1(SetHighBeamLight, void(bool highbeamLight));
  MOCK_CONST_METHOD0(GetHighBeamLight, bool());

  MOCK_METHOD1(SetType, void(mantle_api::EntityType));
  MOCK_METHOD1(SetVehicleClassification, void(mantle_api::VehicleClass));
  MOCK_METHOD1(SetSourceReference, void(const OWL::ExternalReference& externalReference));
  MOCK_METHOD1(AddWheel, void(const WheelData& wheelData));

  MOCK_METHOD0(GetSteeringWheelAngle, Angle());
  MOCK_METHOD1(SetSteeringWheelAngle, void(const Angle));

  MOCK_METHOD1(SetFrontAxleSteeringYaw, void(const Angle));
  MOCK_METHOD4(SetWheelsRotationRateAndOrientation,
               void(const units::velocity::meters_per_second_t,
                    const units::length::meter_t,
                    const units::length::meter_t,
                    const units::time::second_t));
  MOCK_METHOD3(SetWheelRotationRate, void(const int, const int, const double));
  MOCK_METHOD2(GetWheelData, std::optional<const OWL::WheelData>(unsigned int, unsigned int));

  MOCK_CONST_METHOD1(CopyToGroundTruth, void(osi3::GroundTruth&));
};
}  //namespace OWL::Fakes
