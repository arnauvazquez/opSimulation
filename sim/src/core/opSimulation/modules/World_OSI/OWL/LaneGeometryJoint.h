/********************************************************************************
 * Copyright (c) 2018-2019 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "OWL/Primitives.h"
#include "common/globalDefinitions.h"
#include "common/vector2d.h"

namespace OWL
{
namespace Primitive
{

/// Struct representing lane geometry joint
struct LaneGeometryJoint
{
  /// \brief Tripple describing the sampled points of the lane
  /// \note Left/Right refers to the direction of the road
  struct Points
  {
    Common::Vector2d<units::length::meter_t> left;       //!< Left point of the lane
    Common::Vector2d<units::length::meter_t> reference;  //!< Reference point of the lane
    Common::Vector2d<units::length::meter_t> right;      //!< Right point of the lane
  } points;                                              //!< Sampled points of the lane

  units::curvature::inverse_meter_t curvature;  //!< \brief Curvature of the reference line at this joint
  units::length::meter_t sOffset;               //!< \brief Offset with respect to the start of the road
  units::angle::radian_t sHdg;                  //!< \brief Heading of the s projection axis
};

}  // namespace Primitive
}  // namespace OWL
