/********************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2023 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#ifndef OPENPASS_MOVINGOBJECT_H
#define OPENPASS_MOVINGOBJECT_H

#include "DataTypes.h"

namespace OWL::Implementation
{

/// This class represents MovingObject that can be on the road and have a position and a dimension
class MovingObject : public Interfaces::MovingObject
{
public:
  /// @brief MovingObject constructor
  /// @param osiMovingObject Pointer to the OSI moving object
  MovingObject(osi3::MovingObject *osiMovingObject);
  ~MovingObject() override = default;

  Id GetId() const override;
  mantle_api::Dimension3 GetDimension() const override;
  mantle_api::Vec3<units::length::meter_t> GetReferencePointPosition() const override;
  const RoadIntervals &GetTouchedRoads() const override;
  units::length::meter_t GetDistanceReferencePointToLeadingEdge() const override;
  mantle_api::Orientation3<units::angle::radian_t> GetAbsOrientation() const override;
  Primitive::LaneOrientation GetLaneOrientation() const override;
  mantle_api::Vec3<units::velocity::meters_per_second_t> GetAbsVelocity() const override;
  units::velocity::meters_per_second_t GetAbsVelocityDouble() const override;
  Primitive::AbsAcceleration GetAbsAcceleration() const override;
  units::acceleration::meters_per_second_squared_t GetAbsAccelerationDouble() const override;
  mantle_api::Orientation3<units::angular_velocity::radians_per_second_t> GetAbsOrientationRate() const override;
  Primitive::AbsOrientationAcceleration GetAbsOrientationAcceleration() const override;
  void SetDimension(const mantle_api::Dimension3 &newDimension) override;
  void SetLength(const units::length::meter_t newLength) override;
  void SetWidth(const units::length::meter_t newWidth) override;
  void SetHeight(const units::length::meter_t newHeight) override;
  void SetBoundingBoxCenterToRear(const units::length::meter_t distanceX,
                                  const units::length::meter_t distanceY,
                                  const units::length::meter_t distanceZ) override;
  void SetBoundingBoxCenterToFront(const units::length::meter_t distanceX,
                                   const units::length::meter_t distanceY,
                                   const units::length::meter_t distanceZ) override;
  void SetReferencePointPosition(const mantle_api::Vec3<units::length::meter_t> &newPosition) override;
  void SetX(const units::length::meter_t newX) override;
  void SetY(const units::length::meter_t newY) override;
  void SetZ(const units::length::meter_t newZ) override;
  void SetTouchedRoads(const RoadIntervals &touchedRoads) override;
  void SetAbsOrientation(const mantle_api::Orientation3<units::angle::radian_t> &newOrientation) override;
  void SetYaw(const units::angle::radian_t newYaw) override;
  void SetPitch(const units::angle::radian_t newPitch) override;
  void SetRoll(const units::angle::radian_t newRoll) override;
  void SetAbsVelocity(const mantle_api::Vec3<units::velocity::meters_per_second_t> &newVelocity) override;
  void SetAbsVelocity(const units::velocity::meters_per_second_t newVelocity) override;
  void SetAbsAcceleration(const Primitive::AbsAcceleration &newAcceleration) override;
  void SetAbsAcceleration(const units::acceleration::meters_per_second_squared_t newAcceleration) override;
  void SetAbsOrientationRate(
      const mantle_api::Orientation3<units::angular_velocity::radians_per_second_t> &newOrientationRate) override;
  void SetAbsOrientationAcceleration(const Primitive::AbsOrientationAcceleration &newOrientationAcceleration) override;
  void AddLaneAssignment(const Interfaces::Lane &lane, const std::optional<RoadPosition> &referencePoint) override;
  const Interfaces::Lanes &GetLaneAssignments() const override;
  void ClearLaneAssignments() override;
  void SetIndicatorState(IndicatorState indicatorState) override;
  IndicatorState GetIndicatorState() const override;
  void SetBrakeLightState(bool brakeLightState) override;
  bool GetBrakeLightState() const override;
  void SetHeadLight(bool headLight) override;
  bool GetHeadLight() const override;
  void SetHighBeamLight(bool highbeamLight) override;
  bool GetHighBeamLight() const override;
  void SetType(mantle_api::EntityType type) override;
  void SetVehicleClassification(mantle_api::VehicleClass vehicleClassification) override;
  void SetSourceReference(const ExternalReference &externalReference) override;
  void AddWheel(const WheelData &wheelData) override;
  std::optional<const WheelData> GetWheelData(unsigned int axleIndex, unsigned int rowIndex) override;
  Angle GetSteeringWheelAngle() override;
  void SetSteeringWheelAngle(const Angle newValue) override;
  void SetFrontAxleSteeringYaw(const Angle wheelYaw) override;
  void SetWheelsRotationRateAndOrientation(const units::velocity::meters_per_second_t velocity,
                                           const units::length::meter_t wheelRadiusFront,
                                           const units::length::meter_t wheelRadiusRear,
                                           const units::time::second_t cycleTime) override;
  void SetWheelRotationRate(const int axleIndex, const int wheelIndex, double rotationRate) override;
  void CopyToGroundTruth(osi3::GroundTruth &target) const override;

private:
  osi3::MovingObject *osiObject;
  const RoadIntervals *touchedRoads;
  Interfaces::Lanes assignedLanes;

  mutable Lazy<double> frontDistance;
  mutable Lazy<double> rearDistance;

  const Implementation::InvalidLane invalidLane;
  const Implementation::InvalidSection invalidSection;
  const Implementation::InvalidRoad invalidRoad;
};

/// This class represents MovingObjectfactory
class DefaultMovingObjectFactory
{
public:
  /// @brief Assign default values of the OSI moving object to the moving object factory
  /// @param _osiMovingObject OSI moving object
  void AssignDefaultValues(osi3::MovingObject *_osiMovingObject);

private:
  osi3::Dimension3d d_dimension;
  osi3::Orientation3d d_orientation;
  osi3::Vector3d d_3d;
  osi3::Identifier d_identifier;
  osi3::MovingObject_Type d_type;
  google::protobuf::uint32 d_uint32;
  std::string model_reference;
  double d_double;

  void AssignDefaultTypes();

  void AssignDefaultBase(osi3::BaseMoving *base);

  void AssignDefaultVehicleAttributes(osi3::MovingObject_VehicleAttributes *vehicleAttributes);

  void AssignDefaultVehicleClassification(osi3::MovingObject_VehicleClassification *vehicleClassifcation);

  void AssignDefaultMovingObjectClassification(osi3::MovingObject_MovingObjectClassification *objectClassifcation);
};

}  //namespace OWL::Implementation

#endif  //OPENPASS_MOVINGOBJECT_H
