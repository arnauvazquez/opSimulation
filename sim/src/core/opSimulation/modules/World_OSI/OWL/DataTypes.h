/********************************************************************************
 * Copyright (c) 2020 HLRS, University of Stuttgart
 *               2018-2021 in-tech GmbH
 *               2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2023 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  DataTypes.h
//! @brief This file provides the basic datatypes of the osi world layer (OWL)
//-----------------------------------------------------------------------------

#pragma once

#include <iterator>
#include <list>
#include <memory>
#include <osi3/osi_common.pb.h>
#include <osi3/osi_groundtruth.pb.h>
#include <osi3/osi_trafficsign.pb.h>
#include <tuple>
#include <units.h>
#include <vector>

#include "OWL/LaneGeometryElement.h"
#include "OWL/LaneGeometryJoint.h"
#include "OWL/OpenDriveTypeMapper.h"
#include "OWL/Primitives.h"
#include "common/globalDefinitions.h"
#include "common/opMath.h"
#include "include/roadInterface/roadInterface.h"
#include "include/roadInterface/roadLaneInterface.h"
#include "include/roadInterface/roadLaneSectionInterface.h"
#include "include/roadInterface/roadSignalInterface.h"
#include "include/worldObjectInterface.h"

using namespace units::literals;

namespace OWL
{

/// Container for OSI Id
using Id = uint64_t;
/// 'invalid' Id
constexpr Id InvalidId = std::numeric_limits<::google::protobuf::uint64>::max();

/// Container for OpenDrive Id
using OdId = int64_t;

/// Container for steering wheel angle
using Angle = units::angle::radian_t;

/// Container for priorities of connectors
using Priorities = std::vector<std::pair<std::string, std::string>>;

/// Event horizon
constexpr double EVENTHORIZON = 2000;

/// The point on the road to measure
enum class MeasurementPoint
{
  RoadStart,
  RoadEnd
};

/// Lane Marking Side: Left and Right for a double line, Single for single line
enum class LaneMarkingSide
{
  Left,
  Right,
  Single
};

//! This struct describes the intersection of an object with a lane
struct LaneOverlap
{
  /// Starting s coordinate of the object on the lane
  GlobalRoadPosition sMin{"", 0, units::length::meter_t{std::numeric_limits<double>::infinity()}, 0_m, 0_rad};
  /// Ending s coordinate of the object on the lane
  GlobalRoadPosition sMax{"", 0, units::length::meter_t{-std::numeric_limits<double>::infinity()}, 0_m, 0_rad};
  /// Leftmost t coordinate of the object on the lane
  GlobalRoadPosition tMin{"", 0, 0_m, units::length::meter_t{std::numeric_limits<double>::infinity()}, 0_rad};
  /// Rightmost t coordinate of the object on the lane
  GlobalRoadPosition tMax{"", 0, 0_m, units::length::meter_t{-std::numeric_limits<double>::infinity()}, 0_rad};
};

/// Intersection info for the road to the junction
struct IntersectionInfo
{
  /// Intersecting road
  std::string intersectingRoad;
  /// Rank of one junction connection w.r.t. another
  IntersectingConnectionRank relativeRank;

  //! For each pair of lanes on the own road (first id) and the intersecting road (second id)
  //! this contains the start s and end s of the intersection on the own lane
  std::map<std::pair<Id, Id>, std::pair<units::length::meter_t, units::length::meter_t>> sOffsets;
};

/**
 * @brief Returns the dimension of the OSI object
 *
 * @param[in] osiObject OSI object
 * @return dimension of the OSI object
 */
template <typename OsiObject>
mantle_api::Dimension3 GetDimensionFromOsiObject(const OsiObject &osiObject)
{
  const auto &d = osiObject->base().dimension();
  return {units::length::meter_t(d.length()), units::length::meter_t(d.width()), units::length::meter_t(d.height())};
}

/// Value that is evaluated lazily
template <typename T>
struct Lazy
{
private:
  T value{};          /// the value
  bool valid{false};  /// whether the value was already calculated this timestep

public:
  /// @brief Update the value
  /// @tparam T       type of the value
  /// @param newValue value
  void Update(T newValue)
  {
    value = newValue;
    valid = true;
  }

  /// @brief Function to set the valid status as false
  void Invalidate() { valid = false; }

  /// @return Returns the value
  T Get() const
  {
    if (valid)
    {
      return value;
    }
    else
    {
      throw std::logic_error("retrieved invalidated lazy");
    }
  }

  /// Function to check if valid
  /// @return True, if valid
  bool IsValid() const { return valid; }
};

/// References to external objects. The external reference is an optional recommendation to refer to objects defined
/// outside of OSI.
struct ExternalReference
{
  std::string reference;                ///< The source of the external references
  std::string type;                     ///< The type of the external references
  std::vector<std::string> identifier;  ///< The external identifier reference value
};

/// This struct describes the wheel regarding the perceivable information from the outside. It is not intended to be
/// used for dynamic calculations, for example.
struct WheelData
{
  //! The axle which contains this wheel
  unsigned int axle;
  //! The index of the wheel on the axle, counting in the direction of positive-y, that is, right-to-left
  unsigned int index;
  //! Median width of the tire
  units::length::meter_t width = units::length::meter_t(std::numeric_limits<double>::signaling_NaN());
  //! Median radius of the wheel measured from the center of the wheel to the outer part of the tire
  units::length::meter_t wheelRadius = units::length::meter_t(std::numeric_limits<double>::signaling_NaN());
  //! Rotation rate of the wheel
  units::angular_velocity::revolutions_per_minute_t rotation_rate
      = units::angular_velocity::revolutions_per_minute_t(std::numeric_limits<double>::signaling_NaN());
  //! Median radius of the rim measured from the center to the outer, visible part of the rim
  units::length::meter_t rim_radius = units::length::meter_t(std::numeric_limits<double>::signaling_NaN());
  //! A vector pointing from the vehicle's reference system (center of bounding box) to the geometric center of the
  //! wheel
  mantle_api::Vec3<units::length::meter_t> position{};
  //! Orientation of the wheel
  mantle_api::Orientation3<units::angle::radian_t> orientation{};

  /**
   * @brief Sets wheel data from the OSI wheel data
   *
   * @param[in] data  Detailed OSI wheel data
   */
  void SetFromOsi(const osi3::MovingObject_VehicleAttributes_WheelData *data)
  {
    axle = data->axle();
    index = data->index();
    width = units::length::meter_t(data->width());
    wheelRadius = units::length::meter_t(data->wheel_radius());
    rotation_rate = units::angular_velocity::revolutions_per_minute_t(data->rotation_rate());
    rim_radius = units::length::meter_t(data->rim_radius());
    position.x = units::length::meter_t(data->position().x());
    position.y = units::length::meter_t(data->position().y());
    position.z = units::length::meter_t(data->position().z());
    orientation.yaw = units::angle::radian_t(data->orientation().yaw());
    orientation.pitch = units::angle::radian_t(data->orientation().pitch());
    orientation.roll = units::angle::radian_t(data->orientation().roll());
  }
};

namespace Interfaces
{
class Lane;
class LaneBoundary;
class Section;
class Road;
class Junction;
class WorldObject;
class StationaryObject;
class MovingObject;
class TrafficSign;
class TrafficLight;
class RoadMarking;

using LaneGeometryElements
    = std::vector<Primitive::LaneGeometryElement *>;  ///< List of pointers to the lane geometry element
using LaneGeometryJoints = std::vector<Primitive::LaneGeometryJoint>;  ///< List of lane geometry joints
using Lanes = std::vector<const Lane *>;                               ///< List of pointers to the lane
using Sections = std::vector<const Section *>;                         ///< List of pointers to the section
using Roads = std::vector<const Road *>;                               ///< List of pointers to the road
using WorldObjects = std::vector<WorldObject *>;                       ///< List of pointers to the world object
using MovingObjects = std::vector<MovingObject *>;                     ///< List of pointers to the moving object
using StationaryObjects = std::vector<StationaryObject *>;             ///< List of pointers to the stationary object
using TrafficSigns = std::vector<TrafficSign *>;                       ///< List of pointers to the traffic sign
using TrafficLights = std::vector<TrafficLight *>;                     ///< List of pointers to the traffic light
using RoadMarkings = std::vector<RoadMarking *>;                       ///< List of pointers to the road marking
using LaneAssignment
    = std::pair<LaneOverlap, const Interfaces::WorldObject *>;  ///< Pair of lane overlap and pointer to the world
                                                                ///< object used as an lane assignment
using LaneAssignments = std::vector<LaneAssignment>;            ///< List of lane assignments

//! This class represents a single lane inside a section
class Lane
{
public:
  virtual ~Lane() = default;  //!< default destructor

  /*!
   * \brief Copies the underlying OSI object to the given GroundTruth
   *
   * \param[in]   target   The target OSI GroundTruth
   */
  virtual void CopyToGroundTruth(osi3::GroundTruth &target) const = 0;

  //! Returns the OSI Id
  //!
  //! @return OSI Id
  virtual Id GetId() const = 0;

  //! @return Returns the OSI Id of the logical lane
  virtual Id GetLogicalLaneId() const = 0;

  //! Returns the OpenDrive Id
  //!
  //! @return OpenDrive Id
  virtual OdId GetOdId() const = 0;

  //! Returns false if this lane is invalid, otherwise returns true
  //!
  //! @return false if this lane is invalid, otherwise returns true
  virtual bool Exists() const = 0;

  //!Returns the section that this lane is part of
  //!
  //! @return section that this lane is part of
  virtual const Section &GetSection() const = 0;

  //!Returns the road that this lane is part of
  //!
  //! @return road that this lane is part of
  virtual const Road &GetRoad() const = 0;

  //!Returns all lane geometry elements of the lane
  //!
  //! @return lane geometry elements
  virtual const LaneGeometryElements &GetLaneGeometryElements() const = 0;

  //!Returns the length of the lane
  //!
  //! @return length of the lane
  virtual units::length::meter_t GetLength() const = 0;

  //!Returns the number of lanes to the right of this lane
  //!
  //! @return number of lanes to the right of this lane
  virtual int GetRightLaneCount() const = 0;

  //!Returns the type of the lane
  //!
  //! @return type of the lane
  virtual LaneType GetLaneType() const = 0;

  //!Returns the curvature of the lane at the specified distance
  //!
  //! @param distance s coordinate
  //! @return curvature of the lane at the specified distance
  virtual units::curvature::inverse_meter_t GetCurvature(units::length::meter_t distance) const = 0;

  //!Returns the width of the lane at the specified distance
  //!
  //! @param distance s coordinate
  //! @return width of the lane at the specified distance
  virtual units::length::meter_t GetWidth(units::length::meter_t distance) const = 0;

  //!Returns the direction of the lane at the specified distance
  //!
  //! @param distance s coordinate
  //! @return direction of the lane at the specified distance
  virtual units::angle::radian_t GetDirection(units::length::meter_t distance) const = 0;

  //!Returns the left, right and reference point at the specified s coordinate interpolated between the geometry joints
  //!
  //! @param distance s coordinate
  //! @return left, right and reference point at the specified s coordinate interpolated between the geometry joints
  virtual const Primitive::LaneGeometryJoint::Points GetInterpolatedPointsAtDistance(
      units::length::meter_t distance) const
      = 0;

  //!Returns the ids of all successors of this lane
  //!
  //! \return ids of all successors of this lane
  virtual const std::vector<Id> &GetNext() const = 0;

  //!Returns the ids of all predecessors of this lane
  //!
  //! \return ids of all predecessors of this lane
  virtual const std::vector<Id> &GetPrevious() const = 0;

  //!Returns the (possibly invalid) lane to the left of this lane
  //!
  //! \return lane to the left of this lane
  virtual const Lane &GetLeftLane() const = 0;

  //!Returns the (possibly invalid) lane to the right of this lane
  //!
  //! \return lane to the right of this lane
  virtual const Lane &GetRightLane() const = 0;

  //! Returns the ids of all left lane boundaries
  //!
  //! \return ids of all left lane boundaries
  virtual const std::vector<Id> GetLeftLaneBoundaries() const = 0;

  //! Returns the ids of all right lane boundaries
  //!
  //! \return ids of all right lane boundaries
  virtual const std::vector<Id> GetRightLaneBoundaries() const = 0;

  //! @return Returns the id of the left logical lane boundary
  virtual const std::vector<Id> GetLeftLogicalLaneBoundaries() const = 0;

  //! @return Returns the id of the right logical lane boundary
  virtual const std::vector<Id> GetRightLogicalLaneBoundaries() const = 0;

  //!Returns the s coordinate of the lane start if measure point is road start
  //! or the s coordinate of the lane end if measure point is road end
  //!
  //! \param measurementPoint The point on the road to measure
  //! \return s coordinate
  virtual units::length::meter_t GetDistance(MeasurementPoint measurementPoint) const = 0;

  //!Returns true if the specified distance is valid on this lane, otherwise returns false
  //!
  //! \param distance s coordinate
  //! \return true if the specified distance is valid on this lane, otherwise returns false
  virtual bool Covers(units::length::meter_t distance) const = 0;

  //!Sets the sucessor of the lane.
  //! @param lane                 Pointer to the lane
  //! @param atBeginOfOtherLane   True, if the lane is at the begin of another lane
  virtual void AddNext(const Lane *lane, bool atBeginOfOtherLane) = 0;

  //!Sets the predecessor of the lane.
  //! @param lane                 Pointer to the lane
  //! @param atBeginOfOtherLane   True, if the lane is at the begin of another lane
  virtual void AddPrevious(const Lane *lane, bool atBeginOfOtherLane) = 0;

  //!Sets the lane pairings in OSI
  //! This method is called after all predecessors and successors are set with AddNext and AddPrevious
  virtual void SetLanePairings() = 0;

  //!Add a lane to the list of overlapping lanes
  //!
  //! @param overlappingLaneId    id of the overlapping lane
  //! @param startS               start of overlapping section on this lane
  //! @param endS                 end of overlapping section on this lane
  //! @param startSOther          start of overlapping section on the other lane
  //! @param endSOther            end of overlapping section on the other lane
  virtual void AddOverlappingLane(OWL::Id overlappingLaneId,
                                  units::length::meter_t startS,
                                  units::length::meter_t endS,
                                  units::length::meter_t startSOther,
                                  units::length::meter_t endSOther)
      = 0;

  //!Adds a new lane geometry joint to the end of the current list of joints
  //!
  //! @param pointLeft    left point of the new joint
  //! @param pointCenter  reference point of the new joint
  //! @param pointRight   right point of the new joint
  //! @param sOffset      s offset of the new joint
  //! @param curvature    curvature of the lane at sOffset
  //! @param heading      heading of the lane at sOffset
  virtual void AddLaneGeometryJoint(const Common::Vector2d<units::length::meter_t> &pointLeft,
                                    const Common::Vector2d<units::length::meter_t> &pointCenter,
                                    const Common::Vector2d<units::length::meter_t> &pointRight,
                                    units::length::meter_t sOffset,
                                    units::curvature::inverse_meter_t curvature,
                                    units::angle::radian_t heading)
      = 0;

  //!Sets type of the lane
  //!
  //! \param specifiedType    Type of the lane
  virtual void SetLaneType(LaneType specifiedType) = 0;

  //!Sets lane to the left of this lane
  //!
  //! \param lane                     Lane to be placed to the left of this lane
  //! \param transferLaneBoundary     true if "transfer lane boundary", else false
  virtual void SetLeftLane(const Interfaces::Lane &lane, bool transferLaneBoundary) = 0;

  //!Sets lane to the right of this lane
  //!
  //! \param lane                     Lane to be placed to the right of this lane
  //! \param transferLaneBoundary     true if "transfer lane boundary", else false
  virtual void SetRightLane(const Interfaces::Lane &lane, bool transferLaneBoundary) = 0;

  //! Adds the ids to the list of left lane boundaries
  //! \param laneBoundaryIds   ids to add
  //!
  virtual void SetLeftLaneBoundaries(const std::vector<OWL::Id> &laneBoundaryIds) = 0;

  //! Adds the ids to the list of right lane boundaries
  //! \param laneBoundaryIds   ids to add
  //!
  virtual void SetRightLaneBoundaries(const std::vector<OWL::Id> &laneBoundaryIds) = 0;

  //! Adds the ids to the list of left logical lane boundaries
  //!
  //! \param laneBoundaryIds ids of logical lane boundaries sorted by ascending s-coordinate
  virtual void SetLeftLogicalLaneBoundaries(const std::vector<OWL::Id> &laneBoundaryIds) = 0;

  //! Adds the given ids to the list of right logical lane boundaries
  //!
  //! \param laneBoundaryIds ids of logical lane boundaries sorted by ascending s-coordinate
  virtual void SetRightLogicalLaneBoundaries(const std::vector<OWL::Id> &laneBoundaryIds) = 0;

  //!Returns a vector of all WorldObjects that are currently in this lane
  //!
  //! @param direction    true for looking "in stream direction", else false
  //! @return vector of all WorldObjects that are currently in this lane
  virtual const LaneAssignments &GetWorldObjects(bool direction) const = 0;

  //!Returns a vector of all traffic signs that are assigned to this lane
  //!
  //! @return vector of all traffic signs that are assigned to this lane
  virtual const Interfaces::TrafficSigns &GetTrafficSigns() const = 0;

  //!Returns a vector of all road markings that are assigned to this lane
  //!
  //! @return vector of all road markings that are assigned to this lane
  virtual const Interfaces::RoadMarkings &GetRoadMarkings() const = 0;

  //!Returns a vector of all traffic lights that are assigned to this lane
  //!
  //! @return vector of all traffic lights that are assigned to this lane
  virtual const Interfaces::TrafficLights &GetTrafficLights() const = 0;

  //!Adds a MovingObject to the list of objects currently in this lane
  //!
  //! @param[in]  movingObject    MovingObject to add
  //! @param[in]  laneOverlap     Minimum and maximum s coordinate of the object
  virtual void AddMovingObject(OWL::Interfaces::MovingObject &movingObject, const LaneOverlap &laneOverlap) = 0;

  //!Adds a StationaryObject to the list of objects currently in this lane
  //!
  //! @param[in]  stationaryObject    StationaryObject to add
  //! @param[in]  laneOverlap         Minimum and maximum s coordinate of the object
  virtual void AddStationaryObject(OWL::Interfaces::StationaryObject &stationaryObject, const LaneOverlap &laneOverlap)
      = 0;

  //!Adds a WorldObject to the list of objects currently in this lane
  //!
  //! @param[in]  worldObject     WorldObject to add
  //! @param[in]  laneOverlap     Minimum and maximum s coordinate of the object
  virtual void AddWorldObject(Interfaces::WorldObject &worldObject, const LaneOverlap &laneOverlap) = 0;

  //!Adds a traffic sign to this lane
  //!
  //! @param[in]  trafficSign     Traffic sign to add
  virtual void AddTrafficSign(Interfaces::TrafficSign &trafficSign) = 0;

  //!Adds a road marking to this lane
  //!
  //! @param[in]  roadMarking     Road marking to add
  virtual void AddRoadMarking(Interfaces::RoadMarking &roadMarking) = 0;

  //!Assigns a traffic light to this lane
  //!
  //! @param[in]  trafficLight    Traffic light to add
  virtual void AddTrafficLight(Interfaces::TrafficLight &trafficLight) = 0;

  //!Removes all MovingObjects from the list of objects currently in this lane while keeping StationaryObjects
  virtual void ClearMovingObjects() = 0;
};

//! This class represents the boundary between two lanes
class LaneBoundary
{
public:
  virtual ~LaneBoundary() = default;  //!< default destructor

  //! Returns the OSI Id
  //!
  //! @return OSI Id
  virtual Id GetId() const = 0;

  //! Returns the width of the boundary
  //!
  //! @return width of the boundary
  virtual units::length::meter_t GetWidth() const = 0;

  //! Returns the s coordinate, where this boundary starts
  //!
  //! @return s coordinate, where this boundary starts
  virtual units::length::meter_t GetSStart() const = 0;

  //! Returns the s coordinate, where this boundary ends
  //!
  //! @return s coordinate, where this boundary ends
  virtual units::length::meter_t GetSEnd() const = 0;

  //! Returns the type of the boundary
  //!
  //! @return type of the boundary
  virtual LaneMarking::Type GetType() const = 0;

  //! Return the color of the boundary
  //!
  //! @return color of the boundary
  virtual LaneMarking::Color GetColor() const = 0;

  //! Returns the side (Left/Right) of this boundary if it is part of a double line, or Single otherwise
  //!
  //! @return side of boundary for double line, or Single for single line
  virtual LaneMarkingSide GetSide() const = 0;

  //! Adds a new point the end of the list of boundary points
  //!
  //! \param point    point to add
  //! \param heading  heading of the lane
  //!
  virtual void AddBoundaryPoint(const Common::Vector2d<units::length::meter_t> &point, units::angle::radian_t heading)
      = 0;

  /*!
   * \brief Copies the underlying OSI object to the given GroundTruth
   *
   * \param[in]   target   The target OSI GroundTruth
   */
  virtual void CopyToGroundTruth(osi3::GroundTruth &target) const = 0;
};

//! This class represents the boundary between two lanes
class LogicalLaneBoundary
{
public:
  virtual ~LogicalLaneBoundary() = default;  //!< default destructor

  //! @return Returns the OSI Id
  virtual Id GetId() const = 0;

  //! Returns the s coordinate, where this boundary starts
  //!
  //! @return s coordinate, where this boundary starts
  virtual units::length::meter_t GetSStart() const = 0;

  //! Returns the s coordinate, where this boundary ends
  //!
  //! @return s coordinate, where this boundary ends
  virtual units::length::meter_t GetSEnd() const = 0;

  //! Adds a new point the end of the list of boundary points
  //!
  //! \param point    point to add
  //! \param s        s position of the lane
  //! \param t        t position of the lane
  //!
  virtual void AddBoundaryPoint(const Common::Vector2d<units::length::meter_t> &point,
                                units::length::meter_t s,
                                units::length::meter_t t)
      = 0;

  //! Sets the ids of the physical lane boundaries that are represented by this logical lane boundary,
  //! replacing prior assignments.
  //!
  //! \param ids ids of physical lane boundaries sorted by ascending s-coordinate on this logical lane
  virtual void SetPhysicalBoundaryIds(const std::vector<OWL::Id> &ids) = 0;

  //!
  //! \brief Copies the underlying OSI object to the given GroundTruth
  //!
  //! \param[in]   target   The target OSI GroundTruth
  //!
  virtual void CopyToGroundTruth(osi3::GroundTruth &target) const = 0;
};

/// @brief Class representing a reference line type
class ReferenceLine
{
public:
  virtual ~ReferenceLine() = default;  //!< default destructor

  //! @return Returns the OSI Id
  virtual Id GetId() const = 0;

  //! Adds a new point the end of the list of points
  //!
  //! \param point    point to add
  //! \param s        s position of the lane
  //!
  virtual void AddPoint(const Common::Vector2d<units::length::meter_t> &point, units::length::meter_t s) = 0;

  //!
  //! \brief Copies the underlying OSI object to the given GroundTruth
  //!
  //! \param[in]   target   The target OSI GroundTruth
  //!
  virtual void CopyToGroundTruth(osi3::GroundTruth &target) const = 0;
};

//!This class represents one section of a road.
class Section
{
public:
  virtual ~Section() = default;  //!< default destructor

  //!Sets the sucessor of the section. Throws an error if the section already has a sucessor
  //!
  //! \param section  Sucessor of the section
  virtual void AddNext(const Interfaces::Section &section) = 0;

  //!Sets the predecessor of the section. Throws an error if the section already has a predecessor
  //!
  //! \param section  Predecessor of the section
  virtual void AddPrevious(const Interfaces::Section &section) = 0;

  //!Adds a lane to this section
  //!
  //! \param lane Lane to add
  virtual void AddLane(const Interfaces::Lane &lane) = 0;

  //!Sets the road that this section is part of
  //!
  //! \param road Road to set
  virtual void SetRoad(Interfaces::Road *road) = 0;

  //!Returns a vector of all lanes of the section
  //!
  //! @return lanes of the section
  virtual const Lanes &GetLanes() const = 0;

  //!Returns the road that this section is part of
  //!
  //! @return road
  virtual const Interfaces::Road &GetRoad() const = 0;

  //!Returns the s coordinate of the section start if measure point is road start
  //! or the s coordinate of the section end if measure point is road end
  //!
  //! \param measurementPoint The point on the road to measure
  //! \return s coordinate
  virtual units::length::meter_t GetDistance(MeasurementPoint measurementPoint) const = 0;

  //!Returns true if the specified distance is valid on this sections, otherwise returns false
  //!
  //! \param distance s coordinate
  //! \return true if the specified distance is valid on this sections, otherwise returns false
  virtual bool Covers(units::length::meter_t distance) const = 0;

  //!Returns true if at least one s coordinate between startDistance and endDistance is valid on this section
  //!
  //! \param startDistance    Start of interval
  //! \param endDistance      End of interval
  //! \return true if at least one s coordinate in interval is valid on this section
  virtual bool CoversInterval(units::length::meter_t startDistance, units::length::meter_t endDistance) const = 0;

  //!Returns the s coordinate of the section start
  //!
  //! @return s coordinate of the section start
  virtual units::length::meter_t GetSOffset() const = 0;

  //!Returns the length of the section
  //!
  //! @return length of the section
  virtual units::length::meter_t GetLength() const = 0;

  //! Sets the ids of the lane boundaries of the center lane
  //! \param laneBoundaryIds  ids of center lane boundaries
  //!
  virtual void SetCenterLaneBoundary(std::vector<Id> laneBoundaryIds) = 0;

  //! Returns the ids of the lane boundaries of the center lane
  //!
  //! @return ids of the lane boundaries of the center lane
  virtual std::vector<Id> GetCenterLaneBoundary() const = 0;

  //! Sets the ids of the logical lane boundaries of the center lane
  //! \param logicalLaneBoundaryIds   ids of center logical lane boundaries
  //!
  virtual void SetCenterLogicalLaneBoundary(std::vector<Id> logicalLaneBoundaryIds) = 0;

  //! Returns the ids of the logical lane boundaries of the center lane
  //! @return                        ids of center logical lane boundaries
  //!
  virtual std::vector<Id> GetCenterLogicalLaneBoundary() const = 0;

  //! Sets the id of the reference line
  //! \param referenceLineId   id of reference line
  //!
  virtual void SetReferenceLine(Id referenceLineId) = 0;

  //! Returns the id of the reference line
  //! @return                  id of reference line
  //!
  virtual Id GetReferenceLine() const = 0;
};

//!This class represents a road in the world
class Road
{
public:
  virtual ~Road() = default;  //!< default destructor

  //!Returns the Id of the road
  //!
  //! @return Id of the road
  virtual const std::string &GetId() const = 0;

  //!Adds a section to this road
  //!
  //! @param section  Section to this road
  virtual void AddSection(Interfaces::Section &section) = 0;

  //!Returns a vector of all sections that this road consists of
  //!
  //! @return sections of the road
  virtual const Sections &GetSections() const = 0;

  //!Returns the length of the road
  //!
  //! @return length of the road
  virtual units::length::meter_t GetLength() const = 0;

  //!Returns the id of the successor of the road (i.e. next road or junction)
  //!
  //! @return successor of the road
  virtual const std::string &GetSuccessor() const = 0;

  //!Returns the id of the predecessor of the road (i.e. previous road or junction)
  //!
  //! @return predecessor of the road
  virtual const std::string &GetPredecessor() const = 0;

  //! Sets the successor of the road
  //! \param successor    id of successor (road or junction)
  virtual void SetSuccessor(const std::string &successor) = 0;

  //! Sets the predecessor of the road
  //! \param predecessor  id of predecessor (road or junction)
  virtual void SetPredecessor(const std::string &predecessor) = 0;

  //!Returns true if the direction of the road is the same as the direction of the road stream,
  //! false otherwise
  //!
  //! @return true if the direction of the road is the same as the direction of the road stream
  virtual bool IsInStreamDirection() const = 0;

  //! Returns the distance to a point on the road, relative to the road itself
  //!
  //! \param mp the point on the road to measure
  //! \return distance to a point on the road, relative to the road itself
  virtual units::length::meter_t GetDistance(MeasurementPoint mp) const = 0;
};

//!This class represents a junction in the world
class Junction
{
public:
  virtual ~Junction() = default;  //!< default destructor

  //!Returns the Id of the Junction
  //!
  //! @return Id of the Junction
  virtual const std::string &GetId() const = 0;

  //!Adds a connecting road to this road
  //!
  //! @param connectingRoad   Connecting road
  virtual void AddConnectingRoad(const Interfaces::Road *connectingRoad) = 0;

  //!Returns a vector of all connecting roads that this road consists of
  //!
  //! @return connecting roads
  virtual const Roads &GetConnectingRoads() const = 0;

  //!Adds a priority entry for this junction
  //!
  //! @param high High priority
  //! @param low  Low priority
  virtual void AddPriority(const std::string &high, const std::string &low) = 0;

  //!Returns all priorities of connectors in this junction
  //!
  //! @return priorities of connectors in this junction
  virtual const Priorities &GetPriorities() const = 0;

  //!Adds intersection info for the road with roadId to the junction
  //!
  //! @param roadId           Id of the road
  //! @param intersectionInfo Intersection info to add
  virtual void AddIntersectionInfo(const std::string &roadId, const IntersectionInfo &intersectionInfo) = 0;

  //!Gets all of this junction's roads' intersection info
  //!
  //! @return junction's roads' intersection info
  virtual const std::map<std::string, std::vector<IntersectionInfo>> &GetIntersections() const = 0;
};

//!Common base for all objects in the world that can be on the road and have a position and a dimension
//! WorldObjects are either MovingObjects or StationaryObjects
class WorldObject
{
public:
  virtual ~WorldObject() = default;  //!< default destructor

  /*!
   * \brief Copies the underlying OSI object to the given GroundTruth
   *
   * \param[in]   target   The target OSI GroundTruth
   */
  virtual void CopyToGroundTruth(osi3::GroundTruth &target) const = 0;

  //!Returns the OSI Id of the object
  //!
  //! @return OSI Id
  virtual Id GetId() const = 0;

  //!Returns the dimension of the object
  //!
  //! @return dimension of the object
  virtual mantle_api::Dimension3 GetDimension() const = 0;

  //!Returns the position of the reference point of the object in absolute coordinates (i. e. world coordinates)
  //!
  //! @return position of the reference point of the object in absolute coordinates
  virtual mantle_api::Vec3<units::length::meter_t> GetReferencePointPosition() const = 0;

  //!Returns the orientation of the object in absolute coordinates (i. e. world coordinates)
  //!
  //! @return orientation of the object in absolute coordinates
  virtual mantle_api::Orientation3<units::angle::radian_t> GetAbsOrientation() const = 0;

  //!Returns the absolute value of the velocity if the object is moving in road direction
  //! or the inverse if the object is moving against the road direction
  //!
  //! @return absolute value of the velocity
  virtual units::velocity::meters_per_second_t GetAbsVelocityDouble() const = 0;

  //!Returns the absolute value of the acceleration if the object is moving in road direction
  //! or the inverse if the object is moving against the road direction
  //!
  //! @return absolute value of the acceleration
  virtual units::acceleration::meters_per_second_squared_t GetAbsAccelerationDouble() const = 0;

  //!Returns the road intervals touched by the object
  //!
  //! @return road intervals touched by the object
  virtual const RoadIntervals &GetTouchedRoads() const = 0;

  //!Assigns a lane to this object
  //! @param lane             Reference to the lane interface
  //! @param referencePoint   Reference point in road position coordinate system, as optional
  virtual void AddLaneAssignment(const Interfaces::Lane &lane, const std::optional<RoadPosition> &referencePoint) = 0;

  //!Returns a vector of all lanes that this object is assigned to
  //!
  //! @return lanes that this object is assigned to
  virtual const Interfaces::Lanes &GetLaneAssignments() const = 0;

  //!Clears the list of lanes that this object is assigned to
  virtual void ClearLaneAssignments() = 0;

  //!Sets the position of the reference point of the object in absolute coordinates (i. e. world coordinates)
  //!
  //! @param newPosition  position of the reference point of the object in absolute coordinates
  virtual void SetReferencePointPosition(const mantle_api::Vec3<units::length::meter_t> &newPosition) = 0;

  //!Sets the dimension of the object
  //!
  //! @param newDimension dimension of the object
  virtual void SetDimension(const mantle_api::Dimension3 &newDimension) = 0;

  //!Sets the orientation of the object in absolute coordinates (i. e. world coordinates)
  //!
  //! @param newOrientation   orientation of the object in absolute coordinates
  virtual void SetAbsOrientation(const mantle_api::Orientation3<units::angle::radian_t> &newOrientation) = 0;

  //!Sets the road intervals touched by the object
  //!
  //! @param touchedRoads road intervals touched by the object
  virtual void SetTouchedRoads(const RoadIntervals &touchedRoads) = 0;

  /// @brief Links the object to the world
  /// @param linkedObject Linked object
  void SetLink(void *linkedObject) { this->linkedObject = linkedObject; }

  //!Returns the WorldObjectInterface that this WorldObject is linked to and casts it to T*
  //! T can be
  //! -WorldObjectsAdapter or
  //! -AgentAdapter, if this is a MovingObject or
  //! -TrafficAdapter, if this is a StationaryObject
  //!
  //! @return WorldObjectInterface that this WorldObject is linked to and casts it to T*
  template <typename T>
  T *GetLink()
  {
    auto link = dynamic_cast<T *>(static_cast<WorldObjectInterface *>(linkedObject));
    assert(link != nullptr);
    return link;
  }

  //!Returns the WorldObjectInterface that this WorldObject is linked to and casts it to T*
  //! T can be
  //! -WorldObjectsAdapter or
  //! -AgentAdapter, if this is a MovingObject or
  //! -TrafficAdapter, if this is a StationaryObject
  //!
  //! @return WorldObjectInterface that this WorldObject is linked to and casts it to T*
  template <typename T>
  T *GetLink() const
  {
    auto link = dynamic_cast<T *>(static_cast<WorldObjectInterface *>(linkedObject));
    assert(link != nullptr);
    return link;
  }

  //!Returns true if this object is of type T otherwise false
  //!
  //! @return true if this object is of type T otherwise false
  template <typename T>
  bool Is() const
  {
    return !(dynamic_cast<const T *>(this) == nullptr);
  }

  //!Returns this object casted to T*
  //!
  //! @return this object casted to T*
  template <typename T>
  T *As() const
  {
    return static_cast<T *>(this);
  }

  //!Returns this object casted to T*
  //!
  //! @return this object casted to T*
  template <typename T>
  T *As()
  {
    return static_cast<T *>(this);
  }

protected:
  /// Object linked to this WorldObject
  void *linkedObject{nullptr};
};

//! This class represents StationaryObject in the world that can be on the road and have a position and a dimension
class StationaryObject : public Interfaces::WorldObject
{
public:
  virtual ~StationaryObject() = default;  //!< default destructor

  virtual const RoadIntervals &GetTouchedRoads() const = 0;

  virtual void SetReferencePointPosition(const mantle_api::Vec3<units::length::meter_t> &newPosition) = 0;

  virtual void SetDimension(const mantle_api::Dimension3 &newDimension) = 0;
  virtual void SetAbsOrientation(const mantle_api::Orientation3<units::angle::radian_t> &newOrientation) = 0;

  virtual void SetTouchedRoads(const RoadIntervals &touchedRoads) = 0;

  /// @brief Set the source reference for this stationary object
  /// @param odId Id of the object in opendrive
  virtual void SetSourceReference(const std::string &odId) = 0;
};

//! This class represents MovingObject in the world that can be on the road and have a position and a dimension
class MovingObject : public Interfaces::WorldObject
{
public:
  virtual ~MovingObject() = default;  //!< default destructor

  virtual const RoadIntervals &GetTouchedRoads() const = 0;

  /**
   * @brief Currently not implemented
   *
   * @return
   */
  virtual Primitive::LaneOrientation GetLaneOrientation() const = 0;

  /**
   * @brief Returns the velocity of the object in absolute coordinates (i. e. world coordinates)
   *
   * @return velocity of the object in absolute coordinates
   */
  virtual mantle_api::Vec3<units::velocity::meters_per_second_t> GetAbsVelocity() const = 0;

  /**
   * @brief Returns the acceleration of the object in absolute coordinates (i. e. world coordinates)
   *
   * @return acceleration of the object in absolute coordinates
   */
  virtual Primitive::AbsAcceleration GetAbsAcceleration() const = 0;

  /**
   * @brief Returns the orientation rate of the object in absolute coordinates (i. e. world coordinates)
   *
   * @return orientation rate of the object in absolute coordinates
   */
  virtual mantle_api::Orientation3<units::angular_velocity::radians_per_second_t> GetAbsOrientationRate() const = 0;

  /**
   * @brief Returns the orientation acceleration of the object in absolute coordinates (i. e. world coordinates)
   *
   * @return orientation acceleration of the object in absolute coordinates
   */
  virtual Primitive::AbsOrientationAcceleration GetAbsOrientationAcceleration() const = 0;

  virtual void SetDimension(const mantle_api::Dimension3 &newDimension) = 0;

  /**
   * @brief Sets the length of the object
   *
   * @param[in] newLength Length of the object
   */
  virtual void SetLength(const units::length::meter_t newLength) = 0;

  /**
   * @brief Sets the width of the object
   *
   * @param[in] newWidth Width of the object
   */
  virtual void SetWidth(const units::length::meter_t newWidth) = 0;

  /**
   * @brief Sets the height of the object
   *
   * @param[in] newHeight Height of the object
   */
  virtual void SetHeight(const units::length::meter_t newHeight) = 0;

  /**
   * @brief Retrieves the distance from the reference point to the leading edge
   *
   * @return distance from the reference point to the leading edge
   */
  virtual units::length::meter_t GetDistanceReferencePointToLeadingEdge() const = 0;

  virtual void SetReferencePointPosition(const mantle_api::Vec3<units::length::meter_t> &newPosition) = 0;

  /**
   * @brief Sets the bounding box center point to the middle (in x, y and z) of the rear axle in object coordinates
   *
   * @param[in] distanceX Distance from the bounding box center point to the middle of the rear axle in x coordinate
   * @param[in] distanceY Distance from the bounding box center point to the middle of the rear axle in y coordinate
   * @param[in] distanceZ Distance from the bounding box center point to the middle of the rear axle in z coordinate
   */
  virtual void SetBoundingBoxCenterToRear(const units::length::meter_t distanceX,
                                          const units::length::meter_t distanceY,
                                          const units::length::meter_t distanceZ)
      = 0;

  /**
   * @brief Sets the bounding box center point to the middle (in x, y and z) of the front axle in object coordinates
   *
   * @param[in] distanceX Distance from the bounding box center point to the middle of the front axle in x coordinate
   * @param[in] distanceY Distance from the bounding box center point to the middle of the front axle in y coordinate
   * @param[in] distanceZ Distance from the bounding box center point to the middle of the front axle in z coordinate
   */
  virtual void SetBoundingBoxCenterToFront(const units::length::meter_t distanceX,
                                           const units::length::meter_t distanceY,
                                           const units::length::meter_t distanceZ)
      = 0;

  /**
   * @brief Sets the x-coordinate
   *
   * @param[in] newX  x-coordinate
   */
  virtual void SetX(const units::length::meter_t newX) = 0;

  /**
   * @brief Sets the y-coordinate
   *
   * @param[in] newY  y-coordinate
   */
  virtual void SetY(const units::length::meter_t newY) = 0;

  /**
   * @brief Sets the z-coordinate
   *
   * @param[in] newZ  z-coordinate
   */
  virtual void SetZ(const units::length::meter_t newZ) = 0;

  virtual void SetTouchedRoads(const RoadIntervals &touchedRoads) = 0;

  virtual void SetAbsOrientation(const mantle_api::Orientation3<units::angle::radian_t> &newOrientation) = 0;
  /**
   * @brief Sets yaw angle of the object
   *
   * @param[in] newYaw  Yaw angle
   */
  virtual void SetYaw(const units::angle::radian_t newYaw) = 0;

  /**
   * @brief Sets pitch angle of the object
   *
   * @param[in] newPitch  Pitch angle
   */
  virtual void SetPitch(const units::angle::radian_t newPitch) = 0;

  /**
   * @brief Sets roll angle of the object
   *
   * @param[in] newRoll  Roll angle
   */
  virtual void SetRoll(const units::angle::radian_t newRoll) = 0;

  /**
   * @brief Sets the velocity of the object in absolute coordinates (i. e. world coordinates)
   *
   * @param[in] newVelocity   Velocity of the object in absolute coordinates
   */
  virtual void SetAbsVelocity(const mantle_api::Vec3<units::velocity::meters_per_second_t> &newVelocity) = 0;
  /**
   * @brief Sets the velocity of the object in absolute coordinates (i. e. world coordinates)
   *
   * @param[in] newVelocity   Velocity to set
   */
  virtual void SetAbsVelocity(const units::velocity::meters_per_second_t newVelocity) = 0;

  /**
   * @brief Sets the acceleration of the object in absolute coordinates (i. e. world coordinates)
   *
   * @param[in] newAcceleration   Acceleration of the object in absolute coordinates
   */
  virtual void SetAbsAcceleration(const Primitive::AbsAcceleration &newAcceleration) = 0;

  /**
   * @brief Sets the acceleration of the object in absolute coordinates (i. e. world coordinates)
   *
   * @param[in] newAcceleration   Acceleration to set
   */
  virtual void SetAbsAcceleration(const units::acceleration::meters_per_second_squared_t newAcceleration) = 0;

  /**
   * @brief Sets the orientation rate of the object in absolute coordinates (i. e. world coordinates)
   *
   * @param[in] newOrientationRate    Orientation rate of the object in absolute coordinates
   */
  virtual void SetAbsOrientationRate(
      const mantle_api::Orientation3<units::angular_velocity::radians_per_second_t> &newOrientationRate)
      = 0;

  /**
   * @brief Sets the orientation acceleration of the object in absolute coordinates (i. e. world coordinates)
   *
   * @param[in] newOrientationAcceleration    Orientation acceleration of the object in absolute coordinates
   */
  virtual void SetAbsOrientationAcceleration(const Primitive::AbsOrientationAcceleration &newOrientationAcceleration)
      = 0;

  virtual void AddLaneAssignment(const Interfaces::Lane &lane, const std::optional<RoadPosition> &referencePoint) = 0;

  virtual const Interfaces::Lanes &GetLaneAssignments() const = 0;

  virtual void ClearLaneAssignments() = 0;

  /**
   * @brief Sets the state of the object's indicators
   *
   * @param[in] indicatorState    State of the object's indicators
   */
  virtual void SetIndicatorState(IndicatorState indicatorState) = 0;

  /**
   * @brief Retrieves the state of the object's indicators
   *
   * @return state of the object's indicators
   */
  virtual IndicatorState GetIndicatorState() const = 0;

  /**
   * @brief Sets the state of the brake lights
   *
   * @param[in] brakeLightState    Status of brake lights
   */
  virtual void SetBrakeLightState(bool brakeLightState) = 0;

  /**
   * @brief Retrieves the status of the brake lights
   *
   * @return true if brake lights are on with normal intensity or false if brake lights are off
   */
  virtual bool GetBrakeLightState() const = 0;

  /**
   * @brief Sets the head light on or off
   *
   * @param[in] headLight     Status of head light
   */
  virtual void SetHeadLight(bool headLight) = 0;

  /**
   * @brief Retrieves the status of the head light
   *
   * @return true if head light is on or false if head light is off
   */
  virtual bool GetHeadLight() const = 0;

  /**
   * @brief Sets the high beam light on or off
   *
   * @param[in] highbeamLight     Status of high beam light
   */
  virtual void SetHighBeamLight(bool highbeamLight) = 0;

  /**
   * @brief Retrieves the status of the high beam light
   *
   * @return true if high beam light is on or false if high beam light is off
   */
  virtual bool GetHighBeamLight() const = 0;

  /// @brief Set the type of the agent vehicle
  /// @param type Type of the agent vehicle
  virtual void SetType(mantle_api::EntityType type) = 0;

  //! @brief Set the classification of the vehicle
  //! @param vehicleClassification    Classification of the vehicle
  virtual void SetVehicleClassification(mantle_api::VehicleClass vehicleClassification) = 0;

  /**
   * @brief Currently not implemented
   *
   * @param[in] externalReference References to external objects (objects defined outside of OSI)
   */
  virtual void SetSourceReference(const ExternalReference &externalReference) = 0;

  /// @brief Add the wheel data to the moving object
  /// @param[in] wheelData Wheel data of the moving object
  virtual void AddWheel(const WheelData &wheelData) = 0;

  /// @brief Get wheel data of the moving object
  /// @param axleIndex Index of the axle
  /// @param rowIndex  Index of the row
  /// @return Wheel data, optional
  virtual std::optional<const WheelData> GetWheelData(unsigned int axleIndex, unsigned int rowIndex) = 0;

  /**
   * @brief Gets current angle of the steering wheel in radian
   *
   * @return current steering wheel angle
   */
  virtual Angle GetSteeringWheelAngle() = 0;

  /**
   * @brief Sets the angle of the steering wheel in radian
   *
   * @param[in] newValue  Angle of the steering wheel
   */
  virtual void SetSteeringWheelAngle(const Angle newValue) = 0;

  /// @param wheelYaw Setter function to set wheel yaw rate to the moving object
  virtual void SetFrontAxleSteeringYaw(const Angle wheelYaw) = 0;

  /// @brief  Set wheels rotation rate and its orientation to the moving object
  /// @param velocity             Velocity of the moving object
  /// @param wheelRadiusFront     Front wheel radius
  /// @param wheelRadiusRear      Rear wheel radius
  /// @param cycleTime            Cycle time
  virtual void SetWheelsRotationRateAndOrientation(const units::velocity::meters_per_second_t velocity,
                                                   const units::length::meter_t wheelRadiusFront,
                                                   const units::length::meter_t wheelRadiusRear,
                                                   const units::time::second_t cycleTime)
      = 0;

  /// @brief Sets the rotation rate of selected wheel
  /// @param     axleIndex	selected Axle (value 0 refers to the front axle; index increases towards the rear axle)
  /// @param	   wheelIndex	selected Wheel (value 0 refers to a right wheel,index increases towards the left wheel)
  /// @param	   rotationRate rotation rate of selected wheel
  virtual void SetWheelRotationRate(const int axleIndex, const int wheelIndex, double rotationRate) = 0;

  /*!
   * \brief Copies the underlying OSI object to the given GroundTruth
   *
   * \param[in]   target   The target OSI GroundTruth
   */
  virtual void CopyToGroundTruth(osi3::GroundTruth &target) const = 0;
};

//! This class represents a static traffic sign
class TrafficSign
{
public:
  virtual ~TrafficSign() = default;  //!< default destructor

  //! Returns the OpenDrive ID
  //!
  //! @return OpenDrive ID
  virtual std::string GetId() const = 0;

  //! Returns the s coordinate
  //!
  //! @return s coordinate
  virtual units::length::meter_t GetS() const = 0;

  //! Returns the value of the sign converted in SI Units
  //!
  //! @param osiValue SI value
  //! @param osiUnit  SI Units
  //! @return value of the sign converted in SI Units
  virtual std::pair<double, CommonTrafficSign::Unit> GetValueAndUnitInSI(
      const double osiValue, const osi3::TrafficSignValue_Unit osiUnit) const
      = 0;

  //! Returns the specification of the sign with the set relative distance
  //!
  //! @param relativeDistance assign relativeDistance
  //! @return specification of the sign
  virtual CommonTrafficSign::Entity GetSpecification(const units::length::meter_t relativeDistance) const = 0;

  //! Returns whether the sign is valid for the specified lane
  //!
  //! @param laneId   Id of the lane
  //! @return true if the sign is valid for the specified lane otherwise false
  virtual bool IsValidForLane(OWL::Id laneId) const = 0;

  //!Returns the position of the reference point of the object in absolute coordinates (i. e. world coordinates)
  //!
  //! @return position of the reference point of the object in absolute coordinates
  virtual mantle_api::Vec3<units::length::meter_t> GetReferencePointPosition() const = 0;

  //!Returns the dimension of the sign
  //!
  //! @return dimension of the sign
  virtual mantle_api::Dimension3 GetDimension() const = 0;

  //! Sets the s coordinate
  //!
  //! @param sPos s position
  virtual void SetS(units::length::meter_t sPos) = 0;

  /// @brief set valid for lane given lane and road signal interface
  /// @param lane             Reference to Lane interface
  /// @param specification    Reference to road signal interface
  virtual void SetValidForLane(const OWL::Interfaces::Lane &lane, const RoadSignalInterface &specification) = 0;

  //! Converts the specification imported from OpenDrive to OSI.
  //!
  //! \param signal       OpenDrive specification
  //! \param position     position in the world
  //! \return     true if succesful, false if the sign can not be converted
  virtual bool SetSpecification(RoadSignalInterface *signal, Position position) = 0;

  //! Adds a supplementary sign to this sign and converts its specifation from OpenDrive to OSI
  //!
  //! \param odSignal     OpenDrive specification of supplementary sign
  //! \param position     position of the supplementary sign
  //! \return     true if succesful, false if the sign can not be converted
  virtual bool AddSupplementarySign(RoadSignalInterface *odSignal, Position position) = 0;

  /*!
   * \brief Copies the underlying OSI object to the given GroundTruth
   *
   * \param[in]   target   The target OSI GroundTruth
   */
  virtual void CopyToGroundTruth(osi3::GroundTruth &target) const = 0;
};

//! This class represents a traffic light
class TrafficLight
{
public:
  virtual ~TrafficLight() = default;  //!< default destructor

  //! Returns the OpenDrive ID
  //!
  //! @return OpenDrive ID
  virtual std::string GetId() const = 0;

  //! Returns the s coordinate
  //!
  //! @return s coordinate
  virtual units::length::meter_t GetS() const = 0;

  //! Returns whether the sign is valid for the specified lane
  //!
  //! @param laneId  Id of the lane
  //! @return true if  the sign is valid for the specified lane
  virtual bool IsValidForLane(OWL::Id laneId) const = 0;

  //!Returns the position of the reference point of the object in absolute coordinates (i. e. world coordinates)
  //!
  //! @return position of the reference point of the object in absolute coordinates
  virtual mantle_api::Vec3<units::length::meter_t> GetReferencePointPosition() const = 0;

  //! Sets the s coordinate
  //!
  //! @param sPos s position
  virtual void SetS(units::length::meter_t sPos) = 0;

  //!Returns the dimension of the sign
  //!
  //! @return the dimension of the sign
  virtual mantle_api::Dimension3 GetDimension() const = 0;

  //! Converts the specification imported from OpenDrive to OSI.
  //!
  //! \param signal       OpenDrive specification
  //! \param position     position in the world
  //! \return     true if succesfull, false if the sign can not be converted
  virtual bool SetSpecification(RoadSignalInterface *signal, const Position &position) = 0;

  /// @brief set valid for lane given lane and road signal interface
  /// @param lane             Reference to Lane interface
  /// @param specification    Reference to road signal interface
  virtual void SetValidForLane(const OWL::Interfaces::Lane &lane, const RoadSignalInterface &specification) = 0;

  /*!
   * \brief Copies the underlying OSI object to the given GroundTruth
   *
   * \param[in]   target   The target OSI GroundTruth
   */
  virtual void CopyToGroundTruth(osi3::GroundTruth &target) const = 0;

  //! Sets the state
  //!
  //! @param  newState    assign state
  virtual void SetState(CommonTrafficLight::State newState) = 0;

  //! Returns the specification of the light with the set relative distance
  //!
  //! @param relativeDistance assign relativeDistance
  //! @return specification of the sign
  virtual CommonTrafficLight::Entity GetSpecification(const units::length::meter_t relativeDistance) const = 0;

  //!Returns the current state
  //!
  //! @return current state
  virtual CommonTrafficLight::State GetState() const = 0;
};

//! This class represents a traffic sign that is painted on the road surface
class RoadMarking
{
public:
  virtual ~RoadMarking() = default;  //!< default destructor

  //! Returns the OpenDrive ID
  //!
  //! @return OpenDrive ID
  virtual std::string GetId() const = 0;

  //! Returns the s coordinate
  //!
  //! @return s coordinate
  virtual units::length::meter_t GetS() const = 0;

  //! Returns the specification of the sign with the set relative distance
  //!
  //! @param relativeDistance assign relativeDistance
  //! @return specification of the sign
  virtual CommonTrafficSign::Entity GetSpecification(const units::length::meter_t relativeDistance) const = 0;

  //! Returns whether the sign is valid for the specified lane
  //!
  //! @param laneId  Id of the lane
  //! @return true if  the sign is valid for the specified lane
  virtual bool IsValidForLane(OWL::Id laneId) const = 0;

  //!Returns the position of the reference point of the object in absolute coordinates (i. e. world coordinates)
  //!
  //! @return position of the reference point of the object in absolute coordinates
  virtual mantle_api::Vec3<units::length::meter_t> GetReferencePointPosition() const = 0;

  //!Returns the dimension of the sign
  //!
  //! @return the dimension of the sign
  virtual mantle_api::Dimension3 GetDimension() const = 0;

  //! Sets the s coordinate
  //!
  //! @param sPos s position
  virtual void SetS(units::length::meter_t sPos) = 0;

  /// @brief set valid for lane given lane and road signal interface
  /// @param lane             Reference to Lane interface
  /// @param specification    Reference to road signal interface
  virtual void SetValidForLane(const OWL::Interfaces::Lane &lane, const RoadSignalInterface &specification) = 0;

  /// @brief set valid for lane given lane and road object interface
  /// @param lane             Reference to Lane interface
  /// @param specification    Reference to road object interface
  virtual void SetValidForLane(const OWL::Interfaces::Lane &lane, const RoadObjectInterface &specification) = 0;

  //! Converts the specification imported from OpenDrive to OSI.
  //!
  //! \param signal       OpenDrive specification
  //! \param position     position in the world
  //! \return     true if succesfull, false if the road marking can not be converted
  virtual bool SetSpecification(RoadSignalInterface *signal, Position position) = 0;

  //! Converts the specification imported from OpenDrive to OSI.
  //!
  //! \param signal       OpenDrive specification
  //! \param position     position in the world
  //! \return     true if succesfull, false if the road marking can not be converted
  virtual bool SetSpecification(RoadObjectInterface *signal, Position position) = 0;

  /*!
   * \brief Copies the underlying OSI object to the given GroundTruth
   *
   * \param[in]   target   The target OSI GroundTruth
   */
  virtual void CopyToGroundTruth(osi3::GroundTruth &target) const = 0;
};

}  // namespace Interfaces

namespace Implementation
{

//! This class represents a single lane inside a section
class Lane : public Interfaces::Lane
{
public:
  //! @param[in] osiLane          representation of the road in OpenDrive
  //! @param[in] osiLogicalLane   representation of the logical lane in OpenDrive
  //! @param[in] section          section that this lane is part of
  //! @param[in] odId             id of the road in opendrive
  Lane(osi3::Lane *osiLane, osi3::LogicalLane *osiLogicalLane, const Interfaces::Section *section, OdId odId);

  ~Lane() override;

  void CopyToGroundTruth(osi3::GroundTruth &target) const override;

  Id GetId() const override;

  /// @return Returns id of the logica lane
  Id GetLogicalLaneId() const override;

  OdId GetOdId() const override;

  bool Exists() const override;

  const Interfaces::Section &GetSection() const override;

  const Interfaces::Road &GetRoad() const override;

  units::length::meter_t GetLength() const override;

  int GetRightLaneCount() const override;

  units::curvature::inverse_meter_t GetCurvature(units::length::meter_t distance) const override;
  units::length::meter_t GetWidth(units::length::meter_t distance) const override;
  units::angle::radian_t GetDirection(units::length::meter_t distance) const override;

  const std::vector<Id> &GetNext() const override { return next; }

  const std::vector<Id> &GetPrevious() const override { return previous; }

  const Interfaces::Lane &GetLeftLane() const override;

  const Interfaces::Lane &GetRightLane() const override;

  const std::vector<Id> GetLeftLaneBoundaries() const override;

  const std::vector<Id> GetRightLaneBoundaries() const override;

  /// @return Returns ID of left logical lane boundary
  const std::vector<Id> GetLeftLogicalLaneBoundaries() const override;

  /// @return Returns ID of right logical lane boundary
  const std::vector<Id> GetRightLogicalLaneBoundaries() const override;

  units::length::meter_t GetDistance(MeasurementPoint measurementPoint) const override;

  LaneType GetLaneType() const override;
  bool Covers(units::length::meter_t distance) const override;

  /// @brief Add next lane information to the current lane
  /// @param lane                 Information about the lane
  /// @param atBeginOfOtherLane   Boolean, if the other lane is at the begin
  void AddNext(const Interfaces::Lane *lane, bool atBeginOfOtherLane) override;

  /// @brief Add previous lane information to the current lane
  /// @param lane                 Information about the lane
  /// @param atBeginOfOtherLane   Boolean, if the other lane is at the begin
  void AddPrevious(const Interfaces::Lane *lane, bool atBeginOfOtherLane) override;

  void SetLanePairings() override;

  void AddOverlappingLane(OWL::Id overlappingLaneId,
                          units::length::meter_t startS,
                          units::length::meter_t endS,
                          units::length::meter_t startSOther,
                          units::length::meter_t endSOther) override;

  const Interfaces::LaneGeometryElements &GetLaneGeometryElements() const override;
  void AddLaneGeometryJoint(const Common::Vector2d<units::length::meter_t> &pointLeft,
                            const Common::Vector2d<units::length::meter_t> &pointCenter,
                            const Common::Vector2d<units::length::meter_t> &pointRight,
                            units::length::meter_t sOffset,
                            units::curvature::inverse_meter_t curvature,
                            units::angle::radian_t heading) override;

  void SetLeftLane(const Interfaces::Lane &lane, bool transferLaneBoundary) override;

  void SetRightLane(const Interfaces::Lane &lane, bool transferLaneBoundary) override;

  void SetLeftLaneBoundaries(const std::vector<OWL::Id> &laneBoundaryIds) override;

  void SetRightLaneBoundaries(const std::vector<OWL::Id> &laneBoundaryIds) override;

  void SetLeftLogicalLaneBoundaries(const std::vector<OWL::Id> &laneBoundaryIds) override;

  void SetRightLogicalLaneBoundaries(const std::vector<OWL::Id> &laneBoundaryIds) override;

  void SetLaneType(LaneType specifiedType) override;

  const Interfaces::LaneAssignments &GetWorldObjects(bool direction) const override;

  const Interfaces::TrafficSigns &GetTrafficSigns() const override;

  const Interfaces::RoadMarkings &GetRoadMarkings() const override;

  const Interfaces::TrafficLights &GetTrafficLights() const override;

  void AddMovingObject(OWL::Interfaces::MovingObject &movingObject, const LaneOverlap &laneOverlap) override;

  void AddStationaryObject(OWL::Interfaces::StationaryObject &stationaryObject,
                           const LaneOverlap &laneOverlap) override;

  void AddWorldObject(Interfaces::WorldObject &worldObject, const LaneOverlap &laneOverlap) override;

  void AddTrafficSign(Interfaces::TrafficSign &trafficSign) override;

  void AddRoadMarking(Interfaces::RoadMarking &roadMarking) override;

  void AddTrafficLight(Interfaces::TrafficLight &trafficLight) override;

  void ClearMovingObjects() override;

  /// @brief Get neighbouring joint
  /// @param distance Distance to the joint
  /// @return left, right and reference point at the specified s coordinate interpolated between the geometry joints
  std::tuple<const Primitive::LaneGeometryJoint *, const Primitive::LaneGeometryJoint *> GetNeighbouringJoints(
      units::length::meter_t distance) const;

  const Primitive::LaneGeometryJoint::Points GetInterpolatedPointsAtDistance(
      units::length::meter_t distance) const override;

  //! @brief Collects objects assigned to lanes and manages their order w.r.t the querying direction
  //!
  //! Depending on the querying direction, objects on a lane need to be sorted
  //! either by their starting coordinate (s_min) or their ending coordinate (s_max).
  //! When looking downstream, objects are sorted ascending by their s_min coordinate.
  //! When looking upstream, objects are sorted descending by their s_max coordinate.
  class LaneAssignmentCollector
  {
  public:
    LaneAssignmentCollector() noexcept;

    //! @brief Insert an object, given it's lane overlap w.r.t the current lane
    //! @param laneOverlap minimum and maximum s coordinate of the object
    //! @param object      the object
    void Insert(const LaneOverlap &laneOverlap, const Interfaces::WorldObject *object);

    //! @brief Get list of LaneAssignements
    //! @param downstream  true for looking "in stream direction", else false
    //! @return list of LaneAssignements
    const Interfaces::LaneAssignments &Get(bool downstream) const;

    //! @brief Clears the managed collections
    void Clear();

  private:
    //! @brief Sorts the managed collections using a custom sorter for LaneOverlap
    //!
    //! The two internal collecions are sorted by the following comparisons:
    //! Ascending: Unless equal, the smaller s_min wins, else the smaller s_max
    //! Descending: Unless equal, the larger s_max wins, else the larger s_min
    void Sort() const;

    //! @brief  Flag indicating, that collections need sorting
    //!
    //! For performance reasons, this class uses lazy sorting, meaning that insertion
    //! happens unordered and sorting is applied at the very first access of the collections.
    bool mutable dirty{false};

    Interfaces::LaneAssignments mutable downstreamOrderAssignments;
    Interfaces::LaneAssignments mutable upstreamOrderAssignments;

    static constexpr size_t INITIAL_COLLECTION_SIZE
        = 32;  //!!< Minimum reserved collection size to prevent to many reallocations
  };

protected:
  osi3::Lane *osiLane{nullptr};                ///< OSI lane type
  osi3::LogicalLane *osiLogicalLane{nullptr};  ///< OSI logical lane typ

private:
  OdId odId;
  LaneType laneType{LaneType::Undefined};
  LaneAssignmentCollector worldObjects;
  Interfaces::LaneAssignments stationaryObjects;
  Interfaces::TrafficSigns trafficSigns;
  Interfaces::RoadMarkings roadMarkings;
  Interfaces::TrafficLights trafficLights;
  const Interfaces::Section *section;
  Interfaces::LaneGeometryJoints laneGeometryJoints;
  Interfaces::LaneGeometryElements laneGeometryElements;
  std::vector<Id> next;
  std::vector<Id> previous;
  const Interfaces::Lane *leftLane;
  const Interfaces::Lane *rightLane;
  units::length::meter_t length{0.0};
  bool leftLaneIsDummy{true};
  bool rightLaneIsDummy{true};
};

//! This class represents invalid lane inside a section
class InvalidLane : public Lane
{
public:
  InvalidLane() : Lane(new osi3::Lane(), nullptr, nullptr, 0)
  {
    // set 'invalid' id
    osiLane->mutable_id()->set_value(InvalidId);
  }

  virtual ~InvalidLane();  //!< default destructor

  InvalidLane(const InvalidLane &) = delete;

  InvalidLane &operator=(const InvalidLane &) = delete;

  InvalidLane(InvalidLane &&) = delete;

  InvalidLane &operator=(InvalidLane &&) = delete;

  const Interfaces::Lane &GetLeftLane() const override { return *this; }

  const Interfaces::Lane &GetRightLane() const override { return *this; }
};

//! This class represents the boundary between two lanes
class LaneBoundary : public Interfaces::LaneBoundary
{
public:
  /**
   * @brief LaneBoundary constructor
   *
   * @param[in] osiLaneBoundary   Underlying OSI object
   * @param[in] width             width of the line
   * @param[in] sStart            s coordinate where this boundary starts
   * @param[in] sEnd              s coordinate where this boundary ends
   * @param[in] side              Side (Left/Right) if this boundary is part of a double line or Single otherwise
   */
  LaneBoundary(osi3::LaneBoundary *osiLaneBoundary,
               units::length::meter_t width,
               units::length::meter_t sStart,
               units::length::meter_t sEnd,
               LaneMarkingSide side);

  Id GetId() const override;
  units::length::meter_t GetWidth() const override;
  units::length::meter_t GetSStart() const override;
  units::length::meter_t GetSEnd() const override;

  LaneMarking::Type GetType() const override;

  LaneMarking::Color GetColor() const override;

  LaneMarkingSide GetSide() const override;
  void AddBoundaryPoint(const Common::Vector2d<units::length::meter_t> &point, units::angle::radian_t heading) override;

  void CopyToGroundTruth(osi3::GroundTruth &target) const override;

private:
  osi3::LaneBoundary *osiLaneBoundary;  //! underlying OSI object
  units::length::meter_t sStart;        //! s coordinate where this boundary starts
  units::length::meter_t sEnd;          //! s coordinate where this boundary ends
  units::length::meter_t width;         //! width of the line
  LaneMarkingSide side;  //! Side (Left/Right) if this boundary is part of a double line or Single otherwise
};

/// @brief Class representing a logical lane boundary
class LogicalLaneBoundary : public Interfaces::LogicalLaneBoundary
{
public:
  /// @brief Create a logical lane boundary
  /// @param osiLaneBoundary  Pointer to the OSI lane boundary
  /// @param sStart           s coordinate where this boundary starts
  /// @param sEnd             s coordinate where this boundary ends
  LogicalLaneBoundary(osi3::LogicalLaneBoundary *osiLaneBoundary,
                      units::length::meter_t sStart,
                      units::length::meter_t sEnd);

  Id GetId() const override;
  units::length::meter_t GetSStart() const override;
  units::length::meter_t GetSEnd() const override;

  void AddBoundaryPoint(const Common::Vector2d<units::length::meter_t> &point,
                        units::length::meter_t s,
                        units::length::meter_t t) override;

  void CopyToGroundTruth(osi3::GroundTruth &target) const override;

  void SetPhysicalBoundaryIds(const std::vector<OWL::Id> &ids) override;

private:
  osi3::LogicalLaneBoundary *osiLogicalLaneBoundary;  //!< underlying OSI object
  units::length::meter_t sStart;                      //!< s coordinate where this boundary starts
  units::length::meter_t sEnd;                        //!< s coordinate where this boundary ends
};

/// @brief Class representing a reference line type
class ReferenceLine : public Interfaces::ReferenceLine
{
public:
  /// @brief Create a reference line object
  /// @param osiReferenceLine Pointer to the OSI reference line
  ReferenceLine(osi3::ReferenceLine *osiReferenceLine);

  Id GetId() const override;

  void AddPoint(const Common::Vector2d<units::length::meter_t> &point, units::length::meter_t s) override;

  void CopyToGroundTruth(osi3::GroundTruth &target) const override;

private:
  osi3::ReferenceLine *osiReferenceLine;  //! underlying OSI object
};

/// @brief Class representing a road section
class Section : public Interfaces::Section
{
public:
  /**
   * @brief Section constructor
   *
   * @param[in] sOffset   s coordinate of the section start
   */
  Section(units::length::meter_t sOffset);

  void AddNext(const Interfaces::Section &section) override;

  void AddPrevious(const Interfaces::Section &section) override;

  void AddLane(const Interfaces::Lane &lane) override;

  const Interfaces::Lanes &GetLanes() const override;

  void SetRoad(Interfaces::Road *road) override;

  const Interfaces::Road &GetRoad() const override;

  units::length::meter_t GetDistance(MeasurementPoint measurementPoint) const override;
  bool Covers(units::length::meter_t distance) const override;
  bool CoversInterval(units::length::meter_t startDistance, units::length::meter_t endDistance) const override;

  units::length::meter_t GetSOffset() const override;
  units::length::meter_t GetLength() const override;

  void SetCenterLaneBoundary(std::vector<Id> laneBoundary) override;

  std::vector<Id> GetCenterLaneBoundary() const override;

  void SetCenterLogicalLaneBoundary(std::vector<Id> logicalLaneBoundary) override;

  std::vector<Id> GetCenterLogicalLaneBoundary() const override;

  void SetReferenceLine(Id referenceLineId) override;

  Id GetReferenceLine() const override;

private:
  Interfaces::Lanes lanes;
  const Interfaces::Road *road;
  Interfaces::Sections nextSections;
  Interfaces::Sections previousSections;
  std::vector<Id> centerLaneBoundary;
  std::vector<Id> centerLogicalLaneBoundary;
  units::length::meter_t sOffset;
  Id referenceLine;

public:
  Section(const Section &) = delete;

  Section &operator=(const Section &) = delete;

  Section(Section &&) = delete;

  Section &operator=(Section &&) = delete;

  ~Section() override = default;
};

//! This class represents invalid section of a road
class InvalidSection : public Section
{
public:
  InvalidSection() : Section(0_m) {}

  InvalidSection(const InvalidSection &) = delete;

  InvalidSection &operator=(const InvalidSection &) = delete;

  InvalidSection(InvalidSection &&) = delete;

  InvalidSection &operator=(InvalidSection &&) = delete;
};

//! This class represents a road in the world
class Road : public Interfaces::Road
{
public:
  /**
   * @brief Road constructor
   *
   * @param[in] isInStreamDirection   Is the direction of the road the same as the direction of the road stream
   * @param[in] id                    Id of the road
   */
  Road(bool isInStreamDirection, const std::string &id);

  Road(const Road &) = delete;

  Road &operator=(const Road &) = delete;

  Road(Road &&) = delete;

  Road &operator=(Road &&) = delete;

  ~Road() override = default;

  const std::string &GetId() const override;

  const Interfaces::Sections &GetSections() const override;

  void AddSection(Interfaces::Section &section) override;

  units::length::meter_t GetLength() const override;

  const std::string &GetSuccessor() const override;

  const std::string &GetPredecessor() const override;

  void SetSuccessor(const std::string &successor) override;

  void SetPredecessor(const std::string &predecessor) override;

  bool IsInStreamDirection() const override;

  units::length::meter_t GetDistance(MeasurementPoint mp) const override;

private:
  Interfaces::Sections sections;
  std::string successor{};
  std::string predecessor{};
  bool isInStreamDirection;
  std::string id;
};

//! This class represents a invalid road in the world
class InvalidRoad : public Road
{
public:
  InvalidRoad() : Road(true, "") {}

  InvalidRoad(const InvalidRoad &) = delete;

  InvalidRoad &operator=(const InvalidRoad &) = delete;

  InvalidRoad(InvalidRoad &&) = delete;

  InvalidRoad &operator=(InvalidRoad &&) = delete;
};

//! This class represents a junction in the world
class Junction : public Interfaces::Junction
{
public:
  /**
   * @brief Junction constructor
   *
   * @param[in] id    Id of the Junction
   */
  Junction(const std::string &id) : id(id) {}

  ~Junction() override = default;

  const std::string &GetId() const override { return id; }

  void AddConnectingRoad(const Interfaces::Road *connectingRoad) override { connectingRoads.push_back(connectingRoad); }

  const std::vector<const Interfaces::Road *> &GetConnectingRoads() const override { return connectingRoads; }

  void AddPriority(const std::string &high, const std::string &low) override
  {
    priorities.emplace_back(std::pair{high, low});
  }

  const Priorities &GetPriorities() const override { return priorities; }

  void AddIntersectionInfo(const std::string &roadId, const IntersectionInfo &intersectionInfo) override
  {
    auto &intersectionInfos = intersections[roadId];

    // if roadId already has intersection info registered to it, check if any intersection info is registered for the
    // intersectionInfo.intersectingRoad
    auto existingIntersectionInfoForRoadPairIter
        = std::find_if(intersectionInfos.begin(),
                       intersectionInfos.end(),
                       [&intersectionInfo](const auto &existingIntersectionInfo) -> bool
                       { return intersectionInfo.intersectingRoad == existingIntersectionInfo.intersectingRoad; });

    // if it does, add the new sOffsets to the already existing intersectionInfo for the pair of roads
    if (existingIntersectionInfoForRoadPairIter != intersectionInfos.end())
    {
      existingIntersectionInfoForRoadPairIter->sOffsets.insert(intersectionInfo.sOffsets.begin(),
                                                               intersectionInfo.sOffsets.end());
    }
    else
    {
      intersectionInfos.emplace_back(intersectionInfo);
    }
  }

  const std::map<std::string, std::vector<IntersectionInfo>> &GetIntersections() const override
  {
    return intersections;
  }

  Junction(const Junction &) = delete;

  Junction &operator=(const Junction &) = delete;

  Junction(Junction &&) = delete;

  Junction &operator=(Junction &&) = delete;

private:
  std::string id;
  std::vector<const Interfaces::Road *> connectingRoads;
  Priorities priorities;
  std::map<std::string, std::vector<IntersectionInfo>> intersections;
};

//! This class represents StationaryObject in the world that can be on the road and have a position and a dimension
class StationaryObject : public Interfaces::StationaryObject
{
public:
  /// @brief StationaryObject constructor
  /// @param[in] osiStationaryObject OSI stationary object
  /// @param[in] linkedObject        Linked object
  StationaryObject(osi3::StationaryObject *osiStationaryObject, void *linkedObject);

  ~StationaryObject() override = default;

  mantle_api::Dimension3 GetDimension() const override;

  mantle_api::Orientation3<units::angle::radian_t> GetAbsOrientation() const override;

  mantle_api::Vec3<units::length::meter_t> GetReferencePointPosition() const override;

  units::velocity::meters_per_second_t GetAbsVelocityDouble() const override;

  units::acceleration::meters_per_second_squared_t GetAbsAccelerationDouble() const override;

  const RoadIntervals &GetTouchedRoads() const override;

  void SetReferencePointPosition(const mantle_api::Vec3<units::length::meter_t> &newPosition) override;

  void SetDimension(const mantle_api::Dimension3 &newDimension) override;

  void SetAbsOrientation(const mantle_api::Orientation3<units::angle::radian_t> &newOrientation) override;

  Id GetId() const override;

  void AddLaneAssignment(const Interfaces::Lane &lane, const std::optional<RoadPosition> &referencePoint) override;

  const Interfaces::Lanes &GetLaneAssignments() const override;

  void ClearLaneAssignments() override;

  void SetTouchedRoads(const RoadIntervals &touchedRoads) override;

  void SetSourceReference(const std::string &odId) override;

  void CopyToGroundTruth(osi3::GroundTruth &target) const override;

private:
  osi3::StationaryObject *osiObject;
  const RoadIntervals *touchedRoads;
  Interfaces::Lanes assignedLanes;
};

//! This class represents a static traffic sign
class TrafficSign : public Interfaces::TrafficSign
{
public:
  /**
   * @brief TrafficSign constructor
   *
   * @param[in] osiObject OSI Traffic sign
   */
  TrafficSign(osi3::TrafficSign *osiObject);

  ~TrafficSign() override = default;

  std::string GetId() const override { return id; }

  units::length::meter_t GetS() const override { return s; }

  virtual std::pair<double, CommonTrafficSign::Unit> GetValueAndUnitInSI(
      const double osiValue, const osi3::TrafficSignValue_Unit osiUnit) const override;

  CommonTrafficSign::Entity GetSpecification(const units::length::meter_t relativeDistance) const override;

  mantle_api::Vec3<units::length::meter_t> GetReferencePointPosition() const override;

  mantle_api::Dimension3 GetDimension() const override;

  void SetS(units::length::meter_t sPos) override { s = sPos; }

  /// @brief set valid for lane given lane and road signal interface
  /// @param lane             Reference to Lane interface
  /// @param specification    Reference to road signal interface
  void SetValidForLane(const OWL::Interfaces::Lane &lane, const RoadSignalInterface &specification) override;

  bool SetSpecification(RoadSignalInterface *signal, Position position) override;

  bool AddSupplementarySign(RoadSignalInterface *odSignal, Position position) override;

  bool IsValidForLane(OWL::Id laneId) const override;

  void CopyToGroundTruth(osi3::GroundTruth &target) const override;

private:
  std::string id{""};
  units::length::meter_t s{0.0};

  osi3::TrafficSign *osiSign{nullptr};
};

//! This class represents a traffic sign that is painted on the road surface
class RoadMarking : public Interfaces::RoadMarking
{
public:
  /**
   * @brief RoadMarking constructor
   *
   * @param[in] osiObject Road surface marking
   */
  RoadMarking(osi3::RoadMarking *osiObject);

  ~RoadMarking() override = default;

  std::string GetId() const override { return id; }

  units::length::meter_t GetS() const override { return s; }

  CommonTrafficSign::Entity GetSpecification(const units::length::meter_t relativeDistance) const override;

  mantle_api::Vec3<units::length::meter_t> GetReferencePointPosition() const override;

  mantle_api::Dimension3 GetDimension() const override;

  void SetS(units::length::meter_t sPos) override { s = sPos; }

  /// @brief set valid for lane given lane and road signal interface
  /// @param lane             Reference to Lane interface
  /// @param specification    Reference to road signal interface
  void SetValidForLane(const OWL::Interfaces::Lane &lane, const RoadSignalInterface &specification) override;

  /// @brief set valid for lane given lane and road signal interface
  /// @param lane             Reference to Lane interface
  /// @param specification    Reference to road object interface
  void SetValidForLane(const OWL::Interfaces::Lane &lane, const RoadObjectInterface &specification) override;

  bool SetSpecification(RoadSignalInterface *signal, Position position) override;

  bool SetSpecification(RoadObjectInterface *object, Position position) override;

  bool IsValidForLane(OWL::Id laneId) const override;

  void CopyToGroundTruth(osi3::GroundTruth &target) const override;

private:
  std::string id{""};
  units::length::meter_t s{0.0};

  osi3::RoadMarking *osiSign{nullptr};
};

}  // namespace Implementation

/*
using LaneGeometryElement  = Primitive::LaneGeometryElement;
using Lane                 = Implementation::Lane;
using Section              = Implementation::Section;
using Road                 = Implementation::Road;
using StationaryObject     = Implementation::StationaryObject;
using MovingObject         = Implementation::MovingObject;
using Vehicle              = Implementation::Vehicle;

using CLaneGeometryElement  = const Primitive::LaneGeometryElement;
using CLane                 = const Implementation::Lane;
using CSection              = const Implementation::Section;
using CRoad                 = const Implementation::Road;
using CStationaryObject     = const Implementation::StationaryObject;
using CMovingObject         = const Implementation::MovingObject;
using CVehicle              = const Implementation::Vehicle;
*/

//using Sections             = std::vector<Implementation::Section<>>;

//using StationaryObject     = Interfaces::StationaryObject;
//using MovingObject         = Interfaces::MovingObject;
//using Vehicle              = Interfaces::Vehicle;

//using LaneGeometryElements = std::vector<Primitive::LaneGeometryElement>;
//using Lanes                = std::vector<Interfaces::Lane>;
//using Roads                = std::vector<Interfaces::Road>;
//using StationaryObjects    = std::vector<Interfaces::StationaryObject>;
//using MovingObjects        = std::vector<Interfaces::MovingObject>;
//using Vehicles             = std::vector<Interfaces::Vehicle>;

}  // namespace OWL
