/********************************************************************************
 * Copyright (c) 2021-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  RoadStream.h
//! @brief
//-----------------------------------------------------------------------------

#pragma once

#include <memory>
#include <vector>

#include "LaneStream.h"
#include "OWL/DataTypes.h"
#include "include/streamInterface.h"

using namespace units::literals;

//! This class represents one element of a RoadStream.
struct RoadStreamElement
{
  const OWL::Interfaces::Road* road;  //!< road represented by this object
  units::length::meter_t sOffset;     //!< S Offset of the start point of the element from the beginning of the stream
  bool
      inStreamDirection;  //!< Specifies whether the direction of the element is the same as the direction of the stream

  RoadStreamElement() = default;

  //! RoadStreamElement constructor
  //!
  //! @param[in] road                 Road represented by this object
  //! @param[in] sOffset              S Offset of the start point of the element from the beginning of the stream
  //! @param[in] inStreamDirection    Specifies whether the direction of the element is the same as the direction of the
  //! stream
  RoadStreamElement(const OWL::Interfaces::Road* road, units::length::meter_t sOffset, bool inStreamDirection)
      : road(road), sOffset(sOffset), inStreamDirection(inStreamDirection)
  {
  }

  /// @brief Functio call operator
  /// @return Reference to the road
  const OWL::Interfaces::Road& operator()() const { return *road; }

  //! Transform the s coordinate on the element to the s coordinate on the stream
  //!
  //! \param elementPosition position relative to the start of the element
  //! \return position relative to the start of the stream
  units::length::meter_t GetStreamPosition(units::length::meter_t elementPosition) const
  {
    return sOffset + (inStreamDirection ? elementPosition : -elementPosition);
  }

  //! Transform the s coordinate on the stream to the s coordinate on the element
  //!
  //! \param streamPosition position relative to the start of the stream
  //! \return position relative to the start of the element
  units::length::meter_t GetElementPosition(units::length::meter_t streamPosition) const
  {
    return inStreamDirection ? streamPosition - sOffset : sOffset - streamPosition;
  }

  /**
   * @brief Returns the stream position of the start of the road
   *
   * @return stream position of the start of the road
   */
  units::length::meter_t StartS() const { return sOffset - (inStreamDirection ? 0_m : road->GetLength()); }

  /**
   * @brief Returns the stream position of the end of the road
   *
   * @return stream position of the end of the road
   */
  units::length::meter_t EndS() const { return sOffset + (inStreamDirection ? road->GetLength() : 0_m); }
};

//! This class represents a connected sequence of roads in the road network
class RoadStream : public RoadStreamInterface
{
public:
  //! RoadStream constructor
  //!
  //! @param[in] elements List of RoadStreamElement
  RoadStream(const std::vector<RoadStreamElement>&& elements) : elements(elements) {}

  StreamPosition GetStreamPosition(const GlobalRoadPosition& roadPosition) const override;

  GlobalRoadPosition GetRoadPosition(const StreamPosition& streamPosition) const override;

  std::vector<std::unique_ptr<LaneStreamInterface>> GetAllLaneStreams() const override;

  std::unique_ptr<LaneStreamInterface> GetLaneStream(const StreamPosition& startPosition,
                                                     const int laneId) const override;

  std::unique_ptr<LaneStreamInterface> GetLaneStream(const GlobalRoadPosition& startPosition) const override;

  //! Public for testing
  //! Creates the list of the elements of the lane stream that corresponds to the raod stream
  //!
  //! @param[in] startPosition    Start position of the stream
  //! @param[in] laneId           Id of the lane
  //! @return List of the elements of the lane stream
  std::vector<LaneStreamElement> CreateLaneStream(const StreamPosition& startPosition, const int laneId) const;

  //! Creates the list of the elements of the lane stream that corresponds to the raod stream
  //!
  //! @param[in] startPosition    Start position of the stream in global road coordiantes
  //! @return List of the elements of the lane stream
  std::vector<LaneStreamElement> CreateLaneStream(const GlobalRoadPosition& startPosition) const;

  units::length::meter_t GetLength() const override;

private:
  const std::vector<RoadStreamElement> elements;
};
