/********************************************************************************
 * Copyright (c) 2020-2021 in-tech GmbH
 *               2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include "include/agentInterface.h"
#include "include/egoAgentInterface.h"
#include "include/worldInterface.h"

using namespace units::literals;

using namespace units::literals;

using namespace units::literals;

/// This class represent an agent's (and especially driver's) view of his surroundings.
class EgoAgent : public EgoAgentInterface
{
public:
  /**
   * @brief EgoAgent constructor
   *
   * @param[in]   agent   Pointer to the agent
   * @param[in]   world   Pointer to the world
   */
  EgoAgent(const AgentInterface *agent, const WorldInterface *world) : agent(agent), world(world) {}

  const AgentInterface *GetAgent() const override;

  void SetRoadGraph(const RoadGraph &&roadGraph, RoadGraphVertex current, RoadGraphVertex target) override;

  void Update() override;

  /// @brief Update position of the ego agent in the road graph
  void UpdatePositionInGraph();

  bool HasValidRoute() const override;

  void SetNewTarget(size_t alternativeIndex) override;

  const std::string &GetRoadId() const override;

  units::velocity::meters_per_second_t GetVelocity(VelocityScope velocityScope) const override;

  units::velocity::meters_per_second_t GetVelocity(VelocityScope velocityScope,
                                                   const WorldObjectInterface *object) const override;

  units::length::meter_t GetDistanceToEndOfLane(units::length::meter_t range, int relativeLane = 0) const override;

  units::length::meter_t GetDistanceToEndOfLane(units::length::meter_t range,
                                                int relativeLane,
                                                const LaneTypes &acceptableLaneTypes) const override;

  RelativeWorldView::Lanes GetRelativeLanes(units::length::meter_t range,
                                            int relativeLane = 0,
                                            bool includeOncoming = true) const override;

  std::optional<int> GetRelativeLaneId(const WorldObjectInterface *object, ObjectPoint point) const override;

  [[deprecated]] RelativeWorldView::Roads GetRelativeJunctions(units::length::meter_t range) const override;

  RelativeWorldView::Roads GetRelativeRoads(units::length::meter_t range) const override;

  std::vector<const WorldObjectInterface *> GetObjectsInRange(units::length::meter_t backwardRange,
                                                              units::length::meter_t forwardRange,
                                                              int relativeLane = 0) const override;

  AgentInterfaces GetAgentsInRange(units::length::meter_t backwardRange,
                                   units::length::meter_t forwardRange,
                                   int relativeLane = 0) const override;

  std::vector<CommonTrafficSign::Entity> GetTrafficSignsInRange(units::length::meter_t range,
                                                                int relativeLane = 0) const override;

  std::vector<CommonTrafficSign::Entity> GetRoadMarkingsInRange(units::length::meter_t range,
                                                                int relativeLane = 0) const override;

  std::vector<CommonTrafficLight::Entity> GetTrafficLightsInRange(units::length::meter_t range,
                                                                  int relativeLane = 0) const override;

  std::vector<LaneMarking::Entity> GetLaneMarkingsInRange(units::length::meter_t range,
                                                          Side side,
                                                          int relativeLane = 0) const override;

  std::optional<units::length::meter_t> GetDistanceToObject(const WorldObjectInterface *otherObject,
                                                            const ObjectPoint &ownPoint,
                                                            const ObjectPoint &otherPoint) const override;

  std::optional<units::length::meter_t> GetNetDistance(const WorldObjectInterface *otherObject) const override;

  Obstruction GetObstruction(const WorldObjectInterface *otherObject,
                             const std::vector<ObjectPoint> points) const override;

  units::angle::radian_t GetRelativeYaw() const override;

  units::length::meter_t GetPositionLateral() const override;

  units::length::meter_t GetLaneRemainder(Side side) const override;

  units::length::meter_t GetLaneWidth(int relativeLane = 0) const override;

  std::optional<units::length::meter_t> GetLaneWidth(units::length::meter_t distance,
                                                     int relativeLane = 0) const override;

  units::curvature::inverse_meter_t GetLaneCurvature(int relativeLane = 0) const override;

  std::optional<units::curvature::inverse_meter_t> GetLaneCurvature(units::length::meter_t distance,
                                                                    int relativeLane = 0) const override;

  units::angle::radian_t GetLaneDirection(int relativeLane = 0) const override;

  std::optional<units::angle::radian_t> GetLaneDirection(units::length::meter_t distance,
                                                         int relativeLane = 0) const override;

  const std::optional<GlobalRoadPosition> &GetMainLocatePosition() const override;

  std::optional<GlobalRoadPosition> GetReferencePointPosition() const override;

  int GetLaneIdFromRelative(int relativeLaneId) const override;

  std::optional<Position> GetWorldPosition(units::length::meter_t sDistance,
                                           units::length::meter_t tDistance,
                                           units::angle::radian_t yaw = 0_rad) const override;

  GlobalRoadPositions GetRoadPositions(const ObjectPoint &point, const WorldObjectInterface *object) const override;

private:
  std::optional<RouteElement> GetPreviousRoad(size_t steps = 1) const;

  std::optional<GlobalRoadPosition> GetReferencePointPosition(const WorldObjectInterface *object) const;

  std::optional<RoadGraphVertex> GetReferencePointVertex() const;

  std::optional<GlobalRoadPosition> GetPositionOnRoute(GlobalRoadPositions roadPositions) const;

  ExecuteReturn<DistanceToEndOfLane> executeQueryDistanceToEndOfLane(
      DistanceToEndOfLaneParameter parameter) const override;

  ExecuteReturn<ObjectsInRange> executeQueryObjectsInRange(ObjectsInRangeParameter parameter) const override;

  void SetWayToTarget(RoadGraphVertex targetVertex);

  const AgentInterface *agent;
  const WorldInterface *world;
  bool graphValid{false};
  RoadGraph roadGraph{};
  RoadGraphVertex current{0};
  RoadGraph wayToTarget{};
  RoadGraphVertex rootOfWayToTargetGraph{0};
  std::vector<RouteElement> wayToTargetRoute{};  //! same information as wayToTarget but as vector
  std::vector<RoadGraphVertex> alternatives{};
  std::optional<GlobalRoadPosition> mainLocatePosition;
};
