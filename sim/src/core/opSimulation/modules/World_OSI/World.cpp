/********************************************************************************
 * Copyright (c) 2016-2018 ITK Engineering GmbH
 *               2017-2021 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  World.cpp
//! @brief This file contains the DLL wrapper.
//-----------------------------------------------------------------------------

#include "World.h"

#include "core/opSimulation/modules/World_OSI/WorldImplementation.h"
#include "include/worldInterface.h"

/// The version of the current module - has to be incremented manually
const std::string Version = "0.0.1";
static const CallbackInterface *Callbacks = nullptr;

/**
 * @brief   dll-function to obtain the version of the current module
 *
 * @return  Version of the current module
 */
extern "C" WORLD_SHARED_EXPORT const std::string &OpenPASS_GetVersion()
{
  return Version;
}

/**
 * @brief   dll-function to create an instance of the module
 *
 * @param[in]   callbacks       Pointer to the callbacks
 * @param[in]   stochastics     Pointer to the stochastics class loaded by the framework
 * @param[in]   dataBuffer      Pointer to the data buffer that provides write-only access to the data
 * @return  A pointer to the created module instance
 */
extern "C" WORLD_SHARED_EXPORT WorldInterface *OpenPASS_CreateInstance(CallbackInterface *callbacks,
                                                                       StochasticsInterface *stochastics,
                                                                       DataBufferWriteInterface *dataBuffer)
{
  Callbacks = callbacks;

  try
  {
    static std::unique_ptr<OWL::WorldData> worldData_;
    worldData_ = std::make_unique<OWL::WorldData>(callbacks);
    return (WorldInterface *)(new (std::nothrow)
                                  WorldImplementation(callbacks, stochastics, dataBuffer, *worldData_.get()));
  }
  catch (const std::runtime_error &ex)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, ex.what());
    }
    return nullptr;
  }
  catch (...)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, "unexpected exception");
    }

    return nullptr;
  }
}

/**
 * @brief   dll-function to destroy/delete an instance of the module
 *
 * @param[in]   implementation  The instance that should be freed
 */
extern "C" WORLD_SHARED_EXPORT void OpenPASS_DestroyInstance(WorldInterface *implementation)
{
  delete implementation;
}
