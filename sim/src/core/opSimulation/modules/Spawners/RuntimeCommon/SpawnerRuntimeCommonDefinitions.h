/********************************************************************************
 * Copyright (c) 2019-2021 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <string>
#include <unordered_map>
#include <vector>

#include "common/SpawnerDefinitions.h"
#include "include/parameterInterface.h"

namespace SpawnPointRuntimeCommonDefinitions
{
using RoadId = std::string;
using LaneId = int;
using LaneIds = std::vector<LaneId>;
using SPosition = units::length::meter_t;

//! Details of spawn
struct SpawnDetails
{
  int spawnTime;                                               //!< Time of spawn
  SpawnPointDefinitions::EntityInformation entityInformation;  //!< Information of the entity
};

//! Struct representing the spawn position
struct SpawnPosition
{
  const RoadId roadId;        //!< Id of the road on which the agent is spawned
  const LaneId laneId;        //!< Id of the lane on which the agent is spawned
  const SPosition sPosition;  //!< S coordinate at which the agent is spawned

  //! Comparison operator
  //!
  //! @param[in] other SpawnPosition to compare to
  //! @return true if SpawnPositions are considered equal, false otherwise
  bool operator==(const SpawnPosition& other) const
  {
    return this->roadId == other.roadId && this->laneId == other.laneId && this->sPosition == other.sPosition;
  }
};

//! Parameters of the RuntimeCommonSpawner
struct SpawnPointRuntimeCommonParameters
{
  const std::variant<double, openpass::parameter::StochasticDistribution>
      minimumSeparationBuffer;                      //!< Minimum distance between two agents
  const std::vector<SpawnPosition> spawnPositions;  //!< Areas to spawn in
  const SpawnPointDefinitions::AgentProfileLaneMaps
      agentProfileLaneMaps;  //!< AgentProfiles to spawn, separated for left lane and other lanes
};
}  //namespace SpawnPointRuntimeCommonDefinitions
