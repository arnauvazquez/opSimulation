/********************************************************************************
 * Copyright (c) 2020 ITK Engineering GmbH
 *               2017-2021 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/*!
 * \file  observation_logImplementation.h
 * \brief Adds the RunStatistic information to the simulation output.
 * \details  Writes the RunStatistic information into the simulation output.
 *           Also manages the stop reasons of the simulation.
 */
//-----------------------------------------------------------------------------

#pragma once

#include <set>
#include <string>
#include <tuple>

#include <QFile>
#include <QTextStream>

#include "common/runtimeInformation.h"
#include "include/observationInterface.h"
#include "observationCyclics.h"
#include "observationFileHandler.h"
#include "runStatistic.h"

//-----------------------------------------------------------------------------
/** \brief This class adds the RunStatistic information to the simulation output.
 *   \details This class inherits the ObservationLogGeneric which creates the basic simulation output
 *            and adds the RunStatistic information to the output.
 *            This class also manages the stop reasons of the simulation.
 *
 *   \ingroup ObservationLog
 */
//-----------------------------------------------------------------------------
class ObservationLogImplementation : ObservationInterface
{
public:
  const std::string COMPONENTNAME = "ObservationLog";

  /**
   * @brief ObservationLogImplementation constructor
   *
   * @param[in]   stochastics     Pointer to the stochastics class loaded by the framework
   * @param[in]   world           Pointer to the world
   * @param[in]   parameters      Pointer to the parameters of the module
   * @param[in]   callbacks       Pointer to the callbacks
   * @param[in]   dataBuffer      Pointer to the data buffer that provides read-only access to the data
   */
  ObservationLogImplementation(StochasticsInterface* stochastics,
                               WorldInterface* world,
                               const ParameterInterface* parameters,
                               const CallbackInterface* callbacks,
                               DataBufferReadInterface* dataBuffer);
  ObservationLogImplementation(const ObservationLogImplementation&) = delete;
  ObservationLogImplementation(ObservationLogImplementation&&) = delete;
  ObservationLogImplementation& operator=(const ObservationLogImplementation&) = delete;
  ObservationLogImplementation& operator=(ObservationLogImplementation&&) = delete;
  ~ObservationLogImplementation() override = default;

  void OpSimulationPreHook() override;
  void OpSimulationPreRunHook() override;
  void OpSimulationPostRunHook(const RunResultInterface& runResult) override;
  void OpSimulationUpdateHook(int, RunResultInterface&) override;
  void OpSimulationManagerPreHook() override {}
  void OpSimulationManagerPostHook(const std::string&) override {}
  void OpSimulationPostHook() override;

  const std::string OpSimulationResultFile() override { return ""; }

private:
  const openpass::common::RuntimeInformation& runtimeInformation;
  DataBufferReadInterface* dataBuffer;
  ObservationFileHandler fileHandler;
  ObservationCyclics cyclics;
  Events events;
  RunStatistic runStatistic = RunStatistic(-1);
  std::vector<std::string> selectedColumns;
  std::vector<std::pair<std::string, std::string>> selectedRegexColumns;
};
