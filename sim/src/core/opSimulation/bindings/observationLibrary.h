/********************************************************************************
 * Copyright (c) 2016-2018 ITK Engineering GmbH
 *               2017-2021 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  observationLibrary.h
//! @brief This file contains the internal representation of the library of an
//!        observation module.
//-----------------------------------------------------------------------------

#pragma once

#include <list>
#include <string>

#include <QLibrary>

#include "common/runtimeInformation.h"
#include "include/callbackInterface.h"
#include "include/observationInterface.h"

namespace core
{

class ObservationModule;

//! This class represents the library of an observation module
class ObservationLibrary
{
public:
  //! Type of function pointer that points to dll-function that obtain the version information of the module
  typedef const std::string& (*ObservationInterface_GetVersion)();
  //! Type of function pointer that points to dll-function that creates an instance of the module
  typedef ObservationInterface* (*ObservationInterface_CreateInstanceType)(StochasticsInterface* stochastics,
                                                                           WorldInterface* world,
                                                                           const ParameterInterface* parameters,
                                                                           const CallbackInterface* callbacks,
                                                                           DataBufferReadInterface* const dataBuffer);
  //! Type of function pointer that points to dll-function that destroys/deletes an instance of the module
  typedef void (*ObservationInterface_DestroyInstanceType)(ObservationInterface* implementation);

  //! Type of function pointer that points to dll-function that manages prehook of the simulation
  typedef bool (*ObservationInterface_OpSimulationManagerPreHook)(ObservationInterface* implementation);
  //! Type of function pointer that points to dll-function that manages posthook of the simulation
  typedef bool (*ObservationInterface_OpSimulationManagerPostHook)(ObservationInterface* implementation,
                                                                   const std::string& filename);

  //! Type of function pointer that points to dll-function that manages prehook of the simulation
  typedef bool (*ObservationInterface_OpSimulationPreHook)(ObservationInterface* implementation);

  //! Type of function pointer that points to dll-function that manages pre run hook of the simulation
  typedef bool (*ObservationInterface_OpSimulationPreRunHook)(ObservationInterface* implementation);

  //! Type of function pointer that points to dll-function that update hook of the simulation
  typedef bool (*ObservationInterface_OpSimulationUpdateHook)(ObservationInterface* implementation,
                                                              int time,
                                                              RunResultInterface& runResult);

  //! Type of function pointer that points to dll-function that manages post run hook of the simulation
  typedef bool (*ObservationInterface_OpSimulationPostRunHook)(ObservationInterface* implementation,
                                                               const RunResultInterface& runResult);

  //! Type of function pointer that points to dll-function that manages post hook of the simulation
  typedef bool (*ObservationInterface_OpSimulationPostHook)(ObservationInterface* implementation);

  //! Type of function pointer that points to dll-function that manages results file of the simulation
  typedef const std::string (*ObservationInterface_OpSimulationResultFile)(ObservationInterface* implementation);

  /**
   * @brief ObservationLibrary constructor
   *
   * @param[in] libraryPath   Path to the library
   * @param[in] callbacks     Pointer to the callbacks
   */
  ObservationLibrary(const std::string libraryPath, CallbackInterface* callbacks)
      : libraryPath(libraryPath), callbacks(callbacks)
  {
  }
  ObservationLibrary(const ObservationLibrary&) = delete;
  ObservationLibrary(ObservationLibrary&&) = delete;
  ObservationLibrary& operator=(const ObservationLibrary&) = delete;
  ObservationLibrary& operator=(ObservationLibrary&&) = delete;
  virtual ~ObservationLibrary();

  /**
   * Inits the observation library by obtaining the function pointers to all
   * "Interface.h" functions defined via typedef.
   *
   * @return     Flag if init was successful
   */
  bool Init();

  /**
   * Releases an observation module instance by calling the destroyInstance method
   * on the instance and removing the instance from the list of observation modules.
   *
   * @param[in]   observationModule   Observation module to release
   * @return     Flag if release was successful
   */
  bool ReleaseObservationModule(ObservationModule* observationModule);

  /**
   * Creates an observation module based on the observation instance in the run
   * config and the observation library stored in the mapping for the library
   * name of the observation instance.
   *
   * @param[in]     runtimeInformation   Common runtimeInformation
   * @param[in]     parameters           Observation parameters
   * @param[in]     stochastics          The stochastics interface
   * @param[in]     world                The world interface
   * @param[in]     dataBuffer           Pointer to the data buffer that provides read-only access to the data
   * @return                             Observation module created from the
   *                                     observation instance
   */
  ObservationModule* CreateObservationModule(const openpass::common::RuntimeInformation& runtimeInformation,
                                             const openpass::parameter::ParameterSetLevel1& parameters,
                                             StochasticsInterface* stochastics,
                                             WorldInterface* world,
                                             DataBufferReadInterface* const dataBuffer);

  /// @brief Simulation prehook (once at simulation start) for the observation interface
  /// @param implementation Pointer to the observation interface
  /// @return True, when prehook is successfull
  bool SimulationPreHook(ObservationInterface* implementation) { return simulationPreHookFunc(implementation); }

  /// @brief Simulation prerunhook (once before each run) for the observation interface
  /// @param implementation Pointer to the observation interface
  /// @return True, when prehook is successfull
  bool SimulationPreRunHook(ObservationInterface* implementation) { return simulationPreRunHookFunc(implementation); }

  /// @brief Simulation updatehook (after each timestep) for the observation interface
  /// @param implementation Pointer to the observation interface
  /// @param time           Current time
  /// @param runResult      RunResult to write
  /// @return True, when updatehook is successfull
  bool SimulationUpdateHook(ObservationInterface* implementation, int time, RunResultInterface& runResult)
  {
    return simulationUpdateHookFunc(implementation, time, runResult);
  }

  /// @brief Simulation postrunhook (once after each run) for the observation interface
  /// @param implementation Pointer to the observation interface
  /// @param runResult      Result of the run
  /// @return True, when postrunhook is successfull
  bool SimulationPostRunHook(ObservationInterface* implementation, const RunResultInterface& runResult)
  {
    return simulationPostRunHookFunc(implementation, runResult);
  }

  /// @brief Simulation posthook (once at simulation end) for the observation interface
  /// @param implementation Pointer to the observation interface
  /// @return True, when posthook is successfull
  bool SimulationPostHook(ObservationInterface* implementation) { return simulationPostHookFunc(implementation); }

  /// @brief Returns the name of the simulation result file
  /// @param implementation Pointer to the observation interface
  /// @return name of the simulation result file
  const std::string SimulationResultFile(ObservationInterface* implementation)
  {
    return simulationResultFileFunc(implementation);
  }

private:
  const std::string DllGetVersionId = "OpenPASS_GetVersion";
  const std::string DllCreateInstanceId = "OpenPASS_CreateInstance";
  const std::string DllDestroyInstanceId = "OpenPASS_DestroyInstance";
  const std::string DllOpSimulationManagerPreHookId = "OpenPASS_OpSimulationPreHook";
  const std::string DllOpSimulationManagerPostHookId = "OpenPASS_OpSimulationPostHook";
  const std::string DllOpSimulationPreHookId = "OpenPASS_OpSimulationPreHook";
  const std::string DllOpSimulationPreRunHookId = "OpenPASS_OpSimulationPreRunHook";
  const std::string DllOpSimulationUpdateHookId = "OpenPASS_OpSimulationUpdateHook";
  const std::string DllOpSimulationPostRunHookId = "OpenPASS_OpSimulationPostRunHook";
  const std::string DllOpSimulationPostHookId = "OpenPASS_OpSimulationPostHook";
  const std::string DllOpSimulationResultFileId = "OpenPASS_OpSimulationResultFile";

  const std::string libraryPath;
  std::vector<ObservationModule*> observationModules;
  QLibrary* library = nullptr;
  CallbackInterface* callbacks;

  ObservationInterface_GetVersion getVersionFunc{nullptr};
  ObservationInterface_CreateInstanceType createInstanceFunc{nullptr};
  ObservationInterface_DestroyInstanceType destroyInstanceFunc{nullptr};
  ObservationInterface_OpSimulationManagerPreHook opSimulationManagerPreHookFunc{nullptr};
  ObservationInterface_OpSimulationManagerPostHook opSimulationManagerPostHookFunc{nullptr};
  ObservationInterface_OpSimulationPreHook simulationPreHookFunc{nullptr};
  ObservationInterface_OpSimulationPreRunHook simulationPreRunHookFunc{nullptr};
  ObservationInterface_OpSimulationUpdateHook simulationUpdateHookFunc{nullptr};
  ObservationInterface_OpSimulationPostRunHook simulationPostRunHookFunc{nullptr};
  ObservationInterface_OpSimulationPostHook simulationPostHookFunc{nullptr};
  ObservationInterface_OpSimulationResultFile simulationResultFileFunc{nullptr};
};

}  // namespace core
