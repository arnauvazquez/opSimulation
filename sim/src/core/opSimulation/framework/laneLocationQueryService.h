/*******************************************************************************
 * Copyright (c) 2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <limits>  // workaround for missing includes in MantleAPI headers
#include <optional>

#include "MantleAPI/Map/i_lane_location_query_service.h"
#include "common/opExport.h"
#include "include/worldInterface.h"

namespace core
{

//! This class represents a lane location query service
class SIMULATIONCOREEXPORT LaneLocationQueryService : public mantle_api::ILaneLocationQueryService
{
public:
  LaneLocationQueryService() = default;
  ~LaneLocationQueryService() = default;

  //! @brief A method that initializes the world
  //!
  //! @param[in] world    Pointer to the world
  void Init(WorldInterface* world) { this->world = world; }

  mantle_api::Orientation3<units::angle::radian_t> GetLaneOrientation(
      const mantle_api::Vec3<units::length::meter_t>& position) const override;
  mantle_api::Vec3<units::length::meter_t> GetUpwardsShiftedLanePosition(
      const mantle_api::Vec3<units::length::meter_t>& position,
      double upwards_shift,
      bool allow_invalid_positions = false) const override;
  bool IsPositionOnLane(const mantle_api::Vec3<units::length::meter_t>& position) const override;
  std::vector<mantle_api::UniqueId> GetLaneIdsAtPosition(
      const mantle_api::Vec3<units::length::meter_t>& position) const override;
  std::optional<mantle_api::Pose> FindLanePoseAtDistanceFrom(const mantle_api::Pose& reference_pose_on_lane,
                                                             units::length::meter_t distance,
                                                             mantle_api::Direction direction) const override;
  std::optional<units::length::meter_t> GetLongitudinalLaneDistanceBetweenPositions(
      const mantle_api::Vec3<units::length::meter_t>& start_position,
      const mantle_api::Vec3<units::length::meter_t>& target_position) const override;
  std::optional<mantle_api::Pose> FindRelativeLanePoseAtDistanceFrom(
      const mantle_api::Pose& reference_pose_on_lane,
      int relative_target_lane,
      units::length::meter_t distance,
      units::length::meter_t lateral_offset) const override;
  std::optional<mantle_api::LaneId> GetRelativeLaneId(const mantle_api::Pose& reference_pose_on_lane,
                                                      int relative_lane_target) const override;

  std::optional<mantle_api::Vec3<units::length::meter_t>> GetPosition(
      const mantle_api::Pose& reference_pose,
      mantle_api::LateralDisplacementDirection direction,
      units::length::meter_t distance) const override;

  std::optional<mantle_api::Pose> GetProjectedPoseAtLane(
      const mantle_api::Vec3<units::length::meter_t>& reference_position_on_lane,
      mantle_api::LaneId target_lane_id) const override;

  std::optional<mantle_api::Vec3<units::length::meter_t>> GetProjectedCenterLinePoint(
      const mantle_api::Vec3<units::length::meter_t>& position) const override;

private:
  WorldInterface* world;
};
}  // namespace core
