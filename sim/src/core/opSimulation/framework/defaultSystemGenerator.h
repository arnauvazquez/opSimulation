/********************************************************************************
 * Copyright (c) 2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "include/agentBlueprintProviderInterface.h"

//! This class represents a default system generator
class DefaultSystemGenerator
{
public:
  //! @brief Returns a default system with only the basic components
  //!
  //! @return Default system
  static System make();
};