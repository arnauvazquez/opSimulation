/********************************************************************************
 * Copyright (c) 2016-2018 ITK Engineering GmbH
 *               2017-2021 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "scheduler.h"

#include "agent.h"
#include "agentParser.h"
#include "common/log.h"
#include "runResult.h"
#include "taskBuilder.h"
#include "units.h"

namespace core::scheduling
{

using namespace core;

Scheduler::Scheduler(WorldInterface &world,
                     SpawnPointNetworkInterface &spawnPointNetwork,
                     CollisionDetectionInterface &collisionDetection,
                     ObservationNetworkInterface &observationNetwork,
                     DataBufferInterface &dataInterface,
                     EnvironmentInterface &environment)
    : world(world),
      spawnPointNetwork(spawnPointNetwork),
      collisionDetection(collisionDetection),
      observationNetwork(observationNetwork),
      dataInterface(dataInterface),
      environment(environment)
{
}

bool Scheduler::Run(int startTime, RunResult &runResult, std::unique_ptr<mantle_api::IScenarioEngine> scenarioEngine)
{
  environment.SetSimulationTime(units::make_unit<mantle_api::Time>(startTime));
  scenarioEngine->Init();

  currentTime = startTime;
  TaskBuilder taskBuilder(currentTime,
                          runResult,
                          FRAMEWORK_UPDATE_RATE,
                          &world,
                          &spawnPointNetwork,
                          &observationNetwork,
                          &collisionDetection,
                          &dataInterface,
                          &environment);

  auto bootstrapTasks = taskBuilder.CreateBootstrapTasks();
  auto postScenarioTasks = taskBuilder.CreatePostScenarioTasks();
  auto spawningTasks = taskBuilder.CreateSpawningTasks();
  auto preAgentTasks = taskBuilder.CreatePreAgentTasks();
  auto synchronizeTasks = taskBuilder.CreateSynchronizeTasks();
  auto finalizeTasks = taskBuilder.CreateFinalizeTasks();

  auto taskList = SchedulerTasks(bootstrapTasks,
                                 postScenarioTasks,
                                 spawningTasks,
                                 preAgentTasks,
                                 synchronizeTasks,
                                 finalizeTasks,
                                 FRAMEWORK_UPDATE_RATE);

  if (ExecuteTasks(taskList.GetBootstrapTasks()) == false)
  {
    return Scheduler::FAILURE;
  }

  do
  {
    environment.SetSimulationTime(units::make_unit<mantle_api::Time>(currentTime));
    scenarioEngine->Step();
    if (scenarioEngine->IsFinished()) break;

    if (!ExecuteTasks(taskList.GetPostScenarioTasks(currentTime))
        || !ExecuteTasks(taskList.GetSpawningTasks(currentTime)))
    {
      return Scheduler::FAILURE;
    }

    UpdateAgents(taskList, world);

    if (!ExecuteTasks(taskList.GetPreAgentTasks(currentTime))
        || !ExecuteTasks(taskList.ConsumeNonRecurringAgentTasks(currentTime))
        || !ExecuteTasks(taskList.GetRecurringAgentTasks(currentTime))
        || !ExecuteTasks(taskList.GetSynchronizeTasks(currentTime)))
    {
      return Scheduler::FAILURE;
    }
    currentTime = taskList.GetNextTimestamp(currentTime);
  } while (!scenarioEngine->IsFinished());

  if (!ExecuteTasks(taskList.GetFinalizeTasks()))
  {
    return Scheduler::FAILURE;
  }

  LOG_INTERN(LogLevel::DebugCore) << "Scheduler: End of operation (end time reached)";
  return Scheduler::SUCCESS;
}

template <typename T>
bool Scheduler::ExecuteTasks(T tasks)
{
  for (const auto &task : tasks)
  {
    if (task.func() == false)
    {
      return false;
    }
  }
  return true;
}

void Scheduler::UpdateAgents(SchedulerTasks &taskList, WorldInterface &world)
{
  for (const auto &agent : environment.GetEntityRepository().ConsumeNewAgents())
  {
    agent->LinkSchedulerTime(&currentTime);
    ScheduleAgentTasks(taskList, *agent);
  }

  std::vector<int> removedAgents;
  for (const auto &agentId : world.GetRemovedAgentsInPreviousTimestep())
  {
    removedAgents.push_back(agentId);
  }
  taskList.DeleteAgentTasks(removedAgents);
}

void Scheduler::ScheduleAgentTasks(SchedulerTasks &taskList, const Agent &agent)
{
  AgentParser agentParser(currentTime);
  agentParser.Parse(agent);

  taskList.ScheduleNewRecurringTasks(agentParser.GetRecurringTasks());
  taskList.ScheduleNewNonRecurringTasks(agentParser.GetNonRecurringTasks());
}

}  // namespace core::scheduling
