/*******************************************************************************
 * Copyright (c) 2021 in-tech GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
//! @file   routeSampler.h
//-----------------------------------------------------------------------------

#pragma once

#include "MantleAPI/Common/vector.h"
#include "MantleAPI/Traffic/control_strategy.h"
#include "common/RoutePlanning/RouteCalculation.h"
#include "common/commonTools.h"
#include "include/agentBlueprintInterface.h"
#include "include/stochasticsInterface.h"
#include "include/worldInterface.h"

constexpr size_t MAX_DEPTH = 10;  //!< Limits search depths in case of cyclic network

//! This class provides the interface for the RouteSampler
class RouteSamplerInterface
{
public:
  //! @brief Method which samples route
  //!
  //! @param[in] inert_pos    World position
  //! @param[in] yaw          Heading angle
  //! @return Sampled route if successful
  virtual Route Sample(const mantle_api::Vec3<units::length::meter_t>& inert_pos, units::angle::radian_t yaw) const = 0;

  //! @brief Method which calculates a route from waypoints
  //!
  //! @param[in] route    The list of waypoints with a RouteStrategies
  //! @return Route in Scenario if waypoints are reachable
  virtual Route CalculateRouteFromWaypoints(const std::vector<mantle_api::RouteWaypoint>& route) const = 0;
};

//! This class represents a route sampler
class RouteSampler : public RouteSamplerInterface
{
public:
  //! RouteSampler constructor
  //!
  //! @param[in] stochastics  Pointer to the stochastics
  RouteSampler(StochasticsInterface* stochastics) : stochastics(stochastics) {}

  //! @brief Method which sets the world
  //!
  //! @param[in] world    The world to be used
  void SetWorld(const WorldInterface* world) { this->world = world; }

  Route Sample(const mantle_api::Vec3<units::length::meter_t>& inert_pos, units::angle::radian_t yaw) const override
  {
    const auto& roadPositions = world->WorldCoord2LaneCoord(inert_pos.x, inert_pos.y, yaw);
    auto [roadGraph, root]
        = world->GetRoadGraph(CommonHelper::GetRoadWithLowestHeading(roadPositions, *world), MAX_DEPTH);
    std::map<RoadGraph::edge_descriptor, double> weights = world->GetEdgeWeights(roadGraph);
    auto target = RouteCalculation::SampleRoute(roadGraph, root, weights, *stochastics);

    return Route{roadGraph, root, target};
  }

  Route CalculateRouteFromWaypoints(const std::vector<mantle_api::RouteWaypoint>& route) const override
  {
    if (route.empty())
    {
      throw(std::runtime_error{"Route is empty"});
    }

    auto startPosition
        = world->WorldCoord2LaneCoord(route.front().waypoint.x, route.front().waypoint.y, 0_rad).begin()->second;
    auto [roadGraph, root]
        = world->GetRoadGraph({startPosition.roadId, startPosition.laneId < 0}, std::max(MAX_DEPTH, route.size()));

    RoadGraphVertex target = root;
    for (auto waypoint = route.begin() + 1; waypoint != route.end(); ++waypoint)
    {
      bool foundSuccessor = false;
      auto position = world->WorldCoord2LaneCoord(waypoint->waypoint.x, waypoint->waypoint.y, 0_rad);
      for (auto [successor, successorsEnd] = adjacent_vertices(target, roadGraph); successor != successorsEnd;
           ++successor)
      {
        for (auto [roadId, roadPosition] : position)
        {
          if (get(RouteElement(), roadGraph, *successor) == RouteElement{roadId, roadPosition.laneId < 0})
          {
            foundSuccessor = true;
            target = *successor;
            break;
          }
        }
      }
      if (!foundSuccessor)
      {
        throw(std::runtime_error{"Invalid route defined in Scenario. Waypoint ("
                                 + std::to_string(waypoint->waypoint.x.value()) + ", "
                                 + std::to_string(waypoint->waypoint.y.value()) + ") not reachable"});
      }
    }

    return Route{roadGraph, root, target};
  }

private:
  const WorldInterface* world;
  StochasticsInterface* stochastics;
};
