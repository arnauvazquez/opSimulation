/********************************************************************************
 * Copyright (c) 2020 HLRS, University of Stuttgart
 *               2017-2019 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <list>
#include <string>

#include <QCommandLineParser>
#include <QCoreApplication>
#include <QString>
#include <QStringList>

#include "common/opExport.h"

/// Information about command line arguments
struct SIMULATIONCOREEXPORT CommandLineArguments
{
  int logLevel;             ///< Level of log
  std::string libPath;      ///< Path of the library
  std::string logFile;      ///< Name of log file
  std::string configsPath;  ///< Path to configs
  std::string resultsPath;  ///< Path to store results
};

/// Information on command line options
struct SIMULATIONCOREEXPORT CommandLineOption
{
  QString name;          ///< Name of the command
  QString description;   ///< Description of the command
  QString valueName;     ///< Value of the command argument
  QString defaultValue;  ///< Default value of the command argument
};

/// Class to parse command line
class SIMULATIONCOREEXPORT CommandLineParser
{
public:
  /// @brief Parse the string of arguments
  /// @param arguments List of arguments
  /// @return Command line arguments
  static CommandLineArguments Parse(const QStringList& arguments);

  /// @brief Getter log on parsing
  /// @return Log on parsing the arguments
  static std::vector<std::string> GetParsingLog();

private:
  static QList<QCommandLineOption> GetOptions();
  static CommandLineArguments CompileCommandLineArguments(const QCommandLineParser& commandLineParser);
  static void EvaluateDefaultedValues(const QCommandLineParser& commandLineParser);

  static std::vector<std::string> parsingLog;
  static const std::vector<CommandLineOption> commandLineOptions;
};
