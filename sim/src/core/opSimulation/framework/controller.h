/*******************************************************************************
 * Copyright (c) 2021 in-tech GmbH
 *               2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "MantleAPI/Traffic/i_controller.h"
#include "entity.h"
#include "include/agentBlueprintInterface.h"
#include "common/log.h"

namespace core
{

//! Base class for all controllers
class Controller : public mantle_api::IController
{
public:
  //! Controller constructor
  //!
  //! @param[in] id       Unique id
  //! @param[in] name     Name of the controller
  //! @param[in] system   System of an agent
  explicit Controller(const mantle_api::UniqueId id, const std::string& name, const System& system) noexcept
  : id{id}, name{name}, system{system} {}

  mantle_api::UniqueId GetUniqueId() const override final { return id; }

  void SetName(const std::string& name) override final { this->name = name; }

  const std::string& GetName() const override final { return name; }

  //! @brief Method which retrieves the systen of an agent
  //!
  //! @return System of an agent
  const System& GetSystem() const { return system; }

  void AddEntity(Entity* entity)
  {
    entity->SetSystem(system);
    entities.push_back(entity);
  }

  void ChangeState(LateralState lateral_state, LongitudinalState longitudinal_state) override
  {
    for (auto entity : entities)
    {
      bool current = entity->GetScenarioControl()->UseCustomController();
      bool lateral = lateral_state == LateralState::kActivate || (lateral_state == LateralState::kNoChange && current);
      bool longitudinal = longitudinal_state == LongitudinalState::kActivate
                       || (longitudinal_state == LongitudinalState::kNoChange && current);
      ThrowIfFalse(lateral == longitudinal,
                   "Different activation of longitudinal and lateral domain for controller is not supported");
      entity->GetScenarioControl()->SetCustomController(lateral);
    }
  }

private:
  const mantle_api::UniqueId id;
  const System system;

  std::vector<Entity*> entities{};

  std::string name = "";
};
}  // namespace core
