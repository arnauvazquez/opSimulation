/*******************************************************************************
 * Copyright (c) 2021 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** \addtogroup Environment
 * @{
 * \file  environment.h
 * @} */
//-----------------------------------------------------------------------------

#pragma once

#include <algorithm>

#include "MantleAPI/Common/time_utils.h"
#include "agentBlueprintProvider.h"
#include "controllerRepository.h"
#include "coordConverter.h"
#include "directories.h"
#include "entityRepository.h"
#include "geometryHelper.h"
#include "idManager.h"
#include "importer/scenery.h"
#include "importer/sceneryImporter.h"
#include "include/environmentInterface.h"
#include "include/worldInterface.h"
#include "laneLocationQueryService.h"
#include "trafficSwarmService.h"

using namespace units::literals;

namespace core
{

//-----------------------------------------------------------------------------
/** \brief Implements the functionality of the interface.*/
//-----------------------------------------------------------------------------
class SIMULATIONCOREEXPORT Environment final : public EnvironmentInterface
{
public:
  //! Environment constructor
  //!
  //! @param[in] configurationDir         References the directory of the configuration files
  //! @param[in] agentBlueprintProvider   References the agent blueprint provider
  //! @param[in] agentFactory             References the agent factory of the framework
  //! @param[in] stochastics              References the stochastics functionality of the framework
  //! @param[in] turningRates             References the turning rates
  Environment(const std::string &configurationDir,
              AgentBlueprintProvider &agentBlueprintProvider,
              core::AgentFactoryInterface *agentFactory,
              StochasticsInterface *stochastics,
              const TurningRates &turningRates,
              const CallbackInterface *callbacks)
      : configurationDir(configurationDir),
        agentBlueprintProvider(agentBlueprintProvider),
        agentFactory(agentFactory),
        routeSampler(stochastics),
        entityRepository(world, agentFactory, &routeSampler),
        controllerRepository(idManager, agentBlueprintProvider),
        turningRates(turningRates),
        coordConverter(callbacks)
  {
  }

  ~Environment() = default;

  void SetSimulationTime(mantle_api::Time simulationTime) override { this->simulationTime = simulationTime; }

  void SyncWorld() override
  {
    world->SyncGlobalData(int(simulationTime.value()));

    std::vector<mantle_api::UniqueId> entitiesToDelete;
    for (const auto &entity : entityRepository.GetEntities())
    {
      if (!dynamic_cast<Entity *>(entity.get())->IsAgentInWorld())
      {
        entitiesToDelete.push_back(entity->GetUniqueId());
      }
    }
    for (const auto &entityToDelete : entitiesToDelete)
    {
      entityRepository.Delete(entityToDelete);
    }

    world->RemoveAgents();
  }

  void ResetControlStrategyStatus()
  {
    for (const auto &entity : entityRepository.GetEntities())
    {
      dynamic_cast<Entity *>(entity.get())->GetScenarioControl()->UpdateForNextTimestep();
    }
  }

  void CreateMap(const std::string &map_file_path, const mantle_api::MapDetails &map_details) override final
  {
    if (world->isInstantiated())
    {
      return;
    }
    auto concaternated_path = openpass::core::Directories::Concat(configurationDir, map_file_path);
    ThrowIfFalse(world, "World has not been set");

    ThrowIfFalse(Importer::SceneryImporter::Import(concaternated_path, &scenery), "Could not import scenery");

    ThrowIfFalse(world->Instantiate(), "Failed to instantiate World");
    world->CreateScenery(&scenery, turningRates);
  }

  void AddEntityToController(mantle_api::IEntity &entity, std::uint64_t controller_id) override final
  {
    auto controller = controllerRepository.controllers.at(controller_id);
    controller->AddEntity(&entityRepository.GetEntity(entity.GetUniqueId()));
  }

  void RemoveEntityFromController(mantle_api::UniqueId entity_id, mantle_api::UniqueId controller_id) override final{};

  void UpdateControlStrategies(
      std::uint64_t entity_id,
      std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies) override final
  {
    for (auto strategy : control_strategies)
    {
      if (auto follow_trajectory_control_strategy
          = std::dynamic_pointer_cast<mantle_api::FollowTrajectoryControlStrategy>(strategy))
      {
        auto &polyline = std::get<mantle_api::PolyLine>(follow_trajectory_control_strategy->trajectory.type);
        std::for_each(begin(polyline),
                      end(polyline),
                      [&](auto &point)
                      {
                        point.pose.position = geometryHelper.TranslateGlobalPositionLocally(
                            point.pose.position,
                            point.pose.orientation,
                            -entityRepository.GetEntity(entity_id).GetProperties()->bounding_box.geometric_center);
                      });
      }
    }
    entityRepository.GetEntity(entity_id).GetScenarioControl()->SetStrategies(control_strategies);
  }

  // TODO CK: This IQueryService needs to be derived by the worlddataquery
  const mantle_api::ILaneLocationQueryService &GetQueryService() const override final
  {
    return laneLocationQueryService;
  }

  const mantle_api::ICoordConverter *GetConverter() const override final { return &coordConverter; }

  const mantle_api::IGeometryHelper *GetGeometryHelper() const override final { return &geometryHelper; }

  EntityRepository &GetEntityRepository() override final { return entityRepository; }

  const EntityRepository &GetEntityRepository() const override final { return entityRepository; }

  ControllerRepository &GetControllerRepository() override final { return controllerRepository; }

  const ControllerRepository &GetControllerRepository() const override final { return controllerRepository; }

  void SetDateTime(mantle_api::Time date_time) override final {}

  mantle_api::Time GetDateTime() override final { return {}; }

  void SetWeather(mantle_api::Weather weather) override final { world->SetWeather(weather); }

  void SetRoadCondition(std::vector<mantle_api::FrictionPatch> friction_patches) override final {}

  void SetTrafficSignalState(const std::string &traffic_signal_name,
                             const std::string &traffic_signal_state) override final
  {
    world->SetTrafficSignalState(traffic_signal_name, traffic_signal_state);
  }

  bool HasControlStrategyGoalBeenReached(std::uint64_t entity_id,
                                         mantle_api::ControlStrategyType type) const override final
  {
    return entityRepository.GetEntity(entity_id).GetScenarioControl()->HasControlStrategyGoalBeenReached(type);
  }

  // This needs to be called before calling create map
  // This should actually be part of the init function to ensure correct usage. WorldBinding & library might need to
  // move into this class
  void SetWorld(WorldInterface *world) override
  {
    this->world = world;
    laneLocationQueryService.Init(world);
    coordConverter.Init(world);
    routeSampler.SetWorld(world);
  }

  mantle_api::Time GetSimulationTime() override final { return simulationTime; }

  void ExecuteCustomCommand(const std::vector<std::string> &actors,
                            const std::string &type,
                            const std::string &command) override
  {
    for (const auto &actor : actors)
    {
      auto entity = entityRepository.Get(actor);
      if (!entity.has_value())
      {
        continue;
      }
      entityRepository.GetEntity(entity.value().get().GetUniqueId()).GetScenarioControl()->AddCustomCommand(command);
    }
  }

  void SetUserDefinedValue(const std::string &name, const std::string &value) override {}

  std::optional<std::string> GetUserDefinedValue(const std::string &name) override { return std::nullopt; }

  void SetDefaultRoutingBehavior(mantle_api::DefaultRoutingBehavior default_routing_behavior) override {}

  void AssignRoute(mantle_api::UniqueId entity_id, mantle_api::RouteDefinition route_definition) override
  {
    return entityRepository.GetEntity(entity_id).AssignRoute(route_definition);
  }

  void InitTrafficSwarmService(const mantle_api::TrafficSwarmParameters &parameters) override {}

  /// @brief Returns the traffic swarm service
  mantle_api::ITrafficSwarmService &GetTrafficSwarmService() override { return trafficSwarmService; }

  void Reset() override
  {
    idManager.Reset();
    entityRepository = {world, agentFactory, &routeSampler};
    controllerRepository = {idManager, agentBlueprintProvider};
    world->Reset();
  }

  IdManager idManager{};                              //!< References the idManager that handles unique IDs
  Scenery scenery;                                    //!< References the scenery as a list of roads
  WorldInterface *world{nullptr};                     //!< References the world of the framework
  LaneLocationQueryService laneLocationQueryService;  //!< References the laneLocationQueryService
  CoordConverter
      coordConverter;  //!< References the coord converter that transforms different position types, curvature etc.
  GeometryHelper geometryHelper;        //!< References the geometry helper that performs geometrical calculations
  const std::string &configurationDir;  //!< References the directory of the configuration files
  AgentBlueprintProvider &agentBlueprintProvider;      //!< References the agentBlueprintProvider
  core::AgentFactoryInterface *agentFactory{nullptr};  //!< References the agent factory of the framework
  const TurningRates &turningRates;                    //!< References the turning rates
  RouteSampler routeSampler;                           //!< References the route sampler
  EntityRepository
      entityRepository;  //!< References the entity repository that provides CRUD functionality for scenario entities
  ControllerRepository
      controllerRepository;  //!< References the controller repository that provides CRUD functionality for controllers
  TrafficSwarmService trafficSwarmService;
  mantle_api::Time simulationTime{};  //!< Time since start of simulation
};

}  //namespace core