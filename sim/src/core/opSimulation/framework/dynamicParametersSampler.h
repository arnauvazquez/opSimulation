/********************************************************************************
 * Copyright (c) 2019 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once
#include <unordered_map>

#include "common/sensorDefinitions.h"
#include "include/profilesInterface.h"
#include "include/stochasticsInterface.h"

class DynamicParameterSampler;

/// Information of dynamic parameters
struct DynamicParameters
{
  friend DynamicParameterSampler;                   ///< Information of dynamic paramter samplers
  std::unordered_map<int, double> sensorLatencies;  ///< Map of sensors and its latencies

  /// @brief make a dynamic parameter sampler
  /// @param stochastics          Reference to stochastics interface
  /// @param vehicleProfileName   Reference to the name of the vehicle profile
  /// @param profiles             Pointer to the profiles interface
  /// @return Returns a dynamic prarameter sampler
  static DynamicParameterSampler make(StochasticsInterface& stochastics,
                                      std::string& vehicleProfileName,
                                      const ProfilesInterface* profiles);

  /// @return dynamic parameters (For testing)
  static DynamicParameters empty();

private:
  DynamicParameters() = default;
};

/// Information of dynamic paramter samplers
class DynamicParameterSampler
{
public:
  /// @brief DynamicParameterSampler constructor
  /// @param stochastics          Reference to stochastics interface
  /// @param vehicleProfileName   Reference to the name of the vehicle profile
  /// @param profiles             Pointer to the profiles interface
  DynamicParameterSampler(StochasticsInterface& stochastics,
                          std::string& vehicleProfileName,
                          const ProfilesInterface* profiles);

  /// @brief Move operator
  operator DynamicParameters&&() { return std::move(dynamicParameter); }

  /// @return Getter function for sample sensor latencies
  DynamicParameterSampler& SampleSensorLatencies();

private:
  DynamicParameters dynamicParameter;
  StochasticsInterface& stochastics;
  std::string& vehicleProfileName;
  const ProfilesInterface* profiles;
};
