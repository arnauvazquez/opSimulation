/********************************************************************************
 * Copyright (c) 2020 HLRS, University of Stuttgart
 *               2019-2020 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <string>
#include <vector>

#include "common/observationLibraryDefinitions.h"
#include "common/opExport.h"
#include "common/spawnPointLibraryDefinitions.h"
#include "directories.h"

namespace
{
SpawnPointLibraryInfoCollection ConcatenateSpawnPointLibraries(
    const std::string& libraryDir, const SpawnPointLibraryInfoCollection& spawnPointLibraries)
{
  SpawnPointLibraryInfoCollection libraries = spawnPointLibraries;
  for (auto& libraryInfo : libraries)
  {
    libraryInfo.libraryName = openpass::core::Directories::Concat(libraryDir, libraryInfo.libraryName);
  }

  return libraries;
}

ObservationInstanceCollection ConcatenateObservationLibraries(const std::string& libraryDir,
                                                              const ObservationInstanceCollection& observationLibraries)
{
  ObservationInstanceCollection libraries = observationLibraries;
  for (auto& libraryInfo : libraries)
  {
    libraryInfo.libraryName = openpass::core::Directories::Concat(libraryDir, libraryInfo.libraryName);
  }

  return libraries;
}
}  //namespace

/// Information on framework modules
struct SIMULATIONCOREEXPORT FrameworkModules
{
public:
  /// @brief FrameworkModules constrcutor
  /// @param logLevel                 Level of log
  /// @param libraryDir               Reference to the library directory
  /// @param dataBufferLibrary        Reference to the data buffer directory
  /// @param observationLibraries     Reference to the observation library
  /// @param stochasticsLibrary       Reference to the Stochastics library
  /// @param worldLibrary             Reference to the world library
  /// @param spawnPointLibraries      Reference to the spawn point library
  FrameworkModules(const int logLevel,
                   const std::string& libraryDir,
                   const std::string& dataBufferLibrary,
                   const ObservationInstanceCollection& observationLibraries,
                   const std::string& stochasticsLibrary,
                   const std::string& worldLibrary,
                   const SpawnPointLibraryInfoCollection& spawnPointLibraries)
      : logLevel{logLevel},
        libraryDir{libraryDir},
        dataBufferLibrary{openpass::core::Directories::Concat(libraryDir, dataBufferLibrary)},
        observationLibraries{ConcatenateObservationLibraries(libraryDir, observationLibraries)},
        stochasticsLibrary{openpass::core::Directories::Concat(libraryDir, stochasticsLibrary)},
        worldLibrary{openpass::core::Directories::Concat(libraryDir, worldLibrary)},
        spawnPointLibraries{ConcatenateSpawnPointLibraries(libraryDir, spawnPointLibraries)}
  {
  }

  const int logLevel;                                        ///< Level of the log
  const std::string libraryDir;                              ///< Directory of the library
  const std::string dataBufferLibrary;                       ///< Data buffer library
  const ObservationInstanceCollection observationLibraries;  ///< Observation library
  const std::string stochasticsLibrary;                      ///< Stochastics library
  const std::string worldLibrary;                            ///< World library
  SpawnPointLibraryInfoCollection spawnPointLibraries;       ///< Spawn point library
};
