/********************************************************************************
 * Copyright (c) 2017-2020 in-tech GmbH
 *               2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "roadSignal.h"

#include <exception>

std::string RoadSignal::GetCountry() const
{
  return signal.country;
}

std::string RoadSignal::GetCountryRevision() const
{
  return signal.countryRevision;
}

std::string RoadSignal::GetType() const
{
  return signal.type;
}

std::string RoadSignal::GetSubType() const
{
  return signal.subtype;
}

std::string RoadSignal::GetId() const
{
  return signal.id;
}

std::optional<double> RoadSignal::GetValue() const
{
  return signal.value;
}

RoadSignalUnit RoadSignal::GetUnit() const
{
  return signal.unit;
}

std::string RoadSignal::GetText() const
{
  return signal.text;
}

units::length::meter_t RoadSignal::GetS() const
{
  return signal.s;
}

units::length::meter_t RoadSignal::GetT() const
{
  return signal.t;
}

bool RoadSignal::IsValidForLane(int laneId) const
{
  return signal.validity.all
      || (std::find(signal.validity.lanes.begin(), signal.validity.lanes.end(), laneId) != signal.validity.lanes.end());
}

units::length::meter_t RoadSignal::GetHeight() const
{
  return signal.height;
}

units::length::meter_t RoadSignal::GetWidth() const
{
  return signal.width;
}

units::angle::radian_t RoadSignal::GetPitch() const
{
  return signal.pitch;
}

units::angle::radian_t RoadSignal::GetRoll() const
{
  return signal.roll;
}

bool RoadSignal::GetIsDynamic() const
{
  return signal.dynamic == "yes";
}

std::vector<std::string> RoadSignal::GetDependencies() const
{
  return signal.dependencyIds;
}

units::length::meter_t RoadSignal::GetZOffset() const
{
  return signal.zOffset;
}

bool RoadSignal::GetOrientation() const
{
  return signal.orientation == "+";
}

units::angle::radian_t RoadSignal::GetHOffset() const
{
  return signal.hOffset;
}
