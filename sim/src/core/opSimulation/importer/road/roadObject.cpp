/********************************************************************************
 * Copyright (c) 2017-2021 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "roadObject.h"

#include <exception>

RoadObjectType RoadObject::GetType() const
{
  return object.type;
}

units::length::meter_t RoadObject::GetLength() const
{
  return object.length;
}

units::length::meter_t RoadObject::GetWidth() const
{
  return object.width;
}

bool RoadObject::IsContinuous() const
{
  return object.continuous;
}

units::angle::radian_t RoadObject::GetHdg() const
{
  return object.hdg;
}

units::length::meter_t RoadObject::GetHeight() const
{
  return object.height;
}

units::angle::radian_t RoadObject::GetPitch() const
{
  return object.pitch;
}

units::angle::radian_t RoadObject::GetRoll() const
{
  return object.roll;
}

std::string RoadObject::GetId() const
{
  return object.id;
}

units::length::meter_t RoadObject::GetS() const
{
  return object.s;
}

units::length::meter_t RoadObject::GetT() const
{
  return object.t;
}

units::length::meter_t RoadObject::GetZOffset() const
{
  return object.zOffset;
}

bool RoadObject::IsValidForLane(int laneId) const
{
  return object.validity.all
      || (std::find(object.validity.lanes.begin(), object.validity.lanes.end(), laneId) != object.validity.lanes.end());
}

std::string RoadObject::GetName() const
{
  return object.name;
}
