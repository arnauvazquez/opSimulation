/********************************************************************************
 * Copyright (c) 2016-2018 ITK Engineering GmbH
 *               2017-2021 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  channel.h
//! @brief This file contains the internal representation of a channel instance
//!        during a simulation run.
//-----------------------------------------------------------------------------

#pragma once

#include <cstddef>
#include <list>
#include <string>
#include <tuple>

#include "include/componentInterface.h"
#include "include/modelInterface.h"

namespace core
{

class Agent;
class ChannelBuffer;

//! This class represents a channel instance during a simulation run
class Channel
{
public:
  //! Index within tuple to identify (link/component pair) of channel targets
  enum class TargetLinkType : std::size_t
  {
    LinkId = 0,
    Component
  };

  //! Channel constructor
  //!
  //! @param[in] id   Id of the channel
  Channel(int id);
  Channel(const Channel &) = delete;
  Channel(Channel &&) = delete;
  Channel &operator=(const Channel &) = delete;
  Channel &operator=(Channel &&) = delete;
  virtual ~Channel() = default;

  //! Sets the agent
  //!
  //! @param[in] agent    Agent to assign
  void SetAgent(Agent *agent);

  //! Returns the stored agent
  //!
  //! @return Stored agent
  Agent *GetAgent() const;

  //! Sets the channel source
  //!
  //! @param[in] source       Channel source
  //! @param[in] sourceLinkId The sourceLinkId representing the channel source
  //! @return False if channel source is already set, true otherwise
  bool SetSource(ComponentInterface *source, int sourceLinkId);

  //! Sets the channel buffer
  //!
  //! @param[in] buffer       Channel buffer
  //! @return False if channel buffer is already set, true otherwise
  bool AttachSourceBuffer(ChannelBuffer *buffer);

  //! Adds the channel target to the stored list of channel targets
  //!
  //! @param[in] target       Channel target to add
  //! @param[in] targetLinkId The targetLinkId representing the channel target
  //! @return Flag if adding the channel target was successful
  bool AddTarget(ComponentInterface *target, int targetLinkId);

  //! Returns the channel source
  //!
  //! @return Channel source
  ComponentInterface *GetSource() const;

  //! Returns the channel targets
  //!
  //! @return Channel targets
  const std::vector<std::pair<int, ComponentInterface *>> &GetTargets() const;

private:
  static const int Undefined = -1;

  Agent *agent;
  int id;
  int sourceLinkId;
  ComponentInterface *source;
  std::vector<std::pair<int, ComponentInterface *>> targets;
  ChannelBuffer *buffer;
};

}  // namespace core
