/********************************************************************************
 * Copyright (c) 2016-2018 ITK Engineering GmbH
 *               2017-2019 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  parameters.h
//! @brief This file contains the interface of the internal representation of
//!        configuration parameters.
//-----------------------------------------------------------------------------

#pragma once

#include <list>
#include <map>
#include <memory>
#include <string>
#include <vector>

#include "common/opExport.h"
#include "include/parameterInterface.h"

namespace SimulationCommon
{

/// This class represents a configuration parameters
class Parameters : public ParameterInterface
{
public:
  ~Parameters() override = default;

  /**
   * @brief Parameters constructor
   *
   * @param[in] runtimeInformation    Common runtimeInformation
   */
  Parameters(const openpass::common::RuntimeInformation& runtimeInformation) : runtimeInformation(runtimeInformation) {}

  /**
   * @brief Parameters constructor
   *
   * @param[in] other Configuration parameters
   */
  Parameters(const ParameterInterface& other)
      : runtimeInformation{other.GetRuntimeInformation()},
        parametersDouble{other.GetParametersDouble()},
        parametersInt{other.GetParametersInt()},
        parametersBool{other.GetParametersBool()},
        parametersString{other.GetParametersString()},
        parametersDoubleVector{other.GetParametersDoubleVector()},
        parametersIntVector{other.GetParametersIntVector()},
        parametersBoolVector{other.GetParametersBoolVector()},
        parametersStringVector{other.GetParametersStringVector()},
        parametersStochastic{other.GetParametersStochastic()},
        parameterLists{other.GetParameterLists()}
  {
  }

  bool AddParameterDouble(std::string name, double value) override;
  bool AddParameterInt(std::string name, int value) override;
  bool AddParameterBool(std::string name, bool value) override;
  bool AddParameterString(std::string name, const std::string& value) override;
  bool AddParameterDoubleVector(std::string name, const std::vector<double> value) override;
  bool AddParameterIntVector(std::string name, const std::vector<int> value) override;
  bool AddParameterBoolVector(std::string name, const std::vector<bool> value) override;
  bool AddParameterStringVector(std::string name, const std::vector<std::string> value) override;
  bool AddParameterStochastic(std::string name, const openpass::parameter::StochasticDistribution value) override;

  ParameterInterface& InitializeListItem(std::string key) override;

  //NOTE: The primitive getters are in header on purpose

  const openpass::common::RuntimeInformation& GetRuntimeInformation() const override { return runtimeInformation; }

  const std::map<std::string, double>& GetParametersDouble() const override { return parametersDouble; }

  const std::map<std::string, int>& GetParametersInt() const override { return parametersInt; }

  const std::map<std::string, bool>& GetParametersBool() const override { return parametersBool; }

  const std::map<std::string, const std::string>& GetParametersString() const override { return parametersString; }

  const std::map<std::string, const std::vector<double>>& GetParametersDoubleVector() const override
  {
    return parametersDoubleVector;
  }

  const std::map<std::string, const std::vector<int>>& GetParametersIntVector() const override
  {
    return parametersIntVector;
  }

  const std::map<std::string, const std::vector<bool>>& GetParametersBoolVector() const override
  {
    return parametersBoolVector;
  }

  const std::map<std::string, const std::vector<std::string>>& GetParametersStringVector() const override
  {
    return parametersStringVector;
  }

  const std::map<std::string, const openpass::parameter::StochasticDistribution>& GetParametersStochastic()
      const override
  {
    return parametersStochastic;
  }

  const std::map<std::string, ParameterLists>& GetParameterLists() const override { return parameterLists; }

protected:
  /// Runtime information from the interface
  const openpass::common::RuntimeInformation& runtimeInformation;

  /// Mapping of "id" to "value"
  std::map<std::string, double> parametersDouble;                                ///< Parameters of type double
  std::map<std::string, int> parametersInt;                                      ///< Parameters of type int
  std::map<std::string, bool> parametersBool;                                    ///< Parameters of type bool
  std::map<std::string, const std::string> parametersString;                     ///< Parameters of type string
  std::map<std::string, const std::vector<double>> parametersDoubleVector;       ///< Parameters of type double vector
  std::map<std::string, const std::vector<int>> parametersIntVector;             ///< Parameters of type int vector
  std::map<std::string, const std::vector<bool>> parametersBoolVector;           ///< Parameters of type bool vector
  std::map<std::string, const std::vector<std::string>> parametersStringVector;  ///< Parameters of type string vector
  std::map<std::string, const openpass::parameter::StochasticDistribution>
      parametersStochastic;                              ///< Parameters of stochastic type
  std::map<std::string, ParameterLists> parameterLists;  ///< Parameter lists
};

/// This class represents a model parameters
class ModelParameters : public Parameters
{
public:
  /**
   * @brief ModelParameters constructor
   *
   * @param[in] runtimeInformation    Common runtimeInformation
   */
  ModelParameters(const openpass::common::RuntimeInformation& runtimeInformation) : Parameters(runtimeInformation) {}
};

/// This class represents a spawn point parameters
class SpawnPointParameters : public Parameters
{
public:
  /**
   * @brief SpawnPointParameters constructor
   *
   * @param[in] runtimeInformation    Common runtimeInformation
   */
  SpawnPointParameters(const openpass::common::RuntimeInformation& runtimeInformation) : Parameters(runtimeInformation)
  {
  }
};

/// This class represents an observation parameters
class ObservationParameters : public Parameters
{
public:
  /**
   * @brief ObservationParameters constructor
   *
   * @param[in] runtimeInformation    Common runtimeInformation
   */
  ObservationParameters(const openpass::common::RuntimeInformation& runtimeInformation) : Parameters(runtimeInformation)
  {
  }
};

/// This class represents a world parameters
class WorldParameters : public Parameters
{
public:
  /**
   * @brief WorldParameters constructor
   *
   * @param[in] runtimeInformation    Common runtimeInformation
   */
  WorldParameters(const openpass::common::RuntimeInformation& runtimeInformation) : Parameters(runtimeInformation) {}
};

}  // namespace SimulationCommon
