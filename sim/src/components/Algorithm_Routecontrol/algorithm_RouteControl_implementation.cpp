/********************************************************************************
 * Copyright (c) 2020-2021 ITK Engineering GmbH
 *               2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2023 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/**
 * @defgroup module_rc Route Control
 * image html @cond RC_00_Overview.png @endcond "RouteControl function overview"
 * This algorithm receives a trajectory in two dimensions (x- and y-values in cartzesian coordinates with corresponding
 * velocities and time steps). Thus, the actual position and velocity of the agent are analyzed and the deviation to the
 * desired values is obtained. In order to correct vehicle's position and velocity, following outputs are generated:
 * - throttle pedal state [0; 1]
 * - brake pedal state [0; 1]
 * - steering angle [-pi/4; pi/4]
 */

/**
 * @addtogroup module_rc
 * Furthermore, different driver types are considered. The driver type is described by an aggressivity parameter (a
 * scaling factor between "0" and "1"). Lower aggressivity introduces scaled bounds to the three outputs decribed above
 * image @cond html RC_30_Driver.png @endcond "RouteControl driver types"
 * Abbreviations:
 * - CS = coordinate system
 */

/**
 * @ingroup module_rc
 * @defgroup init_rc Initialization
 */

/**
 * @ingroup module_rc
 * @defgroup sim_step_00_rc_start Simulation step entry
 */

/**
 * @ingroup module_rc
 * @defgroup sim_step_10_rc_error Calculation of errors
 */

/**
 * @ingroup module_rc
 * @defgroup sim_step_20_rc_steer Calculation of steering control
 * image html @cond RC_10_Steer.png @endcond "Steering control"
 */

/**
 * @ingroup module_rc
 * @defgroup sim_step_30_rc_accel Calculation of acceleration control
 * image html @cond RC_20_Accel.png @endcond "Acceleration control"
 */

/**
 * @ingroup module_rc
 * @defgroup sim_step_40_rc_out Simulation step output
 */

#include "algorithm_RouteControl_implementation.h"

#include <array>
#include <memory>

#include <QtGlobal>

#include "common/commonTools.h"
#include "components/common/vehicleProperties.h"

Algorithm_Routecontrol_Implementation::Algorithm_Routecontrol_Implementation(
    std::string componentName,
    bool isInit,
    int priority,
    int offsetTime,
    int responseTime,
    int cycleTime,
    StochasticsInterface *stochastics,
    WorldInterface *world,
    const ParameterInterface *parameters,
    PublisherInterface *const publisher,
    const CallbackInterface *callbacks,
    AgentInterface *agent,
    std::shared_ptr<ScenarioControlInterface> const scenarioControl)
    : UnrestrictedControllStrategyModelInterface(componentName,
                                                 isInit,
                                                 priority,
                                                 offsetTime,
                                                 responseTime,
                                                 cycleTime,
                                                 stochastics,
                                                 world,
                                                 parameters,
                                                 publisher,
                                                 callbacks,
                                                 agent,
                                                 scenarioControl)
{
  LOGINFO(QString().sprintf("Constructing %s for agent %d", COMPONENTNAME.c_str(), agent->GetId()).toStdString());

  mTimeStep = units::time::second_t((double)cycleTime / 1000.0);

  try
  {
    auto parameterMapDoubleExternal = parameters->GetParametersDouble();
    foreach (auto &iterator, parameterMapDouble)
    {
      std::string id = iterator.first;
      parameterMapDouble.at(id)->SetValue(parameterMapDoubleExternal.at(id));
    }
  }
  catch (...)
  {
    const std::string msg = COMPONENTNAME + " could not init parameters";
    LOG(CbkLogLevel::Error, msg);
    throw std::runtime_error(msg);
  }

  LOGDEBUG(QString().sprintf("Construction of %s successful", COMPONENTNAME.c_str()).toStdString());
}

Algorithm_Routecontrol_Implementation::~Algorithm_Routecontrol_Implementation()
{
  LOGDEBUG(QString().sprintf("Destroying %s", COMPONENTNAME.c_str()).toStdString());

  delete waypoints;
  waypoints = nullptr;

  delete routeControl;
  routeControl = nullptr;

  LOGDEBUG(QString().sprintf("Destruction of %s successful", COMPONENTNAME.c_str()).toStdString());
}

void Algorithm_Routecontrol_Implementation::UpdateInput(int localLinkId,
                                                        const std::shared_ptr<SignalInterface const> &data,
                                                        int time)
{
  Q_UNUSED(time);
  Q_UNUSED(localLinkId);

  LOGDEBUG(QString().sprintf("%s UpdateInput", COMPONENTNAME.c_str()).toStdString());
}

void Algorithm_Routecontrol_Implementation::UpdateOutput(int localLinkId,
                                                         std::shared_ptr<SignalInterface const> &data,
                                                         int time)
{
  Q_UNUSED(time);
  if (localLinkId == 0)
  {
    try
    {
      data = std::make_shared<LongitudinalSignal const>(
          outComponentState, outAcceleratorPedalPosition, outBrakePedalPosition, outGear, GetComponentName());
    }
    catch (const std::bad_alloc &)
    {
      const std::string msg = COMPONENTNAME + " could not instantiate signal";
      LOG(CbkLogLevel::Debug, msg);
      throw std::runtime_error(msg);
    }
  }
  else if (localLinkId == 1)
  {
    try
    {
      data = std::make_shared<SteeringSignal const>(
          outComponentState, outSteeringWheelAngle * 1_rad, GetComponentName());
    }
    catch (const std::bad_alloc &)
    {
      const std::string msg = COMPONENTNAME + " could not instantiate signal";
      LOG(CbkLogLevel::Debug, msg);
      throw std::runtime_error(msg);
    }
  }
  else
  {
    const std::string msg = COMPONENTNAME + " invalid link";
    LOG(CbkLogLevel::Debug, msg);
    throw std::runtime_error(msg);
  }
}

void Algorithm_Routecontrol_Implementation::Trigger(int time_ms)
{
  /** @addtogroup init_rc
   * Read trajectory to be followed:
   *  - time steps
   *  - x-position in cartesian coordinates
   *  - y-position in cartesian coordinates
   *  - vehicle's longitudinal velocity in vehicle's CS
   */
  if (!routeControl)  // only allocate and initialize once
  {
    routeControl = new RouteControl(mTimeStep);
    /** @addtogroup init_rc
     * Define vehicle's and driver's characteristics:
     *  - total vehicle mass
     *  - engine power
     *  - maximum brake torque
     *  - driver aggressivity
     */

    auto weight = GetAgent()->GetVehicleModelParameters()->mass;

    routeControl->SetVehicleProperties(weight,
                                       maxpower.GetValue(),
                                       units::torque::newton_meter_t(mintorque.GetValue()),
                                       drivingAgressivity.GetValue());

    routeControl->SetPIDParameters(pedalsKp.GetValue(),
                                   pedalsKi.GetValue(),
                                   pedalsKd.GetValue(),
                                   steeringKp.GetValue(),
                                   steeringKi.GetValue(),
                                   steeringKd.GetValue());
  }

  if (!waypoints)  // only allocate once
  {
    ReadWayPointData();  // for periodic trajectory update: move this line outside the IF statement
  }

  routeControl->SetRequestedTrajectory(*waypoints);

  if (routeControl == nullptr)
  {
    LOGERROR(QString().sprintf("%s Trigger not callable", COMPONENTNAME.c_str()).toStdString());
    return;
  }

  // perform calculations
  routeControl->Perform(units::time::millisecond_t(time_ms),
                        GetAgent()->GetPositionX(),
                        GetAgent()->GetPositionY(),
                        GetAgent()->GetYaw(),
                        GetAgent()->GetVelocity().Length());

  LOGDEBUG(QString()
               .sprintf("%s output (agent %d) : FrontWheelAngle = %f, ThrottlePedal = %f, BrakePedal = %f",
                        COMPONENTNAME.c_str(),
                        GetAgent()->GetId(),
                        180 / 3.14 * routeControl->GetFrontWheelAngle().value(),
                        routeControl->GetThrottlePedal(),
                        routeControl->GetBrakePedal())
               .toStdString());

  /** @addtogroup sim_step_40_rc_out
   * Write new output signals:
   * - steering angle
   * - throttle pedal state
   * - brake pedal state
   * - brake superposition state
   */
  auto steeringRatio
      = helper::map::query(GetAgent()->GetVehicleModelParameters()->properties, vehicle::properties::SteeringRatio);
  THROWIFFALSE(steeringRatio.has_value(), "SteeringRatio was not defined in VehicleCatalog");
  outBrakePedalPosition = routeControl->GetBrakePedal();
  outSteeringWheelAngle = routeControl->GetFrontWheelAngle().value() * std::stod(steeringRatio.value());
  outAcceleratorPedalPosition = routeControl->GetThrottlePedal();
  outGear = 1;
  outComponentState = ComponentState::Acting;

  LOGDEBUG(QString().sprintf("%s Trigger successful", COMPONENTNAME.c_str()).toStdString());
  return;
}

void Algorithm_Routecontrol_Implementation::ReadWayPointData()
{
  const auto controlStrategies
      = GetScenarioControl()->GetStrategies(mantle_api::ControlStrategyType::kFollowTrajectory);
  if (controlStrategies.empty())
  {
    return;
  }
  const mantle_api::PolyLine &trajectory = std::get<mantle_api::PolyLine>(
      std::dynamic_pointer_cast<mantle_api::FollowTrajectoryControlStrategy const>(controlStrategies.front())
          ->trajectory.type);
  unsigned int n = trajectory.size();

  waypoints = new std::vector<WaypointData>(n);

  units::velocity::meters_per_second_t vel{0.0};

  for (unsigned int i = 0; i < n; ++i)
  {
    auto polyLinePoint = trajectory.at(i);

    if (!polyLinePoint.time.has_value())
    {
      LOGERROR(QString()
                   .sprintf("%s ReadWayPointData failed. Time in PolyLinePoint not set.", COMPONENTNAME.c_str())
                   .toStdString());
    }
    waypoints->at(i).time = polyLinePoint.time.value();  // s
    waypoints->at(i).position.x = polyLinePoint.pose.position.x;
    waypoints->at(i).position.y = polyLinePoint.pose.position.y;
    if (i < n - 1)
    {
      auto polyLinePointNext = trajectory.at(i + 1);
      vel = units::math::sqrt(units::math::pow<2>(polyLinePointNext.pose.position.x - polyLinePoint.pose.position.x)
                              + units::math::pow<2>(polyLinePointNext.pose.position.y - polyLinePoint.pose.position.y))
          / mTimeStep;  // uniform motion approximation
    }
    waypoints->at(i).longVelocity = vel;
  }
}
