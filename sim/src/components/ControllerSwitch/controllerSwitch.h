/********************************************************************************
 * Copyright (c) 2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** @file  controllerSwitch.h
 *	@brief This file provides the exported methods.
 *
 *   This file provides the exported methods which are available outside of the library. */
//-----------------------------------------------------------------------------

#pragma once

#pragma once
#include <QtCore/qglobal.h>

#if defined(CONTROLLER_SWITCH_LIBRARY)
#define CONTROLLER_SWITCH_SHARED_EXPORT Q_DECL_EXPORT  //!< Export of the dll-functions
#else
#define CONTROLLER_SWITCH_SHARED_EXPORT Q_DECL_IMPORT  //!< Import of the dll-functions
#endif

#include "include/modelInterface.h"
