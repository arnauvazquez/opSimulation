/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//-----------------------------------------------------------------------------
//! @file  dynamics_scenario_implementation.cpp
//! @brief This file contains the implementation header file
//-----------------------------------------------------------------------------

#include "dynamics_scenario_implementation.h"

#include <algorithm>
#include <cassert>
#include <memory>

#include <qglobal.h>

#include "include/agentInterface.h"
#include "include/egoAgentInterface.h"
#include "include/worldInterface.h"

void DynamicsScenarioImplementation::UpdateInput(int localLinkId,
                                                 const std::shared_ptr<SignalInterface const> &data,
                                                 int time)
{
}

void DynamicsScenarioImplementation::UpdateOutput(int localLinkId,
                                                  std::shared_ptr<SignalInterface const> &data,
                                                  int time)
{
  Q_UNUSED(time);

  if (localLinkId == 0)
  {
    try
    {
      data = std::make_shared<DynamicsSignal const>(dynamicsSignal);
    }
    catch (const std::bad_alloc &)
    {
      const std::string msg = COMPONENTNAME + " could not instantiate signal";
      LOG(CbkLogLevel::Debug, msg);
      throw std::runtime_error(msg);
    }
  }
  else
  {
    const std::string msg = COMPONENTNAME + " invalid link";
    LOG(CbkLogLevel::Debug, msg);
    throw std::runtime_error(msg);
  }
}

void DynamicsScenarioImplementation::Trigger(int time)
{
  const auto agent = GetAgent();

  if (GetScenarioControl()->HasNewLongitudinalStrategy())
  {
    const auto newControlStrategy
        = GetScenarioControl()->GetStrategies(mantle_api::MovementDomain::kLongitudinal).front();
    longitudinalStrategy = newControlStrategy->type;
    timeLongitudinalStrategy = 0.0_s;
    switch (newControlStrategy->type)
    {
      case mantle_api::ControlStrategyType::kKeepVelocity:
        velocityToKeep = agent->GetVelocity().Length();
        break;
      case mantle_api::ControlStrategyType::kFollowVelocitySpline:
        velocity_splines
            = std::dynamic_pointer_cast<mantle_api::FollowVelocitySplineControlStrategy const>(newControlStrategy)
                  ->velocity_splines;
        break;
      case mantle_api::ControlStrategyType::kFollowTrajectory:
        trajectory = std::get<mantle_api::PolyLine>(
            std::dynamic_pointer_cast<mantle_api::FollowTrajectoryControlStrategy const>(newControlStrategy)
                ->trajectory.type);
        break;
      default:
        break;
    }
  }

  if (GetScenarioControl()->HasNewLateralStrategy())
  {
    const auto newControlStrategy = GetScenarioControl()->GetStrategies(mantle_api::MovementDomain::kLateral).front();
    lateralStrategy = newControlStrategy->type;
    timeLateralStrategy = 0.0_s;
    switch (newControlStrategy->type)
    {
      case mantle_api::ControlStrategyType::kPerformLaneChange:
        trajectory = CalculateSinusiodalLaneChange(
            std::dynamic_pointer_cast<mantle_api::PerformLaneChangeControlStrategy const>(newControlStrategy));
        break;
      default:
        break;
    }
  }

  const auto dt = units::make_unit<units::time::millisecond_t>(GetCycleTime());
  timeLongitudinalStrategy += dt;
  timeLateralStrategy += dt;

  units::velocity::meters_per_second_t velocity{};

  switch (longitudinalStrategy)
  {
    case mantle_api::ControlStrategyType::kKeepVelocity:
      velocity = velocityToKeep;
      break;
    case mantle_api::ControlStrategyType::kFollowVelocitySpline:
      velocity = GetVelocityFromSplines();
      break;
    case mantle_api::ControlStrategyType::kFollowTrajectory:
      break;
    default:
      LOGWARN("Unsupported longitudinal strategy");
      break;
  }

  units::length::meter_t tDistance{};
  units::angle::radian_t yaw{0.0_rad};
  units::length::meter_t sDistance = velocity * dt * units::math::cos(yaw);
  std::optional<Position> newPosition;
  const auto currVelocity = agent->GetVelocity().Length();

  switch (lateralStrategy)
  {
    case mantle_api::ControlStrategyType::kKeepLaneOffset:
      tDistance = 0.0_m;
      newPosition = agent->GetEgoAgent().GetWorldPosition(sDistance, tDistance, yaw);
      if (!newPosition)
      {
        LOGINFO("Cannot calculate new Position");
        return;
      }

      dynamicsSignal.dynamicsInformation.acceleration = (velocity - currVelocity) / dt;
      dynamicsSignal.dynamicsInformation.velocityX = velocity * units::math::cos(newPosition->yawAngle);
      dynamicsSignal.dynamicsInformation.velocityY = velocity * units::math::sin(newPosition->yawAngle);
      dynamicsSignal.dynamicsInformation.positionX = newPosition->xPos;
      dynamicsSignal.dynamicsInformation.positionY = newPosition->yPos;
      dynamicsSignal.dynamicsInformation.yaw = newPosition->yawAngle;
      dynamicsSignal.dynamicsInformation.yawRate = 0.0_rad_per_s;
      break;

    case mantle_api::ControlStrategyType::kPerformLaneChange:
    case mantle_api::ControlStrategyType::kFollowTrajectory:
      dynamicsSignal.dynamicsInformation = ReadWayPointData();
      break;

    default:
      LOGERROR("Unsupported lateral strategy");
      break;
  }

  dynamicsSignal.dynamicsInformation.yawAcceleration = 0.0_rad_per_s_sq;
  dynamicsSignal.dynamicsInformation.roll = 0.0_rad;
  dynamicsSignal.dynamicsInformation.steeringWheelAngle = 0.0_rad;
  dynamicsSignal.dynamicsInformation.centripetalAcceleration = 0.0_mps_sq;
  dynamicsSignal.dynamicsInformation.travelDistance = 0.0_m;
  dynamicsSignal.longitudinalController = GetComponentName();
  dynamicsSignal.lateralController = GetComponentName();
}

units::velocity::meters_per_second_t DynamicsScenarioImplementation::GetVelocityFromSplines()
{
  for (auto &splineSection : velocity_splines)
  {
    if (timeLongitudinalStrategy >= splineSection.start_time && timeLongitudinalStrategy < splineSection.end_time)
    {
      return std::get<0>(splineSection.polynomial) * timeLongitudinalStrategy * timeLongitudinalStrategy
               * timeLongitudinalStrategy
           + std::get<1>(splineSection.polynomial) * timeLongitudinalStrategy * timeLongitudinalStrategy
           + std::get<2>(splineSection.polynomial) * timeLongitudinalStrategy + std::get<3>(splineSection.polynomial);
    }
  }
  auto &lastSplineSection = velocity_splines.back();
  auto lastTime = lastSplineSection.end_time;
  GetScenarioControl()->SetControlStrategyGoalReached(mantle_api::ControlStrategyType::kFollowVelocitySpline);
  return std::get<0>(lastSplineSection.polynomial) * lastTime * lastTime * lastTime
       + std::get<1>(lastSplineSection.polynomial) * lastTime * lastTime
       + std::get<2>(lastSplineSection.polynomial) * lastTime + std::get<3>(lastSplineSection.polynomial);
}

mantle_api::PolyLine DynamicsScenarioImplementation::CalculateSinusiodalLaneChange(
    std::shared_ptr<const mantle_api::PerformLaneChangeControlStrategy> laneChange)
{
  units::length::meter_t deltaS;
  units::time::second_t deltaTime;
  if (laneChange->transition_dynamics.dimension == mantle_api::Dimension::kDistance)
  {
    deltaS = units::length::meter_t(laneChange->transition_dynamics.value);
    deltaTime = deltaS / GetAgent()->GetVelocity().Length();
  }
  else if (laneChange->transition_dynamics.dimension == mantle_api::Dimension::kTime)
  {
    deltaS = units::time::second_t(laneChange->transition_dynamics.value) * GetAgent()->GetVelocity().Length();
    deltaTime = units::time::second_t(laneChange->transition_dynamics.value);
  }
  else
  {
    LOGERRORANDTHROW("Unknown lane change transition dynamics dimension")
  }
  const auto &roadId = GetAgent()->GetEgoAgent().GetRoadId();
  const auto &agentReferencePoint = GetAgent()->GetRoadPosition(ObjectPointPredefined::Reference);
  if (agentReferencePoint.count(roadId) == 0)
  {
    throw std::runtime_error("Could not calculate LaneChange trajectory. Reference curr is not on route.");
  }
  const auto currentLaneId = agentReferencePoint.at(roadId).laneId;
  const auto currentS = agentReferencePoint.at(roadId).roadPosition.s;
  const auto currentT = agentReferencePoint.at(roadId).roadPosition.t;
  units::length::meter_t deltaT = laneChange->target_lane_offset - currentT;
  if (laneChange->target_lane_id > currentLaneId)  //Change to left
  {
    deltaT += 0.5 * GetWorld()->GetLaneWidth(roadId, currentLaneId, currentS)
            + 0.5 * GetWorld()->GetLaneWidth(roadId, laneChange->target_lane_id, currentS);
    for (int laneId = currentLaneId + 1; laneId < laneChange->target_lane_id; ++laneId)
    {
      deltaT += GetWorld()->GetLaneWidth(roadId, laneId, currentS);
    }
  }
  else if (laneChange->target_lane_id < currentLaneId)  //Change to right
  {
    deltaT += -0.5 * GetWorld()->GetLaneWidth(roadId, currentLaneId, currentS)
            - 0.5 * GetWorld()->GetLaneWidth(roadId, laneChange->target_lane_id, currentS);
    for (int laneId = currentLaneId - 1; laneId > laneChange->target_lane_id; --laneId)
    {
      deltaT -= GetWorld()->GetLaneWidth(roadId, laneId, currentS);
    }
  }
  else
  {
    return mantle_api::PolyLine();
  }
  GlobalRoadPosition startPosition{
      roadId, currentLaneId, currentS, currentT, agentReferencePoint.at(roadId).roadPosition.hdg};
  auto startT = startPosition.roadPosition.t
              + (startPosition.laneId > 0 ? 0.5 : -0.5)
                    * world->GetLaneWidth(startPosition.roadId, startPosition.laneId, startPosition.roadPosition.s);

  if (startPosition.laneId > 0)
  {
    for (int laneId = 1; laneId < startPosition.laneId; ++laneId)
    {
      startT += GetWorld()->GetLaneWidth(startPosition.roadId, laneId, startPosition.roadPosition.s);
    }
  }
  else
  {
    for (int laneId = -1; laneId > startPosition.laneId; --laneId)
    {
      startT -= GetWorld()->GetLaneWidth(startPosition.roadId, laneId, startPosition.roadPosition.s);
    }
  }
  const auto dt = units::make_unit<units::time::millisecond_t>(GetCycleTime());
  mantle_api::PolyLine polyLine{};
  for (units::time::second_t timeSinceStart = 0_s; timeSinceStart <= deltaTime + 0.5 * dt; timeSinceStart += dt)
  {
    timeSinceStart
        = std::min(timeSinceStart, deltaTime);  //For reducing rounding errors we also consider an additional curr if
                                                //difference of last time to deltaTime was at most half a dt
    double alpha = timeSinceStart / deltaTime;  // Position on the sinus curve (0 = start, 1 = end)
    auto s = startPosition.roadPosition.s + alpha * deltaS;
    auto t = startT + deltaT * 0.5 * (1 - units::math::cos(alpha * 180_deg));
    auto yaw = deltaT / deltaS * 90_deg * units::math::sin(alpha * 180_deg);
    constexpr static auto NOT_IMPLEMENTED = 0.0_rad;
    auto worldPosition = GetWorld()->RoadCoord2WorldCoord({s, t, NOT_IMPLEMENTED}, startPosition.roadId);
    mantle_api::PolyLinePoint nextPoint{
        {{worldPosition->xPos, worldPosition->yPos, 0._m}, {worldPosition->yawAngle + yaw, 0._rad, 0._rad}},
        timeSinceStart};
    polyLine.push_back(nextPoint);
  }
  return polyLine;
}

DynamicsInformation DynamicsScenarioImplementation::ReadWayPointData()
{
  static constexpr units::time::second_t TIME_EPSILON{1_us};
  const auto dt = units::make_unit<units::time::millisecond_t>(GetCycleTime());
  auto info = dynamicsSignal.dynamicsInformation;

  for (auto iterCurr = trajectory.begin(); iterCurr != trajectory.end(); ++iterCurr)
  {
    if (iterCurr->time.value() >= timeLateralStrategy - TIME_EPSILON)
    {
      auto iterPrev = std::prev(iterCurr);
      if (iterPrev == trajectory.end())
      {
        throw std::runtime_error("Trajectory ends at first step or time not reached!");
      }
      auto &currTime = iterCurr->time.value();
      auto &prevTime = iterPrev->time.value();

      auto fraction = (timeLateralStrategy - prevTime) / (currTime - prevTime);

      auto &currPose = iterCurr->pose;
      auto &prevPose = iterPrev->pose;

      info.positionX = fraction * currPose.position.x + (1 - fraction) * prevPose.position.x;
      info.positionY = fraction * currPose.position.y + (1 - fraction) * prevPose.position.y;
      info.yaw = fraction * currPose.orientation.yaw + (1 - fraction) * prevPose.orientation.yaw;
      info.yawRate = (info.yaw - GetAgent()->GetYaw()) / dt;
      info.velocityX = (info.positionX - GetAgent()->GetPositionX()) / dt;
      info.velocityY = (info.positionY - GetAgent()->GetPositionY()) / dt;

      auto velocity = units::math::hypot(info.velocityX, info.velocityY);
      info.acceleration = (velocity - GetAgent()->GetVelocity().Length()) / dt;

      if ((iterCurr + 1) == trajectory.end() && timeLongitudinalStrategy + dt > currTime)
      {
        GetScenarioControl()->SetControlStrategyGoalReached(lateralStrategy);
      }
      return info;
    }
  }

  return info;
}
