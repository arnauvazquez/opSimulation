/********************************************************************************
 * Copyright (c) 2020-2021 ITK Engineering GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#ifndef DYNAMICS_COLLISION_IMPLEMENTATION_H
#define DYNAMICS_COLLISION_IMPLEMENTATION_H

#include <vector>

#include "common/componentPorts.h"
#include "common/postCrashDynamic.h"
#include "common/primitiveSignals.h"
#include "include/modelInterface.h"

/**
 * \addtogroup Components_PCM openPASS components pcm
 * @{
 * \addtogroup Dynamics_Collision
 * \brief Dynamic component to model the dynamic of collision.
 *
 * \details This module uses the #PostCrashDynamic data of the @cond \ref CollisionDetection_Impact module @endcond
 * and overwrites the agents data with the dynamic data of the collision.
 * Therefore this component has to have the lowest priority of all dynamic components in the agent equipment.
 * In this case this component is always scheduled last and can overwrite any other changes of the agent an other
 * dynamics component has made before.\n
 *
 * As soon as it receives a signal that a collision occured, it retrieves the latest #PostCrashDynamic data
 * by calling the agentInterface function #AgentInterface::GetCollisionData. Based on this data the new dynamic
 * is realized for this simulation step. The Realization of the #PostCrashDynamic is only made up once
 * as long as the collision lasts.
 *
 * After the collision is over this component is ready for another collision and #PostCrashDynamic to realize.
 *
 *
 * @} */

/*!
 * \copydoc Dynamics_Collision
 * \ingroup Dynamics_Collision
 */
class Dynamics_Collision_Implementation : public DynamicsInterface
{
public:
  /// Name of this component
  const std::string COMPONENTNAME = "Dynamics_Collision";

  //! Constructor
  //!
  //! @param[in]     componentName  Name of the component
  //! @param[in]     isInit         Corresponds to "init" of "Component"
  //! @param[in]     priority       Corresponds to "priority" of "Component"
  //! @param[in]     offsetTime     Corresponds to "offsetTime" of "Component"
  //! @param[in]     responseTime   Corresponds to "responseTime" of "Component"
  //! @param[in]     cycleTime      Corresponds to "cycleTime" of "Component"
  //! @param[in]     stochastics    Pointer to the stochastics class loaded by the framework
  //! @param[in]     world          Pointer to the world
  //! @param[in]     parameters     Pointer to the parameters of the module
  //! @param[in]     publisher      Pointer to the publisher instance
  //! @param[in]     callbacks      Pointer to the callbacks
  //! @param[in]     agent          Pointer to agent instance
  Dynamics_Collision_Implementation(std::string componentName,
                                    bool isInit,
                                    int priority,
                                    int offsetTime,
                                    int responseTime,
                                    int cycleTime,
                                    StochasticsInterface *stochastics,
                                    WorldInterface *world,
                                    const ParameterInterface *parameters,
                                    PublisherInterface *const publisher,
                                    const CallbackInterface *callbacks,
                                    AgentInterface *agent);
  ~Dynamics_Collision_Implementation() override = default;
  Dynamics_Collision_Implementation(const Dynamics_Collision_Implementation &) = delete;
  Dynamics_Collision_Implementation(Dynamics_Collision_Implementation &&) = delete;
  Dynamics_Collision_Implementation &operator=(const Dynamics_Collision_Implementation &) = delete;
  Dynamics_Collision_Implementation &operator=(Dynamics_Collision_Implementation &&) = delete;

  void UpdateInput(int localLinkId, const std::shared_ptr<SignalInterface const> &data, int time) override;
  void UpdateOutput(int localLinkId, std::shared_ptr<SignalInterface const> &data, int time) override;
  void Trigger(int time) override;

private:
  std::map<int, ComponentPort *> inputPorts;  //!< map for all InputPort
  /** \addtogroup Dynamics_Collision
   *  @{
   *      \name InputPorts
   *      All input ports with PortId
   *      @{
   */
  InputPort<BoolSignal, bool> collisionOccured{0, &inputPorts};  //!< current x-coordinate of agent
  /**
   *      @}
   *  @}
   */

  //! Specifies the current state of collision the agent is in
  enum class CollisionState
  {
    NOCOLLISION,
    COLLISIONIMPULS,
    COLLISION
  } collisionState
      = CollisionState::NOCOLLISION;

  //! current postCrashDynamic
  PostCrashDynamic *postCrashDynamic = nullptr;

  units::time::second_t cycleTimeSec;
};

#endif  // DYNAMICS_COLLISION_IMPLEMENTATION_H
