/*******************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <MantleAPI/Common/position.h>
#include <MantleAPI/Common/trajectory.h>
#include <google/protobuf/message.h>
#include <memory>
#include <osi3/osi_trafficcommand.pb.h>

#include "SignalMessageVisitor.h"
#include "include/agentInterface.h"
#include "include/callbackInterface.h"
#include "include/signalInterface.h"

/// @brief Structure representing a translator for all input signals
struct InputSignalTranslator
{
  WorldInterface& world;                       ///< Reference to the World interface
  AgentInterface& agent;                       ///< Reference to the agent interface
  const CallbackInterface& callbackInterface;  ///< Reference to the callback interface

  /// @brief Method to initialize a input signal translator
  /// @param world                Reference to the World interface
  /// @param agent                Reference to the agent interface
  /// @param callbackInterface    Reference to the callback interface
  InputSignalTranslator(WorldInterface& world, AgentInterface& agent, const CallbackInterface& callbackInterface);

  /// @brief Function to translate a signal message to a protobuf message
  /// @param message Signal message
  /// @return Translated protobuf message
  virtual const google::protobuf::Message* translate(std::shared_ptr<const SignalInterface>,
                                                     const google::protobuf::Message* const message)
      = 0;
};

/// @brief Structure representing a factory of translator for all input signals
struct InputSignalTranslatorFactory
{
  /// @brief Method to build an appropriate signal message for the given local link
  /// @param localLinkId          Local link id value
  /// @param world                Reference to the World interface
  /// @param agent                Reference to the agent interface
  /// @param callbackInterface    Reference to the callback interface
  /// @return A pointer to the corresponding input signal translator
  [[nodiscard]] static std::optional<std::shared_ptr<InputSignalTranslator>> build(
      int localLinkId, WorldInterface& world, AgentInterface& agent, const CallbackInterface& callbackInterface);
};

//! Struct representing a visitor for output signal
struct OutputSignalVisitor
{
  //! Constructor
  //! @param outputType         Type of signal that should be generated
  //! @param outputSignals      All output signals defined for the fmu
  //! @param fmuEnumerations    Enumerations defined for the FMU
  //! @param componentName      Name of the component
  //! @param componentState     State of the component
  //! @param agent              Pointer to the agent interface
  //! @param tempFMI            FMI version
  //! @param getFmuSignalValue  Function that creates FmuValue from value in an output signal and VariableType
  OutputSignalVisitor(const SignalType& outputType,
                      std::set<SignalType> outputSignals,
                      FmuEnumerations fmuEnumerations,
                      const std::string& componentName,
                      ComponentState componentState,
                      AgentInterface* agent,
                      size_t tempFMI,
                      std::function<FmuValue&(SignalValue, VariableType)> getFmuSignalValue)
      : outputType{outputType},
        outputSignals{outputSignals},
        fmuEnumerations{fmuEnumerations},
        componentName{componentName},
        componentState{componentState},
        agent{agent},
        tempFMI{tempFMI},
        getFmuSignalValue{getFmuSignalValue}
  {
  }

  //! @brief Call operator function to generate the output signal
  //!
  //! @param osiSource osi message as source for the signal (if any)
  //! @return pointer to the signal interface
  template <typename OsiMessageType>
  std::shared_ptr<const SignalInterface> operator()(const OsiMessageType& osiSource) const
  {
    if constexpr (std::is_same<OsiMessageType, std::monostate>())
    {
      switch (outputType)
      {
        case (SignalType::DynamicsSignal):
          return SignalMessage::parse<SignalType::DynamicsSignal>(
              outputSignals, componentName, componentState, tempFMI, getFmuSignalValue);
        case (SignalType::AccelerationSignal):
          return SignalMessage::parse<SignalType::AccelerationSignal>(
              outputSignals, componentName, componentState, tempFMI, getFmuSignalValue);
        case (SignalType::LongitudinalSignal):
          return SignalMessage::parse<SignalType::LongitudinalSignal>(
              outputSignals, componentName, componentState, tempFMI, getFmuSignalValue);
        case (SignalType::SteeringSignal):
          return SignalMessage::parse<SignalType::SteeringSignal>(
              outputSignals, componentName, componentState, tempFMI, getFmuSignalValue);
        case (SignalType::CompCtrlSignal):
          return SignalMessage::parse<SignalType::CompCtrlSignal>(
              outputSignals, componentName, componentState, tempFMI, fmuEnumerations, getFmuSignalValue);
        default:
          throw std::runtime_error("SignalTranslator: Unsupported SignalType "
                                   + std::to_string(static_cast<int>(outputType)));
      }
    }
    else
    {
      return SignalMessage::parse(*agent, componentName, outputType, osiSource);
    }
  }

  const SignalType outputType;         //!< Type of signal that should be generated
  std::set<SignalType> outputSignals;  //!< Container that contains a sorted set of output signal
  FmuEnumerations fmuEnumerations;     //!< Enumerations defined for the FMU
  const std::string& componentName;    //!< Name of the component
  ComponentState componentState;       //!< State of the component
  //! Function that creates FmuValue from value in an output signal and VariableType
  std::function<FmuValue&(SignalValue, VariableType)> getFmuSignalValue;
  AgentInterface* agent;  //!< Pointer to the agent interface
  size_t tempFMI;         //!< FMI version
};
