/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2019-2021 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Traffic/entity_properties.h>
#include <any>
#include <cmath>
#include <filesystem>
#include <fstream>
#include <map>
#include <memory>
#include <osi3/osi_groundtruth.pb.h>
#include <osi3/osi_sensordata.pb.h>
#include <osi3/osi_sensorview.pb.h>
#include <osi3/osi_sensorviewconfiguration.pb.h>
#include <osi3/osi_trafficcommand.pb.h>
#include <osi3/osi_trafficupdate.pb.h>
#include <regex>
#include <set>
#include <stdexcept>
#include <string>
#include <unordered_map>

#include "ChannelDefinitionParser.h"
#include "FmuCommunication.h"
#include "FmuHelper.h"
#include "MantleAPI/Common/position.h"
#include "MantleAPI/Common/trajectory.h"
#include "common/acquirePositionSignal.h"  //<src/...>
#include "common/agentCompToCompCtrlSignal.h"
#include "common/globalDefinitions.h"
#include "common/runtimeInformation.h"
#include "fmuFileHelper.h"
#include "include/agentInterface.h"
#include "include/fmuHandlerInterface.h"
#include "include/parameterInterface.h"
#include "include/stochasticsInterface.h"
#include "include/worldInterface.h"

class AgentInterface;
class CallbackInterface;
class ScenarioCommandInterface;

template <size_t FMI>
class FmuHandler : public FmuHandlerInterface  ///< Class representing a fmu handler
{
private:
  template <typename T>
  struct TypeContainer;

  const std::map<std::string, std::map<std::string, std::optional<std::string>&>> variableMapping{
      {"Input",
       {{"SensorView", sensorViewVariable},
        {"SensorViewConfig", sensorViewConfigVariable},
        {"SensorData", sensorDataInVariable},
        {"TrafficCommand", trafficCommandVariable}}},
      {"Output",
       {{"SensorData", sensorDataOutVariable},
        {"SensorViewConfigRequest", sensorViewConfigRequestVariable},
        {"TrafficUpdate", trafficUpdateVariable},
        {"HostVehicleData", hostVehicleDataVariable}}},
      {"Init", {{"GroundTruth", groundTruthVariable}}}};

  template <typename T>
  struct TypeContainer
  {
    using type = T;
    VariableType variableType;
  };

  /// regex to match parameter transformations (e.g. Parameter_Transform[ScenarioName>Id]_TargetVariable
  const std::regex transformationRegex{R"((\w+)\[(\w+>\w+)\]_(\w[\w\._]*))"};

  const std::set<std::string> parameterTransformations{"Transform", "TransformList"};

  /// regex to match parameter assignments (e.g. Parameter_AssignSpecial_TargetVariable
  const std::regex assignmentRegex{R"(AssignSpecial_(\w[\w\._]*))"};

  /*!
   * Retrieves the agent's mantle_api::VehicleProperties
   *
   * \returns Pointer to the agent's VehicleProperties
   *
   * \throws std::runtime_error if the agent is not a vehicle (and thus holds only the EntityProperty base class)
   */
  std::shared_ptr<const mantle_api::VehicleProperties> GetVehicleProperties() const
  {
    if (auto vehicleProperties
        = std::dynamic_pointer_cast<const mantle_api::VehicleProperties>(agent->GetVehicleModelParameters()))
    {
      return vehicleProperties;
    }

    throw std::runtime_error("Can't retrieve VehicleProperties, as agent isn't a vehicle)");
  }

  /*!
   * Retrieve an entity property
   *
   * Retrieves a property (which is of type string) from the agent's vehicle model parameters and converts the string to
   * the given type.
   *
   * \tparam T          Type to convert the property string to. Supports int and double.
   * \param  property   The name of the property to retrieve
   *
   * \returns The value of the requested property, convertet to type T
   *
   * \throws std::runtime_error if the requested property is not defined, value is out of range or value is not valid for the requested type T.
   */
  template <typename T>
  T ValueFromEntityProperty(const std::string& property) const
  {
    try
    {
      if constexpr (std::is_same_v<T, int>)
      {
        return std::stoi(agent->GetVehicleModelParameters()->properties.at(property));
      }
      else if constexpr (std::is_same_v<T, double>)
      {
        return std::stod(agent->GetVehicleModelParameters()->properties.at(property));
      }
    }
    catch (const std::out_of_range&)
    {
      throw std::runtime_error("Property '" + property
                               + "' not available in EntityProperties (or value out of range).");
    }
    catch (const std::invalid_argument&)
    {
      throw std::runtime_error("Invalid value for '" + property + "' EntityProperty.");
    }
  }

  /// TODO: storing with std::any works fine but poses potential problems when any-casting to
  /// const/volatile/reference/pointer mismatch
  const std::map<std::string, std::any> parameterTransformationMappings{
      {"ScenarioName>Id",
       static_cast<std::function<std::string(const std::string&)>>(
           [this](const std::string& agentScenarioName)
           {
             const auto agent = world->GetAgentByName(agentScenarioName);
             if (agent == nullptr) LOGERRORANDTHROW("Agent '" + agentScenarioName + "' not found in world.");
             return std::to_string(agent->GetId());
           })}};

  /// Return type of an AssignSpecial lambda function
  using AssignFunctionReturnType = std::variant<bool, int, std::uint32_t, double, std::string>;

  /// Function signature of an AssignSpecial lambda function without parameters
  using AssignmentFunctionNoParam = std::function<AssignFunctionReturnType()>;

  /// Function signature of an AssignSpecial lambda function with a string parameter
  using AssignmentFunctionParam = std::function<AssignFunctionReturnType(const std::string&)>;

  /// Variant of possible function signatures of AssignSpecial lambda functions
  using AssignmentFunction = std::variant<AssignmentFunctionNoParam, AssignmentFunctionParam>;

  /// Mapping of AssignSpecial parameter values to their corresponding calculation
  const std::map<std::string, AssignmentFunction> parameterAssignmentMappings{
      {"RandomSeed", [this]() { return stochastics->GetRandomSeed(); }},
      {"OutputPath", [this]() { return outputBaseDir.string(); }},
      {"MaxSteering", [this]() { return GetVehicleProperties()->front_axle.max_steering.template to<double>(); }},
      {"SteeringRatio", [this]() { return ValueFromEntityProperty<double>("SteeringRatio"); }},
      {"NumberOfGears", [this]() { return ValueFromEntityProperty<int>("NumberOfGears"); }},
      {"GearRatio",
       [this](const std::string& param)
       {
         try
         {
           return ValueFromEntityProperty<double>("GearRatio" + param);
         }
         catch (const std::runtime_error&)
         {
           return 0.0;
         }
       }}};

public:
  /// @brief Functio to construct FMU handler
  /// @param componentName        Name of the component
  /// @param cdata                FMU check data
  /// @param fmuVariables         FMU variables
  /// @param fmuVariableValues    Values of FMU variables
  /// @param parameters           Pointer to the parameter interface
  /// @param world                Pointer to the world interface
  /// @param agent                Pointer to the agent interface
  /// @param stochastics          Pointer to the stochastics interface
  /// @param callbacks            Pointer to the callback interface
  /// @param scenarioControl      scenarioControl of entity
  FmuHandler<FMI>(std::string componentName,
                  fmu_check_data_t& cdata,
                  FmuVariables& fmuVariables,
                  std::map<ValueReferenceAndType, FmuValue>& fmuVariableValues,
                  const ParameterInterface* parameters,
                  WorldInterface* world,
                  AgentInterface* agent,
                  StochasticsInterface* stochastics,
                  const CallbackInterface* callbacks,
                  std::shared_ptr<ScenarioControlInterface> const scenarioControl);

  ~FmuHandler() override;

  void PrepareInit() override;

  void Init() override;

  void PreStep(int time) override;
  void PostStep(int time) override;

  void UpdateInput(int localLinkId, const std::shared_ptr<SignalInterface const>& data, int time) override;
  void UpdateOutput(int localLinkId, std::shared_ptr<SignalInterface const>& data, int time) override;

  /*!
   * \brief Parses the channel definitions from FMU wrapper confi.
   * \param parameters    parameters defined in the config
   * \param fmuVariables  variables of the fmu
   *
   * Map from output link id to FMI value reference and type hash code
   */
  void ParseChannelDefinitions(const ParameterInterface* parameters, const FmuVariables& fmuVariables);

  ///! Sets the parameters in the FMU
  void SetFmuParameters();

  /// @brief Synchronize FMU variables and its paramters
  void SyncFmuVariablesAndParameters() override;

  ///! Public for testing
  ///! Reads an output value of the FMU
  ///! @param valueReferenc    value to the reference
  ///! @param variableType     Type of the variable
  ///! @return FMU value
  FmuValue& GetValue(int valueReferenc, VariableType variableType) const;

  /// @brief Function to read FMU variable values
  void ReadValues() override;

  /// @brief Function to write FMU variable values
  void WriteValues() override;

  /// @brief Function to set FMU values
  /// @param valueReference   value of a reference
  /// @param fmuValueIn       Input FMU value
  /// @param dataType         Type of the variable
  void SetFmuValue(int valueReference, FmuValue fmuValueIn, VariableType dataType) override;

  /// @brief Function to set list of FMU values
  /// @param valueReferences   list of values of a reference
  /// @param fmuValuesIn       list of Input FMU value
  /// @param dataType          Type of the variable
  void SetFmuValues(std::vector<int> valueReferences,
                    std::vector<FmuValue> fmuValuesIn,
                    VariableType dataType) override;

  /// @brief Function to get FMU values
  /// @param valueReference   value of a reference
  /// @param fmuValueOut      Input FMU value
  /// @param dataType         Type of the variable
  void GetFmuValue(int valueReference, FmuValue& fmuValueOut, VariableType dataType) override;

  /// @brief Function to get list of FMU values
  /// @param valueReferences   list of values of a reference
  /// @param fmuValuesOut      list of Input FMU value
  /// @param dataType          Type of the variable
  void GetFmuValues(std::vector<int> valueReferences,
                    std::vector<FmuValue>& fmuValuesOut,
                    VariableType dataType) override;

  /// @return Function to get FMU TypeDefinitions
  std::unordered_map<std::string, std::unordered_map<int, std::string>> GetFmuTypeDefinitions() override;

  /// @return Function to get FMU variables
  FmuVariables GetFmuVariables() override;

  /// @brief Function to handle FMI status
  /// @param status       Status of
  /// @param logPrefix    Log prefix
  void HandleFmiStatus(jm_status_enu_t status, std::string logPrefix) override;

  /// @brief Prepare FMU initialization
  /// @return status of FMI Library
  jm_status_enu_t PrepareFmuInit() override;

  /// @brief End handling of FMI library
  /// @return status of FMI Library
  jm_status_enu_t FmiEndHandling() override;

  /// @brief Simulate step of FMI library
  /// @param time Time of the simulation
  /// @return status of FMI Library
  jm_status_enu_t FmiSimulateStep(double time) override;

  /// @brief Prepare simulation for FMI
  /// @return status of FMI Library
  jm_status_enu_t FmiPrepSimulate() override;

private:
  void InitOsiVariables(const ParameterInterface* parameters);
  FmuValue& GetFmuSignalValue(SignalValue signalValue, VariableType variableType);

  bool IsDefinedAsParameter(int desiredValRef, VariableType desiredType);
  std::variant<FmuVariable1*, FmuVariable2*>* GetFmuVariable(int desiredValRef, VariableType desiredType);

  template <typename FmuTypeParameters, typename UnderlyingType>
  void AddParameter(const std::string& variableName,
                    const UnderlyingType& value,
                    FmuTypeParameters fmuTypeParameters,
                    const TypeContainer<UnderlyingType> typeContainer);

  void AddTransformationParameter(std::smatch transformationMatch,
                                  const std::string& value,
                                  FmuParameters<std::string>* fmuTypeParameters,
                                  const TypeContainer<std::string> typeContainer);

  void AddAssignmentParameter(std::smatch assignmentMatch,
                              const std::string& value,
                              const TypeContainer<std::string> typeContainer);

  template <typename GetFunction, typename TargetParameterField, typename UnderlyingType>
  void ParseFmuParametersByType(GetFunction getParametersType,
                                const ParameterInterface* parameterInterface,
                                TargetParameterField fmuTypeParameters,
                                TypeContainer<UnderlyingType> typeContainer);

  template <typename GetFunction>
  void ParseFmuParametersByType(GetFunction getParametersType,
                                const ParameterInterface* parameterInterface,
                                FmuParameters<std::string>* fmuTypeParameters,
                                TypeContainer<std::string> typeContainer);

  //! Reads the parameters in the profile that should be forwarded to the FMU
  void ParseFmuParameters(const ParameterInterface* parameters);

  //! Reads the stochastic parameters in the profile that should be forwarded to the FMU
  void ParseFmuParametersStochastic(const ParameterInterface* parameters);

  //! Sets the SensorView as input for the FMU
  void SetSensorViewInput(const osi3::SensorView& data);

  //! Sets the SensorData as input for the FMU
  void SetSensorDataInput(const osi3::SensorData& data);

  //! Reads the SensorData from the FMU
  void GetSensorData();

  //! Sets the SensorViewConfig as input for the FMU
  void SetSensorViewConfig();

  //! Sets the SensorViewConfigRequest
  void SetSensorViewConfigRequest();

  //! Sets the TrafficCommand as input for the FMU
  void SetTrafficCommandInput(const osi3::TrafficCommand& data);

  //! Sets the TrafficCommand as input for the FMU
  void SetHostVehicleDataInput(const osi3::HostVehicleData& data);

  //! Reads the TrafficUpdate from the FMU
  void GetTrafficUpdate();

  //! Reads the TrafficUpdate from the FMU
  void GetHostVehicleData();

  //! Sets GroundTruth
  void SetGroundTruth();

  //! adds TrafficAction
  void AddTrafficAction();

private:
  fmi_version_enu_t fmiVersionEnum;

  //! Mapping from FMI value reference and C++ type id to FmuWrapper value (union). Provided by FmuWrapper on
  //! construction.
  std::map<ValueReferenceAndType, FmuValue>& fmuVariableValues;

  FmuOutputs fmuOutputs;  //!< Mapping from component output link id to to FMI value reference and C++ type id
  std::set<SignalType> outputSignals;
  FmuInputs fmuRealInputs;
  FmuInputs fmuIntegerInputs;
  FmuInputs fmuBooleanInputs;
  FmuInputs fmuStringInputs;
  FmuParameters<int> fmuIntegerParameters;
  FmuParameters<double> fmuDoubleParameters;
  FmuParameters<bool> fmuBoolParameters;
  FmuParameters<std::string> fmuStringParameters;

  FmuEnumerations fmuEnumerations;

  std::vector<int> sensors;
  WorldInterface* world;
  const ParameterInterface* parameters;

  FmuVariables& fmuVariables;
  const std::string agentIdString;
  std::string fmuName;
  std::unordered_map<std::string, std::unordered_map<int, std::string>> fmuTypeDefinitions;

  std::string serializedSensorDataIn;
  std::string appendedSerializedSensorDataIn;
  std::string appendedSerializedSensorDataOut;
  std::string previousSerializedSensorDataIn;
  std::string serializedSensorView;
  std::string previousSerializedSensorView;
  std::string appendedSerializedSensorView;
  void* previousSensorDataOut{nullptr};
  osi3::SensorViewConfiguration sensorViewConfig;
  osi3::SensorViewConfiguration sensorViewConfigRequest;
  osi3::GroundTruth groundTruth;
  std::string serializedSensorViewConfig;
  std::string appendedSerializedSensorViewConfig;
  std::string serializedSensorViewConfigRequest;
  std::string appendedSerializedSensorViewConfigRequest;
  std::string previousSerializedSensorViewConfigRequest;
  osi3::SensorData sensorDataIn;
  osi3::SensorData sensorDataOut;
  std::string serializedGroundTruth;
  std::string appendedSerializedGroundTruth;
  std::string serializedTrafficCommand;
  std::string appendedSerializedTrafficCommand;
  std::string previousSerializedTrafficCommand;
  std::string serializedHostVehicleData;
  std::string appendedSerializedHostVehicleData;
  std::string previousSerializedHostVehicleData;
  osi3::TrafficUpdate trafficUpdate;
  std::string serializedTrafficUpdate;
  std::string appendedSerializedTrafficUpdate;
  void* previousTrafficUpdate{nullptr};
  void* previousHostVehicleData{nullptr};
  osi3::TrafficCommand trafficCommand;
  osi3::HostVehicleData hostVehicleData;

  std::filesystem::path outputBaseDir{};
  std::filesystem::path jsonOutputDir{};
  std::filesystem::path traceOutputDir{};
  std::string oldFileName{};

  std::map<std::string, FmuFileHelper::TraceEntry> fileToOutputTracesMap{};

  Common::Vector2d<units::length::meter_t> previousPosition{0.0_m, 0.0_m};
  std::ofstream traceOutputFile;

  units::length::meter_t bb_center_offset_x{
      0.0};  //!< Offset of bounding box center to agent reference point (rear axle)

  //Use existing methods in FmuHandler if possible, instead of using FmuCommunication directly
  std::shared_ptr<FmuCommunication<FMI>> fmuCommunications;
  bool fmuVariablesParsed{false};

  bool isInitialized{false};

  uint64_t lastTrafficCommandId{ScenarioCommand::ID_UNSET};

  std::optional<std::string> sensorViewVariable;
  std::optional<std::string> sensorViewConfigVariable;
  std::optional<std::string> sensorViewConfigRequestVariable;
  std::optional<std::string> sensorDataInVariable;
  std::optional<std::string> sensorDataOutVariable;
  std::optional<std::string> groundTruthVariable;
  std::optional<std::string> trafficCommandVariable;
  std::optional<std::string> trafficUpdateVariable;
  std::optional<std::string> hostVehicleDataVariable;

  bool writeSensorView{false};
  bool writeSensorViewConfig{false};
  bool writeSensorViewConfigRequest{false};
  bool writeSensorData{false};
  bool writeGroundTruth{false};
  bool writeTrafficCommand{false};
  bool writeTrafficUpdate{false};
  bool writeHostVehicleData{false};

  bool writeTraceSensorView{false};
  bool writeTraceSensorViewConfig{false};
  bool writeTraceSensorViewConfigRequest{false};
  bool writeTraceSensorData{false};
  bool writeTraceGroundTruth{false};
  bool writeTraceTrafficCommand{false};
  bool writeTraceTrafficUpdate{false};
  bool writeTraceHostVehicleData{false};

  //! check for double buffering of OSI messages allocated by FMU
  bool enforceDoubleBuffering{false};

  StochasticsInterface* stochastics;  //!< References the stochastics interface of the framework

  //! Pointer to the scenarioControl
  std::shared_ptr<ScenarioControlInterface> const scenarioControl;

  //! defines the type of output for each output link
  std::map<int, SignalType> linkMap = {{0, SignalType::DynamicsSignal},
                                       {1, SignalType::AccelerationSignal},
                                       {2, SignalType::LongitudinalSignal},
                                       {3, SignalType::SteeringSignal},
                                       {4, SignalType::CompCtrlSignal},
                                       {6, SignalType::SensorDataSignal}};

  //! defines the osi message used for calculating the output of each output link
  //! if std::monostate, output is calculated from fmi interface variables
  std::map<int, OsiSource> osiSourceMap = {{0, std::monostate{}},
                                           {1, std::monostate{}},
                                           {2, std::monostate{}},
                                           {3, std::monostate{}},
                                           {4, std::monostate{}},
                                           {6, std::monostate{}}};
};

#include "FmuHandler.tpp"
