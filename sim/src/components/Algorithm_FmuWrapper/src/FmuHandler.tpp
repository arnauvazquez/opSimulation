/********************************************************************************
 * Copyright (c) 2019-2021 in-tech GmbH
 *               2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <exception>
#include <stdexcept>
#include <variant>

#include "FmuCalculations.h"
#include "SignalInterface/SignalTranslator.h"
#include "common/agentCompToCompCtrlSignal.h"
#include "common/driverWarning.h"
#include "common/dynamicsSignal.h"
#include "components/Algorithm_FmuWrapper/src/ChannelDefinitionParser.h"
#include "framework/sampler.h"
#include "include/fmuHandlerInterface.h"
#include "include/scenarioControlInterface.h"
#include "include/stochasticsInterface.h"

template <size_t FMI>
FmuHandler<FMI>::FmuHandler(std::string componentName,
                            fmu_check_data_t& cdata,
                            FmuVariables& fmuVariables,
                            std::map<ValueReferenceAndType, FmuValue>& fmuVariableValues,
                            const ParameterInterface* parameters,
                            WorldInterface* world,
                            AgentInterface* agent,
                            StochasticsInterface* stochastics,
                            const CallbackInterface* callbacks,
                            std::shared_ptr<ScenarioControlInterface> const scenarioControl)
    : FmuHandlerInterface(componentName, cdata, agent, stochastics, callbacks),
      fmiVersionEnum{cdata.version},
      fmuVariables(fmuVariables),
      fmuVariableValues(fmuVariableValues),
      world(world),
      parameters(parameters),
      bb_center_offset_x(agent->GetVehicleModelParameters()->bounding_box.geometric_center.x),
      agentIdString(FmuFileHelper::CreateAgentIdString(agent->GetId())),
      stochastics(stochastics),
      scenarioControl(scenarioControl)
{
  fmuIntegerParameters.emplace<FMI>();
  fmuDoubleParameters.emplace<FMI>();
  fmuBoolParameters.emplace<FMI>();
  fmuStringParameters.emplace<FMI>();

  fmuCommunications = std::make_shared<FmuCommunication<FMI>>(
      componentName, cdata, fmuVariableValues, parameters, world, agent, callbacks);
}

template <size_t FMI>
std::unordered_map<std::string, std::unordered_map<int, std::string>> FmuHandler<FMI>::GetFmuTypeDefinitions()
{
  return fmuCommunications->GetFmuTypeDefinitions();
}

template <size_t FMI>
FmuVariables FmuHandler<FMI>::GetFmuVariables()
{
  return fmuCommunications->GetFmuVariables();
}

template <size_t FMI>
void FmuHandler<FMI>::InitOsiVariables(const ParameterInterface* parameters)
{
  if (cdata.FMUPath != nullptr)
  {
    std::filesystem::path fmuPath = cdata.FMUPath;
    fmuName = fmuPath.filename().replace_extension().string();
  }
  else
  {
    LOGWARN("FmuHandler: FMU cdata has no FMU path.");
  }

  for (const auto& [key, value] : parameters->GetParametersString())
  {
    const auto pos = key.find('_');
    const auto type = key.substr(0, pos);
    const auto variableName = key.substr(pos + 1);
    if (type == "Input" || type == "Output" || type == "Init")
    {
      const auto findResult = variableMapping.at(type).find(value);
      if (findResult != variableMapping.at(type).cend())
      {
        findResult->second = variableName;
      }
    }
  }

  auto writeSensorDataFlag = parameters->GetParametersBool().find("WriteJson_SensorData");
  if (writeSensorDataFlag != parameters->GetParametersBool().end())
  {
    writeSensorData = writeSensorDataFlag->second;
  }
  auto writeTraceSensorDataFlag = parameters->GetParametersBool().find("WriteTrace_SensorData");
  if (writeTraceSensorDataFlag != parameters->GetParametersBool().end())
  {
    writeTraceSensorData = writeTraceSensorDataFlag->second;
  }
  auto writeSensorViewFlag = parameters->GetParametersBool().find("WriteJson_SensorView");
  if (writeSensorViewFlag != parameters->GetParametersBool().end())
  {
    writeSensorView = writeSensorViewFlag->second;
  }
  auto writeTraceSensorViewFlag = parameters->GetParametersBool().find("WriteTrace_SensorView");
  if (writeTraceSensorViewFlag != parameters->GetParametersBool().end())
  {
    writeTraceSensorView = writeTraceSensorViewFlag->second;
  }
  auto writeSensorViewConfigFlag = parameters->GetParametersBool().find("WriteJson_SensorViewConfig");
  if (writeSensorViewConfigFlag != parameters->GetParametersBool().end())
  {
    writeSensorViewConfig = writeSensorViewConfigFlag->second;
  }
  auto writeTraceSensorViewConfigFlag = parameters->GetParametersBool().find("WriteTrace_SensorViewConfig");
  if (writeTraceSensorViewConfigFlag != parameters->GetParametersBool().end())
  {
    writeTraceSensorViewConfig = writeTraceSensorViewConfigFlag->second;
  }
  auto writeSensorViewConfigRequestFlag = parameters->GetParametersBool().find("WriteJson_SensorViewConfigRequest");
  if (writeSensorViewConfigRequestFlag != parameters->GetParametersBool().end())
  {
    writeSensorViewConfigRequest = writeSensorViewConfigRequestFlag->second;
  }
  auto writeTraceSensorViewConfigRequestFlag
      = parameters->GetParametersBool().find("WriteTrace_SensorViewConfigRequest");
  if (writeTraceSensorViewConfigRequestFlag != parameters->GetParametersBool().end())
  {
    writeTraceSensorViewConfigRequest = writeTraceSensorViewConfigRequestFlag->second;
  }
  auto writeGroundTruthFlag = parameters->GetParametersBool().find("WriteJson_GroundTruth");
  if (writeGroundTruthFlag != parameters->GetParametersBool().end())
  {
    writeGroundTruth = writeGroundTruthFlag->second;
  }
  auto writeTraceGroundTruthFlag = parameters->GetParametersBool().find("WriteTrace_GroundTruth");
  if (writeTraceGroundTruthFlag != parameters->GetParametersBool().end())
  {
    writeTraceGroundTruth = writeTraceGroundTruthFlag->second;
  }
  auto writeTrafficCommandFlag = parameters->GetParametersBool().find("WriteJson_TrafficCommand");
  if (writeTrafficCommandFlag != parameters->GetParametersBool().end())
  {
    writeTrafficCommand = writeTrafficCommandFlag->second;
  }
  auto writeTraceTrafficCommandFlag = parameters->GetParametersBool().find("WriteTrace_TrafficCommand");
  if (writeTraceTrafficCommandFlag != parameters->GetParametersBool().end())
  {
    writeTraceTrafficCommand = writeTraceTrafficCommandFlag->second;
  }
  auto writeTrafficUpdateFlag = parameters->GetParametersBool().find("WriteJson_TrafficUpdate");
  if (writeTrafficUpdateFlag != parameters->GetParametersBool().end())
  {
    writeTrafficUpdate = writeTrafficUpdateFlag->second;
  }
  auto writeTraceTrafficUpdateFlag = parameters->GetParametersBool().find("WriteTrace_TrafficUpdate");
  if (writeTraceTrafficUpdateFlag != parameters->GetParametersBool().end())
  {
    writeTraceTrafficUpdate = writeTraceTrafficUpdateFlag->second;
  }
  auto writeHostVehicleDataFlag = parameters->GetParametersBool().find("WriteJson_HostVehicleData");
  if (writeHostVehicleDataFlag != parameters->GetParametersBool().end())
  {
    writeHostVehicleData = writeTrafficUpdateFlag->second;
  }
  auto writeTraceHostVehicleDataFlag = parameters->GetParametersBool().find("WriteTrace_HostVehicleData");
  if (writeTraceHostVehicleDataFlag != parameters->GetParametersBool().end())
  {
    writeTraceHostVehicleData = writeTraceTrafficUpdateFlag->second;
  }

  bool writeJsonOutput = writeSensorData || writeSensorView || writeSensorViewConfig || writeSensorViewConfigRequest
                      || writeTrafficCommand || writeTrafficUpdate || writeGroundTruth || writeHostVehicleData;
  bool writeTraceOutput = writeTraceSensorData || writeTraceSensorView || writeTraceSensorViewConfig
                       || writeTraceSensorViewConfigRequest || writeTraceTrafficCommand || writeTraceTrafficUpdate
                       || writeTraceGroundTruth || writeTraceHostVehicleData;

  outputBaseDir
      = FmuFileHelper::CreateOutputDir(fmuName, agent->GetId(), parameters->GetRuntimeInformation().directories.output);

  if (writeJsonOutput)
  {
    jsonOutputDir = outputBaseDir / "JsonFiles";

    if (!std::filesystem::exists(jsonOutputDir))
    {
      std::filesystem::create_directories(jsonOutputDir);
    }
  }

  if (writeTraceOutput)
  {
    traceOutputDir = outputBaseDir / "BinaryTraceFiles";

    if (!std::filesystem::exists(traceOutputDir))
    {
      std::filesystem::create_directories(traceOutputDir);
    }
  }

  auto enforceDoubleBufferingFlag = parameters->GetParametersBool().find("EnforceDoubleBuffering");
  if (enforceDoubleBufferingFlag != parameters->GetParametersBool().end())
  {
    enforceDoubleBuffering = enforceDoubleBufferingFlag->second;
  }
}

template <size_t FMI>
FmuHandler<FMI>::~FmuHandler()
{
  FmuFileHelper::WriteTracesToFile(traceOutputDir, fileToOutputTracesMap);
}

template <size_t FMI>
void FmuHandler<FMI>::PrepareInit()
{
  this->fmuVariables = GetFmuVariables();
  this->fmuTypeDefinitions = GetFmuTypeDefinitions();

  const auto sensorList = parameters->GetParameterLists().find("SensorLinks");
  if (sensorList != parameters->GetParameterLists().cend())
  {
    for (const auto& sensorLink : sensorList->second)
    {
      if (sensorLink->GetParametersString().at("InputId") == "Camera")
      {
        sensors.push_back(sensorLink->GetParametersInt().at("SensorId"));
      }
    }
  }

  InitOsiVariables(parameters);
  ParseChannelDefinitions(parameters, fmuVariables);
  ParseFmuParameters(parameters);
}

template <size_t FMI>
void FmuHandler<FMI>::Init()
{
  if (groundTruthVariable.has_value())
  {
    SetGroundTruth();

    if (writeGroundTruth)
    {
      FmuFileHelper::WriteJson(groundTruth, "GroundTruth.json", this->jsonOutputDir);
    }
    if (writeTraceGroundTruth)
    {
      FmuHelper::AppendMessages(appendedSerializedGroundTruth, serializedGroundTruth);
      FmuFileHelper::WriteBinaryTrace(
          appendedSerializedGroundTruth, "GroundTruth", fmuName, 1000 / cycleTime, "gt", fileToOutputTracesMap);
    }
  }
  if (sensorViewConfigRequestVariable.has_value())
  {
    SetSensorViewConfigRequest();
    SetSensorViewConfig();

    if (writeSensorViewConfig)
    {
      FmuFileHelper::WriteJson(sensorViewConfig, "SensorViewConfig.json", this->jsonOutputDir);
    }
    if (writeTraceSensorViewConfig)
    {
      FmuHelper::AppendMessages(appendedSerializedSensorViewConfig, serializedSensorViewConfig);
      FmuFileHelper::WriteBinaryTrace(appendedSerializedSensorViewConfig,
                                      "SensorViewConfig",
                                      fmuName,
                                      1000 / cycleTime,
                                      "sv",
                                      fileToOutputTracesMap);
    }
    if (writeSensorViewConfigRequest)
    {
      FmuFileHelper::WriteJson(sensorViewConfigRequest, "SensorViewConfigRequest.json", this->jsonOutputDir);
    }
    if (writeTraceSensorViewConfigRequest)
    {
      FmuHelper::AppendMessages(appendedSerializedSensorViewConfigRequest, serializedSensorViewConfigRequest);
      FmuFileHelper::WriteBinaryTrace(appendedSerializedSensorViewConfigRequest,
                                      "SensorViewConfigRequest",
                                      fmuName,
                                      1000 / cycleTime,
                                      "sv",
                                      fileToOutputTracesMap);
    }
  }
  else
  {
    sensorViewConfig = FmuHelper::GenerateDefaultSensorViewConfiguration();
  }

  isInitialized = true;
}

template <size_t FMI>
void FmuHandler<FMI>::UpdateInput(int localLinkId,
                                  const std::shared_ptr<SignalInterface const>& data,
                                  [[maybe_unused]] int time)
{
  if (auto inputSignalTranslator = InputSignalTranslatorFactory::build(localLinkId, *world, *agent, *this->callbacks))
  {
    const google::protobuf::Message* message;
    if (localLinkId == 2)
    {
      message = inputSignalTranslator.value()->translate(data, nullptr);
      sensorDataIn = *dynamic_cast<const osi3::SensorData*>(message);
    }
  }

  if (sensorViewConfigRequestVariable.has_value())
  {
    SetSensorViewConfigRequest();

    if (previousSerializedSensorViewConfigRequest != serializedSensorViewConfigRequest)
    {
      SetSensorViewConfig();

      if (writeSensorViewConfig)
      {
        FmuFileHelper::WriteJson(
            sensorViewConfig, "SensorViewConfig-" + std::to_string(time) + ".json", this->jsonOutputDir);
      }
      if (writeSensorViewConfigRequest)
      {
        FmuFileHelper::WriteJson(
            sensorViewConfigRequest, "SensorViewConfigRequest-" + std::to_string(time) + ".json", this->jsonOutputDir);
      }
    }
  }
}

template <size_t FMI>
void FmuHandler<FMI>::UpdateOutput(int localLinkId, std::shared_ptr<const SignalInterface>& data, int time)
{
  Q_UNUSED(time)
  LOGDEBUG("UpdateOutput started");

  try
  {
    auto componentStateValueReference = std::get<FMI>(fmuOutputs).find(SignalValue::ComponentState);
    ComponentState componentState = ComponentState::Acting;

    if (componentStateValueReference != std::get<FMI>(fmuOutputs).end())
    {
      auto componentStates = fmuEnumerations.componentStates.find(
          GetFmuSignalValue(SignalValue::ComponentState, VariableType::Enum).intValue);
      if (componentStates == fmuEnumerations.componentStates.cend())
      {
        LOGERRORANDTHROW("Enumeration is not defined in FMU or enumeration value is invalid for output type ComponentState");
      }
      componentState = componentStates->second;
    }
    data = std::visit(OutputSignalVisitor(linkMap.at(localLinkId),
                                          outputSignals,
                                          fmuEnumerations,
                                          componentName,
                                          componentState,
                                          agent,
                                          FMI,
                                          [this](SignalValue signalValue, VariableType variableType) -> FmuValue&
                                          { return GetFmuSignalValue(signalValue, variableType); }),
                      osiSourceMap.at(localLinkId));
  }
  catch (const std::bad_alloc& e)
  {
    throw std::runtime_error("Could not instantiate output signal: " + std::string(e.what()));
  }

  LOGDEBUG("UpdateOutput finished");
}

template <size_t FMI>
void FmuHandler<FMI>::PreStep([[maybe_unused]] int time)
{
  LOGDEBUG("Start of PreStep");

  const auto& objectsInRange
      = agent->GetEgoAgent().GetObjectsInRange(0_m, units::length::meter_t(std::numeric_limits<double>::max()), 0);
  const auto* frontObject = objectsInRange.empty() ? nullptr : objectsInRange.at(0);
  const auto* frontfrontObject = (objectsInRange.size() < 2) ? nullptr : objectsInRange.at(1);
  auto sensorFusionInfo = FmuCalculations::CalculateSensorFusionInfo(sensorDataIn, world, agent);

  size_t i = 0;
  std::vector<int> realValueReferencesVec;
  std::vector<FmuValue> realDataVec;
  for (const auto& fmuInput : std::get<FMI>(fmuRealInputs))
  {
    double dataToAdd = 0;
    realValueReferencesVec.resize(i + 1);
    realValueReferencesVec[i] = fmuInput.valueReference;
    switch (fmuInput.type)
    {
      case FmuInputType::VelocityEgo:
        dataToAdd = agent->GetVelocity().Length().value();
        break;
      case FmuInputType::AccelerationEgo:
        dataToAdd = agent->GetAcceleration().Projection(agent->GetYaw()).value();
        break;
      case FmuInputType::CentripetalAccelerationEgo:
        dataToAdd = agent->GetCentripetalAcceleration().value();
        break;
      case FmuInputType::SteeringWheelEgo:
        dataToAdd = agent->GetSteeringWheelAngle().value();
        break;
      case FmuInputType::AccelerationPedalPositionEgo:
        dataToAdd = agent->GetEffAccelPedal();
        break;
      case FmuInputType::BrakePedalPositionEgo:
        dataToAdd = agent->GetEffBrakePedal();
        break;
      case FmuInputType::DistanceRefToFrontEdgeEgo:
        dataToAdd = units::unit_cast<double>(agent->GetDistanceReferencePointToLeadingEdge());
        break;
      case FmuInputType::PositionXEgo:
        dataToAdd = units::unit_cast<double>(agent->GetPositionX());
        break;
      case FmuInputType::PositionYEgo:
        dataToAdd = units::unit_cast<double>(agent->GetPositionY());
        break;
      case FmuInputType::YawEgo:
        dataToAdd = units::unit_cast<double>(agent->GetYaw());
        break;
      case FmuInputType::PositionSEgo:
        dataToAdd
            = agent->GetEgoAgent().GetReferencePointPosition().value_or(GlobalRoadPosition{}).roadPosition.s.value();
        break;
      case FmuInputType::PositionTEgo:
        dataToAdd
            = agent->GetEgoAgent().GetReferencePointPosition().value_or(GlobalRoadPosition{}).roadPosition.t.value();
        break;
      case FmuInputType::PositionXFront:
        dataToAdd = frontObject ? frontObject->GetPositionX().value() : 0.0;
        break;
      case FmuInputType::PositionYFront:
        dataToAdd = frontObject ? frontObject->GetPositionY().value() : 0.0;
        break;
      case FmuInputType::YawFront:
        dataToAdd = frontObject ? frontObject->GetYaw().value() : 0.0;
        break;
      case FmuInputType::PositionSFront:
        dataToAdd = frontObject ? frontObject->GetRoadPosition(ObjectPointPredefined::Reference)
                                      .at(agent->GetEgoAgent().GetRoadId())
                                      .roadPosition.s.value()
                                : 0.0;
        break;
      case FmuInputType::PositionTFront:
        dataToAdd = frontObject ? frontObject->GetRoadPosition(ObjectPointPredefined::Reference)
                                      .at(agent->GetEgoAgent().GetRoadId())
                                      .roadPosition.t.value()
                                : 0.0;
        break;
      case FmuInputType::RelativeDistanceFront:
        dataToAdd = frontObject ? agent->GetEgoAgent().GetNetDistance(frontObject).value().value() : 0.0;
        break;
      case FmuInputType::WidthFront:
        dataToAdd = frontObject ? units::unit_cast<double>(frontObject->GetWidth()) : 0.0;
        break;
      case FmuInputType::LengthFront:
        dataToAdd = frontObject ? units::unit_cast<double>(frontObject->GetLength()) : 0.0;
        break;
      case FmuInputType::DistanceRefToFrontEdgeFront:
        dataToAdd = frontObject ? units::unit_cast<double>(frontObject->GetDistanceReferencePointToLeadingEdge()) : 0.0;
        break;
      case FmuInputType::VelocityFront:
        dataToAdd = frontObject ? frontObject->GetVelocity().Length().value() : 0.0;
        break;
      case FmuInputType::PositionXFrontFront:
        dataToAdd = frontfrontObject ? units::unit_cast<double>(frontfrontObject->GetPositionX()) : 0.0;
        break;
      case FmuInputType::PositionYFrontFront:
        dataToAdd = frontfrontObject ? units::unit_cast<double>(frontfrontObject->GetPositionY()) : 0.0;
        break;
      case FmuInputType::VelocityFrontFront:
        dataToAdd = frontfrontObject ? frontfrontObject->GetVelocity().Length().value() : 0.0;
        break;
      case FmuInputType::RelativeDistanceFrontFront:
        dataToAdd = frontfrontObject ? agent->GetEgoAgent().GetNetDistance(frontfrontObject).value().value() : 0.0;
        break;
      case FmuInputType::SpeedLimit:
        dataToAdd = FmuCalculations::CalculateSpeedLimit(std::get<double>(fmuInput.additionalParameter), agent);
        break;
      case FmuInputType::RoadCurvature:
        dataToAdd = units::unit_cast<double>(
            agent->GetEgoAgent()
                .GetLaneCurvature(units::length::meter_t(std::get<double>(fmuInput.additionalParameter)))
                .value_or(0.0_i_m));
        break;
      case FmuInputType::SensorFusionRelativeS:
        dataToAdd = sensorFusionInfo.size() <= std::get<size_t>(fmuInput.additionalParameter)
                      ? 0
                      : sensorFusionInfo[std::get<size_t>(fmuInput.additionalParameter)].s.value();
        break;
      case FmuInputType::SensorFusionRelativeNetS:
        dataToAdd = sensorFusionInfo.size() <= std::get<size_t>(fmuInput.additionalParameter)
                      ? 0
                      : sensorFusionInfo[std::get<size_t>(fmuInput.additionalParameter)].net_s.value();
        break;
      case FmuInputType::SensorFusionRelativeT:
        dataToAdd = sensorFusionInfo.size() <= std::get<size_t>(fmuInput.additionalParameter)
                      ? 0
                      : sensorFusionInfo[std::get<size_t>(fmuInput.additionalParameter)].t.value();
        break;
      case FmuInputType::SensorFusionRelativeX:
        dataToAdd = sensorFusionInfo.size() <= std::get<size_t>(fmuInput.additionalParameter)
                      ? 0
                      : sensorFusionInfo[std::get<size_t>(fmuInput.additionalParameter)].rel_x.value();
        break;
      case FmuInputType::SensorFusionRelativeY:
        dataToAdd = sensorFusionInfo.size() <= std::get<size_t>(fmuInput.additionalParameter)
                      ? 0
                      : sensorFusionInfo[std::get<size_t>(fmuInput.additionalParameter)].rel_y.value();
        break;
      case FmuInputType::SensorFusionRelativeNetLeft:
        dataToAdd = sensorFusionInfo.size() <= std::get<size_t>(fmuInput.additionalParameter)
                      ? 0
                      : sensorFusionInfo[std::get<size_t>(fmuInput.additionalParameter)].t_left.value();
        break;
      case FmuInputType::SensorFusionRelativeNetRight:
        dataToAdd = sensorFusionInfo.size() <= std::get<size_t>(fmuInput.additionalParameter)
                      ? 0
                      : sensorFusionInfo[std::get<size_t>(fmuInput.additionalParameter)].t_right.value();
        break;
      case FmuInputType::SensorFusionRelativeNetX:
        dataToAdd = sensorFusionInfo.size() <= std::get<size_t>(fmuInput.additionalParameter)
                      ? 0
                      : sensorFusionInfo[std::get<size_t>(fmuInput.additionalParameter)].net_x.value();
        break;
      case FmuInputType::SensorFusionRelativeNetY:
        dataToAdd = sensorFusionInfo.size() <= std::get<size_t>(fmuInput.additionalParameter)
                      ? 0
                      : sensorFusionInfo[std::get<size_t>(fmuInput.additionalParameter)].net_y.value();
        break;
      case FmuInputType::SensorFusionVelocity:
        dataToAdd = sensorFusionInfo.size() <= std::get<size_t>(fmuInput.additionalParameter)
                      ? 0
                      : sensorFusionInfo[std::get<size_t>(fmuInput.additionalParameter)].velocity.value();
        break;
      case FmuInputType::SensorFusionVelocityX:
        dataToAdd = sensorFusionInfo.size() <= std::get<size_t>(fmuInput.additionalParameter)
                      ? 0
                      : sensorFusionInfo[std::get<size_t>(fmuInput.additionalParameter)].velocity_x.value();
        break;
      case FmuInputType::SensorFusionVelocityY:
        dataToAdd = sensorFusionInfo.size() <= std::get<size_t>(fmuInput.additionalParameter)
                      ? 0
                      : sensorFusionInfo[std::get<size_t>(fmuInput.additionalParameter)].velocity_y.value();
        break;
      case FmuInputType::SensorFusionYaw:
        dataToAdd = sensorFusionInfo.size() <= std::get<size_t>(fmuInput.additionalParameter)
                      ? 0
                      : sensorFusionInfo[std::get<size_t>(fmuInput.additionalParameter)].yaw.value();
        break;
      default:
        LOGERRORANDTHROW("Input can not be mapped to variable of type Real");
    }
    realDataVec.resize(i + 1);
    FmuValue fmuValue;
    fmuValue.realValue = dataToAdd;
    realDataVec[i] = fmuValue;
    ++i;
  }

  std::vector<int> integerValueReferencesVec;
  std::vector<FmuValue> integerDataVec;
  i = 0;
  for (const auto& fmuInput : std::get<FMI>(fmuIntegerInputs))
  {
    int dataToAdd = 0;
    integerValueReferencesVec.resize(i + 1);
    integerValueReferencesVec[i] = fmuInput.valueReference;
    switch (fmuInput.type)
    {
      case FmuInputType::LaneEgo:
        dataToAdd
            = agent->GetEgoAgent().HasValidRoute() ? agent->GetEgoAgent().GetMainLocatePosition().value().laneId : 0;
        break;
      case FmuInputType::LaneFront:
        dataToAdd = frontObject ? frontObject->GetRoadPosition(ObjectPointPredefined::Reference)
                                      .at(agent->GetEgoAgent().GetRoadId())
                                      .laneId
                                : 0.0;
        break;
      case FmuInputType::LaneFrontFront:
        dataToAdd = frontfrontObject ? frontfrontObject->GetRoadPosition(ObjectPointPredefined::Reference)
                                           .at(agent->GetEgoAgent().GetRoadId())
                                           .laneId
                                     : 0.0;
        break;
      case FmuInputType::LaneCountLeft:
        dataToAdd = FmuCalculations::CalculateLaneCount(Side::Left, agent);
        break;
      case FmuInputType::LaneCountRight:
        dataToAdd = FmuCalculations::CalculateLaneCount(Side::Right, agent);
        break;
      case FmuInputType::SensorFusionObjectId:
        dataToAdd = sensorFusionInfo.size() <= std::get<size_t>(fmuInput.additionalParameter)
                      ? -1
                      : sensorFusionInfo[std::get<size_t>(fmuInput.additionalParameter)].id;
        break;
      case FmuInputType::SensorFusionNumberOfDetectingSensors:
        dataToAdd = sensorFusionInfo.size() <= std::get<size_t>(fmuInput.additionalParameter)
                      ? -1
                      : sensorFusionInfo[std::get<size_t>(fmuInput.additionalParameter)].numberOfSensors;
        break;
      case FmuInputType::SensorFusionLane:
        dataToAdd = sensorFusionInfo.size() <= std::get<size_t>(fmuInput.additionalParameter)
                      ? 0
                      : sensorFusionInfo[std::get<size_t>(fmuInput.additionalParameter)].lane;
        break;
      default:
        LOGERRORANDTHROW("Input can not be mapped to variable of type Integer");
    }
    integerDataVec.resize(i + 1);
    FmuValue fmuValue;
    fmuValue.intValue = dataToAdd;
    integerDataVec[i] = fmuValue;
    ++i;
  }

  std::vector<int> booleanValueReferencesVec;
  std::vector<FmuValue> booleanDataVec;
  i = 0;
  for (const auto& fmuInput : std::get<FMI>(fmuBooleanInputs))
  {
    int dataToAdd = 0;
    booleanValueReferencesVec.resize(i + 1);
    booleanValueReferencesVec[i] = fmuInput.valueReference;
    switch (fmuInput.type)
    {
      case FmuInputType::ExistenceFront:
        dataToAdd = static_cast<bool>(frontObject);
        break;
      case FmuInputType::ExistenceFrontFront:
        dataToAdd = static_cast<bool>(frontfrontObject);
        break;
      default:
        LOGERRORANDTHROW("Only ExistenceFront and ExistenceFrontFront can be mapped to variable of type Boolean");
    }
    booleanDataVec.resize(i + 1);
    FmuValue fmuValue;
    fmuValue.boolValue = dataToAdd;
    booleanDataVec[i] = fmuValue;
    ++i;
  }
  SetFmuValues(realValueReferencesVec, realDataVec, VariableType::Double);
  SetFmuValues(integerValueReferencesVec, integerDataVec, VariableType::Int);
  SetFmuValues(booleanValueReferencesVec, booleanDataVec, VariableType::Bool);

  if (sensorViewVariable)
  {
    auto worldData = static_cast<OWL::Interfaces::WorldData*>(world->GetWorldData());
    auto sensorView = worldData->GetSensorView(sensorViewConfig, agent->GetId(), time);

    SetSensorViewInput(*sensorView);
    if (writeSensorView)
    {
      FmuFileHelper::WriteJson(*sensorView, "SensorView-" + std::to_string(time) + ".json", this->jsonOutputDir);
    }
    if (writeTraceSensorView)
    {
      FmuHelper::AppendMessages(appendedSerializedSensorView, serializedSensorView);
      FmuFileHelper::WriteBinaryTrace(
          appendedSerializedSensorView, "SensorView", fmuName, time, "sv", fileToOutputTracesMap);
    }
  }
  if (sensorDataInVariable)
  {
    SetSensorDataInput(sensorDataIn);
    if (writeSensorData)
    {
      FmuFileHelper::WriteJson(sensorDataIn, "SensorDataIn-" + std::to_string(time) + ".json", this->jsonOutputDir);
    }
    if (writeTraceSensorData)
    {
      FmuHelper::AppendMessages(appendedSerializedSensorDataIn, serializedSensorDataIn);
      FmuFileHelper::WriteBinaryTrace(
          appendedSerializedSensorDataIn, "SensorData", fmuName, time, "sd", fileToOutputTracesMap);
    }
  }
  if (trafficCommandVariable)
  {
    trafficCommand.Clear();
    osi3::utils::SetTimestamp(trafficCommand, time);
    osi3::utils::SetVersion(trafficCommand);
    AddTrafficAction();
    for (const auto customCommand : scenarioControl->GetCustomCommands())
    {
      if (!customCommand.empty())
      {
        trafficCommand.add_action()->mutable_custom_action()->set_command(customCommand);
      }
    }
    for (auto strategy : scenarioControl->GetStrategies(mantle_api::ControlStrategyType::kFollowTrajectory))
    {
      auto followTrajectoryStrategy = std::dynamic_pointer_cast<mantle_api::FollowTrajectoryControlStrategy>(strategy);
      if (followTrajectoryStrategy != nullptr)
      {
        mantle_api::Trajectory trajectory = followTrajectoryStrategy->trajectory;
        FmuHelper::AddTrafficCommandActionFromOpenScenarioTrajectory(trafficCommand.add_action(), trajectory);
      }
    }
    if (scenarioControl->HasNewLongitudinalStrategy())
    {
      for (auto strategy : scenarioControl->GetStrategies(mantle_api::ControlStrategyType::kFollowVelocitySpline))
      {
        auto followVelocitySplineStrategy
            = std::dynamic_pointer_cast<mantle_api::FollowVelocitySplineControlStrategy>(strategy);
        if (followVelocitySplineStrategy != nullptr)
        {
          auto& splineSection = followVelocitySplineStrategy->velocity_splines.back();
          auto lastTime = splineSection.end_time;
          units::velocity::meters_per_second_t targetSpeed
              = std::get<0>(splineSection.polynomial) * lastTime * lastTime * lastTime
              + std::get<1>(splineSection.polynomial) * lastTime * lastTime
              + std::get<2>(splineSection.polynomial) * lastTime + std::get<3>(splineSection.polynomial);
          trafficCommand.add_action()->mutable_speed_action()->set_absolute_target_speed(
              units::unit_cast<double>(targetSpeed));
        }
      }
    }

    SetTrafficCommandInput(trafficCommand);
    if (writeTrafficCommand)
    {
      FmuFileHelper::WriteJson(trafficCommand, "TrafficCommand-" + std::to_string(time) + ".json", this->jsonOutputDir);
    }
    if (writeTraceTrafficCommand)
    {
      FmuHelper::AppendMessages(appendedSerializedTrafficCommand, serializedTrafficCommand);
      FmuFileHelper::WriteBinaryTrace(
          appendedSerializedTrafficCommand, "TrafficCommand", fmuName, time, "tc", fileToOutputTracesMap);
    }
  }
  if (hostVehicleDataVariable)
  {
    SetHostVehicleDataInput(hostVehicleData);
    if (writeHostVehicleData)
    {
      FmuFileHelper::WriteJson(
          hostVehicleData, "HostVehicleData-" + std::to_string(time) + ".json", this->jsonOutputDir);
    }
    if (writeTraceHostVehicleData)
    {
      FmuHelper::AppendMessages(appendedSerializedHostVehicleData, serializedHostVehicleData);
      FmuFileHelper::WriteBinaryTrace(
          appendedSerializedHostVehicleData, "HostVehicleData", fmuName, time, "tc", fileToOutputTracesMap);
    }
  }

  LOGDEBUG("End of PreStep");
}

template <size_t FMI>
void FmuHandler<FMI>::PostStep([[maybe_unused]] int time)
{
  LOGDEBUG("Start of PostStep");

  if (sensorDataOutVariable)
  {
    GetSensorData();
    if (writeSensorData)
    {
      FmuFileHelper::WriteJson(sensorDataOut, "SensorDataOut-" + std::to_string(time) + ".json", this->jsonOutputDir);
    }
    if (writeTraceSensorData)
    {
      int valueReference;
      valueReference = std::get<FMI>(fmuVariables).at(sensorDataOutVariable.value() + ".size").valueReference;
      //valueReference.emplace<FMI>((std::get<FMI>(fmuVariables).at(sensorDataOutVariable.value()+".size").valueReference));
      std::string serializedSensorDataOut{
          static_cast<const char*>(previousSensorDataOut),
          static_cast<std::string::size_type>(GetValue(valueReference, VariableType::Int).intValue)};
      FmuHelper::AppendMessages(appendedSerializedSensorDataOut, serializedSensorDataOut);
      FmuFileHelper::WriteBinaryTrace(
          appendedSerializedSensorDataOut, "SensorData", fmuName, time, "sd", fileToOutputTracesMap);
    }
  }
  if (trafficUpdateVariable)
  {
    GetTrafficUpdate();
    if (writeTrafficUpdate)
    {
      FmuFileHelper::WriteJson(trafficUpdate, "TrafficUpdate-" + std::to_string(time) + ".json", this->jsonOutputDir);
    }
    if (writeTraceTrafficUpdate)
    {
      FmuHelper::AppendMessages(appendedSerializedTrafficUpdate, serializedTrafficUpdate);
      FmuFileHelper::WriteBinaryTrace(
          appendedSerializedTrafficUpdate, "TrafficUpdate", fmuName, time, "tu", fileToOutputTracesMap);
    }
  }
  if (hostVehicleDataVariable)
  {
    GetHostVehicleData();
    if (writeHostVehicleData)
    {
      FmuFileHelper::WriteJson(
          hostVehicleData, "HostVehicleData-" + std::to_string(time) + ".json", this->jsonOutputDir);
    }
    if (writeTraceHostVehicleData)
    {
      FmuHelper::AppendMessages(appendedSerializedHostVehicleData, serializedHostVehicleData);
      FmuFileHelper::WriteBinaryTrace(
          appendedSerializedHostVehicleData, "HostVehicleData", fmuName, time, "hv", fileToOutputTracesMap);
    }
  }

  LOGDEBUG("End of PostStep");
}

template <size_t FMI>
void FmuHandler<FMI>::ParseChannelDefinitions(const ParameterInterface* parameters, const FmuVariables& fmuVariables)
{
  ChannelDefinitionParser<FMI> channelDefinitionParser(fmuVariables, cdata.version, callbacks);

  for (const auto& [paramKey, paramValue] : parameters->GetParametersString())
  {
    const auto pos = paramKey.find('_');
    const auto channelCausality = paramKey.substr(0, pos);
    const auto channelType = paramKey.substr(pos + 1);

    if (channelCausality == "Output"
        && variableMapping.at(channelCausality).find(paramValue) == variableMapping.at(channelCausality).cend())
    {
      channelDefinitionParser.AddOutputChannel(paramValue, channelType, fmuTypeDefinitions);
    }
    else if (channelCausality == "Input"
             && variableMapping.at(channelCausality).find(paramValue) == variableMapping.at(channelCausality).cend())
    {
      channelDefinitionParser.AddInputChannel(paramValue, channelType);
    }
    else if (channelCausality == "Parameter"
             && (channelType.find('[') == std::string::npos)      // skip Transform[x] style parameters
             && (channelType.substr(0, 14) != "AssignSpecial_"))  // skip AssignSpecial style parameters
    {
      channelDefinitionParser.AddParameter(paramValue, channelType);
    }
  }

  for (const auto& [paramKey, paramValue] : parameters->GetParametersInt())
  {
    const auto pos = paramKey.find('_');
    const auto channelCausality = paramKey.substr(0, pos);
    const auto channelType = paramKey.substr(pos + 1);

    if (channelCausality == "Parameter")
    {
      channelDefinitionParser.AddParameter(paramValue, channelType);
    }
  }

  for (const auto& [paramKey, paramValue] : parameters->GetParametersDouble())
  {
    const auto pos = paramKey.find('_');
    const auto channelCausality = paramKey.substr(0, pos);
    const auto channelType = paramKey.substr(pos + 1);

    if (channelCausality == "Parameter")
    {
      channelDefinitionParser.AddParameter(paramValue, channelType);
    }
  }

  for (const auto& [paramKey, paramValue] : parameters->GetParametersBool())
  {
    const auto pos = paramKey.find('_');
    const auto channelCausality = paramKey.substr(0, pos);
    const auto channelType = paramKey.substr(pos + 1);

    if (channelCausality == "Parameter")
    {
      channelDefinitionParser.AddParameter(paramValue, channelType);
    }
  }

  channelDefinitionParser.ParseOutputSignalTypes();

  fmuOutputs = channelDefinitionParser.GetFmuOutputs();
  outputSignals = channelDefinitionParser.GetOutputSignals();
  fmuRealInputs = channelDefinitionParser.GetFmuRealInputs();
  fmuIntegerInputs = channelDefinitionParser.GetFmuIntegerInputs();
  fmuBooleanInputs = channelDefinitionParser.GetFmuBooleanInputs();
  fmuStringInputs = channelDefinitionParser.GetFmuStringInputs();
  fmuStringParameters = channelDefinitionParser.GetFmuStringParameters();
  fmuIntegerParameters = channelDefinitionParser.GetFmuIntegerParameters();
  fmuDoubleParameters = channelDefinitionParser.GetFmuDoubleParameters();
  fmuBoolParameters = channelDefinitionParser.GetFmuBoolParameters();
  fmuEnumerations = channelDefinitionParser.GetFmuEnumerations();
}

template <size_t FMI>
void FmuHandler<FMI>::ParseFmuParameters(const ParameterInterface* parameters)
{
  ParseFmuParametersByType(
      &ParameterInterface::GetParametersInt, parameters, &fmuIntegerParameters, TypeContainer<int>{VariableType::Int});
  ParseFmuParametersByType(
      &ParameterInterface::GetParametersBool, parameters, &fmuBoolParameters, TypeContainer<bool>{VariableType::Bool});
  ParseFmuParametersByType(&ParameterInterface::GetParametersDouble,
                           parameters,
                           &fmuDoubleParameters,
                           TypeContainer<double>{VariableType::Double});
  ParseFmuParametersByType(&ParameterInterface::GetParametersString,
                           parameters,
                           &fmuStringParameters,
                           TypeContainer<std::string>{VariableType::String});
  ParseFmuParametersStochastic(parameters);
}

template <size_t FMI>
void FmuHandler<FMI>::ParseFmuParametersStochastic(const ParameterInterface* parameters)
{
  for (const auto& [paramKey, paramValue] : parameters->GetParametersStochastic())
  {
    const auto pos = paramKey.find('_');

    if (pos == std::string::npos)
    {
      continue;
    }

    const auto channelCausality = paramKey.substr(0, pos);
    const auto channelType = paramKey.substr(pos + 1);

    if (channelCausality == "Parameter")
    {
      auto unmappedVariableItem = std::get<FMI>(fmuVariables).find(channelType);
      if (unmappedVariableItem == std::get<FMI>(fmuVariables).cend())
        LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString) + "No variable with name \"" + channelType
                         + "\" found in the FMU");
      if (unmappedVariableItem->second.variableType != VariableType::Double)
        LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString) + "Variable \"" + channelType
                         + "\" has different type in FMU");
      const auto& [valueReference, variableType, causality, variablility] = unmappedVariableItem->second;
      auto sampledValue = Sampler::RollForStochasticAttribute(paramValue, stochastics);
      std::get<FMI>(fmuDoubleParameters).emplace_back(sampledValue, valueReference);
    }
  }
}

template <size_t FMI>
template <typename FmuTypeParameters, typename UnderlyingType>
void FmuHandler<FMI>::AddParameter(const std::string& variableName,
                                   const UnderlyingType& value,
                                   FmuTypeParameters fmuTypeParameters,
                                   const TypeContainer<UnderlyingType> typeContainer)
{
  const auto foundVariable = std::get<FMI>(fmuVariables).find(variableName);

  if (foundVariable == std::get<FMI>(fmuVariables).cend())
    LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString) + "No variable with name \"" + variableName
                     + "\" found in the FMU");
  if (foundVariable->second.variableType != typeContainer.variableType)
    LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString) + "Variable \"" + variableName
                     + "\" has different type in FMU");

  std::get<FMI>(*fmuTypeParameters).emplace_back(value, foundVariable->second.valueReference);
}

template <size_t FMI>
void FmuHandler<FMI>::AddTransformationParameter(std::smatch transformationMatch,
                                                 const std::string& value,
                                                 FmuParameters<std::string>* fmuTypeParameters,
                                                 const TypeContainer<std::string> typeContainer)
{
  const std::string transformationType = transformationMatch[1];
  if (parameterTransformations.find(transformationType) == end(parameterTransformations))
    LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString) + "Unknown transformation type: " + transformationType);
  if (transformationType == "TransformList" && typeContainer.variableType != VariableType::String)
    LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString) + "'TransformList' is only applicable to strings");
  const std::string transformationRule = transformationMatch[2];
  auto mappingEntry = parameterTransformationMappings.find(transformationRule);
  if (mappingEntry == end(parameterTransformationMappings))
    LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString) + "Unknown mapping rule: " + transformationRule);
  std::string variableName = transformationMatch[3];
  std::string transformValue;

  auto mapping = std::any_cast<std::function<std::string(const std::string&)>>(mappingEntry->second);
  if (transformationType == "Transform")
  {
    transformValue = mapping(value);
  }
  if (transformationType == "TransformList")
  {
    std::string s = std::string(value);
    size_t pos = 0;
    std::string token;
    std::vector<std::string> transformedList;
    while ((pos = s.find(',')) != std::string::npos)
    {
      auto mappedValue = mapping(s.substr(0, pos));
      transformedList.emplace_back(mappedValue);
      s.erase(0, pos + 1);
    }
    transformedList.emplace_back(mapping(s));
    transformValue
        = std::accumulate(++cbegin(transformedList),
                          cend(transformedList),
                          *cbegin(transformedList),
                          [](const std::string& left, const std::string& right) { return left + "," + right; });
  }
  AddParameter(variableName, transformValue, fmuTypeParameters, typeContainer);
}

template <class... Ts>
struct overloaded : Ts...
{
  using Ts::operator()...;
};
template <class... Ts>
overloaded(Ts...) -> overloaded<Ts...>;

template <size_t FMI>
void FmuHandler<FMI>::AddAssignmentParameter(std::smatch assignmentMatch,
                                             const std::string& value,
                                             const TypeContainer<std::string> typeContainer)
{
  std::string variableName = assignmentMatch[1];

  const auto foundVariable = std::get<FMI>(fmuVariables).find(variableName);

  if (foundVariable == std::get<FMI>(fmuVariables).cend())
    LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString) + "No variable with name \"" + variableName
                     + "\" found in the FMU");

  try
  {
    std::string transformedValue = value;
    auto lastChar = transformedValue[transformedValue.size() - 1];
    std::string param;

    if ('1' <= lastChar && lastChar <= '9')
    {
      param.push_back(lastChar);
      transformedValue.pop_back();
    }

    auto assignValueFunc = parameterAssignmentMappings.at(transformedValue);
    AssignFunctionReturnType assignValue;

    if (std::holds_alternative<AssignmentFunctionNoParam>(assignValueFunc))
    {
      assignValue = std::get<AssignmentFunctionNoParam>(assignValueFunc)();
    }
    else if (std::holds_alternative<AssignmentFunctionParam>(assignValueFunc))
    {
      assignValue = std::get<AssignmentFunctionParam>(assignValueFunc)(param);
    }
    else
    {
      static_assert("Unhandled AssignmentFunction type");
    }

    auto emplacer = [&foundVariable](auto arg, auto& fmuParameters)
    { std::get<FMI>(fmuParameters).emplace_back(arg, foundVariable->second.valueReference); };

    std::visit(overloaded{[&](bool arg) { emplacer(arg, fmuBoolParameters); },
                          [&](int arg) { emplacer(arg, fmuIntegerParameters); },
                          [&](uint32_t arg) { emplacer(arg, fmuIntegerParameters); },
                          [&](double arg) { emplacer(arg, fmuDoubleParameters); },
                          [&](std::string arg) { emplacer(arg, fmuStringParameters); }},
               assignValue);
  }
  catch (std::out_of_range& e)
  {
    LOGERRORANDTHROW("Unknown 'AssignSpecial' parameter assignment: " + value);
  }
  catch (std::runtime_error& e)
  {
    LOGERRORANDTHROW("Unable to process 'AssignSpecial' parameter assignment with value '" + value + "': " + e.what())
  }
}

template <size_t FMI>
template <typename GetParametersType, typename FmuTypeParameters, typename UnderlyingType>
void FmuHandler<FMI>::ParseFmuParametersByType(const GetParametersType getParametersType,
                                               const ParameterInterface* parameterInterface,
                                               FmuTypeParameters fmuTypeParameters,
                                               const TypeContainer<UnderlyingType> typeContainer)
{
  for (const auto& [key, value] : std::invoke(getParametersType, parameterInterface))
  {
    if (key.substr(0, 10) != "Parameter_") continue;
    auto variableName = key.substr(10);
    AddParameter(variableName, value, fmuTypeParameters, typeContainer);
  }
}

template <size_t FMI>
template <typename GetParametersType>
void FmuHandler<FMI>::ParseFmuParametersByType(const GetParametersType getParametersType,
                                               const ParameterInterface* parameterInterface,
                                               FmuParameters<std::string>* fmuTypeParameters,
                                               const TypeContainer<std::string> typeContainer)
{
  for (const auto& [key, value] : std::invoke(getParametersType, parameterInterface))
  {
    if (key.substr(0, 10) != "Parameter_") continue;
    auto variableName = key.substr(10);
    if (std::smatch transformationMatch; std::regex_search(variableName, transformationMatch, transformationRegex))
    {
      AddTransformationParameter(transformationMatch, value, fmuTypeParameters, typeContainer);
    }
    else if (std::smatch assignmentMatch; std::regex_search(variableName, assignmentMatch, assignmentRegex))
    {
      AddAssignmentParameter(assignmentMatch, value, typeContainer);
    }
    else
    {
      AddParameter(variableName, value, fmuTypeParameters, typeContainer);
    }
  }
}

template <size_t FMI>
bool FmuHandler<FMI>::IsDefinedAsParameter(int desiredValRef, VariableType desiredType)
{
  for (const auto& [fmuParameterValue, valueReference] : std::get<FMI>(fmuStringParameters))
  {
    if (desiredValRef == valueReference && desiredType == VariableType::String)
    {
      return true;
    }
  }
  for (const auto& [fmuParameterValue, valueReference] : std::get<FMI>(fmuIntegerParameters))
  {
    if (desiredValRef == valueReference && desiredType == VariableType::Int || desiredType == VariableType::Enum)
    {
      return true;
    }
  }
  for (const auto& [fmuParameterValue, valueReference] : std::get<FMI>(fmuDoubleParameters))
  {
    if (desiredValRef == valueReference && desiredType == VariableType::Double)
    {
      return true;
    }
  }
  for (const auto& [fmuParameterValue, valueReference] : std::get<FMI>(fmuBoolParameters))
  {
    if (desiredValRef == valueReference && desiredType == VariableType::Bool)
    {
      return true;
    }
  }
  return false;
}

template <size_t FMI>
std::variant<FmuVariable1*, FmuVariable2*>* FmuHandler<FMI>::GetFmuVariable(int desiredValRef, VariableType desiredType)
{
  std::shared_ptr<std::variant<FmuVariable1*, FmuVariable2*>> returnVariable;

  for (auto& fmuVariable : std::get<FMI>(fmuVariables))
  {
    returnVariable = std::make_shared<std::variant<FmuVariable1*, FmuVariable2*>>(&fmuVariable.second);
    ValueReferenceAndType valRefAndType = fmuVariable.second.GetValueReferenceAndType();
    int valueRef = valRefAndType.first;
    auto type = valRefAndType.second;

    if (desiredValRef == valueRef && desiredType == type)
    {
      return returnVariable.get();
    }
  }
  return nullptr;
}

template <size_t FMI>
void FmuHandler<FMI>::SyncFmuVariablesAndParameters()
{
  for (const auto& [fmuParameterValue, valueReference] : std::get<FMI>(fmuStringParameters))
  {
    std::variant<FmuVariable1*, FmuVariable2*>* fmuVariable = GetFmuVariable(valueReference, VariableType::String);
    if (fmuVariable)
    {
      ValueReferenceAndType valRefAndType = std::get<FMI>(*fmuVariable)->GetValueReferenceAndType();
      int valueRef = valRefAndType.first;
      auto dataType = valRefAndType.second;

      if (dataType == VariableType::String)
      {
        fmuVariableValues.at(valRefAndType).stringValue = fmuParameterValue.data();
      }
    }
  }

  for (const auto& [fmuParameterValue, valueReference] : std::get<FMI>(fmuIntegerParameters))
  {
    std::variant<FmuVariable1*, FmuVariable2*>* fmuVariable = GetFmuVariable(valueReference, VariableType::Int);

    if (!fmuVariable)
    {
      std::variant<FmuVariable1*, FmuVariable2*>* fmuVariable = GetFmuVariable(valueReference, VariableType::Enum);
    }

    if (fmuVariable)
    {
      ValueReferenceAndType valRefAndType = std::get<FMI>(*fmuVariable)->GetValueReferenceAndType();
      int valueRef = valRefAndType.first;
      auto dataType = valRefAndType.second;

      if (dataType == VariableType::Int || dataType == VariableType::Enum)
      {
        fmuVariableValues.at(valRefAndType).intValue = fmuParameterValue;
      }
    }
  }

  for (const auto& [fmuParameterValue, valueReference] : std::get<FMI>(fmuDoubleParameters))
  {
    std::variant<FmuVariable1*, FmuVariable2*>* fmuVariable = GetFmuVariable(valueReference, VariableType::Double);

    if (fmuVariable)
    {
      ValueReferenceAndType valRefAndType = std::get<FMI>(*fmuVariable)->GetValueReferenceAndType();
      int valueRef = valRefAndType.first;
      auto dataType = valRefAndType.second;

      if (dataType == VariableType::Double)
      {
        fmuVariableValues.at(valRefAndType).realValue = fmuParameterValue;
      }
    }
  }

  for (const auto& [fmuParameterValue, valueReference] : std::get<FMI>(fmuBoolParameters))
  {
    std::variant<FmuVariable1*, FmuVariable2*>* fmuVariable = GetFmuVariable(valueReference, VariableType::Bool);

    if (fmuVariable)
    {
      ValueReferenceAndType valRefAndType = std::get<FMI>(*fmuVariable)->GetValueReferenceAndType();
      int valueRef = valRefAndType.first;
      auto dataType = valRefAndType.second;

      if (dataType == VariableType::Bool)
      {
        fmuVariableValues.at(valRefAndType).boolValue = fmuParameterValue;
      }
    }
  }
}

template <size_t FMI>
void FmuHandler<FMI>::SetSensorDataInput(const osi3::SensorData& data)
{
  std::swap(serializedSensorDataIn, previousSerializedSensorDataIn);

  std::vector<int> valueReferencesVec;
  std::vector<fmi_integer_t> fmuInputValuesVec;

  valueReferencesVec.resize(3);
  fmuInputValuesVec.resize(3);

  valueReferencesVec[0] = std::get<FMI>(fmuVariables).at(sensorDataInVariable.value() + ".base.lo").valueReference;
  valueReferencesVec[1] = std::get<FMI>(fmuVariables).at(sensorDataInVariable.value() + ".base.hi").valueReference;
  valueReferencesVec[2] = std::get<FMI>(fmuVariables).at(sensorDataInVariable.value() + ".size").valueReference;

  data.SerializeToString(&serializedSensorDataIn);
  FmuHelper::encode_pointer_to_integer<FMI>(serializedSensorDataIn.data(), fmuInputValuesVec[1], fmuInputValuesVec[0]);
  fmuInputValuesVec[2].emplace<FMI>(serializedSensorDataIn.length());

  fmuCommunications->SetValuesFromIntRef(valueReferencesVec, fmuInputValuesVec, 3);
}

template <size_t FMI>
void FmuHandler<FMI>::SetSensorViewInput(const osi3::SensorView& data)
{
  std::swap(serializedSensorView, previousSerializedSensorView);

  std::vector<int> valueReferencesVec;
  std::vector<fmi_integer_t> fmuInputValuesVec;

  valueReferencesVec.resize(3);
  fmuInputValuesVec.resize(3);

  valueReferencesVec[0] = std::get<FMI>(fmuVariables).at(sensorViewVariable.value() + ".base.lo").valueReference;
  valueReferencesVec[1] = std::get<FMI>(fmuVariables).at(sensorViewVariable.value() + ".base.hi").valueReference;
  valueReferencesVec[2] = std::get<FMI>(fmuVariables).at(sensorViewVariable.value() + ".size").valueReference;

  data.SerializeToString(&serializedSensorView);
  FmuHelper::encode_pointer_to_integer<FMI>(serializedSensorView.data(), fmuInputValuesVec[1], fmuInputValuesVec[0]);
  fmuInputValuesVec[2].emplace<FMI>(serializedSensorView.length());

  fmuCommunications->SetValuesFromIntRef(valueReferencesVec, fmuInputValuesVec, 3);
}

template <size_t FMI>
void FmuHandler<FMI>::SetSensorViewConfigRequest()
{
  int valueReferenceHi
      = std::get<FMI>(fmuVariables).at(sensorViewConfigRequestVariable.value() + ".base.hi").valueReference;
  int valueReferenceLo
      = std::get<FMI>(fmuVariables).at(sensorViewConfigRequestVariable.value() + ".base.lo").valueReference;
  int valueReferenceSize
      = std::get<FMI>(fmuVariables).at(sensorViewConfigRequestVariable.value() + ".size").valueReference;

  fmi_integer_t fmiIntHi;
  fmiIntHi.emplace<FMI>(GetValue(valueReferenceHi, VariableType::Int).intValue);
  fmi_integer_t fmiIntLo;
  fmiIntLo.emplace<FMI>(GetValue(valueReferenceLo, VariableType::Int).intValue);
  int intSize;
  intSize = GetValue(valueReferenceSize, VariableType::Int).intValue;

  void* buffer = FmuHelper::decode_integer_to_pointer<FMI>(fmiIntHi, fmiIntLo);
  const auto size = static_cast<std::string::size_type>(intSize);

  previousSerializedSensorViewConfigRequest = serializedSensorViewConfigRequest;

  serializedSensorViewConfigRequest = {static_cast<char*>(buffer), size};
  sensorViewConfigRequest.ParseFromString(serializedSensorViewConfigRequest);
}

template <size_t FMI>
void FmuHandler<FMI>::SetSensorViewConfig()
{
    // Apply requested config structure from FMU to sensorViewConfig in openPASS, which will be sent back to FMU
    serializedSensorViewConfig = serializedSensorViewConfigRequest;
    sensorViewConfig.ParseFromString(serializedSensorViewConfig);

  std::vector<int> valueReferencesVec;
  std::vector<fmi_integer_t> fmuInputValuesVec;

  valueReferencesVec.resize(3);
  fmuInputValuesVec.resize(3);

  valueReferencesVec[0] = std::get<FMI>(fmuVariables).at(sensorViewConfigVariable.value() + ".base.lo").valueReference;
  valueReferencesVec[1] = std::get<FMI>(fmuVariables).at(sensorViewConfigVariable.value() + ".base.hi").valueReference;
  valueReferencesVec[2] = std::get<FMI>(fmuVariables).at(sensorViewConfigVariable.value() + ".size").valueReference;

  FmuHelper::encode_pointer_to_integer<FMI>(
      serializedSensorViewConfig.data(), fmuInputValuesVec[1], fmuInputValuesVec[0]);
  fmuInputValuesVec[2].emplace<FMI>(serializedSensorViewConfig.length());

  fmuCommunications->SetValuesFromIntRef(valueReferencesVec, fmuInputValuesVec, 3);
}

template <size_t FMI>
void FmuHandler<FMI>::GetSensorData()
{
  int valueReferenceHi = std::get<FMI>(fmuVariables).at(sensorDataOutVariable.value() + ".base.hi").valueReference;
  int valueReferenceLo = std::get<FMI>(fmuVariables).at(sensorDataOutVariable.value() + ".base.lo").valueReference;
  int valueReferenceSize = std::get<FMI>(fmuVariables).at(sensorDataOutVariable.value() + ".size").valueReference;

  fmi_integer_t fmiIntHi;
  fmiIntHi.emplace<FMI>(GetValue(valueReferenceHi, VariableType::Int).intValue);
  fmi_integer_t fmiIntLo;
  fmiIntLo.emplace<FMI>(GetValue(valueReferenceLo, VariableType::Int).intValue);
  int intSize;
  intSize = GetValue(valueReferenceSize, VariableType::Int).intValue;

  void* buffer = FmuHelper::decode_integer_to_pointer<FMI>(fmiIntHi, fmiIntLo);

  if (enforceDoubleBuffering && buffer != nullptr && buffer == previousSensorDataOut)
  {
    LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString) + "FMU has no double buffering");
  }

  previousSensorDataOut = buffer;
  sensorDataOut.ParseFromArray(buffer, intSize);
  osiSourceMap[6] = &sensorDataOut;
}

template <size_t FMI>
void FmuHandler<FMI>::GetTrafficUpdate()
{
  int valueReferenceHi = std::get<FMI>(fmuVariables).at(trafficUpdateVariable.value() + ".base.hi").valueReference;
  int valueReferenceLo = std::get<FMI>(fmuVariables).at(trafficUpdateVariable.value() + ".base.lo").valueReference;
  int valueReferenceSize = std::get<FMI>(fmuVariables).at(trafficUpdateVariable.value() + ".size").valueReference;

  fmi_integer_t fmiIntHi;
  fmiIntHi.emplace<FMI>(GetValue(valueReferenceHi, VariableType::Int).intValue);
  fmi_integer_t fmiIntLo;
  fmiIntLo.emplace<FMI>(GetValue(valueReferenceLo, VariableType::Int).intValue);
  int intSize;
  intSize = GetValue(valueReferenceSize, VariableType::Int).intValue;

  void* buffer = FmuHelper::decode_integer_to_pointer<FMI>(fmiIntHi, fmiIntLo);

  if (enforceDoubleBuffering && buffer != nullptr && buffer == previousTrafficUpdate)
  {
    LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString) + "FMU has no double buffering");
  }

  previousTrafficUpdate = buffer;
  trafficUpdate.ParseFromArray(buffer, intSize);
  trafficUpdate.SerializeToString(&serializedTrafficUpdate);
  osiSourceMap[0] = &trafficUpdate;
  osiSourceMap[1] = &trafficUpdate;
  osiSourceMap[2] = &trafficUpdate;
  osiSourceMap[3] = &trafficUpdate;
}

template <size_t FMI>
void FmuHandler<FMI>::GetHostVehicleData()
{
  int valueReferenceHi = std::get<FMI>(fmuVariables).at(hostVehicleDataVariable.value() + ".base.hi").valueReference;
  int valueReferenceLo = std::get<FMI>(fmuVariables).at(hostVehicleDataVariable.value() + ".base.lo").valueReference;
  int valueReferenceSize = std::get<FMI>(fmuVariables).at(hostVehicleDataVariable.value() + ".size").valueReference;

  fmi_integer_t fmiIntHi;
  fmiIntHi.emplace<FMI>(GetValue(valueReferenceHi, VariableType::Int).intValue);
  fmi_integer_t fmiIntLo;
  fmiIntLo.emplace<FMI>(GetValue(valueReferenceLo, VariableType::Int).intValue);
  int intSize;
  intSize = GetValue(valueReferenceSize, VariableType::Int).intValue;

  void* buffer = FmuHelper::decode_integer_to_pointer<FMI>(fmiIntHi, fmiIntLo);

  if (enforceDoubleBuffering && buffer != nullptr && buffer == previousHostVehicleData)
  {
    LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString) + "FMU has no double buffering");
  }

  previousHostVehicleData = buffer;
  hostVehicleData.ParseFromArray(buffer, intSize);
  hostVehicleData.SerializeToString(&serializedHostVehicleData);
}

template <size_t FMI>
void FmuHandler<FMI>::SetTrafficCommandInput(const osi3::TrafficCommand& data)
{
  std::swap(serializedTrafficCommand, previousSerializedTrafficCommand);

  std::vector<int> valueReferencesVec;
  std::vector<fmi_integer_t> fmuInputValuesVec;

  valueReferencesVec.resize(3);
  fmuInputValuesVec.resize(3);

  valueReferencesVec[0] = std::get<FMI>(fmuVariables).at(trafficCommandVariable.value() + ".base.lo").valueReference;
  valueReferencesVec[1] = std::get<FMI>(fmuVariables).at(trafficCommandVariable.value() + ".base.hi").valueReference;
  valueReferencesVec[2] = std::get<FMI>(fmuVariables).at(trafficCommandVariable.value() + ".size").valueReference;

  data.SerializeToString(&serializedTrafficCommand);

  FmuHelper::encode_pointer_to_integer<FMI>(
      serializedTrafficCommand.data(), fmuInputValuesVec[1], fmuInputValuesVec[0]);

  auto length = serializedTrafficCommand.length();

  if (FMI == FMI1)
  {
    if (length > std::numeric_limits<fmi1_integer_t>::max())
    {
      LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString)
                       + "Serialized buffer length of osi::TrafficCommand exceeds fmi integer size");
    }
  }
  else if (FMI == FMI2)
  {
    if (length > std::numeric_limits<fmi2_integer_t>::max())
    {
      LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString)
                       + "Serialized buffer length of osi::TrafficCommand exceeds fmi integer size");
    }
  }

  fmuInputValuesVec[2].emplace<FMI>(length);

  fmuCommunications->SetValuesFromIntRef(valueReferencesVec, fmuInputValuesVec, 3);
}

template <size_t FMI>
void FmuHandler<FMI>::SetHostVehicleDataInput(const osi3::HostVehicleData& data)
{
  std::swap(serializedHostVehicleData, previousSerializedHostVehicleData);

  std::vector<int> valueReferencesVec;
  std::vector<fmi_integer_t> fmuInputValuesVec;

  valueReferencesVec.resize(3);
  fmuInputValuesVec.resize(3);

  valueReferencesVec[0] = std::get<FMI>(fmuVariables).at(hostVehicleDataVariable.value() + ".base.lo").valueReference;
  valueReferencesVec[1] = std::get<FMI>(fmuVariables).at(hostVehicleDataVariable.value() + ".base.hi").valueReference;
  valueReferencesVec[2] = std::get<FMI>(fmuVariables).at(hostVehicleDataVariable.value() + ".size").valueReference;

  data.SerializeToString(&serializedHostVehicleData);

  FmuHelper::encode_pointer_to_integer<FMI>(
      serializedHostVehicleData.data(), fmuInputValuesVec[1], fmuInputValuesVec[0]);

  auto length = serializedHostVehicleData.length();

  if (FMI == FMI1)
  {
    if (length > std::numeric_limits<fmi1_integer_t>::max())
    {
      LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString)
                       + "Serialized buffer length of osi::TrafficCommand exceeds fmi integer size");
    }
  }
  else if (FMI == FMI2)
  {
    if (length > std::numeric_limits<fmi2_integer_t>::max())
    {
      LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString)
                       + "Serialized buffer length of osi::TrafficCommand exceeds fmi integer size");
    }
  }

  fmuInputValuesVec[2].emplace<FMI>(length);

  fmuCommunications->SetValuesFromIntRef(valueReferencesVec, fmuInputValuesVec, 3);
}

template <size_t FMI>
void FmuHandler<FMI>::SetGroundTruth()
{
  auto* worldData = static_cast<OWL::Interfaces::WorldData*>(world->GetWorldData());
  groundTruth = worldData->GetOsiGroundTruth();
  groundTruth.mutable_host_vehicle_id()->set_value(worldData->GetOwlId(agent->GetId()));

  std::vector<int> valueReferencesVec;
  std::vector<fmi_integer_t> fmuInputValuesVec;

  valueReferencesVec.resize(3);
  fmuInputValuesVec.resize(3);

  valueReferencesVec[0] = std::get<FMI>(fmuVariables).at(groundTruthVariable.value() + ".base.lo").valueReference;
  valueReferencesVec[1] = std::get<FMI>(fmuVariables).at(groundTruthVariable.value() + ".base.hi").valueReference;
  valueReferencesVec[2] = std::get<FMI>(fmuVariables).at(groundTruthVariable.value() + ".size").valueReference;

  groundTruth.SerializeToString(&serializedGroundTruth);
  FmuHelper::encode_pointer_to_integer<FMI>(serializedGroundTruth.data(), fmuInputValuesVec[1], fmuInputValuesVec[0]);
  fmuInputValuesVec[2].emplace<FMI>(serializedGroundTruth.length());

  fmuCommunications->SetValuesFromIntRef(valueReferencesVec, fmuInputValuesVec, 3);
}

template <size_t FMI>
FmuValue& FmuHandler<FMI>::GetFmuSignalValue(SignalValue signalValue, VariableType variableType)
{
  int valueReference = std::get<FMI>(fmuOutputs).at(signalValue);
  ValueReferenceAndType valueReferenceAndType;
  valueReferenceAndType.first = valueReference;
  valueReferenceAndType.second = variableType;
  return fmuVariableValues.at(valueReferenceAndType);
}

template <size_t FMI>
FmuValue& FmuHandler<FMI>::GetValue(int valueReference, VariableType variableType) const
{
  ValueReferenceAndType valueReferenceAndType;
  valueReferenceAndType.first = valueReference;
  valueReferenceAndType.second = variableType;
  return fmuVariableValues.at(valueReferenceAndType);
}

template <size_t FMI>
void FmuHandler<FMI>::WriteValues()
{
  for (auto [fmuVarName, fmuVariable] : std::get<FMI>(fmuVariables))
  {
    if (!isInitialized && fmuVariable.WriteAtInit() || isInitialized && fmuVariable.WriteAtTrigger())
    {
      ValueReferenceAndType valRefAndType = fmuVariable.GetValueReferenceAndType();
      int valueRef = valRefAndType.first;
      auto dataType = valRefAndType.second;

      //TODO: Remove this tmp fix for parameter overriding as soon as possible
      if (fmuVariable.IsParameter() && !IsDefinedAsParameter(valueRef, dataType))
      {
        LOGDEBUG(FmuHelper::log_prefix(this->agentIdString, this->componentName) + "'" + fmuVarName
                 + "': Not Written because not defined as parameter");
        continue;
      }

      if (fmuVariableValues.count(valRefAndType) <= 0) continue;
      FmuValue value = fmuVariableValues.at(valRefAndType);
      if (dataType == VariableType::String)
      {
        value.stringValue = fmuVariableValues.at(valRefAndType).stringValue;
      }

      SetFmuValue(valueRef, value, dataType);
      LOGDEBUG(FmuHelper::log_prefix(this->agentIdString, this->componentName) + ""
               + FmuHelper::GenerateString("write", fmuVarName, dataType, value));
    }
    else
    {
      LOGDEBUG(FmuHelper::log_prefix(this->agentIdString, this->componentName) + "'" + fmuVarName
               + "': Not written (variablity: " + FmuHelper::VariabilityToStringMap(fmuVariable.variability)
               + ", causality: " + FmuHelper::CausalityToStringMap(fmuVariable.causality) + ")");
    }
  }
}

template <size_t FMI>
void FmuHandler<FMI>::ReadValues()
{
  for (auto [fmuVarName, fmuVariable] : std::get<FMI>(fmuVariables))
  {
    if (!isInitialized && fmuVariable.ReadAtInit() || isInitialized && fmuVariable.ReadAtTrigger())
    {
      ValueReferenceAndType valRefAndType = fmuVariable.GetValueReferenceAndType();
      int valueRef = valRefAndType.first;
      auto dataType = valRefAndType.second;

      fmi_t dataOut;
      auto& value = fmuVariableValues[valRefAndType];
      GetFmuValue(valueRef, value, dataType);
      LOGDEBUG(FmuHelper::log_prefix(this->agentIdString, this->componentName) + ""
               + FmuHelper::GenerateString("read", fmuVarName, dataType, value));
    }
    else
    {
      LOGDEBUG(FmuHelper::log_prefix(this->agentIdString, this->componentName) + "'" + fmuVarName
               + "': Not read (variablity: " + FmuHelper::VariabilityToStringMap(fmuVariable.variability)
               + ", causality: " + FmuHelper::CausalityToStringMap(fmuVariable.causality) + ")");
    }
  }
}

template <size_t FMI>
void FmuHandler<FMI>::SetFmuValue(int valueReference, FmuValue fmuValueIn, VariableType dataType)
{
  std::vector<int> valueReferences;
  valueReferences.resize(1);
  valueReferences[0] = valueReference;
  std::vector<FmuValue> fmuValuesIn;
  fmuValuesIn.resize(1);
  fmuValuesIn[0] = fmuValueIn;
  fmuCommunications->SetFMI(valueReferences, fmuValuesIn, 1, dataType);
}

template <size_t FMI>
void FmuHandler<FMI>::SetFmuValues(std::vector<int> valueReferences,
                                   std::vector<FmuValue> fmuValuesIn,
                                   VariableType dataType)
{
  /*
  if(!valueReferences.empty())
      fmuCommunications->SetFMI(valueReferences, fmuValuesIn, valueReferences.size(), dataType);
  */
  // TODO The above code has issues in prestep method (storage allocation issue). Fix the issue and use the code above
  // instead if the one below
  for (int i = 0; i < valueReferences.size(); i++)
  {
    SetFmuValue(valueReferences[i], fmuValuesIn[i], dataType);
  }
}

template <size_t FMI>
void FmuHandler<FMI>::GetFmuValue(int valueReference, FmuValue& fmuValueOut, VariableType dataType)
{
  std::vector<int> valueReferences;
  valueReferences.resize(1);
  valueReferences[0] = valueReference;
  std::vector<FmuValue> fmuValuesOut;
  fmuValuesOut.resize(1);
  fmuCommunications->GetFMI(valueReferences, fmuValuesOut, 1, dataType);
  fmuValueOut = fmuValuesOut[0];
}

template <size_t FMI>
void FmuHandler<FMI>::GetFmuValues(std::vector<int> valueReferences,
                                   std::vector<FmuValue>& fmuValuesOut,
                                   VariableType dataType)
{
  fmuCommunications->GetFMI(valueReferences, fmuValuesOut, valueReferences.size(), dataType);
}

template <size_t FMI>
void FmuHandler<FMI>::HandleFmiStatus(jm_status_enu_t status, std::string logPrefix)
{
  return fmuCommunications->HandleFmiStatus(status, logPrefix);
}

template <size_t FMI>
jm_status_enu_t FmuHandler<FMI>::PrepareFmuInit()
{
  return fmuCommunications->PrepareFmuInit();
}

template <size_t FMI>
jm_status_enu_t FmuHandler<FMI>::FmiEndHandling()
{
  return fmuCommunications->FmiEndHandling();
}

template <size_t FMI>
jm_status_enu_t FmuHandler<FMI>::FmiSimulateStep(double time)
{
  return fmuCommunications->FmiSimulateStep(time);
}

template <size_t FMI>
jm_status_enu_t FmuHandler<FMI>::FmiPrepSimulate()
{
  return fmuCommunications->FmiPrepSimulate();
}

template <size_t FMI>
void FmuHandler<FMI>::AddTrafficAction()
{
  auto commands = scenarioControl->GetCommands();

  if (commands.empty())
  {
    return;
  }

  for (const auto& command : commands)
  {
    if (auto scenario_command = std::get_if<ScenarioCommand::AssignRoute>(&command))
    {
      if (scenario_command->id != lastTrafficCommandId)
      {
        FmuHelper::AddTrafficCommandActionFromOpenScenarioRouteWayPoints(trafficCommand.add_action(), scenario_command);
        lastTrafficCommandId = scenario_command->id;
      }
    }
  }
}
