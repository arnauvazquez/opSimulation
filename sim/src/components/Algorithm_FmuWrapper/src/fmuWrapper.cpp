/********************************************************************************
 * Copyright (c) 2018-2021 in-tech GmbH
 *               2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "fmuWrapper.h"

#include <memory>
#include <sstream>
#include <stdlib.h>
#include <string>

#include "FmuHandler.h"
#include "common/commonTools.h"
#include "fmuFileHelper.h"
#include "include/agentInterface.h"
#include "include/fmuHandlerInterface.h"
#include "include/parameterInterface.h"
#include "include/scenarioControlInterface.h"

AlgorithmFmuWrapperImplementation::AlgorithmFmuWrapperImplementation(
    std::string componentName,
    bool isInit,
    int priority,
    int offsetTime,
    int responseTime,
    int cycleTime,
    WorldInterface *world,
    StochasticsInterface *stochastics,
    const ParameterInterface *parameters,
    PublisherInterface *const publisher,
    const CallbackInterface *callbacks,
    AgentInterface *agent,
    std::shared_ptr<ScenarioControlInterface> const scenarioControl)
    : UnrestrictedControllStrategyModelInterface(componentName,
                                                 isInit,
                                                 priority,
                                                 offsetTime,
                                                 responseTime,
                                                 cycleTime,
                                                 stochastics,
                                                 world,
                                                 parameters,
                                                 publisher,
                                                 callbacks,
                                                 agent,
                                                 scenarioControl),
      startTime{0},
      callbacks{callbacks},
      componentName{componentName},
      priority{priority}
{
  LOGDEBUG(FmuHelper::log_prefix(std::to_string(GetAgent()->GetId()), componentName) + "constructor started");

  agentIdString = FmuFileHelper::CreateAgentIdString(GetAgent()->GetId());

  cdata.stepSize = cycleTime / 1000.0;
  cdata.stepSizeSetByUser = 1;

  cdata.write_log_files = 0;
  cdata.write_output_files = 0;
  cdata.log_file_name = nullptr;
  cdata.output_file_name = nullptr;

  const auto fmuPath = helper::map::query(parameters->GetParametersString(), "FmuPath");
  THROWIFFALSE(fmuPath.has_value(), "Missing parameter \"FmuPath\"");
  FMU_configPath = fmuPath.value();

  SetupFilenames();
  SetOutputPath();

  const auto logging = helper::map::query(parameters->GetParametersBool(), "Logging").value_or(DEFAULT_LOGGING);
  if (logging)
  {
    SetupLog();
  }

  const auto csvOutput = helper::map::query(parameters->GetParametersBool(), "CsvOutput").value_or(DEFAULT_CSV_OUTPUT);
  if (csvOutput)
  {
    SetupOutput();
  }

  SetupUnzip();

  LOGDEBUG(FmuHelper::log_prefix(agentIdString, componentName) + "constructor finished");
}

void AlgorithmFmuWrapperImplementation::SetOutputPath()
{
  std::filesystem::path fmuPath = cdata.FMUPath;
  const std::string &resultsPath = GetParameters()->GetRuntimeInformation().directories.output;
  std::filesystem::path agentOutputPath = std::filesystem::path(resultsPath) / "FmuWrapper"
                                        / (std::string("Agent") + agentIdString)
                                        / fmuPath.filename().replace_extension().string();
  outputPath = agentOutputPath.string();
}

void AlgorithmFmuWrapperImplementation::SetupFilenames()
{
  std::filesystem::path fmuPath(FMU_configPath);

  if (!fmuPath.is_absolute())
  {
    std::filesystem::path configBasePath(GetParameters()->GetRuntimeInformation().directories.configuration);
    fmuPath = configBasePath / fmuPath;
  }

  if (!std::filesystem::exists(fmuPath)) LOGERRORANDTHROW("FMU file '" + fmuPath.string() + "' doesn't exist");

  FMU_absPath = fmuPath.string();       // keep string for context struct
  cdata.FMUPath = FMU_absPath.c_str();  // set FMU absolute path in context struct

  const std::string FMU_name = fmuPath.filename().replace_extension().string();

  // log file name: <fmuName>.log
  logFileName.assign(FMU_name);
  logFileName.append(".log");

  // output filename: <fmuName>.csv
  outputFileName.assign(FMU_name);
  outputFileName.append(".csv");
}

void AlgorithmFmuWrapperImplementation::SetupLog()
{
  std::filesystem::path logPath{outputPath};

  MkDirOrThrowError(logPath);

  // construct log file name including full path
  logPath = logPath / logFileName;

  logFileFullName = logPath.string();
  cdata.log_file_name = logFileFullName.c_str();

  cdata.write_log_files = 1;
}

void AlgorithmFmuWrapperImplementation::SetupOutput()
{
  std::filesystem::path csvPath{outputPath};

  MkDirOrThrowError(outputPath);

  // construct output file name including full path
  csvPath = csvPath / outputFileName;

  outputFileFullName = csvPath.string();
  cdata.output_file_name = outputFileFullName.c_str();

  cdata.write_output_files = 1;
}

void AlgorithmFmuWrapperImplementation::SetupUnzip()
{
  std::filesystem::path unzipRoot = std::filesystem::temp_directory_path() / FmuFileHelper::temporaryDirectoryName();

  // make dir if not existing
  MkDirOrThrowError(unzipRoot);

  // set unzipPath to NULL to have the temporary folders removed in fmi1_end_handling
  cdata.unzipPath = nullptr;

  // set unzip temporary path
  tmpPath = unzipRoot.string();
  cdata.tmpPath = const_cast<char *>(tmpPath.c_str());
}

AlgorithmFmuWrapperImplementation::~AlgorithmFmuWrapperImplementation()
{
  LOGDEBUG(FmuHelper::log_prefix(agentIdString, componentName) + "destructor started");

  if (isInitialized)
  {
    cdata_global_ptr = &cdata;  // reassign global pointer, required for FMI 1.0 logging
    fmuHandler->FmiEndHandling();
    isInitialized = false;
  }

  if (fmuHandler)
  {
    delete fmuHandler;
    fmuHandler = nullptr;
  }

  std::error_code ec;
  if (!std::filesystem::remove_all(tmpPath, ec))
  {
    LOGWARN("Failed to remove files from directory: " + tmpPath + " with error msg :" + ec.message());
  }

  LOGDEBUG(FmuHelper::log_prefix(agentIdString, componentName) + "destructor finished");
}

void AlgorithmFmuWrapperImplementation::UpdateInput(int localLinkId,
                                                    const std::shared_ptr<SignalInterface const> &data,
                                                    int time)
{
  if (!isInitialized)
  {
    InitFmu();
    startTime = time;
  }

  fmuHandler->UpdateInput(localLinkId, data, time);
}

void AlgorithmFmuWrapperImplementation::UpdateOutput(int localLinkId,
                                                     std::shared_ptr<SignalInterface const> &data,
                                                     int time)
{
  LOGDEBUG(FmuHelper::log_prefix(agentIdString, componentName) + "UpdateOutput started");

  cdata_global_ptr = &cdata;  // reassign global pointer, required for FMI 1.0 logging

  if (!cdata.simulation_initialized)
  {
    throw std::logic_error("FMU has to be initialized before calling UpdateOutput");
  }

  fmuHandler->UpdateOutput(localLinkId, data, time);

  LOGDEBUG(FmuHelper::log_prefix(agentIdString, componentName) + "UpdateOutput finished");
}

void AlgorithmFmuWrapperImplementation::Trigger(int time)
{
  LOGDEBUG(FmuHelper::log_prefix(agentIdString, componentName) + "Start of Trigger (time: " + std::to_string(time)
           + ")");

  if (!isInitialized)
  {
    InitFmu();
    startTime = time;
  }

  cdata_global_ptr = &cdata;  // reassign global pointer, required for FMI 1.0 logging

  fmuHandler->WriteValues();

  fmuHandler->PreStep(time);

  double actTime = ((double)(time - startTime)) / 1000.0;
  fmuHandler->FmiSimulateStep(actTime);

  fmuHandler->ReadValues();

  fmuHandler->PostStep(time);

  LOGDEBUG(FmuHelper::log_prefix(agentIdString, componentName) + "End of Trigger");
}

void AlgorithmFmuWrapperImplementation::InitFmu()
{
  if (isInitialized) return;

  LOGDEBUG(FmuHelper::log_prefix(agentIdString, componentName) + "FMU init start");
  cdata_global_ptr = nullptr;

  auto fmiStatus = fmuChecker(&cdata);  //! unpack FMU

  LOGDEBUG(FmuHelper::log_prefix(agentIdString, componentName) + "Instantiating OSMP FMU handler");
  if (cdata.version == fmi_version_1_enu)
  {
    fmuHandler = new FmuHandler<FMI1>(componentName,
                                      cdata,
                                      fmuVariables,
                                      fmuVariableValues,
                                      GetParameters(),
                                      GetWorld(),
                                      GetAgent(),
                                      GetStochastics(),
                                      callbacks,
                                      GetScenarioControl());
  }
  else if (cdata.version == fmi_version_2_0_enu)
  {
    fmuHandler = new FmuHandler<FMI2>(componentName,
                                      cdata,
                                      fmuVariables,
                                      fmuVariableValues,
                                      GetParameters(),
                                      GetWorld(),
                                      GetAgent(),
                                      GetStochastics(),
                                      callbacks,
                                      GetScenarioControl());
  }
  fmuHandler->PrepareInit();
  fmuHandler->HandleFmiStatus(fmiStatus, "fmuChecker");
  fmuHandler->PrepareFmuInit();
  fmuHandler->ReadValues();
  fmuHandler->SyncFmuVariablesAndParameters();
  fmuHandler->WriteValues();
  fmuHandler->Init();
  fmuHandler->FmiPrepSimulate();

  isInitialized = true;

  LOGDEBUG(FmuHelper::log_prefix(agentIdString, componentName) + "FMU init finished");
}

void AlgorithmFmuWrapperImplementation::MkDirOrThrowError(const std::filesystem::path &path)
{
  try
  {
    std::filesystem::create_directories(path);
  }
  catch (std::filesystem::filesystem_error &e)
  {
    LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString, componentName) + "could not create folder " + path.string()
                     + ": " + e.what());
  }
}

const FmuHandlerInterface *AlgorithmFmuWrapperImplementation::GetFmuHandler() const
{
  return fmuHandler;
}

void AlgorithmFmuWrapperImplementation::Init()
{
  if (isInitialized) return;
  InitFmu();
}

[[nodiscard]] const FmuVariables &AlgorithmFmuWrapperImplementation::GetFmuVariables() const
{
  if (!isInitialized) LOGERRORANDTHROW("Access to field in uninitialized context.")
  return fmuVariables;
}

[[nodiscard]] const FmuValue &AlgorithmFmuWrapperImplementation::GetValue(int valueReference,
                                                                          VariableType variableType) const
{
  if (!isInitialized)
  {
    LOGERROR("Access to field in uninitialized context.");
    throw not_initialized_exception(componentName);
  }
  ValueReferenceAndType valueReferenceAndType;
  valueReferenceAndType.first = valueReference;
  valueReferenceAndType.second = variableType;
  return fmuVariableValues.at(valueReferenceAndType);
}

void AlgorithmFmuWrapperImplementation::SetValue(const FmuValue &fmuValue,
                                                 int valueReference,
                                                 VariableType variableType)
{
  if (!isInitialized)
  {
    LOGERRORANDTHROW("Access to field in uninitialized context.")
    throw not_initialized_exception(componentName);
  }
  ValueReferenceAndType valueReferenceAndType;
  valueReferenceAndType.first = valueReference;
  valueReferenceAndType.second = variableType;

  fmuVariableValues.at(valueReferenceAndType) = fmuValue;
  fmuHandler->SetFmuValue(valueReference, fmuValue, variableType);
}

void AlgorithmFmuWrapperImplementation::SetFmuValues(std::vector<int> valueReferences,
                                                     std::vector<FmuValue> fmuValuesIn,
                                                     VariableType dataType)
{
  for (int i = 0; i < valueReferences.size(); i++)
  {
    SetValue(fmuValuesIn[i], valueReferences[i], dataType);
  }
  fmuHandler->SetFmuValues(valueReferences, fmuValuesIn, dataType);
}
void AlgorithmFmuWrapperImplementation::GetFmuValues(std::vector<int> valueReferences,
                                                     std::vector<FmuValue> &fmuValuesOut,
                                                     VariableType dataType)
{
  fmuHandler->GetFmuValues(valueReferences, fmuValuesOut, dataType);
}

int AlgorithmFmuWrapperImplementation::getPriority() const
{
  return priority;
}

fmi_version_enu_t AlgorithmFmuWrapperImplementation::getFmiVersion()
{
  return cdata.version;
}