/********************************************************************************
 * Copyright (c) 2023 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "powertrain.h"

#include <exception>

#include <QString>
#include <qglobal.h>

#include "common/primitiveSignals.h"
#include "include/parameterInterface.h"

ActionPowertrain::ActionPowertrain(std::string componentName,
                                   bool isInit,
                                   int priority,
                                   int offsetTime,
                                   int responseTime,
                                   int cycleTime,
                                   StochasticsInterface *stochastics,
                                   const ParameterInterface *parameters,
                                   PublisherInterface *const publisher,
                                   const CallbackInterface *callbacks,
                                   AgentInterface *agent)
    : AlgorithmInterface(componentName,
                         isInit,
                         priority,
                         offsetTime,
                         responseTime,
                         cycleTime,
                         stochastics,
                         parameters,
                         publisher,
                         callbacks,
                         agent)
{
  LOGINFO(QString().asprintf("Constructing PowerTrain for agent %d...", agent->GetId()).toStdString());

  std::shared_ptr<const mantle_api::VehicleProperties> vehicleProperties
      = std::dynamic_pointer_cast<const mantle_api::VehicleProperties>(GetAgent()->GetVehicleModelParameters());

  auto powerEngineMax = helper::map::query(vehicleProperties->properties, "MaximumEnginePower");
  THROWIFFALSE(powerEngineMax.has_value(), "MaximumEnginePower was not defined in VehicleCatalog");

  auto speedEngineMax = helper::map::query(vehicleProperties->properties, "MaximumEngineSpeed");
  THROWIFFALSE(speedEngineMax.has_value(), "MaximumEngineSpeed was not defined in VehicleCatalog");

  auto torqueEngineMax = helper::map::query(vehicleProperties->properties, "MaximumEngineTorque");
  THROWIFFALSE(speedEngineMax.has_value(), "MaximumEngineTorque was not defined in VehicleCatalog");

  engine = new Engine((std::stod(powerEngineMax.value()) * 1_W),
                      ((std::stod(speedEngineMax.value()) / 60.0) * 1_Hz),
                      (std::stod(torqueEngineMax.value()) * 1_Nm));

  auto axleRatio = helper::map::query(vehicleProperties->properties, "AxleRatio");
  THROWIFFALSE(axleRatio.has_value(), "AxleRatio was not defined in VehicleCatalog");
  std::vector<double> gearRatioVector;
  int gear = 1;
  auto gearRatio
      = helper::map::query(vehicleProperties->properties, vehicle::properties::GearRatio + std::to_string(gear));
  while (gearRatio.has_value())
  {
    gearRatioVector.push_back(std::stod(gearRatio.value()));
    gear++;
    gearRatio
        = helper::map::query(vehicleProperties->properties, vehicle::properties::GearRatio + std::to_string(gear));
  }
  Common::Vector2d velocityCar = GetAgent()->GetVelocity();
  velocityCar.Rotate(GetAgent()->GetYaw());

  std::vector<mantle_api::Axle> axles;
  axles.push_back(vehicleProperties->front_axle);
  axles.push_back(vehicleProperties->rear_axle);
  wheelRotationRate.resize(4);
  for (int i = 0; i < axles.size(); ++i)
  {
    units::length::meter_t r_tire = axles[i].wheel_diameter / 2;
    units::frequency::hertz_t rotationVelocityTire = velocityCar.x / r_tire;
    wheelRotationRate[2 * i] = rotationVelocityTire;
    wheelRotationRate[2 * i + 1] = rotationVelocityTire;
  }
  transmission = new Transmission(gearRatioVector, std::stod(axleRatio.value()));
  TypeDrivetrain = parameters->GetParametersString().count("TypeDrivetrain") == 1
                     ? parameters->GetParametersString().at("TypeDrivetrain")
                     : parameters->GetParametersString().at("1");
  FrontRatioAWD = parameters->GetParametersDouble().count("FrontRatioAWD") == 1
                    ? parameters->GetParametersDouble().at("FrontRatioAWD")
                    : parameters->GetParametersDouble().at("2");
}

ActionPowertrain::~ActionPowertrain()
{
  delete (engine);
  engine = nullptr;
  delete (transmission);
  transmission = nullptr;
}

void ActionPowertrain::UpdateInput(int localLinkId, const std::shared_ptr<SignalInterface const> &data, int time)
{
  std::stringstream log;
  log << COMPONENTNAME << " (component " << GetComponentName() << ", agent " << std::to_string(GetAgent()->GetId())
      << ", input data for local link " << localLinkId << ": ";
  LOG(CbkLogLevel::Debug, log.str());

  if (localLinkId == 0)
  {
    const std::shared_ptr<LongitudinalSignal const> signal = std::dynamic_pointer_cast<LongitudinalSignal const>(data);
    if (!signal)
    {
      const std::string msg = COMPONENTNAME + "_" + std::to_string(GetAgent()->GetId()) + " invalid signaltype";
      LOG(CbkLogLevel::Debug, msg);
      throw std::runtime_error(msg);
    }
    longitudinalSignal = *signal;
  }
  else if (localLinkId == 1)
  {
    const std::shared_ptr<SignalVectorDouble const> signal = std::dynamic_pointer_cast<SignalVectorDouble const>(data);
    if (!signal)
    {
      const std::string msg = COMPONENTNAME + "_" + std::to_string(GetAgent()->GetId()) + " invalid signaltype";
      LOG(CbkLogLevel::Debug, msg);
      throw std::runtime_error(msg);
    }

    std::vector<double> wheelRotationRate_temp = signal->value;

    wheelRotationRate.resize(wheelRotationRate_temp.size());
    for (unsigned int idx = 0; idx < wheelRotationRate.size(); idx++)
    {
      wheelRotationRate[idx] = wheelRotationRate_temp[idx] * 1_Hz;
    }
  }
  else
  {
    const std::string msg = COMPONENTNAME + "_" + std::to_string(GetAgent()->GetId()) + " invalid link";
    LOG(CbkLogLevel::Debug, msg);
    throw std::runtime_error(msg);
  }
}

void ActionPowertrain::UpdateOutput(int localLinkId, std::shared_ptr<SignalInterface const> &signal, int time)
{
  Q_UNUSED(time);

  std::stringstream log;
  log << COMPONENTNAME << " UpdateOutput";
  LOG(CbkLogLevel::Debug, log.str());
  log.str(std::string());

  bool success = outputPorts.at(localLinkId)->GetSignalValue(signal);

  if (success)
  {
    log << COMPONENTNAME << " UpdateOutput successful";
    LOG(CbkLogLevel::Debug, log.str());
  }
  else
  {
    log << COMPONENTNAME << " UpdateOutput failed";
    LOG(CbkLogLevel::Error, log.str());
  }
}

void ActionPowertrain::Trigger(int time)
{
  // calculate Engine Speed (average wheel rotation rate / gear ratio)
  units::frequency::hertz_t transmissionRotationRate = 0_Hz;
  if (TypeDrivetrain == "FWD")
  {
    transmissionRotationRate = (wheelRotationRate[0] + wheelRotationRate[1]) / 2;
  }
  else if (TypeDrivetrain == "RWD")
  {
    transmissionRotationRate = (wheelRotationRate[2] + wheelRotationRate[3]) / 2;
  }
  else if (TypeDrivetrain == "AWD")
  {
    transmissionRotationRate
        = (wheelRotationRate[0] + wheelRotationRate[1] + wheelRotationRate[2] + wheelRotationRate[3]) / 4;
  }

  units::frequency::hertz_t EngineSpeed
      = transmission->GetEngineSpeed(transmissionRotationRate, longitudinalSignal.gear);
  // Calculate current engine torque
  engine->CalculateDriveTorque(longitudinalSignal.accPedalPos, EngineSpeed);

  std::vector<double> torque(4);

  // Set tire torque depending on drivetrain type
  if (TypeDrivetrain == "FWD")
  {
    torque[0] = engine->GetDriveTorque().value() * transmission->GetGearRatio(longitudinalSignal.gear) / 2;
    torque[1] = torque[0];
    torque[2] = 0;
    torque[3] = 0;
  }
  else if (TypeDrivetrain == "RWD")
  {
    torque[2] = engine->GetDriveTorque().value() * transmission->GetGearRatio(longitudinalSignal.gear) / 2;
    torque[3] = torque[2];
    torque[0] = 0;
    torque[1] = 0;
  }
  else if (TypeDrivetrain == "AWD")
  {
    torque[0]
        = engine->GetDriveTorque().value() * transmission->GetGearRatio(longitudinalSignal.gear) / 2 * FrontRatioAWD;
    torque[1] = torque[0];
    torque[2] = engine->GetDriveTorque().value() * transmission->GetGearRatio(longitudinalSignal.gear) / 2
              * (1 - FrontRatioAWD);
    torque[3] = torque[2];
  }

  DriveTorqueTires.SetValue(torque);
}