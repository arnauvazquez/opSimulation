/********************************************************************************
 * Copyright (c) 2023 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/** \addtogroup Vehicle_Dynamics
 *@{
 * \addtogroup Powertrain
 * @{
 * \addtogroup Transmission
 * \brief  This module represents a simple gearbox.
 *
 * \details The gearbox is used to transduce the engine torque and the speed
 *
 *  @}
 * @} */

#ifndef TRANSMISSION_H
#define TRANSMISSION_H

#include <vector>

#include "common/commonTools.h"
#include "common/vector2d.h"
#include "units.h"
/*!
 * \copydoc Transmission
 * \ingroup Transmission
 */
class Transmission
{
public:
  Transmission(std::vector<double> gear, double axleratio);
  /*!
   * \brief Get ratio of current gear
   *
   * @param[in]    gear     current gear
   */
  double GetGearRatio(int gear);
  /*!
   * \brief Calculate engine speed in current gear
   *
   * @param[in]    speedtransmission     rotation rate of transmission output
   * @param[in]    gear     current gear
   *
   */
  units::frequency::hertz_t GetEngineSpeed(units::frequency::hertz_t speedtransmission, int gear);

private:
  std::vector<double> gearRatio;  //!< Gear Ratios
  double axleRatio;               //!< Axle Ratio
};

#endif