/********************************************************************************
 * Copyright (c) 2023 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/** \addtogroup Vehicle_Dynamics
 *@{
 * \addtogroup Powertrain
 * @{
 * \addtogroup Engine
 * \brief  This module represents a simple vehicle engine.
 *
 * \details The maximum power and the current number of engines are used to determine the currently available engine
 *torque. the maximum torque is scaled via the accelerator pedal position and serves as input for the gearbox.
 *
 * \section inputs Inputs
 * name | meaning
 * -----|---------
 * throttle Position | current accelerator pedal position
 * engine speed | current engine speed
 *
 * \section outputs Outputs
 * name | meaning
 * -----|---------
 * DriveTorque | engien torque
 *@}
 * @} */

#ifndef ENGINE_H
#define ENGINE_H

#include <vector>

#include <QtGlobal>

#include "common/commonTools.h"
#include "common/vector2d.h"
#include "units.h"
/*!
 * \copydoc Engine
 * \ingroup Engine
 */
class Engine
{
public:
  Engine(units::power::watt_t powerEngine,
         units::frequency::hertz_t speedEngine,
         units::torque::newton_meter_t torqueEngineMax);

  /*!
   * \brief Calculate of engine torque
   *
   * @param[in]    throttlePosition     acceleration pedal position [%]
   * @param[in]     engineSpeed         current engine speed
   */
  void CalculateDriveTorque(double throttlePosition, units::frequency::hertz_t engineSpeed);

  /*!
   * \brief Get Method for drive torque
   *
   *
   */
  units::torque::newton_meter_t GetDriveTorque();

private:
  units::power::watt_t powerEngineLimit;            //!< Power limit of engine
  units::frequency::hertz_t speedEngineLimit;       //!< Speed limit of engine
  units::torque::newton_meter_t torqueEngineLimit;  //!< Torque limit of engine
  units::torque::newton_meter_t driveTorque;        //!< current engine torque
};

#endif