/********************************************************************************
 * Copyright (c) 2023 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "transmission.h"

Transmission::Transmission(std::vector<double> gearRatioInit, double axleRatioInit)
{
  gearRatio = gearRatioInit;
  axleRatio = axleRatioInit;
}

double Transmission::GetGearRatio(int gear)
{
  if (gearRatio.size() >= gear)
    return gearRatio[gear - 1] * axleRatio;
  else
    return 0;
}

units::frequency::hertz_t Transmission::GetEngineSpeed(units::frequency::hertz_t speedtransmission, int gear)
{
  if (gearRatio.size() >= gear)
    return gearRatio[gear - 1] * axleRatio * speedtransmission;
  else
    return 0_Hz;
}