/********************************************************************************
 * Copyright (c) 2023 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/** \addtogroup Vehicle_Dynamics
 *@{
 * \addtogroup Powertrain
 * \brief  This module represents a vehicle powertrain.
 *
 * \details The module represents the powertrain. It consists of two submodules.
 * The gearbox translates torque and torque based on the gear engaged.
 * The engine generates the required torque based on its characteristics and accelerator pedal position.
 * The drive type can be selected via an external parameter
 *
 * \section inputs Inputs
 * name | meaning
 * -----|---------
 * WheelRotationRate | current rotation rate of each wheel
 * Longitudinal Signal | current acceleration pedal position of vehicle
 *
 * \section outputs Outputs
 * name | meaning
 * -----|---------
 * DriveTorqueTires | current drive torque of each tire
 *
 * @} */

#pragma once

#include <map>
#include <unordered_map>

#include "common/componentPorts.h"
#include "common/longitudinalSignal.h"
#include "common/primitiveSignals.h"
#include "common/vector2d.h"
#include "common/vectorSignals.h"
#include "components/common/vehicleProperties.h"
#include "engine.h"
#include "include/modelInterface.h"
#include "include/parameterInterface.h"
#include "transmission.h"

using namespace Common;

/*!
 * \copydoc Powertrain
 * \ingroup Powertrain
 */
class ActionPowertrain : public AlgorithmInterface
{
public:
    const std::string COMPONENTNAME = "Powertrain";

    ActionPowertrain(
        std::string componentName,
        bool isInit,
        int priority,
        int offsetTime,
        int responseTime,
        int cycleTime,
        StochasticsInterface *stochastics,
        const ParameterInterface *parameters,
        PublisherInterface *const publisher,
        const CallbackInterface *callbacks,
        AgentInterface *agent);

    /*!
     * \brief Holds the signal if there are other with higher priority.
     *
     * @param[in]    signal     Signal.
     * @param[in]     time       Timestamp in milliseconds.
     */
    void UpdateInput(int localLinkId, const std::shared_ptr<SignalInterface const> &signal, int time);

    ~ActionPowertrain();
    /*!
     * \brief Changes the signal to a signal on hold if there is still on on hold in the current time step.
     *
     * @param[in]    signal     Signal.
     * @param[in]     time       Timestamp in milliseconds.
     */
    void UpdateOutput(int localLinkId, std::shared_ptr<SignalInterface const> &signal, int time);

    void Trigger(int time);

private:
    /*!
     * \brief Translates the string/int parameter representation to the internally used representation of priorities
     *
     * \param[in] priorities   map linking moduleIds to priorities (larger = higher priority)
     */

    std::string TypeDrivetrain; //!< type of drivetrain
    double FrontRatioAWD;       //!< torque ratio AWD

    LongitudinalSignal longitudinalSignal; //!< input for acceleration pedal position
    std::vector<units::frequency::hertz_t> wheelRotationRate; //!< input for wheel rotation rate

    std::map<int, ComponentPort *> outputPorts; //!< map for all OutputPort

    OutputPort<SignalVectorDouble, std::vector<double>> DriveTorqueTires{0, &outputPorts}; //!< Output port for drive torque of each tire (id=0)

    Engine *engine;             //!< Engine submodule
    Transmission *transmission; //!< Transmission submodule
};
