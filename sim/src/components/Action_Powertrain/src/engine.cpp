/********************************************************************************
 * Copyright (c) 2023 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "engine.h"

#include <cmath>

#include <QFile>

Engine::Engine(units::power::watt_t powerEngine,
               units::frequency::hertz_t speedEngine,
               units::torque::newton_meter_t torqueEngine)
{
  powerEngineLimit = powerEngine;
  speedEngineLimit = speedEngine;
  torqueEngineLimit = torqueEngine;
}

void Engine::CalculateDriveTorque(double throttlePedal, units::frequency::hertz_t engineSpeed)
{
  units::torque::newton_meter_t torqueEngineMax;
  if (!qFuzzyIsNull(engineSpeed.value()))
  {
    torqueEngineMax = powerEngineLimit / engineSpeed;
  }
  else
  {
    torqueEngineMax = powerEngineLimit / 0.001_Hz;
  }

  units::torque::newton_meter_t currentTorqueEngineLimit = torqueEngineLimit;
  if (engineSpeed >= 0.98 * speedEngineLimit)
  {
    currentTorqueEngineLimit
        = torqueEngineLimit - torqueEngineLimit * (1 - (speedEngineLimit - engineSpeed) / speedEngineLimit);
  }
  torqueEngineMax = std::clamp(torqueEngineMax.value(), 0.0, currentTorqueEngineLimit.value()) * 1_Nm;

  driveTorque = throttlePedal * torqueEngineMax;
}

units::torque::newton_meter_t Engine::GetDriveTorque()
{
  return driveTorque;
}