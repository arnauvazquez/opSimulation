/********************************************************************************
 * Copyright (c) 2020 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/** \addtogroup SensorFusionErrorless
 * @{
 * \brief This file models the SensorFusionErrorless.
 *
 * \details This file models the SensorFusionErrorless which can be part of an agent.
 *          This module gets OSI SensorData of the SensorAggregation and combines all
 *          object with the same id into one.
 *
 * \section SensorFusionErrorless_Inputs Inputs
 * Input variables:
 * name | meaning
 * -----|------
 * sensorData | SensorData of a single sensor.
 *
 * Input channel IDs:
 * Input Id | signal class | contained variables
 * ----------|--------------|-------------
 *  0 		| SensorDataSignal  | sensorData
 *
 * \section SensorFusionErrorless_Outputs Outputs
 * Output variables:
 * name | meaning
 * -----|------
 * out_sensorData | Combined SensorData from all sensors.
 *
 * Output channel IDs:
 * Output Id | signal class | contained variables
 * ----------|--------------|-------------
 *  0 		| SensorDataSignal  | out_sensorData
 *
 * @} */

#pragma once

#include <osi3/osi_sensordata.pb.h>

#include "common/sensorDataSignal.h"
#include "include/modelInterface.h"

//-----------------------------------------------------------------------------
/** \brief This class is the SensorFusionErrorless module.
 * 	\details This class contains all logic regarding the sensor fusion.
 *
 * 	\ingroup SensorFusionErrorless
 */
//-----------------------------------------------------------------------------
class SensorFusionErrorlessImplementation : public UnrestrictedModelInterface
{
public:
  /// Name of this component
  const std::string COMPONENTNAME = "SensorFusion";

  /**
   * @brief Construct a new Sensor Fusion Errorless Implementation object
   *
   * \param [in] componentName   Name of the component
   * \param [in] isInit          Query whether the component was just initialized
   * \param [in] priority        Priority of the component
   * \param [in] offsetTime      Offset time of the component
   * \param [in] responseTime    Response time of the component
   * \param [in] cycleTime       Cycle time of this components trigger task [ms]
   * \param [in] stochastics     Provides access to the stochastics functionality of the framework
   * \param [in] world           Provides access to world representation
   * \param [in] parameters      Interface provides access to the configuration parameters
   * \param [in] publisher       Instance  provided by the framework
   * \param [in] callbacks       Interface for callbacks to framework
   * \param [in] agent           This interface provides access to agent parameters, properties, attributes and dynamic states
   */
  SensorFusionErrorlessImplementation(std::string componentName,
                                      bool isInit,
                                      int priority,
                                      int offsetTime,
                                      int responseTime,
                                      int cycleTime,
                                      StochasticsInterface *stochastics,
                                      WorldInterface *world,
                                      const ParameterInterface *parameters,
                                      PublisherInterface *const publisher,
                                      const CallbackInterface *callbacks,
                                      AgentInterface *agent);

  SensorFusionErrorlessImplementation(const SensorFusionErrorlessImplementation &) = delete;
  SensorFusionErrorlessImplementation(SensorFusionErrorlessImplementation &&) = delete;
  SensorFusionErrorlessImplementation &operator=(const SensorFusionErrorlessImplementation &) = delete;
  SensorFusionErrorlessImplementation &operator=(SensorFusionErrorlessImplementation &&) = delete;
  virtual ~SensorFusionErrorlessImplementation() = default;

  /*!
   * \brief Update Inputs
   *
   * Function is called by framework when another component delivers a signal over
   * a channel to this component (scheduler calls update taks of other component).
   *
   * Refer to module description for input channels and input ids.
   *
   * \param[in]     localLinkId    Corresponds to "id" of "ComponentInput"
   * \param[in]     data           Referenced signal (copied by sending component)
   * \param[in]     time           Current scheduling time
   */
  void UpdateInput(int localLinkId, const std::shared_ptr<SignalInterface const> &data, int time) override;

  /*!
   * \brief Update outputs.
   *
   * Function is called by framework when this Component.has to deliver a signal over
   * a channel to another component (scheduler calls update task of this component).
   *
   * Refer to module description for output channels and output ids.
   *
   * \param[in]     localLinkId    Corresponds to "id" of "ComponentOutput"
   * \param[out]    data           Referenced signal (copied by this component)
   * \param[in]     time           Current scheduling time
   */
  void UpdateOutput(int localLinkId, std::shared_ptr<SignalInterface const> &data, int time) override;

  /*!
   * \brief Process data within component.
   *
   * Function is called by framework when the scheduler calls the trigger task
   * of this component.
   *
   * Refer to module description for information about the module's task.
   *
   * \param[in]     time           Current scheduling time
   */
  void Trigger(int time) override;

private:
  void MergeSensorData(const osi3::SensorData &in_SensorData);

  osi3::SensorData out_sensorData;
};
