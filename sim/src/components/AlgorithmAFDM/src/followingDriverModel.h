/********************************************************************************
 * Copyright (c) 2018-2019 AMFD GmbH
 *               2018-2019 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
/**********************************************
***********************************************/

//! @file  followingDriverModel.h
//! \brief This file contains the implementation header file.

#pragma once

#include "common/lateralSignal.h"
#include "common/primitiveSignals.h"
#include "components/Sensor_Driver/src/Signals/sensorDriverSignal.h"
#include "include/callbackInterface.h"
#include "include/modelInterface.h"
#include "include/parameterInterface.h"

class AgentInterface;
class StochasticsInterface;
class ParameterInterface;
class WorldInterface;

//! Calulaction parameters for the lateral acceleration wish [m/s].
struct LateralDynamicConstants
{
  //! Typical acceleration for a lane change [m/s].
  const units::acceleration::meters_per_second_squared_t lateralAcceleration{1.5};
  //! Typical lateral damping for a lane change [m/s].
  const double zeta = 1.0;
  //! aggressiveness of the controller for heading errors
  const units::frequency::hertz_t gainHeadingError{7.5};
};

/**
 * @brief Implementation of algorithm for agent following driver model
 *
 */
class AlgorithmAgentFollowingDriverModelImplementation : public SensorInterface
{
public:
  /// Name of this component
  const std::string COMPONENTNAME = "AlgorithmAgentFollowingDriverModel";

  //! \brief Constructor.
  //!
  //! \param [in] componentName   Component ID
  //! \param [in] isInit          Component's init state
  //! \param [in] priority        Task priority level
  //! \param [in] offsetTime      Start time offset
  //! \param [in] responseTime    Update response time
  //! \param [in] cycleTime       Cycle time
  //! \param [in] stochastics     Stochastics instance
  //! \param [in] world           World interface
  //! \param [in] parameters      Paramaters
  //! \param [in] publisher       Publisher instance
  //! \param [in] callbacks       Callbacks
  //! \param [in] agent           Agent
  AlgorithmAgentFollowingDriverModelImplementation(std::string componentName,
                                                   bool isInit,
                                                   int priority,
                                                   int offsetTime,
                                                   int responseTime,
                                                   int cycleTime,
                                                   StochasticsInterface *stochastics,
                                                   WorldInterface *world,
                                                   const ParameterInterface *parameters,
                                                   PublisherInterface *const publisher,
                                                   const CallbackInterface *callbacks,
                                                   AgentInterface *agent);

  AlgorithmAgentFollowingDriverModelImplementation(const AlgorithmAgentFollowingDriverModelImplementation &) = delete;
  AlgorithmAgentFollowingDriverModelImplementation(AlgorithmAgentFollowingDriverModelImplementation &&) = delete;
  AlgorithmAgentFollowingDriverModelImplementation &operator=(const AlgorithmAgentFollowingDriverModelImplementation &)
      = delete;
  AlgorithmAgentFollowingDriverModelImplementation &operator=(AlgorithmAgentFollowingDriverModelImplementation &&)
      = delete;
  virtual ~AlgorithmAgentFollowingDriverModelImplementation();

  /*!
   * \brief Update Inputs
   *
   * Function is called by framework when another component delivers a signal over
   * a channel to this component (scheduler calls update taks of other component).
   *
   * Refer to module description for input channels and input ids.
   *
   * @param[in]     localLinkId    Corresponds to "id" of "ComponentInput"
   * @param[in]     data           Referenced signal (copied by sending component)
   * @param[in]     time           Current scheduling time
   */
  void UpdateInput(int localLinkId, const std::shared_ptr<SignalInterface const> &data, int time);

  /*!
   * \brief Update outputs.
   *
   * Function is called by framework when this Component.has to deliver a signal over
   * a channel to another component (scheduler calls update task of this component).
   *
   * Refer to module description for output channels and output ids.
   *
   * @param[in]     localLinkId    Corresponds to "id" of "ComponentOutput"
   * @param[out]    data           Referenced signal (copied by this component)
   * @param[in]     time           Current scheduling time
   */
  void UpdateOutput(int localLinkId, std::shared_ptr<SignalInterface const> &data, int time);

  /*!
   * \brief Process data within component.
   *
   * Function is called by framework when the scheduler calls the trigger task
   * of this component
   *
   * @param[in]     time           Current scheduling time
   */
  void Trigger(int time);

private:
  OwnVehicleInformation ownVehicleInformation;

  GeometryInformation geometryInformation;

  SurroundingObjects surroundingObjects;

  //! component state for finely granulated evaluation of signal
  ComponentState componentState = ComponentState::Acting;

  //! parameters for the lateral acceleration wish [m/s]
  const LateralDynamicConstants lateralDynamicConstants;

  // constants from IDM paper
  //! desired speed
  units::velocity::kilometers_per_hour_t vWish{120.0};
  //! free acceleration exponent characterizing how the acceleration decreases with velocity (1: linear, infinity:
  //! constant)
  double delta = 4.0;
  //! desired time gap between ego and front agent
  units::time::second_t tGapWish{1.5};
  //! minimum distance between ego and front (used at slow speeds) also called jam distance
  units::length::meter_t minDistance{2.0};
  //! maximum acceleration in satisfactory way, not vehicle possible acceleration
  units::acceleration::meters_per_second_squared_t maxAcceleration{1.4};
  //! desired deceleration
  units::acceleration::meters_per_second_squared_t decelerationWish{2.0};

  LateralInformation out_lateralInformation{0_m, 0_m, 20.0_rad_per_s_sq, 0_rad, 0_Hz, 0_i_m};

  //! The lateral velocity of the current vehicle [m/s].
  units::velocity::meters_per_second_t out_lateral_speed{0};
  //! The damping constant of the lateral oscillation of the vehicle [].
  double out_lateral_damping = 0;
  //! The lateral oscillation frequency of the vehicle [1/s].
  units::frequency::hertz_t out_lateral_frequency{0};

  //! The longitudinal acceleration of the vehicle [m/s^2].
  units::acceleration::meters_per_second_squared_t out_longitudinal_acc{0};

  //! The state of the turning indicator [-].
  int out_indicatorState = static_cast<int>(IndicatorState::IndicatorState_Off);
  //! Activation of HornSwitch [-].
  bool out_hornSwitch = false;
  //! Activation of Headlight [-].
  bool out_headLight = false;
  //! Activation of Highbeam Light [-].
  bool out_highBeamLight = false;
  //! Activation of Flasher [-].
  bool out_flasher = false;
};
