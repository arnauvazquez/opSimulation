/********************************************************************************
 * Copyright (c) 2020-2021 ITK Engineering GmbH
 *               2023 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** @file  twotrackmodel.cpp */
//-----------------------------------------------------------------------------

#include "twotrackmodel.h"

#include <cmath>

#include <QFile>

TwoTrackVehicleModel::TwoTrackVehicleModel()
{
  forceTotalXY.x = 0.0_N;
  forceTotalXY.y = 0.0_N;
  momentTotalZ = 0.0_Nm;
}

TwoTrackVehicleModel::~TwoTrackVehicleModel() {}

void TwoTrackVehicleModel::SetTireForce(std::vector<double> longitudinalTireForce, std::vector<double> lateralTireForce)
{
  //! Update tire force input of two track model
  tireForce.resize(longitudinalTireForce.size());
  for (unsigned int idx = 0; idx < tireForce.size(); ++idx)
  {
    tireForce[idx].x = longitudinalTireForce[idx] * 1_N;
    tireForce[idx].y = lateralTireForce[idx] * 1_N;
  }
}

void TwoTrackVehicleModel::SetTireAngle(std::vector<double> tireAngle)
{
  //! Update tire angle input of two track model
  this->tireAngle.resize(tireAngle.size());
  for (unsigned int idx = 0; idx < this->tireAngle.size(); ++idx)
  {
    this->tireAngle[idx] = tireAngle[idx] * 1_rad;
  }
}

void TwoTrackVehicleModel::SetTireSelfAligningTorque(std::vector<double> selfAligningTorque)
{
  //! Update tire self aligning torque input of two track model
  this->wheelSelfAligningTorque.resize(selfAligningTorque.size());
  for (unsigned int idx = 0; idx < this->wheelSelfAligningTorque.size(); ++idx)
  {
    this->wheelSelfAligningTorque[idx] = selfAligningTorque[idx] * 1_Nm;
  }
}

void TwoTrackVehicleModel::InitVehicleProperties(std::vector<mantle_api::Axle> axles,
                                                 units::length::meter_t xCOG,
                                                 units::length::meter_t yCOG,
                                                 units::mass::kilogram_t weight,
                                                 double coeffDrag,
                                                 units::area::square_meter_t areaFace,
                                                 units::length::meter_t bb_center)
{
  //! Set Vehicle Mass
  this->massTotal = weight;

  //! Set Position COG in Car CS
  this->PositionCOG.x = xCOG;
  this->PositionCOG.y = yCOG;
  //! Set yaw Velocity to 0
  this->yawVelocity = 0.0_rad_per_s;
  //! init tire positions
  for (unsigned int idx = 0; idx < axles.size(); ++idx)
  {
    Common::Vector2d<units::length::meter_t> positionTire;

    positionTire.x = axles[idx].bb_center_to_axle_center.x + bb_center - PositionCOG.x;
    positionTire.y = axles[idx].track_width / 2 - yCOG;

    this->positionTire.insert(this->positionTire.begin() + 2 * idx, positionTire);

    positionTire.y = -axles[idx].track_width / 2 - yCOG;

    this->positionTire.insert(this->positionTire.begin() + (2 * idx + 1), positionTire);
  }
  //! Set coeffcieint of drag
  this->coeffDrag = coeffDrag;
  //! Set front area
  this->areaFace = areaFace;
}

void TwoTrackVehicleModel::ForceGlobal()
{
  forceTotalXY.x = 0.0_N;
  forceTotalXY.y = 0.0_N;
  momentTotalZ = 0.0_Nm;
  for (int i = 0; i < tireForce.size(); ++i)
  {
    tireForce[i].Rotate(tireAngle[i]);  // car's CS

    // total force
    forceTotalXY.Add(tireForce[i]);

    // total yaw momentum // local plane momentum (around z-axis)
    momentTotalZ += positionTire[i].Cross(tireForce[i]);
    momentTotalZ += wheelSelfAligningTorque[i];
  }

  units::angle::radian_t slideAngle = velocity.Angle();  // ISO

  // air drag

  units::force::newton_t forceAirDrag
      = -0.5 * densityAir * coeffDrag * areaFace * units::math::pow<2>(velocity.Length());
  forceTotalXY.Rotate(-slideAngle);  // traj. CS
  forceTotalXY.Add(forceAirDrag);    // traj. CS
  acceleration.Rotate(-slideAngle);

  acceleration.Scale(massTotal.value());
  //! Add forces from slide angle
  Common::Vector2d<units::force::newton_t> forces;
  forces.x = -acceleration.y.value() * units::math::sin(slideAngle) * 1_N;
  forces.y = acceleration.x.value() * units::math::sin(slideAngle) * 1_N;

  forceTotalXY.Add(forces);

  forceTotalXY.Rotate(slideAngle);  // car CS

  acceleration.Scale(1 / massTotal.value());
  acceleration.Rotate(slideAngle);
}
