/********************************************************************************
 * Copyright (c) 2023 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  dynamics_motionmodel.h
//! @brief This file contains exported interface for model construction and
//!        destruction.
//-----------------------------------------------------------------------------

#include <QtCore/qglobal.h>

#if defined(DYNAMICS_MOTIONMODEL_LIBRARY)
#define DYNAMICS_MOTIONMODEL_SHARED_EXPORT Q_DECL_EXPORT
#else
#define DYNAMICS_MOTIONMODEL_SHARED_EXPORT Q_DECL_IMPORT
#endif
