/********************************************************************************
 * Copyright (c) 2020 HLRS, University of Stuttgart
 *               2017-2020 in-tech GmbH
 *               2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  limiterAccVehCompImpl.cpp
//! @brief This file contains the implementation of the header file
//-----------------------------------------------------------------------------

#include "limiterAccVehCompImpl.h"

#include <QtGlobal>

#include "common/opMath.h"
#include "components/common/vehicleProperties.h"
#include "include/worldInterface.h"

void LimiterAccelerationVehicleComponentsImplementation::UpdateInput(int localLinkId,
                                                                     const std::shared_ptr<SignalInterface const> &data,
                                                                     int time)
{
  Q_UNUSED(time);

  if (localLinkId == 0)
  {
    const std::shared_ptr<ComponentStateSignalInterface const> stateSignal
        = std::dynamic_pointer_cast<ComponentStateSignalInterface const>(data);
    if (stateSignal->componentState == ComponentState::Acting)
    {
      const std::shared_ptr<AccelerationSignal const> signal
          = std::dynamic_pointer_cast<AccelerationSignal const>(data);
      if (!signal)
      {
        const std::string msg = GetComponentName() + " invalid signaltype";
        LOG(CbkLogLevel::Debug, msg);
        throw std::runtime_error(msg);
      }

      incomingAcceleration = signal->acceleration;
      source = signal->source;
    }
    componentState = stateSignal->componentState;
  }
  else if (localLinkId == 100)
  {
    // from ParametersAgent
    const std::shared_ptr<ParametersVehicleSignal const> signal
        = std::dynamic_pointer_cast<ParametersVehicleSignal const>(data);
    if (!signal)
    {
      const std::string msg = GetComponentName() + " invalid signaltype";
      LOG(CbkLogLevel::Debug, msg);
      throw std::runtime_error(msg);
    }
    vehicleModelParameters = signal->vehicleParameters;

    PrepareReferences();
  }
  else
  {
    const std::string msg = GetComponentName() + " invalid link";
    LOG(CbkLogLevel::Debug, msg);
    throw std::runtime_error(msg);
  }
}

void LimiterAccelerationVehicleComponentsImplementation::UpdateOutput(int localLinkId,
                                                                      std::shared_ptr<SignalInterface const> &data,
                                                                      int time)
{
  Q_UNUSED(time);

  if (localLinkId == 0)
  {
    try
    {
      data = std::make_shared<AccelerationSignal const>(componentState, outgoingAcceleration, source);
    }
    catch (const std::bad_alloc &)
    {
      const std::string msg = GetComponentName() + " could not instantiate signal";
      LOG(CbkLogLevel::Debug, msg);
      throw std::runtime_error(msg);
    }
  }

  else
  {
    const std::string msg = GetComponentName() + " invalid link";
    LOG(CbkLogLevel::Debug, msg);
    throw std::runtime_error(msg);
  }
}

void LimiterAccelerationVehicleComponentsImplementation::Trigger(int time)
{
  Q_UNUSED(time);

  const auto &accelerationLimit = CalculateAccelerationLimit();
  const auto &decelerationLimit = CalculateDecelerationLimit();

  outgoingAcceleration = units::math::max(units::math::min(incomingAcceleration, accelerationLimit), decelerationLimit);
}

double LimiterAccelerationVehicleComponentsImplementation::GetVehicleProperty(const std::string &propertyName)
{
  const auto property = helper::map::query(vehicleModelParameters.properties, propertyName);
  THROWIFFALSE(property.has_value(), "Vehicle property \"" + propertyName + "\" was not set in the VehicleCatalog");
  return std::stod(property.value());
}

units::torque::newton_meter_t LimiterAccelerationVehicleComponentsImplementation::InterpolateEngineTorqueBasedOnSpeed(
    const units::angular_velocity::revolutions_per_minute_t &engineSpeed)
{
  if (engineSpeedReferences.size() != engineTorqueReferences.size() || engineSpeedReferences.empty())
  {
    throw std::runtime_error("Interpolation requires same size for vectors and at least one element.");
  }

  if (engineSpeed <= engineSpeedReferences.front())
  {
    return engineTorqueReferences.front();
  }

  if (engineSpeed >= engineSpeedReferences.back())
  {
    return engineTorqueReferences.back();
  }

  for (size_t i = 1; i < engineSpeedReferences.size(); i++)
  {
    if (engineSpeedReferences.at(i) >= engineSpeed)
    {
      const auto differenceBetweenEngineSpeedReferencePoints
          = engineSpeedReferences.at(i) - engineSpeedReferences.at(i - 1);
      const double percentage
          = (engineSpeed - engineSpeedReferences.at(i - 1)) / differenceBetweenEngineSpeedReferencePoints;

      const auto differenceBetweenEngineTorqueReferencePoints
          = engineTorqueReferences.at(i) - engineTorqueReferences.at(i - 1);

      const units::torque::newton_meter_t interpolatedTorque
          = engineTorqueReferences.at(i - 1) + (differenceBetweenEngineTorqueReferencePoints * percentage);

      return interpolatedTorque;
    }
  }

  throw std::runtime_error("Could not interpolate torque.");
}

std::vector<units::torque::newton_meter_t>
LimiterAccelerationVehicleComponentsImplementation::PrepareEngineTorqueVectorBasedOnGearRatios()
{
  std::vector<units::torque::newton_meter_t> engineTorquesBasedOnGearRatios{};

  if (GetVehicleProperty(vehicle::properties::NumberOfGears) < 1)
  {
    throw std::runtime_error("At least on gear is required!");
  }

  for (size_t i = 1; i <= GetVehicleProperty(vehicle::properties::NumberOfGears); i++)
  {
    const auto engineSpeedBasedOnGear = CalculateEngineSpeedBasedOnGear(GetAgent()->GetVelocity().Length(), i);

    if (engineSpeedBasedOnGear > units::angular_velocity::revolutions_per_minute_t(
            GetVehicleProperty(vehicle::properties::MaximumEngineSpeed))
        || engineSpeedBasedOnGear < units::angular_velocity::revolutions_per_minute_t(
               GetVehicleProperty(vehicle::properties::MinimumEngineSpeed)))
    {
      continue;
    }

    const auto interpolatedEngineTorque = InterpolateEngineTorqueBasedOnSpeed(engineSpeedBasedOnGear);

    const units::torque::newton_meter_t engineTorqueBasedOnGearRatio
        = interpolatedEngineTorque * GetVehicleProperty(vehicle::properties::GearRatio + std::to_string(i));
    engineTorquesBasedOnGearRatios.push_back(engineTorqueBasedOnGearRatio);
  }

  return engineTorquesBasedOnGearRatios;
}

units::angular_velocity::revolutions_per_minute_t
LimiterAccelerationVehicleComponentsImplementation::CalculateEngineSpeedBasedOnGear(
    const units::velocity::meters_per_second_t &currentVelocity, const size_t &gear)
{
  const units::angular_velocity::revolutions_per_minute_t engineSpeed
      = 1_rad * currentVelocity * GetVehicleProperty(vehicle::properties::AxleRatio)
      * GetVehicleProperty(vehicle::properties::GearRatio + std::to_string(gear))
      / (0.5 * vehicleModelParameters.rear_axle.wheel_diameter);
  return engineSpeed;
}

void LimiterAccelerationVehicleComponentsImplementation::PrepareReferences()
{
  const auto &maxEngineTorque
      = units::torque::newton_meter_t(GetVehicleProperty(vehicle::properties::MaximumEngineTorque));
  const auto &maxEngineSpeed
      = units::angular_velocity::revolutions_per_minute_t(GetVehicleProperty(vehicle::properties::MaximumEngineSpeed));
  const auto &minEngineSpeed
      = units::angular_velocity::revolutions_per_minute_t(GetVehicleProperty(vehicle::properties::MinimumEngineSpeed));

  engineTorqueReferences = {0.5 * maxEngineTorque,
                            1.0 * maxEngineTorque,
                            1.0 * maxEngineTorque,
                            1.0 * maxEngineTorque,
                            1.0 * maxEngineTorque / maxEngineSpeed * 5000.0_rpm};

  engineSpeedReferences = {minEngineSpeed, 1350_rpm, 4600_rpm, 5000_rpm, maxEngineSpeed};
}

units::acceleration::meters_per_second_squared_t
LimiterAccelerationVehicleComponentsImplementation::CalculateAccelerationLimit()
{
  const auto currentVelocity = GetAgent()->GetVelocity().Length();

  const auto &engineTorquesBasedOnGearRatios = PrepareEngineTorqueVectorBasedOnGearRatios();

  if (engineTorquesBasedOnGearRatios.empty())
  {
    return oneG;
  }

  const auto &engineTorqueBasedOnVelocity
      = *(std::max_element(engineTorquesBasedOnGearRatios.begin(), engineTorquesBasedOnGearRatios.end()));

  const units::force::newton_t forceAtWheel = engineTorqueBasedOnVelocity
                                            * GetVehicleProperty(vehicle::properties::AxleRatio)
                                            / (0.5 * vehicleModelParameters.rear_axle.wheel_diameter);
  const units::force::newton_t forceRoll = vehicleModelParameters.mass * oneG * rollFrictionCoefficient;
  const units::force::newton_t forceAir
      = (airResistance / 2) * units::area::square_meter_t(GetVehicleProperty(vehicle::properties::FrontSurface))
      * GetVehicleProperty(vehicle::properties::AirDragCoefficient) * units::math::pow<2>(currentVelocity);

  const units::acceleration::meters_per_second_squared_t accelerationLimit
      = (forceAtWheel - forceRoll - forceAir) / vehicleModelParameters.mass;

  return accelerationLimit;
}

units::acceleration::meters_per_second_squared_t
LimiterAccelerationVehicleComponentsImplementation::CalculateDecelerationLimit()
{
  const units::acceleration::meters_per_second_squared_t decelerationLimit
      = GetWorld()->GetFriction() * GetVehicleProperty(vehicle::properties::FrictionCoefficient) * (-oneG);

  return decelerationLimit;
}
