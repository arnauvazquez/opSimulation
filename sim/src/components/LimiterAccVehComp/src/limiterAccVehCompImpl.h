/********************************************************************************
 * Copyright (c) 2020 HLRS, University of Stuttgart
 *               2017-2020 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/** \addtogroup LimiterAccelerationVehicleComponents
 * @{
 * \brief Limits the acceleration output from all vehicle components
 *
 * @} */

#pragma once

#include "common/accelerationSignal.h"
#include "common/opMath.h"
#include "common/parametersVehicleSignal.h"
#include "include/modelInterface.h"

/**
 * \brief This class implements the set limit
 */
class LimiterAccelerationVehicleComponentsImplementation : public UnrestrictedModelInterface
{
public:
  /**
   * @brief Construct a new Limiter Acceleration Vehicle Components Implementation object
   *
   * @param[in]     componentName  Name of the component
   * @param[in]     isInit         Corresponds to "init" of "Component"
   * @param[in]     priority       Corresponds to "priority" of "Component"
   * @param[in]     offsetTime     Corresponds to "offsetTime" of "Component"
   * @param[in]     responseTime   Corresponds to "responseTime" of "Component"
   * @param[in]     cycleTime      Corresponds to "cycleTime" of "Component"
   * @param[in]     stochastics    Pointer to the stochastics class loaded by the framework
   * @param[in]     world          Pointer to the world interface
   * @param[in]     parameters     Pointer to the parameters of the module
   * @param[in]     publisher      Pointer to the publisher instance
   * @param[in]     callbacks      Pointer to the callbacks
   * @param[in]     agent          Pointer to agent instance
   */
  LimiterAccelerationVehicleComponentsImplementation(std::string componentName,
                                                     bool isInit,
                                                     int priority,
                                                     int offsetTime,
                                                     int responseTime,
                                                     int cycleTime,
                                                     StochasticsInterface *stochastics,
                                                     WorldInterface *world,
                                                     const ParameterInterface *parameters,
                                                     PublisherInterface *const publisher,
                                                     const CallbackInterface *callbacks,
                                                     AgentInterface *agent)
      : UnrestrictedModelInterface(componentName,
                                   isInit,
                                   priority,
                                   offsetTime,
                                   responseTime,
                                   cycleTime,
                                   stochastics,
                                   world,
                                   parameters,
                                   publisher,
                                   callbacks,
                                   agent)
  {
  }
  LimiterAccelerationVehicleComponentsImplementation(const LimiterAccelerationVehicleComponentsImplementation &)
      = delete;
  LimiterAccelerationVehicleComponentsImplementation(LimiterAccelerationVehicleComponentsImplementation &&) = delete;
  LimiterAccelerationVehicleComponentsImplementation &operator=(
      const LimiterAccelerationVehicleComponentsImplementation &)
      = delete;
  LimiterAccelerationVehicleComponentsImplementation &operator=(LimiterAccelerationVehicleComponentsImplementation &&)
      = delete;
  virtual ~LimiterAccelerationVehicleComponentsImplementation() = default;

  /*!
   * \brief Update Inputs
   *
   * Function is called by framework when another component delivers a signal over
   * a channel to this component (scheduler calls update taks of other component).
   *
   * Refer to module description for input channels and input ids.
   *
   * @param[in]     localLinkId    Corresponds to "id" of "ComponentInput"
   * @param[in]     data           Referenced signal (copied by sending component)
   * @param[in]     time           Current scheduling time
   */
  void UpdateInput(int localLinkId, const std::shared_ptr<SignalInterface const> &data, int time) override;

  /*!
   * \brief Update outputs.
   *
   * Function is called by framework when this component has to deliver a signal over
   * a channel to another component (scheduler calls update task of this component).
   *
   * Refer to module description for output channels and output ids.
   *
   * @param[in]     localLinkId    Corresponds to "id" of "ComponentOutput"
   * @param[out]    data           Referenced signal (copied by this component)
   * @param[in]     time           Current scheduling time
   */
  void UpdateOutput(int localLinkId, std::shared_ptr<SignalInterface const> &data, int time) override;

  /*!
   * \brief Process data within component.
   *
   * Function is called by framework when the scheduler calls the trigger task
   * of this component
   *
   * @param[in]     time           Current scheduling time
   */
  void Trigger(int time) override;

private:
  double GetVehicleProperty(const std::string &propertyName);

  units::torque::newton_meter_t InterpolateEngineTorqueBasedOnSpeed(
      const units::angular_velocity::revolutions_per_minute_t &engineSpeed);

  std::vector<units::torque::newton_meter_t> PrepareEngineTorqueVectorBasedOnGearRatios();

  units::angular_velocity::revolutions_per_minute_t CalculateEngineSpeedBasedOnGear(
      const units::velocity::meters_per_second_t &currentVelocity, const size_t &gear);

  void PrepareReferences();
  units::acceleration::meters_per_second_squared_t CalculateAccelerationLimit();
  units::acceleration::meters_per_second_squared_t CalculateDecelerationLimit();

  ComponentState componentState{ComponentState::Armed};

  const double twoPI{2.0 * M_PI};
  const units::acceleration::meters_per_second_squared_t oneG{9.81};  //mps^2
  const double rollFrictionCoefficient{0.015};
  const units::density::kilograms_per_cubic_meter_t airResistance{1.2};

  mantle_api::VehicleProperties vehicleModelParameters;

  std::vector<units::torque::newton_meter_t> engineTorqueReferences;
  std::vector<units::angular_velocity::revolutions_per_minute_t> engineSpeedReferences;

  units::acceleration::meters_per_second_squared_t incomingAcceleration{0.0};
  units::acceleration::meters_per_second_squared_t outgoingAcceleration{0.0};

  std::string source;
};
