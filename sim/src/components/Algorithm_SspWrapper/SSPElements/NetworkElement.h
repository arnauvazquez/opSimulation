/*******************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <memory>
#include <utility>
#include <vector>

#include "Connector/ConnectorInterface.h"

namespace ssp
{

class SspNetworkVisitorInterface;

/// Class representing an element for visitable network
class VisitableNetworkElement
{
public:
  virtual ~VisitableNetworkElement() = default;

  virtual void Accept(SspNetworkVisitorInterface&) = 0;  ///< Accept an SspNetworkVisitorInterface object

  /// @return Return priority of the network element
  virtual int Priority() = 0;

  /// @return Return Name of the network element
  virtual std::string GetName() = 0;

  /// @return Return list of references to the VisitableNetworkElement
  virtual const std::vector<std::shared_ptr<VisitableNetworkElement>>& GetElements() = 0;

  /// @return Return list of references to the input connector interface
  virtual std::vector<std::shared_ptr<ConnectorInterface>> GetInputConnectors() = 0;

  /// @return Return list of references to the output connector interface
  virtual std::vector<std::shared_ptr<ConnectorInterface>> GetOutputConnectors() = 0;

  /// @brief Log the information
  /// @param logLevel Log level for the log callback
  /// @param file     File Name
  /// @param line     Line
  /// @param message  Message to log
  void Log(CbkLogLevel logLevel, const char* file, int line, const std::string& message) const;
};

/// abstract
class NetworkElement : public VisitableNetworkElement
{
public:
  /// @brief Create a network element object
  /// @param elementName          Name of the element
  /// @param elements             List of references to the ssp VisitableNetworkElement
  /// @param input_connectors     List of references to the input ConnectorInterface
  /// @param output_connectors    List of references to the output ConnectorInterface
  NetworkElement(const std::string& elementName,
                 std::vector<std::shared_ptr<ssp::VisitableNetworkElement>>&& elements,
                 std::vector<std::shared_ptr<ssp::ConnectorInterface>>&& input_connectors,
                 std::vector<std::shared_ptr<ssp::ConnectorInterface>>&& output_connectors);

  /// @brief Create a network element object
  /// @param elementName Name of the element
  explicit NetworkElement(std::string elementName);

  ~NetworkElement() override = default;

  /// @return Return name of the network element
  std::string GetName() override;

  /// @brief Emplace an element to the network element
  /// @param element A reference to the visitable network element
  void EmplaceElement(std::shared_ptr<VisitableNetworkElement>&& element);

  /// @return Return list of references to the VisitableNetworkElement
  const std::vector<std::shared_ptr<VisitableNetworkElement>>& GetElements() override;

  /// @return Return list of references to the input connector interface
  std::vector<std::shared_ptr<ConnectorInterface>> GetInputConnectors() override;

  /// @return Return list of references to the output connector interface
  std::vector<std::shared_ptr<ConnectorInterface>> GetOutputConnectors() override;

  /// @brief Log the information
  /// @param logLevel Log level for the log callback
  /// @param file     File Name
  /// @param line     Line
  /// @param message  Message to log
  void Log(CbkLogLevel logLevel, const char* file, int line, const std::string& message) const;

  std::vector<std::shared_ptr<VisitableNetworkElement>> elements
      = {};  ///< List of references to the VisitableNetworkElement
  std::vector<std::shared_ptr<ssp::ConnectorInterface>> input_connectors = {};   ///< connectorName, connector
  std::vector<std::shared_ptr<ssp::ConnectorInterface>> output_connectors = {};  ///< non standard, connector

  std::string elementName;                ///< Name of the element
  std::filesystem::path outputDirectory;  ///< Name of the output directory
};

}  //namespace ssp
