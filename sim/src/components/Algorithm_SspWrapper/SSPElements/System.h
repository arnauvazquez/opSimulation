/*******************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <memory>
#include <optional>
#include <utility>
#include <vector>

#include "Connection.h"
#include "sim/src/components/Algorithm_FmuWrapper/src/fmuFileHelper.h"
#include "sim/src/components/Algorithm_SspWrapper/SSPElements/Connector/GroupConnector.h"
#include "sim/src/components/Algorithm_SspWrapper/SSPElements/Enumeration/Enumeration.h"
#include "sim/src/components/Algorithm_SspWrapper/SSPElements/NetworkElement.h"

namespace ssp
{
class SspNetworkVisitorInterface;

class System : public NetworkElement  ///< Class representing the system
{
public:
  /// @brief Accept an interface
  /// @param visitorInterface SSP network visitor interface
  void Accept(SspNetworkVisitorInterface& visitorInterface) override;

  /// @return Priority of the system
  int Priority() override;

  /// @param systemName Create a system with the systemname
  explicit System(const std::string& systemName);

  /// @brief Create a system
  /// @param elementName          Name of the element
  /// @param elements             List of references to the visitable network element
  /// @param input_connectors     List of references to the SSP input connector interface
  /// @param output_connectors    List of references to the SSP output connector interface
  /// @param targetToTracesMap    Reference to the target trace entry
  System(const std::string& elementName,
         std::vector<std::shared_ptr<ssp::VisitableNetworkElement>>&& elements,
         std::vector<std::shared_ptr<ssp::ConnectorInterface>>&& input_connectors,
         std::vector<std::shared_ptr<ssp::ConnectorInterface>>&& output_connectors,
         std::shared_ptr<std::map<std::string, FmuFileHelper::TraceEntry>> targetToTracesMap);

  /// @brief Create a system
  /// @param elementName          Name of the element
  /// @param elements             List of references to the visitable network element
  /// @param targetToTracesMap    Reference to the target trace entry
  System(const std::string& elementName,
         std::vector<std::shared_ptr<ssp::VisitableNetworkElement>>&& elements,
         std::shared_ptr<std::map<std::string, FmuFileHelper::TraceEntry>> targetToTracesMap);

  ~System() override;

  /// @brief Manage a system connection
  /// @param connection  A reference to the OSMP Connection
  void ManageConnection(const OSMPConnection& connection);

  /// @brief Manage a system connection
  /// @param connection  A reference to the Connection
  void ManageConnection(const Connection& connection);

  /// @brief Set an output directory
  /// @param outDir Output directory path
  void SetOutputDir(const std::filesystem::path& outDir);

  /// @return REturn output directory path
  const std::filesystem::path& GetOutputDir();

  std::vector<std::shared_ptr<ConnectorInterface>> GetInputConnectors() override { return {systemInputConnector}; }
  std::vector<std::shared_ptr<ConnectorInterface>> GetOutputConnectors() override { return {systemOutputConnector}; }

  /// @brief Set enumeration for the network element
  /// @param  enumerations Pointer to the list of enumeration pointers
  void SetEnumerations(std::shared_ptr<std::vector<std::shared_ptr<Enumeration>>>&);

  /// @brief If the network element has enumeration
  /// @param name Name of the network element
  /// @return True, if the element has enumeration
  bool HasEnumeration(const std::string name);

  std::shared_ptr<GroupConnector> systemInputConnector
      = std::make_shared<GroupConnector>();  ///< A pointer to the system input connector
  std::shared_ptr<GroupConnector> systemOutputConnector
      = std::make_shared<GroupConnector>();  ///< A pointer to the system output connector
  std::shared_ptr<std::vector<Enumeration>> enumerations
      = std::make_shared<std::vector<Enumeration>>();  ///< A pointer to the list of enumerations

private:
  void ConnectInOutConnectors(const Connection& connection);
  void ConnectSystemWithStartConnector(const Connection& connection);
  void ConnectSystemWithEndConnector(const Connection& connection);
  ssp::NetworkElement* FindNetworkElement(const std::string& componentName);

  OSMPConnectionCompletenessCheck oSMPConnectionCompletenessCheck{};
  std::shared_ptr<std::map<std::string, FmuFileHelper::TraceEntry>> targetToTracesMap{};
  std::optional<std::filesystem::path> outputDir;
};

}  //namespace ssp