/********************************************************************************
 * Copyright (c) 2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once
#include <memory>
#include <string>
#include <vector>

namespace ssp
{

/// @brief Class representing a enumerator item
struct EnumeratorItem
{
  std::string name;  ///< Name of the enumerator item
  int value;         ///< Value of the enumerator item
};

/// @brief Class representing a enumeration
class Enumeration
{
public:
  Enumeration();

  /// @brief Construct an enumeration
  /// @param items    Pointer to the list of enumerator items
  Enumeration(std::string, std::shared_ptr<std::vector<EnumeratorItem>> items);
  ~Enumeration();
  std::string name;                                    ///< Name of the enumeration
  std::shared_ptr<std::vector<EnumeratorItem>> items;  ///< Pointer to the list of enumerator items
};

}  //namespace ssp