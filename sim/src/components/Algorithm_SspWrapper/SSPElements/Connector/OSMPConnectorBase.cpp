/*******************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "OSMPConnectorBase.h"

void ssp::OSMPConnectorBase::Accept(ssp::ConnectorVisitorInterface &visitor)
{
  visitor.Visit(this);
}
std::shared_ptr<FmuWrapperInterface> ssp::OSMPConnectorBase::GetFmuWrapperInterface()
{
  return base_lo->fmuWrapperInterface;
}
const std::string &ssp::OSMPConnectorBase::GetConnectorName() const
{
  return connectorName;
}
const std::string &ssp::OSMPConnectorBase::GetOsmpLinkName() const
{
  return osmpLinkName;
}

ssp::OSMPConnectorBase::OSMPConnectorBase(std::unique_ptr<ScalarConnector<VariableTypeInt>> base_lo,
                                          std::unique_ptr<ScalarConnector<VariableTypeInt>> base_hi,
                                          std::unique_ptr<ScalarConnector<VariableTypeInt>> size,
                                          int priority,
                                          std::string variableReference,
                                          std::string osmpLinkName)
    : osmpLinkName(std::move(osmpLinkName)),
      connectorName(std::move(variableReference)),
      base_lo(std::move(base_lo)),
      base_hi(std::move(base_hi)),
      size(std::move(size))
{
}

ssp::OSMPConnectorBase::OSMPConnectorBase(const std::string &connectorName,
                                          const std::string &osmpLinkName,
                                          const std::shared_ptr<FmuWrapperInterface> &fmuWrapper,
                                          int fmuPriority)
    : OSMPConnectorBase(
        std::make_unique<ScalarConnector<VariableTypeInt>>(fmuWrapper, osmpLinkName + ".base.lo", fmuPriority),
        std::make_unique<ScalarConnector<VariableTypeInt>>(fmuWrapper, osmpLinkName + ".base.hi", fmuPriority),
        std::make_unique<ScalarConnector<VariableTypeInt>>(fmuWrapper, osmpLinkName + ".size", fmuPriority),
        fmuPriority,
        connectorName,
        osmpLinkName)
{
}
