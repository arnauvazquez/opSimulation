/*******************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Connector.h"

void ssp::Connector::Log(CbkLogLevel logLevel, const char *file, int line, const std::string &message) const
{
  SspLogger::Log(logLevel, file, line, message);
}
ssp::Connector::Connector(const int &priority) : priority(priority) {}

std::vector<std::shared_ptr<ssp::ConnectorInterface>> ssp::Connector::GetNonParameterConnectors() const
{
  auto trimmedConnectors = std::vector<std::shared_ptr<ConnectorInterface>>();
  for (const auto &connector : connectors)
  {
    if (!connector->IsParameterConnector()) trimmedConnectors.push_back(connector);
  }
  return trimmedConnectors;
}
