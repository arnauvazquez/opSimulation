/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * https://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <components/Algorithm_FmuWrapper/src/variant_visitor.h>
#include <fstream>
#include <utility>

#include <queue>

#include "ParserTypes.h"
#include "components/Algorithm_FmuWrapper/src/fmuFileHelper.h"
#include "include/fmuHandlerInterface.h"
#include "include/fmuWrapperInterface.h"
#include "sim/src/components/Algorithm_SspWrapper/SSPElements/Component.h"
#include "type_helper.h"

enum class RegisteredConnectorDirection;

/// Class representing a factory for scalar connector
class ScalarConnectorFactory
{
  struct ScalarData
  {
    std::string connectorName{};
    std::shared_ptr<FmuWrapperInterface> fmuWrapperInterface{};
    int priority{};
    ConnectorType connectorType{};
    bool isParameter = false;
    ScalarData() = default;
  };

  struct ScalarConnectorBlueprint
  {
    ScalarData data;
    std::vector<std::pair<std::string, std::string>> writeMessageParameters;
    std::filesystem::path outputDir;
    std::shared_ptr<std::map<std::string, FmuFileHelper::TraceEntry>> targetOutputTracesMap;
    bool isParameter;
  };

public:
  /// @brief Construct a scalar connector factory
  /// @param componentName Name of the scalar connector factory component
  explicit ScalarConnectorFactory(const std::string &componentName) : componentName(componentName) {}

  ScalarConnectorFactory() = delete;
  ~ScalarConnectorFactory() = default;

  /// @brief Register a connector in scalar connector factory
  /// @param connector    Reference to the connector of ssp parser type
  /// @param fmuWrapper   Reference to the pointer of Fmu wrapper interface
  /// @param priority     Priority of the connector
  /// @param kind         Kind of the connector
  /// @return Returns the registered connector direction, if it is input/output/none
  RegisteredConnectorDirection RegisterConnector(const SspParserTypes::Connector &connector,
                                                 const std::shared_ptr<FmuWrapperInterface> &fmuWrapper,
                                                 int priority,
                                                 ConnectorKind kind);

  /// @brief Create an input scalar connectors
  /// @param  writeMessageParameters
  /// @param  outputDir
  /// @param  targetOutputTracesMap
  /// @return List of pointer to the ssp connector interface
  std::vector<std::shared_ptr<ssp::ConnectorInterface>> CreateInputScalarConnectors(
      const std::vector<std::pair<std::string, std::string>> &writeMessageParameters,
      const std::filesystem::path &outputDir,
      const std::shared_ptr<std::map<std::string, FmuFileHelper::TraceEntry>> &targetOutputTracesMap);

  /// @brief Create an output scalar connectors
  /// @param  writeMessageParameters
  /// @param  outputDir
  /// @param  targetOutputTracesMap
  /// @return List of pointer to the ssp connector interface
  std::vector<std::shared_ptr<ssp::ConnectorInterface>> CreateOutputScalarConnectors(
      const std::vector<std::pair<std::string, std::string>> &writeMessageParameters,
      const std::filesystem ::path &outputDir,
      const std::shared_ptr<std::map<std::string, FmuFileHelper::TraceEntry>> &targetOutputTracesMap);

private:
  const std::string &componentName;

  void RegisterScalarConnector(std::vector<ScalarData> scalarData,
                               const std::string &connectorName,
                               ConnectorType connectorType,
                               const std::shared_ptr<FmuWrapperInterface> &fmuWrapper,
                               int priority,
                               bool isParameter);

  std::vector<std::shared_ptr<ssp::ConnectorInterface>> CreateScalarConnectors(
      std::vector<ScalarData> scalarData,
      const std::vector<std::pair<std::string, std::string>> &writeMessageParameters,
      const std::filesystem::path &outputDir,
      const std::shared_ptr<std::map<std::string, FmuFileHelper::TraceEntry>> &targetOutputTracesMap);

  std::shared_ptr<ssp::Connector> MakeGenericConnector(const ScalarConnectorBlueprint &blueprint);
  void UpdateBlueprint(const ScalarData &data, ScalarConnectorBlueprint &blueprint);

  std::vector<ScalarData> scalarInput{};
  std::vector<ScalarData> scalarOutput{};

  template <typename T>
  std::shared_ptr<ssp::Connector> SetScalarConnector(std::shared_ptr<ssp::ScalarConnector<T>> connector,
                                                     const ScalarConnectorBlueprint &blueprint) const
  {
    if (blueprint.isParameter)
    {
      connector->SetParameterConnector();
    }
    return connector;
  }
};