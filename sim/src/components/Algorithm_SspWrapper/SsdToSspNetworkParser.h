/********************************************************************************
 * Copyright (c) 2020-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * https://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <memory>

#include "AlgorithmSspWrapperImplementation.h"
#include "Importer/SsdFile.h"
#include "OSMPConnectorFactory.h"
#include "ScalarConnectorFactory.h"
#include "common/xmlParser.h"
#include "include/callbackInterface.h"
#include "sim/src/components/Algorithm_SspWrapper/SSPElements/Component.h"
#include "sim/src/components/Algorithm_SspWrapper/SSPElements/NetworkElement.h"
#include "sim/src/components/Algorithm_SspWrapper/SSPElements/System.h"
#include "src/core/opSimulation/modelElements/parameters.h"

/// Class representing a parser from ssd to ssp network
class SsdToSspNetworkParser
{
public:
  SsdToSspNetworkParser(SsdToSspNetworkParser &) = delete;
  SsdToSspNetworkParser(SsdToSspNetworkParser &&) = delete;
  SsdToSspNetworkParser &operator=(const SsdToSspNetworkParser &) = delete;
  SsdToSspNetworkParser &operator=(SsdToSspNetworkParser &&) = delete;
  ~SsdToSspNetworkParser() = default;

  /**
   * @brief Construct a new SsdToSspNetworkParser object
   *
   * @param componentName               Name of this component
   * @param isInit                      Indicates if component is an init module
   * @param priority                    Priority of this component
   * @param offsetTime                  Offset time of this component
   * @param responseTime                Response time of this component
   * @param cycleTime                   Cycle time of this component
   * @param stochasticsInterface        Reference to the stochastics interface
   * @param worldInterface              World representation
   * @param parameterInterface          Reference to the parameter interface
   * @param publisher                   Publisher instance
   * @param agentInterface              Agent that the component type is a part of
   * @param callbackInterface           Reference to the callback interface
   * @param scenarioControl             Pointer to the scenarioControl interface
   */
  SsdToSspNetworkParser(const std::string componentName,
                        bool isInit,
                        int priority,
                        int offsetTime,
                        int responseTime,
                        int cycleTime,
                        StochasticsInterface *stochasticsInterface,
                        WorldInterface *worldInterface,
                        const ParameterInterface *parameterInterface,
                        PublisherInterface *const publisher,
                        AgentInterface *agentInterface,
                        const CallbackInterface *callbackInterface,
                        std::shared_ptr<ScenarioControlInterface> const scenarioControl);

  /// @brief Get the root system of SSD to SSP Network parser
  /// @param ssdFiles List of pointers to the ssd file
  /// @return A pointer to ssp system
  std::shared_ptr<ssp::System> GetRootSystem(const std::vector<std::shared_ptr<SsdFile>> &ssdFiles);

  /**
   * @brief Get the Connections object
   *
   * @return List of connections, which can connect connectors of two different elements
   */
  [[nodiscard]] const std::vector<SspParserTypes::Connection> &GetConnections() const;

  /**
   * @brief Get the Priorities object
   *
   * @return Map of priorities for all components that are stored inside the system's elements
   */
  [[nodiscard]] const std::map<SspParserTypes::Component, int> &GetPriorities() const;

  /**
   * @brief Get the parameter initialization
   *
   * @return Map of parameter connector initialization for all components that are stored inside the system's elements
   */
  [[nodiscard]] std::vector<ParameterConnectorInitialization> GetParameterInitializations() const
  {
    return parameterInitializations;
  }

  /**
   * @brief Provides callback to LOG() macro
   *
   * @param logLevel  Importance of log
   * @param file      Name of file where log is called
   * @param line      Line within file where log is called
   * @param message   Message to log
   */
  void Log(CbkLogLevel logLevel, const char *file, int line, const std::string &message) const;

private:
  const std::string componentName;
  bool isInit;
  int priority;
  int offsetTime;
  int responseTime;
  int cycleTime;
  StochasticsInterface *stochastics;
  WorldInterface *world;
  const ParameterInterface *parameters;
  PublisherInterface *const publisher;
  AgentInterface *agent;
  const CallbackInterface *callbacks;
  std::shared_ptr<ScenarioControlInterface> const scenarioControl;

  std::map<SspParserTypes::Component, std::unique_ptr<SimulationCommon::Parameters>> parameterForComponents{};
  std::map<SspParserTypes::Component, std::shared_ptr<FmuWrapperInterface>> componentToWrapperMap{};
  std::vector<SspParserTypes::Connection> connections{};
  std::map<SspParserTypes::Component, int> priorities{};
  std::filesystem::path outputDir;
  const std::vector<std::shared_ptr<SsdFile>> *ssdFiles;
  std::vector<ParameterConnectorInitialization> parameterInitializations{};

  std::shared_ptr<ssp::System> ParseSsdFile(const std::shared_ptr<SsdFile> &ssdFile);

  std::filesystem::path GenerateAgentOutputDir();
  void HandleSystemConnectors(const std::shared_ptr<SsdSystem> &ssdSystem,
                              std::vector<std::shared_ptr<ssp::ConnectorInterface>> &systemInputConnectors,
                              std::vector<std::shared_ptr<ssp::ConnectorInterface>> &systemOutputConnectors);
  void AddSystemConnector(std::vector<std::shared_ptr<ssp::ConnectorInterface>> &systemConnectors,
                          std::pair<std::string, SspParserTypes::Connector> systemConnector,
                          std::set<SspParserTypes::Connector> &connectors);

  std::vector<std::shared_ptr<ssp::VisitableNetworkElement>> HandleComponents(
      const std::shared_ptr<SsdFile> &ssdFile,
      const std::shared_ptr<SsdSystem> &ssdSystem,
      std::filesystem::path outputDirComplete,
      std::shared_ptr<std::map<std::string, FmuFileHelper::TraceEntry>> &targetToTracesMap);

  std::vector<std::shared_ptr<ssp::Enumeration>> HandleEnumerations(
      const std::vector<std::shared_ptr<SspParserTypes::Enumeration>> ssdEnumerations);
  void HandleConnections(const std::shared_ptr<SsdSystem> &ssdSystem, std::shared_ptr<ssp::System> &system);
  std::optional<SspParserTypes::Connector> GenerateConnector(std::set<SspParserTypes::Connector> connectors,
                                                             std::string connectorName,
                                                             std::string &osmpRole,
                                                             std::string &osmpLinkName);
  void GenerateSubsystemConnections(std::shared_ptr<ssp::System> &system);
  void GenerateSubsystemConnectors(std::shared_ptr<ssp::GroupConnector> &systemConnector,
                                   std::vector<std::shared_ptr<ssp::ConnectorInterface>> &subsystemConnectorGroups);

  SimulationCommon::Parameters *GetFmuParameters(const std::shared_ptr<SsdFile> &ssdFile,
                                                 const std::shared_ptr<SsdComponent> &ssdComponent);
  std::shared_ptr<FmuWrapperInterface> EmplaceAlgorithmFmuWrapper(const std::shared_ptr<SsdSystem> &ssdSystem,
                                                                  const std::shared_ptr<SsdComponent> &ssdComponent,
                                                                  SimulationCommon::Parameters *fmuWrapperParameters);

  template <size_t FMI>
  std::shared_ptr<ssp::FmuComponent> CreateFmuComponent(
      const std::shared_ptr<SsdComponent> &ssdComponent,
      std::filesystem::path outputDirComplete,
      std::shared_ptr<std::map<std::string, FmuFileHelper::TraceEntry>> &targetToTracesMap);

  void AddConnectors(OSMPConnectorFactory &osmpConnectorFactory,
                     ScalarConnectorFactory &scalarConnectorFactory,
                     const std::shared_ptr<SsdComponent> &ssdComponent);

  void LogWarningConnectionInvalid(std::string startElement,
                                   std::string startConnectorName,
                                   std::string endElement,
                                   std::string endConnectorName) const;

  ConnectorKind ConnectorKindFromString(const std::string &fromString);

  std::set<SspParserTypes::Component> components = {};
  std::set<SspParserTypes::Connector> inputConnectors, outputConnectors = {};
};