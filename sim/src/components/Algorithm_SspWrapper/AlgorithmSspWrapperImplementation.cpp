/********************************************************************************
 * Copyright (c) 2020-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * https://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "AlgorithmSspWrapperImplementation.h"

#include "Importer/SsdFile.h"
#include "Importer/SsdFileImporter.h"
#include "SsdToSspNetworkParser.h"
#include "Visitors/Connector/UpdateInputSignalVisitor.h"
#include "Visitors/Connector/UpdateOutputSignalVisitor.h"
#include "components/Algorithm_SspWrapper/SSPElements/Connector/GroupConnector.h"
#include "components/Algorithm_SspWrapper/Visitors/Network/CalcParamInitVisitor.h"
#include "include/fmuHandlerInterface.h"
#include "include/parameterInterface.h"
#include "sim/src/components/Algorithm_SspWrapper/Visitors/Network/SspInitVisitor.h"
#include "sim/src/components/Algorithm_SspWrapper/Visitors/Network/SspTriggerVisitor.h"
#include "zlibUnzip.h"

AlgorithmSspWrapperImplementation::AlgorithmSspWrapperImplementation(
    const std::string &componentName,
    bool isInit,
    int priority,
    int offsetTime,
    int responseTime,
    int cycleTime,
    StochasticsInterface *stochastics,
    WorldInterface *world,
    const ParameterInterface *parameters,
    PublisherInterface *const publisher,
    AgentInterface *agent,
    const CallbackInterface *callbacks,
    std::shared_ptr<ScenarioControlInterface> const scenarioControl)
    : UnrestrictedControllStrategyModelInterface(componentName,
                                                 isInit,
                                                 priority,
                                                 offsetTime,
                                                 responseTime,
                                                 cycleTime,
                                                 stochastics,
                                                 world,
                                                 parameters,
                                                 publisher,
                                                 callbacks,
                                                 agent,
                                                 scenarioControl),
      agentIdString(FmuFileHelper::CreateAgentIdString(agent->GetId())),
      componentName(componentName),
      agent(agent),
      callbacks(callbacks)
{
  LOGDEBUG(FmuHelper::log_prefix(agentIdString, componentName) + "constructor started");

  std::filesystem::path sspPath = GetAbsolutePath(parameters->GetParametersString().at("SspPath"),
                                                  GetParameters()->GetRuntimeInformation().directories.configuration);
  std::set<std::filesystem::path> fileAndFolderPaths{};
  RecursiveUnpackSsp(sspPath, fileAndFolderPaths);

  std::vector<std::filesystem::path> ssds;
  std::vector<std::filesystem::path> ssps;

  for (const std::filesystem::path path : fileAndFolderPaths)
  {
    if (path.extension() == ".ssd")
    {
      ssds.emplace_back(path);
    }
    else if (path.extension() == ".ssp")
    {
      ssps.emplace_back(path);
    }
  }

  std::vector<std::shared_ptr<SsdFile>> ssdFiles;

  for (const auto &ssdPath : ssds)
  {
    if (!SsdFileImporter::Import(ssdPath, ssdFiles))
    {
      LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString, componentName)
                       + "Error importing ssdFile: " + ssdPath.string());
    }
  }

  SsdToSspNetworkParser parser(componentName,
                               isInit,
                               priority,
                               offsetTime,
                               responseTime,
                               cycleTime,
                               stochastics,
                               world,
                               parameters,
                               publisher,
                               agent,
                               callbacks,
                               scenarioControl);
  rootSystem = parser.GetRootSystem(ssdFiles);

  parameterInitializations = parser.GetParameterInitializations();
  LOGDEBUG(FmuHelper::log_prefix(agentIdString, componentName) + "constructor finished");
  Init();
}

std::filesystem::path AlgorithmSspWrapperImplementation::GetAbsolutePath(const std::string &configPath,
                                                                         const std::basic_string<char> &configBasePath_)
{
  std::filesystem::path sspPath(configPath);
  if (!sspPath.is_absolute())
  {
    std::filesystem::path configBasePath(configBasePath_);
    sspPath = configBasePath / sspPath;
  }

  THROWIFFALSE(std::filesystem::exists(sspPath), "SSP '" + sspPath.string() + "' doesn't exist");

  return sspPath;
}

std::filesystem::path AlgorithmSspWrapperImplementation::GenerateAndRegisterTemporaryPath()
{
  std::filesystem::path unzipRoot = std::filesystem::temp_directory_path() / FmuFileHelper::temporaryDirectoryName();
  unzipRootPaths.emplace_back(unzipRoot);
  std::filesystem::create_directories({unzipRoot.string()});

  return unzipRoot;
}

void AlgorithmSspWrapperImplementation::DeleteTemporaryPaths()
{
  for (const auto &tmpPath : unzipRootPaths)
  {
    std::error_code errorCode;
    if (!std::filesystem::remove_all(tmpPath, errorCode))
    {
      LOGWARN("Failed to remove files from directory: " + tmpPath.string() + " with error msg :" + errorCode.message());
    }
  }
}

void AlgorithmSspWrapperImplementation::UpdateInput(int localLinkId,
                                                    const std::shared_ptr<const SignalInterface> &data,
                                                    int time)
{
  LOGDEBUG(FmuHelper::log_prefix(agentIdString, componentName)
           + "AlgorithmSspWrapperImplementation::UpdateInput started");
  ssp::UpdateInputSignalVisitor updateInputSignalVisitor{localLinkId, data, time, world, agent, callbacks};

  ssp::GroupConnector updateInputSystemConnector{rootSystem->GetInputConnectors()};
  updateInputSystemConnector.Accept(updateInputSignalVisitor);

  LOGDEBUG(FmuHelper::log_prefix(agentIdString, componentName)
           + "AlgorithmSspWrapperImplementation::UpdateInput finished");
}

void AlgorithmSspWrapperImplementation::UpdateOutput(int localLinkId,
                                                     std::shared_ptr<const SignalInterface> &data,
                                                     int time)
{
  LOGDEBUG(FmuHelper::log_prefix(agentIdString, componentName)
           + "AlgorithmSspWrapperImplementation::UpdateOutput started");
  ssp::UpdateOutputSignalVisitor updateOutputSignalVisitor{localLinkId, data, time, world, agent, callbacks};

  ssp::GroupConnector updateOutputSystemConnector{rootSystem->GetOutputConnectors()};
  updateOutputSystemConnector.Accept(updateOutputSignalVisitor);

  LOGDEBUG(FmuHelper::log_prefix(agentIdString, componentName)
           + "AlgorithmSspWrapperImplementation::UpdateOutput finished");
}

void AlgorithmSspWrapperImplementation::Trigger(int time)
{
  LOGDEBUG(FmuHelper::log_prefix(agentIdString, componentName)
           + "Start of AlgorithmSspWrapperImplementation::Trigger (time: " + std::to_string(time) + ")");
  ssp::SspTriggerVisitor triggerVisitor{time};
  rootSystem->Accept(triggerVisitor);

  LOGDEBUG(FmuHelper::log_prefix(agentIdString, componentName) + "End of AlgorithmSspWrapperImplementation::Trigger");
}

void AlgorithmSspWrapperImplementation::Init()
{
  if (!isInitialized)
  {
    LOGDEBUG(FmuHelper::log_prefix(agentIdString, componentName) + "Start of AlgorithmSspWrapperImplementation::Init");
    ssp::SspInitVisitor initVisitor{};
    for (const auto &system : rootSystem->elements)
    {
      system->Accept(initVisitor);
    }
    LOGDEBUG(FmuHelper::log_prefix(agentIdString, componentName) + "End of AlgorithmSspWrapperImplementation::Init");
    SetParameterInitializations();
    PropagateParameterInitializations();
  }
}

std::shared_ptr<ssp::System> AlgorithmSspWrapperImplementation::GetRootSystem()
{
  return rootSystem;
}

void AlgorithmSspWrapperImplementation::SetParameterInitializations()
{
  LOGDEBUG(FmuHelper::log_prefix(agentIdString, componentName)
           + "Start of AlgorithmSspWrapperImplementation::SetParameterInitializations");
  for (const auto &paramInit : parameterInitializations)
  {
    if (paramInit.connectorType == ConnectorType::Integer)
    {
      ssp::ParameterInput<ssp::VariableTypeInt> input{paramInit.connectorName, std::stoi(paramInit.parameterValue)};
      SetParameterInitializations(input, paramInit);
    }

    if (paramInit.connectorType == ConnectorType::Enumeration)
    {
      ssp::ParameterInput<ssp::VariableTypeEnum> input{paramInit.connectorName, std::stoi(paramInit.parameterValue)};
      SetParameterInitializations(input, paramInit);
    }

    if (paramInit.connectorType == ConnectorType::Real)
    {
      ssp::ParameterInput<ssp::VariableTypeDouble> input{paramInit.connectorName, std::stof(paramInit.parameterValue)};
      SetParameterInitializations(input, paramInit);
    }

    if (paramInit.connectorType == ConnectorType::Boolean)
    {
      bool isTrue = paramInit.parameterValue == "true" ? true : false;
      ssp::ParameterInput<ssp::VariableTypeBool> input{paramInit.connectorName, isTrue};
      SetParameterInitializations(input, paramInit);
    }
  }
  LOGDEBUG(FmuHelper::log_prefix(agentIdString, componentName)
           + "AlgorithmSspWrapperImplementation::SetParameterInitializations stopped");
}

void AlgorithmSspWrapperImplementation::PropagateParameterInitializations()
{
  LOGDEBUG(FmuHelper::log_prefix(agentIdString, componentName)
           + "Start of AlgorithmSspWrapperImplementation::PropagateParameterInitializations");
  ssp::CalculatedParameterVisitor calcParameterVisitor{};
  ssp::CalcParamInitVisitor calcParamInit{calcParameterVisitor};
  for (const auto &system : rootSystem->elements)
  {
    system->Accept(calcParamInit);
  }
}

void AlgorithmSspWrapperImplementation::RecursiveUnpackSsp(const std::filesystem::path &sspPath,
                                                           std::set<std::filesystem::path> &unpackedPaths)
{
  auto nestedUnpackedPaths = UnpackZip(sspPath, GenerateAndRegisterTemporaryPath());
  for (const auto &path : nestedUnpackedPaths)
    if (boost::algorithm::ends_with(path.string(), ".ssp")) RecursiveUnpackSsp(path, unpackedPaths);
  unpackedPaths.merge(std::move(nestedUnpackedPaths));
}
