/*******************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "CalcParamInitVisitor.h"

#include "sim/src/components/Algorithm_SspWrapper/SSPElements/Component.h"
#include "sim/src/components/Algorithm_SspWrapper/SSPElements/System.h"
#include "sim/src/components/Algorithm_SspWrapper/Visitors/SspVisitorHelper.h"

void ssp::CalcParamInitVisitor::Visit(const System *ssdSystem)
{
  LOGDEBUG("SSP CalculatedParameter  Visitor: Visit System " + ssdSystem->elementName);
  SSPVisitorHelper::PriorityAcceptVisitableNetworkElements(*this, ssdSystem->elements);
}

void ssp::CalcParamInitVisitor::Visit(const FmuComponent *component)
{
  LOGDEBUG("SSP CalculatedParameter Visitor: Visit FMU component " + component->elementName);
  ssp::GroupConnector outGroup{component->output_connectors};
  outGroup.Accept(calculatedParameterVisitor);
}

void ssp::CalcParamInitVisitor::Visit(const SspComponent *)
{
  LOGDEBUG("SSP CalculatedParameter Visitor: Visit SSP Component");
  LOGWARN("SSP CalculatedParameter Visitor: Visit SSP Component not implemented");
}

void ssp::CalcParamInitVisitor::Log(CbkLogLevel logLevel, const char *file, int line, const std::string &message) const
{
  SspLogger::Log(logLevel, file, line, message);
}