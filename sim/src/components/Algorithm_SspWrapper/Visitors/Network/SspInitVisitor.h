/*******************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <algorithm>
#include <memory>
#include <utility>
#include <vector>

#include <queue>

#include "sim/src/components/Algorithm_SspWrapper/Visitors/Network/SspNetworkVisitorInterface.h"

namespace ssp
{

/// @brief Visitor class for ssp network initialization
class SspInitVisitor : public SspNetworkVisitorInterface
{
public:
  ~SspInitVisitor() override = default;
  explicit SspInitVisitor() = default;
  void Visit(const System *ssdSystem) override;
  void Visit(const FmuComponent *component) override;
  void Visit(const SspComponent *) override;

protected:
  void Log(CbkLogLevel logLevel, const char *file, int line, const std::string &message) const override;
};

}  //namespace ssp