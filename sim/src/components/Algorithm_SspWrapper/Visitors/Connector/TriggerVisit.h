/*******************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "../../SSPElements/Connector/OSMPConnectorBase.h"
#include "../../SSPElements/Connector/ScalarConnectorBase.h"
#include "../SspVisitorHelper.h"
#include "ConnectorVisitorInterface.h"
#include "UpdateInputVisitor.h"
#include "UpdateOutputVisitor.h"

namespace ssp
{

template <typename T>
class TriggerVisit : public ConnectorVisitorInterface  ///< Class representing a trigger visit
{
public:
  /// @param time TriggerVisit constructor with time
  explicit TriggerVisit(const int time);

  /// @brief Visitor function to update output
  /// @param connector Pointer to the scalar connector base
  void Visit(ScalarConnectorBase *connector) override;

  /// @brief Visitor function to update output
  /// @param groupConnector Reference to the group connector
  void Visit(GroupConnector &groupConnector) override;

  /// @brief Visitor function to update output
  /// @param connector Reference to OSMP Connector base
  void Visit(OSMPConnectorBase *connector) override;

  const int time;                                                      ///< time
  std::vector<std::weak_ptr<FmuWrapperInterface>> alreadyTriggered{};  ///< List of triggered pointers

protected:
  //-------------------------------------------------------------------------
  //! Provides callback to LOG() macro.
  //!
  //! @param[in]     logLevel    Importance of log
  //! @param[in]     file        Name of file where log is called
  //! @param[in]     line        Line within file where log is called
  //! @param[in]     message     Message to log
  //-------------------------------------------------------------------------
  void Log(CbkLogLevel logLevel, const char *file, int line, const std::string &message) const override;
};

template <typename T>
TriggerVisit<T>::TriggerVisit(const int time) : time(time)
{
}

template <typename T>
void TriggerVisit<T>::Visit(ScalarConnectorBase *scalarConnector)
{
  SSPVisitorHelper::TriggerScalarConnectorOnce(alreadyTriggered, scalarConnector, time);

  LOGDEBUG("SSP Trigger Visitor: Visit FMU connector " + scalarConnector->GetConnectorName());
  UpdateOutputVisitor<T> outVisitor{};
  scalarConnector->Accept(outVisitor);
  UpdateInputVisitor<T> inputVisit{outVisitor.GetData()};

  for (const auto &connector : scalarConnector->connectors)
  {
    connector->Accept(inputVisit);
  }
}

template <typename T>
void TriggerVisit<T>::Visit(GroupConnector &groupConnector)
{
  LOGDEBUG("SSP Trigger Visitor: Visit system connector ");                 //+ connector.GetConnectorName());
  LOGDEBUG("SSP priority queue of system connector will now be handled.");  //+ connector.GetConnectorName() + "will no
                                                                            //be handled.");
  SSPVisitorHelper::PriorityAccept(groupConnector, *this);
}

template <typename T>
void TriggerVisit<T>::Visit(OSMPConnectorBase *osmpConnector)
{
  SSPVisitorHelper::TriggerOMSPConnectorOnce(alreadyTriggered, osmpConnector, time);

  LOGDEBUG("SSP Trigger Visitor: Visit OSMP connector " + osmpConnector->GetConnectorName());
  UpdateOutputVisitor<T> outVisitor{};
  osmpConnector->Accept(outVisitor);
  UpdateInputVisitor<T> inputVisit{outVisitor.GetData()};

  for (const auto &connector : osmpConnector->connectors)
  {
    connector->Accept(inputVisit);
  }
}

template <typename T>
void ssp::TriggerVisit<T>::Log(CbkLogLevel logLevel, const char *file, int line, const std::string &message) const
{
  SspLogger::Log(logLevel, file, line, message);
}
}  //namespace ssp