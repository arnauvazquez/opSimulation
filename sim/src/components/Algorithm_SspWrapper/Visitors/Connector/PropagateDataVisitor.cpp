
/*******************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "PropagateDataVisitor.h"

#include "../../SSPElements/Connector/OSMPConnectorBase.h"

ssp::PropagateDataVisitor::PropagateDataVisitor(int time) : time(time) {}

void ssp::PropagateDataVisitor::Visit(ssp::ScalarConnectorBase *scalarConnector)
{
  LOGDEBUG("SSP Propagate Data Visitor: Visit FMU connector " + scalarConnector->GetConnectorName());
  scalarConnector->PropagateData();
}

void ssp::PropagateDataVisitor::Visit(ssp::GroupConnector &groupConnector)
{
  LOGDEBUG("SSP Propagate Data Signal Visitor: Visit system connector ");
  LOGWARN("SSP Propagate Data Visitor: Visit system connector not implemented");
}

void ssp::PropagateDataVisitor::Visit(ssp::OSMPConnectorBase *osmpConnector)
{
  LOGDEBUG("SSP Propagate Data Signal Visitor: Visit OSMP connector " + osmpConnector->GetConnectorName());
  const auto message = osmpConnector->GetMessage();
  osmpConnector->HandleFileWriting(time);
  for (const auto &connector : osmpConnector->GetNonParameterConnectors())
    if (const auto otherConnector = std::dynamic_pointer_cast<ssp::OSMPConnectorBase>(connector))
    {
      otherConnector->SetMessage(message.get());
      otherConnector->HandleFileWriting(time);
    }
}

void ssp::PropagateDataVisitor::Log(CbkLogLevel logLevel, const char *file, int line, const std::string &message) const
{
  SspLogger::Log(logLevel, file, line, message);
}