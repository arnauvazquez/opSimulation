/*******************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "CalculatedParameterVisitor.h"

void ssp::CalculatedParameterVisitor::Visit(ssp::ScalarConnectorBase *connector)
{
  LOGDEBUG("SSP CalculatedParameterVisitor Visitor: Visit FMU connector " + connector->GetConnectorName());
  LOGINFO("SSP CalculatedParameterVisitor Visitor: Propagate data");
  if (connector->IsParameterConnector()) connector->PropagateData();
}

void ssp::CalculatedParameterVisitor::Visit(GroupConnector &groupConnector)
{
  LOGDEBUG("SSP CalculatedParameterVisitor Visitor: Visit system connector ");
  SSPVisitorHelper::PriorityAccept(groupConnector, *this);
}

void ssp::CalculatedParameterVisitor::Visit(OSMPConnectorBase *connector)
{
  LOGDEBUG("SSP CalculatedParameterVisitor Visitor: Visit OSMP connector and skip");
}

void ssp::CalculatedParameterVisitor::Log(CbkLogLevel logLevel,
                                          const char *file,
                                          int line,
                                          const std::string &message) const
{
  SspLogger::Log(logLevel, file, line, message);
}