/********************************************************************************
 * Copyright (c) 2020-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * https://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <QtGlobal>

#include "include/modelInterface.h"

#if defined(ALGORITHM_SSPWRAPPER_LIBRARY)
#define ALGORITHM_SSPWRAPPER_SHARED_EXPORT Q_DECL_EXPORT
#else
#define ALGORITHM_SSPWRAPPER_SHARED_EXPORT Q_DECL_IMPORT
#endif

extern "C"
{
  ALGORITHM_SSPWRAPPER_SHARED_EXPORT const std::string &OpenPASS_GetVersion();

  /// @brief Create an instance of lateral algorithm
  /// @param componentName      Name of the component
  /// @param isInit             If instance initialzed
  /// @param priority           Priority of the instance
  /// @param offsetTime         Offset time
  /// @param responseTime       Response time
  /// @param cycleTime          Cycle time
  /// @param stochastics        Reference to the stochastics interface
  /// @param world              Reference to the world interface
  /// @param parameters         Reference to the parameter interface
  /// @param publisher          Reference to the publisher interface
  /// @param agent              Reference to the agent interface
  /// @param callbacks          Reference to the callback interface
  /// @param scenarioControl    scenarioControl of entity
  /// @return model interface
  ALGORITHM_SSPWRAPPER_SHARED_EXPORT
  ModelInterface *OpenPASS_CreateInstance(std::string componentName,
                                          bool isInit,
                                          int priority,
                                          int offsetTime,
                                          int responseTime,
                                          int cycleTime,
                                          StochasticsInterface *stochastics,
                                          WorldInterface *world,
                                          const ParameterInterface *parameters,
                                          PublisherInterface *publisher,
                                          AgentInterface *agent,
                                          const CallbackInterface *callbacks,
                                          std::shared_ptr<ScenarioControlInterface> const scenarioControl);

  //-----------------------------------------------------------------------------
  //! dll-function to destroy/delete an instance of the module.
  //!
  //! @param[in]     implementation    The instance that should be freed
  //-----------------------------------------------------------------------------
  ALGORITHM_SSPWRAPPER_SHARED_EXPORT
  void OpenPASS_DestroyInstance(ModelInterface *implementation);

  ALGORITHM_SSPWRAPPER_SHARED_EXPORT
  bool OpenPASS_UpdateInput(ModelInterface *implementation,
                            int localLinkId,
                            const std::shared_ptr<SignalInterface const> &data,
                            int time);

  ALGORITHM_SSPWRAPPER_SHARED_EXPORT
  bool OpenPASS_UpdateOutput(ModelInterface *implementation,
                             int localLinkId,
                             std::shared_ptr<SignalInterface const> &data,
                             int time);

  ALGORITHM_SSPWRAPPER_SHARED_EXPORT
  bool OpenPASS_Trigger(ModelInterface *implementation, int time);
}
