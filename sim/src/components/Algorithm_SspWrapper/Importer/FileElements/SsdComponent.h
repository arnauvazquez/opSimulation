/********************************************************************************
 * Copyright (c) 2020-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <utility>

#include "SsdURI.h"
#include "components/Algorithm_SspWrapper/OSMPConnectorFactory.h"
#include "include/parameterInterface.h"
#include "src/common/parameter.h"

enum class SspComponentType
{
  x_none,
  x_fmu_sharedlibrary,
  x_ssp_definition
};

/// Structure representing an initilization of paramter connector
struct ParameterConnectorInitialization
{
  std::string connectorName;    ///< Name of the connector
  ConnectorType connectorType;  ///< Type of the connector
  std::string parameterValue;   ///< Value of the parameter
  std::string componentName;    ///< Name of the component

  /// @brief Construct a ParameterConnectorInitialization object
  /// @param parameterName    Name of the connector
  /// @param connectorType    Type of the connector
  /// @param parameterValue   Value of the parameter
  /// @param componentName    Name of the component
  ParameterConnectorInitialization(std::string parameterName,
                                   const ConnectorType connectorType,
                                   std::string parameterValue,
                                   std::string componentName)
      : connectorName(std::move(parameterName)),
        connectorType(connectorType),
        parameterValue(std::move(parameterValue)),
        componentName(std::move(componentName))
  {
  }
};

/// Class representing an SSD component
class SsdComponent
{
public:
  /// @brief Create a ssd component
  /// @param name             Name of the ssd component
  /// @param source           Source of the ssd component
  /// @param componentType    Type of the ssp component
  SsdComponent(std::string name, std::string source, SspComponentType);

  /// @brief Create a ssd component
  /// @param name             Name of the ssd component
  /// @param source           SSD uri
  /// @param componentType    Type of the ssp component
  SsdComponent(std::string name, SsdURI source, SspComponentType);

  /// @param parameters set function to set parameters
  void SetParameters(openpass::parameter::internal::ParameterSetLevel3 parameters);

  /// @brief Emplace a connector to the ssd component
  /// @param kind         Kind of ssp parser
  /// @param connector    Connector type of ssp parser
  void EmplaceConnector(std::string &&kind, SspParserTypes::Connector &&);

  /// @return Get name of the ssd component
  [[nodiscard]] const std::string &GetName() const;

  /// @return Get name of the ssd uri
  [[nodiscard]] const SsdURI &GetSource() const;

  /// @return Get type of the ssp component
  [[nodiscard]] const SspComponentType &GetComponentType() const;

  /// @return Get parameter of the ssd component
  [[nodiscard]] const openpass::parameter::internal::ParameterSetLevel3 &GetParameters() const;

  /// @return Get list of connectors
  [[nodiscard]] const std::vector<std::pair<std::string, SspParserTypes::Connector>> GetConnectors() const;

  /// @return Get priority of the component
  [[nodiscard]] const std::optional<int> GetPriority() const;

  /// @param newPriority set new priority of the component
  void SetPriority(int newPriority);

  /// @return Get list of write message parameters
  [[nodiscard]] const std::vector<std::pair<std::string, std::string>> &GetWriteMessageParameters() const;

  /// @brief Function to set write message parameters
  /// @param jsonParameters Parameters of write message
  void SetWriteMessageParameters(std::vector<std::pair<std::string, std::string>> jsonParameters);

  /// @return Get list of Parameter connectr initialization objects
  [[nodiscard]] const std::vector<ParameterConnectorInitialization> &GetConnectorInitializations() const;

  /// @brief Function to set connector initializations
  /// @param connectorInitializations List of Parameter connectr initialization objects
  void SetConnectorInitializations(std::vector<ParameterConnectorInitialization> &&connectorInitializations);

private:
  std::string name;
  SsdURI source;
  SspComponentType componentType;
  std::optional<int> priority;
  std::vector<std::pair<std::string, std::string>> writeMessageParameters{};
  std::vector<std::pair<std::string, SspParserTypes::Connector>> connectors{};
  openpass::parameter::internal::ParameterSetLevel3 parameters{};
  std::vector<ParameterConnectorInitialization> parameterConnectorInitializations{};
};
