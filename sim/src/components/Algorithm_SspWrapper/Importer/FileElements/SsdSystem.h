/********************************************************************************
 * Copyright (c) 2020-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once
#include "SsdComponent.h"

/// Class representing an ssd system
class SsdSystem
{
public:
  /// @brief construct an ssd system
  /// @param name Name of the ssd system
  explicit SsdSystem(std::string);

  /// @brief Add a component to the SSD system
  /// @param ssdComponent
  void AddComponent(std::shared_ptr<SsdComponent> &&);

  /// @brief Add a connection to the SSD system
  /// @param connection
  void AddConnection(std::shared_ptr<std::map<std::string, std::string>> &&);

  /// @brief Add an SSD system
  /// @param ssdSystem
  void AddSystem(std::shared_ptr<SsdSystem> &&);

  /// @brief Emplace a connector to the SSD system
  /// @param kind         Name of the connector
  /// @param connector    Type of SSP Parser
  void EmplaceConnector(std::string &&kind, SspParserTypes::Connector &&);

  /// @brief Add enumeration of ssp parser types to the ssd system
  /// @param ssdEnumerations Enumerations of SSP parser type
  void AddEnumerations(SspParserTypes::Enumerations &&);

  /// @return Returns the reference to the name of the system
  [[nodiscard]] const std::string &GetName() const;

  /// @return Returns list of reference to the ssd component
  [[nodiscard]] const std::vector<std::shared_ptr<SsdComponent>> &GetComponents() const;

  /// @return Returns list of references to the connections
  [[nodiscard]] const std::vector<std::shared_ptr<std::map<std::string, std::string>>> &GetConnections() const;

  /// @return Returns list of references to the ssd system
  [[nodiscard]] const std::vector<std::shared_ptr<SsdSystem>> &GetSystems() const;

  /// @return Returns list of Connectors
  [[nodiscard]] const std::vector<std::pair<std::string, SspParserTypes::Connector>> &GetConnectors() const;

  /// @return Returns list of pointers to the enumerations of ssp parser type
  [[nodiscard]] const std::vector<std::shared_ptr<SspParserTypes::Enumeration>> &GetEnumerations();

  /// @return Returns list of parameter connector initialization
  std::vector<ParameterConnectorInitialization> GetConnectorInitializations() const;

  /// @brief Check if the ssd system has enumeration
  /// @param name Name of the system
  /// @return True, if the system has enumeration
  bool HasEnumeration(const std::string &name);

private:
  std::string name;
  std::vector<std::shared_ptr<SsdComponent>> components{};
  std::vector<std::shared_ptr<std::map<std::string, std::string>>> connections{};
  std::vector<std::shared_ptr<SsdSystem>> systems{};
  std::vector<std::pair<std::string, SspParserTypes::Connector>> connectors{};
  std::vector<std::shared_ptr<SspParserTypes::Enumeration>> enumerations = {};
};
