/********************************************************************************
 * Copyright (c) 2020-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "SsdSystem.h"

#include <utility>

SsdSystem::SsdSystem(std::string name) : name(std::move(name)) {}
void SsdSystem::AddComponent(std::shared_ptr<SsdComponent> &&ssdComponent)
{
  components.emplace_back(std::move(ssdComponent));
}
void SsdSystem::AddConnection(std::shared_ptr<std::map<std::string, std::string>> &&connection)
{
  connections.emplace_back(std::move(connection));
}
const std::string &SsdSystem::GetName() const
{
  return name;
}

const std::vector<std::shared_ptr<SsdComponent>> &SsdSystem::GetComponents() const
{
  return components;
}

const std::vector<std::shared_ptr<std::map<std::string, std::string>>> &SsdSystem::GetConnections() const
{
  return connections;
}

void SsdSystem::AddSystem(std::shared_ptr<SsdSystem> &&ssdSystem)
{
  systems.emplace_back(std::move(ssdSystem));
}

void SsdSystem::EmplaceConnector(std::string &&kind, SspParserTypes::Connector &&connector)
{
  connectors.emplace_back(std::forward<std::string>(kind), std::forward<SspParserTypes::Connector>(connector));
}

const std::vector<std::shared_ptr<SsdSystem>> &SsdSystem::GetSystems() const
{
  return systems;
}

const std::vector<std::pair<std::string, SspParserTypes::Connector>> &SsdSystem::GetConnectors() const
{
  return connectors;
}

void SsdSystem::AddEnumerations(SspParserTypes::Enumerations &&ssdEnumerations)
{
  for (const auto &enumeration : ssdEnumerations)
  {
    enumerations.emplace_back(std::make_shared<SspParserTypes::Enumeration>(enumeration));
  }
}
const std::vector<std::shared_ptr<SspParserTypes::Enumeration>> &SsdSystem::GetEnumerations()
{
  return enumerations;
}

bool SsdSystem::HasEnumeration(const std::string &enumName)
{
  auto itFoundItem
      = std::find_if(this->enumerations.begin(),
                     this->enumerations.end(),
                     [enumName](std::shared_ptr<SspParserTypes::Enumeration> &e) { return e->first == enumName; });

  return (itFoundItem == this->enumerations.cend() ? false : true);
}

std::vector<ParameterConnectorInitialization> SsdSystem::GetConnectorInitializations() const
{
  std::vector<ParameterConnectorInitialization> initalizations{};
  for (auto component : components)
  {
    initalizations.insert(initalizations.end(),
                          component->GetConnectorInitializations().begin(),
                          component->GetConnectorInitializations().end());
  }
  return initalizations;
}
