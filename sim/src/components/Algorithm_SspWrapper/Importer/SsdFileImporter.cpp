/********************************************************************************
 * Copyright (c) 2020-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "SsdFileImporter.h"

#include <filesystem>
#include <regex>
#include <utility>

#include "FileElements/SsdURI.h"
#include "components/Algorithm_SspWrapper/OSMPConnectorFactory.h"

SspParserTypes::ConnectorTypeAndAttributes SsdFileImporter::FetchConnectorType(const QDomElement &connectorElement,
                                                                               const std::string &connectorName)
{
  QDomElement typeElement;
  if (SimulationCommon::GetFirstChildElement(connectorElement, "Real", typeElement))
  {
    LOGDEBUG("Parse Real Connector from connector element " + connectorName);
    return {ConnectorType::Real, {}};
  }
  else if (SimulationCommon::GetFirstChildElement(connectorElement, "Integer", typeElement))
  {
    LOGDEBUG("Parse Integer Connector from connector element " + connectorName);
    return {ConnectorType::Integer, {}};
  }
  else if (SimulationCommon::GetFirstChildElement(connectorElement, "Boolean", typeElement))
  {
    LOGDEBUG("Parse Boolean Connector from connector element " + connectorName);
    return {ConnectorType::Boolean, {}};
  }
  else if (SimulationCommon::GetFirstChildElement(connectorElement, "Enumeration", typeElement))
  {
    std::string enumerationName{};
    if (SimulationCommon::ParseAttribute(typeElement, "name", enumerationName))
    {
      LOGDEBUG("Parse Enumeration Connector from connector element " + connectorName);
      return {ConnectorType::Enumeration, {{"name", enumerationName}}};
    }
    else
    {
      LogErrorAndThrow("All Enumeration Connectors require the name attribute, " + connectorName + " missing name");
    }
  }
  else
  {
    LOGDEBUG("Parse Connector from connector element " + connectorName);
    return {ConnectorType::None, {}};
  }
}

template <class T>
void SsdFileImporter::ImportConnectors(const QDomElement &connectorsElement,
                                       const std::shared_ptr<T> &ssdComponent,
                                       const std::shared_ptr<SsdSystem> &ssdSystem)
{
  QDomElement connectorElement;
  if (!SimulationCommon::GetFirstChildElement(connectorsElement, "Connector", connectorElement))
  {
    LOGERRORANDTHROW("SSP Importer: Unable to retrieve connectors.");
  }
  while (!connectorElement.isNull())
  {
    bool isOSMP = false;
    std::smatch typeMatch;
    std::string connectorName, binaryVariableName, kind, role, mimeType;

    SimulationCommon::ParseAttribute(connectorElement, "name", connectorName);
    SimulationCommon::ParseAttribute(connectorElement, "kind", kind);

    QDomElement annotationsElement;
    if (SimulationCommon::GetFirstChildElement(connectorElement, "Annotations", annotationsElement))
    {
      QDomElement annotationElement;
      if (SimulationCommon::GetFirstChildElement(annotationsElement, "Annotation", annotationElement))
      {
        while (!annotationElement.isNull())
        {
          std::string annotationType;
          SimulationCommon::ParseAttribute(annotationElement, "type", annotationType);

          if (annotationType == "net.pmsf.osmp")
          {
            isOSMP = true;
            QDomElement binaryVariable;
            if (SimulationCommon::GetFirstChildElement(annotationElement, "osmp-binary-variable", binaryVariable))
            {
              SimulationCommon::ParseAttribute(binaryVariable, "name", binaryVariableName);
              SimulationCommon::ParseAttribute(binaryVariable, "role", role);
              SimulationCommon::ParseAttribute(binaryVariable, "mime-type", mimeType);
            }
            else
            {
              LOGWARN("SSP Importer: Parse unknown annotation: ignore " + annotationType);
            }
          }
          annotationElement = annotationElement.nextSiblingElement("Annotation");
        }
      }
    }
    if (isOSMP)
    {
      std::regex typeRegex{"type=([a-zA-Z]*);", std::regex::optimize};
      if (std::regex_search(mimeType, typeMatch, typeRegex))
      {
        auto connector = SspParserTypes::OSMPSingleLink{ssdComponent->GetName(),
                                                        {connectorName, binaryVariableName, role, typeMatch[1]}};
        ssdComponent->EmplaceConnector(std::move(kind), connector);
      }
      else
      {
        LOGERRORANDTHROW("SSP Importer: Unable to parse osi type from mime-type: " + mimeType);
      }
    }
    else
    {
      //Scalar Connector, because not OSMP type

      SspParserTypes::ConnectorTypeAndAttributes connectorParams = FetchConnectorType(connectorElement, connectorName);
      if (connectorParams.first == ConnectorType::Enumeration)
      {
        auto enumName = connectorParams.second["name"];
        if (ssdSystem->HasEnumeration(enumName))
        {
          ssdComponent->EmplaceConnector(std::move(kind),
                                         SspParserTypes::ScalarConnector{connectorName, connectorParams});
        }
        else
        {
          LOGWARN("SSP Importer: Could not create connector " + connectorName
                  + ", because system enumerations has no enum type " + enumName + ".");
        }
      }
      else
      {
        ssdComponent->EmplaceConnector(std::move(kind),
                                       SspParserTypes::ScalarConnector{connectorName, connectorParams});
      }
    }
    connectorElement = connectorElement.nextSiblingElement("Connector");
  }
}

void SsdFileImporter::ImportComponentParameterSets(QDomElement &parameterSetElement,
                                                   const std::shared_ptr<SsdComponent> &component)
{
  QDomElement parametersElement;
  if (!SimulationCommon::GetFirstChildElement(parameterSetElement, "Parameters", parametersElement))
  {
    LOGERRORANDTHROW("SSP Importer: Unable to retrieve parameters.");
  }
  while (!parameterSetElement.isNull())
  {
    std::string name;
    SimulationCommon::ParseAttribute(parameterSetElement, "name", name);
    if (name == "FmuParameters")
    {
      ImportFmuParameters(parameterSetElement, component);
    }
    else if (name == "WriteMessageParameters")
    {
      ImportWriteMessageParameters(parameterSetElement, component);
    }
    else if (name == "ParameterConnectorInitialization")
    {
      ImportParameterConnectorInitialization(parameterSetElement, component);
    }
    else
    {
      LOGINFO("SSP Importer: Ignoring unknown parameterSet: " + name);
    }
    parameterSetElement = parameterSetElement.nextSiblingElement("ParameterSet");
  }
}

void SsdFileImporter::ImportFmuParameters(QDomElement &parameterSetElement,
                                          const std::shared_ptr<SsdComponent> &component)
{
  QDomElement parametersElement;
  if (!SimulationCommon::GetFirstChildElement(parameterSetElement, "Parameters", parametersElement))
  {
    LOGERRORANDTHROW("SSP Importer: Unable to retrieve FmuParameters.");
  }

  QDomElement parameterElement;
  if (SimulationCommon::GetFirstChildElement(parametersElement, "Parameter", parameterElement))
  {
    openpass::parameter::internal::ParameterSetLevel3 parameters;

    while (!parameterElement.isNull())
    {
      std::string parameterName;
      SimulationCommon::ParseAttribute(parameterElement, "name", parameterName);

      QDomElement valueElement;
      std::string value;
      if (SimulationCommon::GetFirstChildElement(parameterElement, "String", valueElement))
      {
        SimulationCommon::ParseAttribute(valueElement, "value", value);
        parameters.emplace_back(parameterName, value);
      }
      if (SimulationCommon::GetFirstChildElement(parameterElement, "Real", valueElement))
      {
        SimulationCommon::ParseAttribute(valueElement, "value", value);
        parameters.emplace_back(parameterName, atof(value.c_str()));
      }
      if (SimulationCommon::GetFirstChildElement(parameterElement, "Integer", valueElement))
      {
        SimulationCommon::ParseAttribute(valueElement, "value", value);
        parameters.emplace_back(parameterName, atoi(value.c_str()));
      }
      if (SimulationCommon::GetFirstChildElement(parameterElement, "Boolean", valueElement))
      {
        SimulationCommon::ParseAttribute(valueElement, "value", value);

        bool valueRobust = false;
        if (value == "true")
        {
          valueRobust = true;
        }
        parameters.emplace_back(parameterName, valueRobust);
      }

      parameterElement = parameterElement.nextSiblingElement("Parameter");
    }
    component->SetParameters(parameters);
  }
}

void SsdFileImporter::ImportWriteMessageParameters(QDomElement &parameterSetElement,
                                                   const std::shared_ptr<SsdComponent> &component)
{
  QDomElement parametersElement;

  if (!SimulationCommon::GetFirstChildElement(parameterSetElement, "Parameters", parametersElement))
  {
    LOGERRORANDTHROW("SSP Importer: Unable to retrieve WriteMessageParameters.");
  }

  QDomElement parameterElement;
  if (SimulationCommon::GetFirstChildElement(parametersElement, "Parameter", parameterElement))
  {
    std::vector<std::pair<std::string, std::string>> parameters;

    while (!parameterElement.isNull())
    {
      std::string parameterName;
      SimulationCommon::ParseAttribute(parameterElement, "name", parameterName);

      QDomElement valueElement;
      if (SimulationCommon::GetFirstChildElement(parameterElement, "String", valueElement))
      {
        std::string value;
        SimulationCommon::ParseAttribute(valueElement, "value", value);
        parameters.emplace_back(std::make_pair(parameterName, value));
      }
      parameterElement = parameterElement.nextSiblingElement("Parameter");
    }
    component->SetWriteMessageParameters(parameters);
  }
}

void SsdFileImporter::ImportParameterConnectorInitialization(QDomElement &parameterSetElement,
                                                             const std::shared_ptr<SsdComponent> &component)
{
  std::vector<ParameterConnectorInitialization> connectorInit{};
  QDomElement parametersElement;
  QDomElement parameterElement;
  QDomElement enumerationsElement;
  QDomElement enumerationElement;
  std::optional<SspParserTypes::Enumerations> initEnumerations;

  if (SimulationCommon::GetFirstChildElement(parameterSetElement, "Enumerations", enumerationsElement))
  {
    initEnumerations = ImportSsdEnumerations(enumerationsElement);
  }

  if (SimulationCommon::GetFirstChildElement(parameterSetElement, "Parameters", parametersElement))
  {
    if (SimulationCommon::GetFirstChildElement(parametersElement, "Parameter", parameterElement))
    {
      while (!parameterElement.isNull())
      {
        std::string connectorName;
        SimulationCommon::ParseAttribute(parameterElement, "name", connectorName);

        QDomElement valueElement;
        std::string value;
        if (SimulationCommon::GetFirstChildElement(parameterElement, "Real", valueElement))
        {
          auto connectorType = ConnectorType::Real;
          SimulationCommon::ParseAttribute(valueElement, "value", value);
          connectorInit.emplace_back(
              ParameterConnectorInitialization{connectorName, connectorType, value, component->GetName()});
        }
        if (SimulationCommon::GetFirstChildElement(parameterElement, "Integer", valueElement))
        {
          auto connectorType = ConnectorType::Integer;
          SimulationCommon::ParseAttribute(valueElement, "value", value);
          connectorInit.emplace_back(
              ParameterConnectorInitialization{connectorName, connectorType, value, component->GetName()});
        }
        if (SimulationCommon::GetFirstChildElement(parameterElement, "Boolean", valueElement))
        {
          auto connectorType = ConnectorType::Boolean;
          SimulationCommon::ParseAttribute(valueElement, "value", value);
          connectorInit.emplace_back(
              ParameterConnectorInitialization{connectorName, connectorType, value, component->GetName()});
        }
        if (SimulationCommon::GetFirstChildElement(parameterElement, "Enumeration", valueElement))
        {
          auto connectorType = ConnectorType::Enumeration;
          QDomElement nameElement;
          std::string itemName;
          if (initEnumerations.has_value())
          {
            SimulationCommon::ParseAttribute(valueElement, "value", value);
            SimulationCommon::ParseAttribute(valueElement, "name", itemName);
            auto enumerationValue = SSPParserHelper::GetValueFromEnumeration(itemName, value, initEnumerations.value());
            if (enumerationValue.has_value())
            {
              connectorInit.emplace_back(ParameterConnectorInitialization{
                  connectorName, connectorType, enumerationValue.value_or(""), component->GetName()});
            }
            else
            {
              LOGWARN("Enumeration Item: " + value + "not found in enumeration: " + itemName + ", skipping");
            }
          }
          else
          {
            LOGWARN("Enumeration init parameter can't be processed without enumerations tag in ParameterSet, skipping");
          }
        }
        parameterElement = parameterElement.nextSiblingElement("Parameter");
      }
    }
    component->SetConnectorInitializations(std::move(connectorInit));
  }
}

void SsdFileImporter::ImportComponentParameters(QDomElement &parameterBindingsElement,
                                                const std::shared_ptr<SsdComponent> &component,
                                                const std::filesystem::path &filename)
{
  QDomElement parameterBindingElement;
  if (!SimulationCommon::GetFirstChildElement(parameterBindingsElement, "ParameterBinding", parameterBindingElement))
  {
    LOGERRORANDTHROW("SSP Importer: Unable to retrieve parameters.");
  }
  while (!parameterBindingElement.isNull())
  {
    if (parameterBindingElement.hasAttribute("source"))
    {
      SsdURI ssvSourceElement;
      SimulationCommon::ParseAttribute(parameterBindingElement, "source", ssvSourceElement);
      std::filesystem::path ssvSource(filename);
      std::filesystem::path ssvSourceFolder(ssvSourceElement.Path());
      if (!ssvSourceFolder.is_absolute())
      {
        ssvSource = ssvSource.parent_path() / ssvSourceFolder;
      }
      QDomDocument document;

      ImportSsdFileContent(ssvSource, document);

      QDomElement documentRoot = document.documentElement();
      if (!documentRoot.isNull())
      {
        ImportComponentParameterSets(documentRoot, component);
      }
    }
    else
    {
      QDomElement parameterValuesElement;
      if (SimulationCommon::GetFirstChildElement(parameterBindingElement, "ParameterValues", parameterValuesElement))
      {
        QDomElement parameterSetElement;
        if (SimulationCommon::GetFirstChildElement(parameterValuesElement, "ParameterSet", parameterSetElement))
        {
          ImportComponentParameterSets(parameterSetElement, component);
        }
      }
    }
    parameterBindingElement = parameterBindingElement.nextSiblingElement("ParameterBinding");
  }
}

void SsdFileImporter::ImportSystemConnections(const QDomElement &connectionsElement,
                                              const std::shared_ptr<SsdSystem> &ssdSystem)
{
  QDomElement connectionElement;
  if (!SimulationCommon::GetFirstChildElement(connectionsElement, "Connection", connectionElement))
  {
    LOGINFO("SSP Importer: No Connection element present.");
    return;
  }

  std::string startElement;
  std::string endElement;
  std::string startConnector;
  std::string endConnector;

  while (!connectionElement.isNull())
  {
    std::map<std::string, std::string> map;

    if (!SimulationCommon::ParseAttribute(connectionElement, "startConnector", startConnector))
    {
      LOGERRORANDTHROW("SSP Importer: Unable to retrieve connection startConnector.");
    }
    map.insert(std::make_pair("startConnector", startConnector));

    if (!SimulationCommon::ParseAttribute(connectionElement, "endConnector", endConnector))
    {
      LOGERRORANDTHROW("SSP Importer: Unable to retrieve connection endConnector.");
    }
    map.insert(std::make_pair("endConnector", endConnector));

    if (SimulationCommon::ParseAttribute(connectionElement, "startElement", startElement))
      map.emplace("startElement", startElement);
    else
      map.emplace("startElement", ssdSystem->GetName());
    if (SimulationCommon::ParseAttribute(connectionElement, "endElement", endElement))
      map.emplace("endElement", endElement);
    else
      map.emplace("endElement", ssdSystem->GetName());
    ssdSystem->AddConnection(std::make_shared<std::map<std::string, std::string>>(map));
    connectionElement = connectionElement.nextSiblingElement("Connection");
  }
}

void SsdFileImporter::ImportSystemComponents(QDomElement &elementsElement,
                                             const std::shared_ptr<SsdSystem> &ssdSystem,
                                             const std::filesystem::path &filename)
{
  QDomElement componentsElement;
  if (SimulationCommon::GetFirstChildElement(elementsElement, "Component", componentsElement))
  {
    while (!componentsElement.isNull())
    {
      std::string componentName;
      if (!SimulationCommon::ParseAttribute(componentsElement, "name", componentName))
      {
        LOGERRORANDTHROW("SSP Importer: Unable to retrieve component name: " + componentName);
      }

      std::string componentSource;
      if (!SimulationCommon::ParseAttribute(componentsElement, "source", componentSource))
      {
        LOGERRORANDTHROW("SSP Importer: Unable to retrieve component source: " + componentSource);
      }

      std::string componentType;
      if (!SimulationCommon::ParseAttribute(componentsElement, "type", componentType))
      {
        LOGINFO("SSP Importer: Unable to retrieve component type: " + componentType
                + "\nDefauting to application/x-fmu-sharedlibrary");
        componentType = "application/x-fmu-sharedlibrary";
      }

      std::shared_ptr<SsdComponent> ssdComponent{};
      if (componentType == "application/x-fmu-sharedlibrary")
        ssdComponent = std::make_shared<SsdComponent>(
            std::move(componentName), std::move(componentSource), SspComponentType::x_fmu_sharedlibrary);
      else if (componentType == "application/x-ssp-definition")
        ssdComponent = std::make_shared<SsdComponent>(
            std::move(componentName), std::move(componentSource), SspComponentType::x_ssp_definition);

      LOGINFO("SSP Importer: import connectors");
      QDomElement connectorsElement;
      if (SimulationCommon::GetFirstChildElement(componentsElement, "Connectors", connectorsElement))
      {
        ImportConnectors(connectorsElement, ssdComponent, ssdSystem);
      }

      LOGINFO("SSP Importer: import annotations");
      QDomElement annotationsElement;
      if (SimulationCommon::GetFirstChildElement(componentsElement, "Annotations", annotationsElement))
      {
        ImportComponentAnnotations(annotationsElement, ssdComponent);
      }

      LOGINFO("SSP Importer: import parameters");
      QDomElement parameterBindingsElement;
      if (SimulationCommon::GetFirstChildElement(componentsElement, "ParameterBindings", parameterBindingsElement))
      {
        ImportComponentParameters(parameterBindingsElement, ssdComponent, filename);
      }

      ssdSystem->AddComponent(std::move(ssdComponent));
      componentsElement = componentsElement.nextSiblingElement("Component");
    }
  }
}

SspParserTypes::Enumerations SsdFileImporter::ImportSsdEnumerations(const QDomElement &enumerationsElement)
{
  QDomElement enumerationElement;
  SspParserTypes::Enumerations enumerations{};

  if (!SimulationCommon::GetFirstChildElement(enumerationsElement, "Enumeration", enumerationElement))
  {
    LOGINFO("SSP Importer: No Enumeration element present.");
    return enumerations;
  }

  std::string enumName;
  std::vector<SspParserTypes::EnumerationItem> enumItems;

  while (!enumerationElement.isNull())
  {
    if (!SimulationCommon::ParseAttribute(enumerationElement, "name", enumName))
    {
      LOGERRORANDTHROW("SSP Importer: Unable to retrieve enum name.");
    }

    QDomElement enumerationItem;
    if (!SimulationCommon::GetFirstChildElement(enumerationElement, "Item", enumerationItem))
    {
      LOGINFO("SSP Importer: No Enumeration element items present.");
      return enumerations;
    }
    std::string itemName;
    int itemValue;
    while (!enumerationItem.isNull())
    {
      if (!SimulationCommon::ParseAttribute(enumerationItem, "name", itemName))
      {
        LOGERRORANDTHROW("SSP Importer: Unable to retrieve enum item name.");
      }
      if (!SimulationCommon::ParseAttribute(enumerationItem, "value", itemValue))
      {
        LOGERRORANDTHROW("SSP Importer: Unable to retrieve enum item value.");
      }

      SspParserTypes::EnumerationItem enumItem;
      enumItem.first = itemName;
      enumItem.second = itemValue;
      enumItems.emplace_back(enumItem);

      enumerationItem = enumerationItem.nextSiblingElement("Item");
    }
    SspParserTypes::Enumeration enumeration;
    enumeration.first = enumName;
    enumeration.second = enumItems;
    enumerationElement = enumerationElement.nextSiblingElement("Connection");
    enumerations.emplace_back(enumeration);
  }
  return enumerations;
}

bool SsdFileImporter::ImportSsdFileContent(const std::filesystem::path &filename, QDomDocument &document)
{
  std::locale::global(std::locale("C"));

  if (!std::filesystem::exists(filename))
  {
    LOGINFO("SSP Importer: SsdFile: " + filename.string() + " does not exist.");
    return false;
  }

  std::ifstream xmlFile(filename, std::ios::in);
  if (!xmlFile.is_open())
  {
    LOGERRORANDTHROW("SSP Importer: an error occurred during SsdFile import: " + filename.string());
  }

  std::string xmlDataString((std::istreambuf_iterator<char>(xmlFile)), std::istreambuf_iterator<char>());
  QString xmldata = QString::fromStdString(xmlDataString);
  QString errorMsg{};
  int errorLine{};
  if (!document.setContent(xmldata, true, &errorMsg, &errorLine))
  {
    LOGERRORANDTHROW("SSP Importer: invalid xml file format of file " + filename.string() + " in line "
                     + std::to_string(errorLine) + " : " + errorMsg.toStdString());
  }

  return true;
}

bool SsdFileImporter::Import(const std::filesystem::path &filename, std::vector<std::shared_ptr<SsdFile>> &ssdFiles)
{
  try
  {
    QDomDocument document;
    if (!ImportSsdFileContent(filename, document))
    {
      return false;
    }

    QDomElement documentRoot = document.documentElement();
    if (documentRoot.isNull())
    {
      return false;
    }

    QDomElement systemElement;
    QString localName = "ssd";
    QString defValue{};
    const QString &string
        = documentRoot.attributeNS("http://ssp-standard.org/SSP1/SystemStructureDescription", localName, defValue);
    if (SimulationCommon::GetFirstChildElement(documentRoot, "System", systemElement))
    {
      std::string systemId;
      if (!SimulationCommon::ParseAttribute(systemElement, "name", systemId))
      {
        LOGERRORANDTHROW("SSP Importer: Unable to retrieve system name: " + systemId);
      }

      auto ssdSystem = std::make_shared<SsdSystem>(systemId);
      ImportSystem(filename, systemElement, ssdSystem);
      ssdFiles.emplace_back(std::make_shared<SsdFile>(filename, ssdSystem));
    }
    return true;
  }
  catch (const std::runtime_error &e)
  {
    LOGERRORANDTHROW("SSP Importer: SsdFile import failed.");
  }
}

void SsdFileImporter::ImportSystem(const std::filesystem::path &filename,
                                   const QDomElement &systemElement,
                                   const std::shared_ptr<SsdSystem> &ssdSystem)
{
  LOGINFO("SSP Importer: Import enumerations");
  QDomElement enumerationsElement;
  if (SimulationCommon::GetFirstChildElement(systemElement, "Enumerations", enumerationsElement))
  {
    try
    {
      auto enumerations = ImportSsdEnumerations(enumerationsElement);
      ssdSystem->AddEnumerations(std::move(enumerations));
    }
    catch (const std::runtime_error &error)
    {
      LOGERRORANDTHROW("SSP Importer: Unable to import system enumerators.");
    }
  }

  LOGINFO("SSP Importer: Import components");
  QDomElement elementsElement;
  if (SimulationCommon::GetFirstChildElement(systemElement, "Elements", elementsElement))
  {
    try
    {
      ImportSystemComponents(elementsElement, ssdSystem, filename);
    }
    catch (const std::runtime_error &error)
    {
      LOGERRORANDTHROW("SSP Importer: Unable to import system components.");
    }

    LOGINFO("SSP Importer: Import systems");
    QDomElement subsystemElement;
    if (SimulationCommon::GetFirstChildElement(elementsElement, "System", subsystemElement))
    {
      while (!subsystemElement.isNull())
      {
        std::string systemId;
        if (!SimulationCommon::ParseAttribute(systemElement, "name", systemId))
        {
          LOGERRORANDTHROW("SSP Importer: Unable to retrieve system name: " + systemId);
        }

        auto ssdSubSystem = std::make_shared<SsdSystem>(systemId);
        ImportSystem(filename, subsystemElement, ssdSubSystem);
        ssdSystem->AddSystem(std::move(ssdSubSystem));

        subsystemElement = subsystemElement.nextSiblingElement("System");
      }
    }
  }

  LOGINFO("SSP Importer: Import connectors");
  QDomElement connectorsElement;
  if (SimulationCommon::GetFirstChildElement(systemElement, "Connectors", connectorsElement))
  {
    try
    {
      ImportConnectors(connectorsElement, ssdSystem, ssdSystem);
    }
    catch (const std::runtime_error &error)
    {
      LOGERRORANDTHROW("SSP Importer: Unable to import system connectors.");
    }
  }

  LOGINFO("SSP Importer: Import connections");
  QDomElement connectionsElement;
  SimulationCommon::GetFirstChildElement(systemElement, "Connections", connectionsElement);

  try
  {
    ImportSystemConnections(connectionsElement, ssdSystem);
  }
  catch (const std::runtime_error &error)
  {
    LOGERRORANDTHROW("SSP Importer: Unable to import system connections.");
  }
}

void SsdFileImporter::ImportComponentAnnotations(QDomElement &annotationsElement,
                                                 const std::shared_ptr<SsdComponent> &component)
{
  QDomElement annotationElement;
  if (!SimulationCommon::GetFirstChildElement(annotationsElement, "Annotation", annotationElement))
  {
    LOGWARN("SSP Importer: Unable to retrieve component annotations.");
  }

  int priority;

  while (!annotationElement.isNull())
  {
    std::string annotationType;
    SimulationCommon::ParseAttribute(annotationElement, "type", annotationType);

    if (annotationType == "de.setlevel.ssp.scheduling")
    {
      QDomElement binaryVariable;
      if (SimulationCommon::GetFirstChildElement(annotationElement, "co-simulation", binaryVariable))
      {
        SimulationCommon::ParseAttribute(binaryVariable, "priority", priority);
      }
    }

    annotationElement = annotationElement.nextSiblingElement("Annotation");
  }

  component->SetPriority(priority);
}

void SsdFileImporter::Log(CbkLogLevel logLevel, const char *file, int line, const std::string &message)
{
  SspLogger::Log(logLevel, file, line, message);
}
