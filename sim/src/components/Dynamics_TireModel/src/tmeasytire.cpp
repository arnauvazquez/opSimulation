/********************************************************************************
 * Copyright (c) 2023 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "tmeasytire.h"

#include <cmath>

#include <QtGlobal>

Tire::Tire(const units::force::newton_t F_ref,
           const Common::Vector2d<double> mu_tire_max,
           const Common::Vector2d<double> mu_tire_slide,
           const Common::Vector2d<double> s_max,
           const Common::Vector2d<double> mu_tire_max2,
           const Common::Vector2d<double> mu_tire_slide2,
           const Common::Vector2d<double> s_max2,
           const units::length::meter_t r,
           const double mu_scale,
           const Common::Vector2d<double> s_slide,
           const Common::Vector2d<double> s_slide2,
           const Common::Vector2d<units::force::newton_t> F0p,
           const Common::Vector2d<units::force::newton_t> F0p2,
           const Common::Vector2d<units::length::meter_t> positiontire,
           const units::frequency::hertz_t rotationVelocity,
           const double Inertia,
           const units::length::meter_t PneumaticTrail)
    : radius(r), forceZ(F_ref)
{
  // Init Tire parameters

  this->rotationVelocity = rotationVelocity;
  this->rotationAcceleration = 0;

  this->dF0PRef = F0p;
  this->dF0P2Ref = F0p2;

  this->FRef = F_ref;

  forcePeakRef.x = F_ref * mu_tire_max.x * mu_scale;
  forceSatRef.x = F_ref * mu_tire_slide.x * mu_scale;
  forcePeakRef.y = F_ref * mu_tire_max.y * mu_scale;
  forceSatRef.y = F_ref * mu_tire_slide.y * mu_scale;

  forcePeak2Ref.x = 2 * F_ref * mu_tire_max2.x * mu_scale;
  forceSat2Ref.x = 2 * F_ref * mu_tire_slide2.x * mu_scale;
  forcePeak2Ref.y = 2 * F_ref * mu_tire_max2.y * mu_scale;
  forceSat2Ref.y = 2 * F_ref * mu_tire_slide2.y * mu_scale;

  this->slipPeakRef = s_max;
  this->slipSatRef = s_slide;
  this->slipPeak2Ref = s_max2;
  this->slipSat2Ref = s_slide2;

  this->positionTire = positiontire;
  this->inertia = Inertia;
  this->pneumaticTrail = PneumaticTrail;
}

void Tire::CalcTireForce()  // Calculate tire longitudinal and lateral force
{
  units::force::newton_t force;

  // Calculate Normalized Tire Slip
  slipNorm.x = forcePeak.x / dF0P.x;
  slipNorm.y = forcePeak.y / dF0P.y;

  // Calculate Longitudinal Tire Slip
  CalcLongSlip();
  // Calculate Lateral Tire Slip
  CalcLatSlip();

  // Calculate Tire Slip
  double slip = std::sqrt((slipTire.x / slipNorm.x) * (slipTire.x / slipNorm.x)
                          + (slipTire.y / slipNorm.y) * (slipTire.y / slipNorm.y));

  // Get absolute tire slip
  double slipAbs = std::fabs(slip);

  double cos_phi = slipTire.x / (slip * slipNorm.x);
  double sin_phi = slipTire.y / (slip * slipNorm.y);
  units::force::newton_t dF0 = units::math::sqrt(units::math::pow<2>(dF0P.x * slipNorm.x * cos_phi)
                                                 + units::math::pow<2>(dF0P.y * slipNorm.y * sin_phi));
  double s_m
      = std::sqrt(std::pow(slipPeak.x / slipNorm.x * cos_phi, 2) + std::pow(slipPeak.y / slipNorm.y * sin_phi, 2));
  units::force::newton_t F_m
      = units::math::sqrt(units::math::pow<2>(forcePeak.x * cos_phi) + units::math::pow<2>(forcePeak.y * sin_phi));
  double s_g = std::sqrt(std::pow(slipSat.x / slipNorm.x * cos_phi, 2) + std::pow(slipSat.y / slipNorm.y * sin_phi, 2));
  units::force::newton_t F_g
      = units::math::sqrt(units::math::pow<2>(forceSat.x * cos_phi) + units::math::pow<2>(forceSat.y * sin_phi));

  // Get absolute normalized tire slip
  double slipAbsNorm = slipAbs / s_m;

  if (slipAbsNorm <= 1.0)
  {  // adhesion

    force = slipAbsNorm * s_m * dF0 / (1 + slipAbsNorm * (slipAbsNorm + dF0 * s_m / F_m - 2));
  }
  else if (slipAbs < s_g)  //(slipAbs < slipSat)
  {                        // adhesion/sliding

    double sigma = (slipAbs - s_m) / (s_g - s_m);

    force = F_m - (F_m - F_g) * sigma * sigma * (3 - 2 * sigma);
  }
  else
  {  // slide
    force = F_g;
  }
  force = (slip > 0.0) ? force : -force;

  if (qFuzzyIsNull(slip))
  {
    TireForce.x = 0_N;
    TireForce.y = 0_N;
  }
  else
  {
    TireForce.x = (slipTire.x / slipNorm.x) / slip * force;
    TireForce.y = (slipTire.y / slipNorm.y) / slip * force;
  }
}
void Tire::CalcLongSlip()  // Calculate longitudinal tire slip
{
  if (units::math::abs(velocityTire.x - radius * rotationVelocity) < std::numeric_limits<double>::epsilon() * 1.0_mps)
  {
    slipTire.x = 0;
  }
  else
  {
    slipTire.x
        = -(velocityTire.x - radius * rotationVelocity) / (radius * units::math::abs(rotationVelocity) + velocityLimit);
  }
}
void Tire::CalcLatSlip()  // Calculate lateral tire slip
{
  slipTire.y = -velocityTire.y / (radius * units::math::abs(rotationVelocity) + velocityLimit);
}

units::force::newton_t Tire::GetRollFriction()  // Calculate tire roll friction
{
  units::force::newton_t forceFriction = forceZ * frictionRoll;

  if (velocityTire.x < 0.0_mps)
  {
    forceFriction *= -1.0;
  }
  if (units::math::abs(velocityTire.x) < velocityLimit)
  {
    forceFriction *= (velocityTire.x / velocityLimit);
  }

  return forceFriction;
}

void Tire::Rescale(const units::force::newton_t forceZ_update)  // Rescale tire parameters
{
  double scaling = forceZ_update / FRef;

  forceZ = forceZ_update;

  dF0P.x = scaling * (2 * dF0PRef.x - 0.5 * dF0P2Ref.x - (dF0PRef.x - 0.5 * dF0P2Ref.x) * scaling);
  dF0P.y = scaling * (2 * dF0PRef.y - 0.5 * dF0P2Ref.y - (dF0PRef.y - 0.5 * dF0P2Ref.y) * scaling);

  forcePeak.x
      = scaling * (2 * forcePeakRef.x - 0.5 * forcePeak2Ref.x - (forcePeakRef.x - 0.5 * forcePeak2Ref.x) * scaling);
  forcePeak.y
      = scaling * (2 * forcePeakRef.y - 0.5 * forcePeak2Ref.y - (forcePeakRef.y - 0.5 * forcePeak2Ref.y) * scaling);

  forceSat.x = scaling * (2 * forceSatRef.x - 0.5 * forceSat2Ref.x - (forceSatRef.x - 0.5 * forceSat2Ref.x) * scaling);
  forceSat.y = scaling * (2 * forceSatRef.y - 0.5 * forceSat2Ref.y - (forceSatRef.y - 0.5 * forceSat2Ref.y) * scaling);

  slipPeak.x = slipPeakRef.x + (slipPeak2Ref.x - slipPeakRef.x) * (scaling - 1);
  slipPeak.y = slipPeakRef.y + (slipPeak2Ref.y - slipPeakRef.y) * (scaling - 1);

  slipSat.x = slipSatRef.x + (slipSat2Ref.x - slipSatRef.x) * (scaling - 1);
  slipSat.y = slipSatRef.y + (slipSat2Ref.y - slipSatRef.y) * (scaling - 1);
}

void Tire::CalcRotAcc()  // Calculate tire rotational acceleration
{
  rotationAcceleration = (Torque.value() - (TireForce.x.value() * radius.value())) / inertia;
}

void Tire::CalcRotVel(const double dt)  // Calculate tire rotation velocity
{
  rotationVelocity = rotationVelocity + rotationAcceleration * dt * 1_Hz;
}

void Tire::CalcVelTire(const units::angular_velocity::radians_per_second_t yawVelocity,
                       const Common::Vector2d<units::velocity::meters_per_second_t>
                           velocityCar)  // Calculate tire velocity in tire coordinate system
{
  velocityTire = velocityCar;

  velocityTire.x = velocityTire.x - yawVelocity.value() * positionTire.y.value() * 1_mps;
  velocityTire.y = velocityTire.y + yawVelocity.value() * positionTire.x.value() * 1_mps;
  velocityTire.Rotate(-angleTire);  // tire CS
}

void Tire::CalcSelfAligningTorque()  // Calculate tire self Aligning torque due to pneumatic trail
{
  SelfAligningTorque = -TireForce.y * pneumaticTrail;
}

units::force::newton_t Tire::GetLateralForce()  // Get tire lateral force
{
  return TireForce.y;
};
units::force::newton_t Tire::GetLongitudinalForce()  // Get tire longitudinal force
{
  return TireForce.x;
};
units::length::meter_t Tire::GetTireRadius()  // Get Tire Radius
{
  return radius;
}
void Tire::SetTorque(units::torque::newton_meter_t torque)  // Set tire torque
{
  Torque = torque;
}
Common::Vector2d<units::velocity::meters_per_second_t> Tire::GetVelocityTire()  // Get tire velocity
{
  return velocityTire;
};
units::frequency::hertz_t Tire::GetRotationVelocity()  // Get tire rotational velocity
{
  return rotationVelocity;
}
double Tire::GetRotAcceleration()  // Get tire rotational acceleration
{
  return rotationAcceleration;
}

void Tire::SetTireAngle(units::angle::radian_t angle)  // Set tire angle
{
  angleTire = angle;
}
units::torque::newton_meter_t Tire::GetSelfAligningTorque()  // Get tire self aligning torque
{
  return SelfAligningTorque;
};
void Tire::SetRotAcceleration(double rotAcceleration)  // Set tire rotational acceleration
{
  rotationAcceleration = rotAcceleration;
};
void Tire::SetRotationVelocity(units::frequency::hertz_t rotVelocity)  // Set tire rotational velocity
{
  rotationVelocity = rotVelocity;
};