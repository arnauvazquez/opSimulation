/********************************************************************************
 * Copyright (c) 2020-2021 ITK Engineering GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2023 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "ForceWheelZ.h"

// Calculate the perpendicular force on wheels against inertia
//   - the pitchZ is positive if pitching backward, otherwise negative
//   - the rollZ is positive if rolling right, otherwise negative
bool ForceWheelZ::CalForce(units::force::newton_t fInertiaX,
                           units::force::newton_t fInertiaY,
                           units::length::meter_t pitchZ,
                           units::length::meter_t rollZ,
                           VehicleBasics &carParam)
{
  units::angle::radian_t pitchAngle = units::math::atan2(
      pitchZ,
      (carParam.lenFront + carParam.lenRear));  // the pitch angle is positive if pitching backward, otherwise negative
  units::angle::radian_t rollAngle
      = units::math::atan2(rollZ,
                           (carParam.lenLeft[0] + carParam.lenRight[0])
                               / 2);  // the roll angle is positive if rolling right, otherwise negative
  carParam.Deformation(pitchAngle, rollAngle);

  if (!CalForceInPitch(fInertiaX, pitchAngle, carParam)) return false;

  if (!CalForceInRoll(fInertiaY, rollAngle, carParam)) return false;

  for (int i = 0; i < NUMBER_WHEELS; i++)
  {
    forces[i] = forcesPitch[i] + forcesRoll[i];
  }
  return true;
}

// Calculate the perpendicular force on front and rear wheels against the inertia force in X axis
//   - the pitch angle is positive if pitching backward, otherwise negative
//   - the roll angle is positive if rolling right, otherwise negative
bool ForceWheelZ::CalForceInPitch(units::force::newton_t fInertiaX,
                                  units::angle::radian_t pitchAngle,
                                  VehicleBasics &carParam)
{
  const auto wheelBaseLen = carParam.lenFront + carParam.lenRear;
  units::angle::radian_t frontAngle = units::math::atan2(carParam.heightMC, carParam.lenFront) - pitchAngle;
  units::angle::radian_t rearAngle = units::math::atan2(carParam.heightMC, carParam.lenRear) + pitchAngle;

  // Calculate the perpendicular force on front wheels
  units::length::meter_t dRear2MC
      = units::math::sqrt((carParam.lenRear * carParam.lenRear) + (carParam.heightMC * carParam.heightMC));
  units::force::newton_t fZFront = dRear2MC * fInertiaX * units::math::sin(rearAngle) / wheelBaseLen;

  // Calculate the perpendicular force on rear wheels
  units::length::meter_t dFront2MC
      = units::math::sqrt((carParam.lenFront * carParam.lenFront) + (carParam.heightMC * carParam.heightMC));
  units::force::newton_t fZRear = dFront2MC * (-fInertiaX) * units::math::sin(frontAngle) / wheelBaseLen;

  // separate front force to frontleft and frontright wheels
  forcesPitch[0] = fZFront / (1 + carParam.ratioY[0]);   // frontleft
  forcesPitch[1] = carParam.ratioY[0] * forcesPitch[0];  // frontright

  // separate rear force to rearleft and rearright wheels
  forcesPitch[2] = fZRear / (1 + carParam.ratioY[1]);    // rearleft
  forcesPitch[3] = carParam.ratioY[1] * forcesPitch[2];  // rearright

  return true;
}

// Calculate the perpendicular force on left and right wheels against the inertia force in Y axis
//   - the pitch angle is positive if pitching backward, otherwise negative
//   - the roll angle is positive if rolling right, otherwise negative
bool ForceWheelZ::CalForceInRoll(units::force::newton_t fInertiaY,
                                 units::angle::radian_t rollAngle,
                                 VehicleBasics &carParam)
{
  units::angle::radian_t leftAngle[2], rightAngle[2];

  leftAngle[0] = units::math::atan2(carParam.heightMC, carParam.lenLeft[0]) - rollAngle;
  rightAngle[0] = units::math::atan2(carParam.heightMC, carParam.lenRight[0]) + rollAngle;
  leftAngle[1] = units::math::atan2(carParam.heightMC, carParam.lenLeft[1]) - rollAngle;
  rightAngle[1] = units::math::atan2(carParam.heightMC, carParam.lenRight[1]) + rollAngle;

  // Calculate the perpendicular force on front wheels against the longitudinal inertia
  units::length::meter_t dRight2MC[2];
  units::force::newton_t fZLeft[2];

  dRight2MC[0]
      = units::math::sqrt((carParam.lenRight[0] * carParam.lenRight[0]) + (carParam.heightMC * carParam.heightMC));
  fZLeft[0] = dRight2MC[0] * fInertiaY / (1 + carParam.ratioX) * units::math::sin(rightAngle[0])
            / (carParam.lenLeft[0] + carParam.lenRight[0]);

  dRight2MC[1]
      = units::math::sqrt((carParam.lenRight[1] * carParam.lenRight[1]) + (carParam.heightMC * carParam.heightMC));
  fZLeft[1] = dRight2MC[1] * fInertiaY * carParam.ratioX * units::math::sin(rightAngle[1])
            / (carParam.lenLeft[1] + carParam.lenRight[1]);

  // Calculate the perpendicular force on rear wheels against the longitudinal inertia
  units::length::meter_t dLeft2MC[2];
  units::force::newton_t fZRight[2];

  dLeft2MC[0]
      = units::math::sqrt((carParam.lenLeft[0] * carParam.lenLeft[0]) + (carParam.heightMC * carParam.heightMC));
  fZRight[0] = dLeft2MC[0] * (-fInertiaY) / (1 + carParam.ratioX) * units::math::sin(leftAngle[0])
             / (carParam.lenLeft[0] + carParam.lenRight[0]);

  dLeft2MC[1]
      = units::math::sqrt((carParam.lenLeft[1] * carParam.lenLeft[1]) + (carParam.heightMC * carParam.heightMC));
  fZRight[1] = dLeft2MC[1] * (-fInertiaY) * carParam.ratioX * units::math::sin(leftAngle[1])
             / (carParam.lenLeft[1] + carParam.lenRight[1]);

  // separate left force to frontleft and rearleft wheels
  forcesRoll[0] = fZLeft[0];  // frontleft
  forcesRoll[2] = fZLeft[1];  // rearleft

  // separate right force to frontright and rearright wheels
  forcesRoll[1] = fZRight[0];  // frontright
  forcesRoll[3] = fZRight[1];  // rearright

  return true;
}
