/********************************************************************************
 * Copyright (c) 2023 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  action_brakesystem.h
//! @brief contains DLL export declarations
//-----------------------------------------------------------------------------

#pragma once

#include <QtGlobal>

#if defined(ACTION_BRAKESYSTEM_LIBRARY)
#define ACTION_BRAKESYSTEM_SHARED_EXPORT Q_DECL_EXPORT //!< Export of the dll-functions
#else
#define ACTION_BRAKESYSTEM_SHARED_EXPORT Q_DECL_IMPORT //!< Import of the dll-functions
#endif

#include "include/modelInterface.h"