/********************************************************************************
 * Copyright (c) 2023 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/** \addtogroup Vehicle_Dynamics
 *@{
 * \addtogroup Brake_System
 * \brief  This module represents a linear brake system.
 *
 * \details This module represents a simple brake system. Based on a brake pedal position,
 * the brake torque of each tire is calculated. Parameterizable response time and threshold duration are taken into
 *account. A prefill request can be used to reduce the response
 *
 * \section inputs Inputs
 * name | meaning
 * -----|---------
 * reqPrefil | request Prefill
 * Longitudinal Signal | current brake pedal position of vehicle
 *
 * \section outputs Outputs
 * name | meaning
 * -----|---------
 * wheelBrakeTorque | current brake torque of each tire
 *
 * @} */

#include <common/vectorSignals.h>

#include "common/longitudinalSignal.h"
#include "common/primitiveSignals.h"
#include "components/common/vehicleProperties.h"
#include "include/modelInterface.h"

/*!
 * \copydoc Brake_System
 * \ingroup Brake_System
 */
class ActionBrakeSystem : public ActionInterface
{
public:
  const std::string COMPONENTNAME = "Action_BrakeSystem";

  /**
   * @brief Construct a new Action Brake System Implementation object
   *
   * \param [in] componentName   Name of the component
   * \param [in] isInit          Query whether the component was just initialized
   * \param [in] priority        Priority of the component
   * \param [in] offsetTime      Offset time of the component
   * \param [in] responseTime    Response time of the component
   * \param [in] cycleTime       Cycle time of this components trigger task [ms]
   * \param [in] stochastics     Provides access to the stochastics functionality of the framework
   * \param [in] parameters      Interface provides access to the configuration parameters
   * \param [in] publisher       Instance  provided by the framework
   * \param [in] callbacks       Interface for callbacks to framework
   * \param [in] agent           This interface provides access to agent parameters, properties, attributes and dynamic states
   */
  ActionBrakeSystem(std::string componentName,
                    bool isInit,
                    int priority,
                    int offsetTime,
                    int responseTime,
                    int cycleTime,
                    StochasticsInterface *stochastics,
                    WorldInterface *world,
                    const ParameterInterface *parameters,
                    PublisherInterface *const publisher,
                    const CallbackInterface *callbacks,
                    AgentInterface *agent);

  ActionBrakeSystem(const ActionBrakeSystem &) = delete;
  ActionBrakeSystem(ActionBrakeSystem &&) = delete;
  ActionBrakeSystem &operator=(const ActionBrakeSystem &) = delete;
  ActionBrakeSystem &operator=(ActionBrakeSystem &&) = delete;
  virtual ~ActionBrakeSystem() = default;

  /*!
   * \brief Update Inputs
   *
   * Function is called by framework when another component delivers a signal over
   * a channel to this component (scheduler calls update taks of other component).
   *
   * Refer to module description for input channels and input ids.
   *
   * \param[in]     localLinkId    Corresponds to "id" of "ComponentInput"
   * \param[in]     data           Referenced signal (copied by sending component)
   * \param[in]     time           Current scheduling time
   */
  virtual void UpdateInput(int localLinkId, const std::shared_ptr<SignalInterface const> &data, int time);

  /*!
   * \brief Update outputs.
   *
   * Function is called by framework when this Component has to deliver a output signal over
   * a channel to another component (scheduler calls update task of this component).
   *
   * Refer to module description for output channels and output ids.
   *
   * \param[in]     localLinkId    Corresponds to "id" of "ComponentOutput"
   * \param[out]    data           Referenced signal (copied by this component)
   * \param[in]     time           Current scheduling time
   */
  virtual void UpdateOutput(int localLinkId, std::shared_ptr<SignalInterface const> &data, int time);

  /*!
   * \brief Process data within component.
   *
   * Function is called by framework when the scheduler calls the trigger task
   * of this component.
   *
   * Calculate wheel torque due to brake pedal position
   *
   * \param[in]     time           Current scheduling time
   */
  virtual void Trigger(int time);

private:
  /*!
   * ------------------------------------------------------------------------
   * \brief Calculates the brake torque of each wheel depending on the current
   *        requested possible deceleration and the brake distribution
   *
   * \param acceleration requested possible deceleration
   * ------------------------------------------------------------------------
   */
  void RequestBrakeDeceleration(units::acceleration::meters_per_second_squared_t decceleration);
  /*!
   * ------------------------------------------------------------------------
   * \brief Reads the information of brake distribution, incline and decline factor and response time from the
   *        ParameterInterface and stores them in the Parameters struct.
   *
   * \param parameters ParameterInterface
   * ------------------------------------------------------------------------
   */
  void ParseParameters(const ParameterInterface *parameters);

  /*!
   * ------------------------------------------------------------------------
   * \brief Calculates the brake torque of each wheel depending on the current
   *        requested possible deceleration and the brake distribution
   *
   * \param acceleration requested possible deceleration
   * ------------------------------------------------------------------------
   */
  void SetWheelBrakeTorque(units::acceleration::meters_per_second_squared_t acceleration);

  void ReduceResponseTime();
  void IncreaseResponseTime();

  double cycleTimeMs;                  //!< Cycle time of component  [ms]
  double currentResponseTimeMs = 0.0;  //!< response time of brake in current state  [ms]
  units::acceleration::meters_per_second_squared_t currentBrakeDeceleration
      = 0_mps_sq;  //!< braking deceleration  [m/s^2]
  double BrakePositionLastCycle = 0.0;

  // Parameters:
  double frontAxlePercentage;  //!< brake distribution front axle [0-1]
  double brakeInclineRate;     //!< linear brake incline factor [m/s^3]
  double brakeDeclineRate;     //!< linear brake decline factor [m/s^3]
  double brakeResponseTimeMs;  //!< brake response time [ms]

  // Inputs:
  bool reqPrefill = false;     //!< input signal for requesting a prefill
  double BrakePosition = 0.0;  //!< input signal of the brake pedal position

  // Vehicle Parameters
  units::acceleration::meters_per_second_squared_t maxBrakeDeceleration;  //!< maximum deceleration of vehicle
  units::mass::kilogram_t totalVehicleMass;                               //!< vehicle mass

  // Outputs:
  std::vector<double> wheelBrakeTorque;  //!< calculated brake torque of each wheel
};
