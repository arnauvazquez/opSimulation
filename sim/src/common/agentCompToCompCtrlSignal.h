/********************************************************************************
 * Copyright (c) 2019-2020 in-tech GmbH
 *               2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! \file  agentCompToCompCtrlSignal.h
//! \brief This file contains all functions for class AgentCompToCompCtrlSignal
//!
//! This class contains all functionality of the module.
//-----------------------------------------------------------------------------

#pragma once

#include <sstream>
#include <string>

#include "common/driverWarning.h"
#include "common/globalDefinitions.h"
#include "include/signalInterface.h"

//-----------------------------------------------------------------------------
//! AgentComptToCompCtrlSignal class
//-----------------------------------------------------------------------------
class AgentCompToCompCtrlSignal : public SignalInterface
{
public:
  /// component name
  static constexpr char COMPONENTNAME[] = "AgentCompToCompCtrlSignal";
  /**
   * @brief Construct a new Agent Comp To Comp Ctrl Signal object
   *
   * @param componentType         Type of the component
   * @param agentComponentName    Name of the agent component
   * @param currentState          State of the component
   * @param movementDomain        Movement domain
   * @param warnings              list of warnings
   */
  AgentCompToCompCtrlSignal(const ComponentType componentType,
                            const std::string& agentComponentName,
                            const ComponentState currentState,
                            const MovementDomain movementDomain = MovementDomain::Undefined,
                            const std::vector<ComponentWarningInformation>& warnings = {})
      : componentType(componentType),
        agentComponentName(agentComponentName),
        movementDomain(movementDomain),
        currentState(currentState),
        warnings(warnings)
  {
  }

  AgentCompToCompCtrlSignal() = delete;
  AgentCompToCompCtrlSignal(const AgentCompToCompCtrlSignal&) = delete;
  AgentCompToCompCtrlSignal(AgentCompToCompCtrlSignal&&) = delete;
  AgentCompToCompCtrlSignal& operator=(const AgentCompToCompCtrlSignal&) = delete;
  AgentCompToCompCtrlSignal& operator=(AgentCompToCompCtrlSignal&&) = delete;

  virtual ~AgentCompToCompCtrlSignal() {}

  //-----------------------------------------------------------------------------
  //! Returns the content/payload of the signal as an std::string
  //!
  //! \return     Content/payload of the signal as an std::string
  //-----------------------------------------------------------------------------
  virtual operator std::string() const
  {
    std::ostringstream stream;
    std::string currentStateStr{"Undefined"};

    stream << COMPONENTNAME << ": Intended Component States for " << agentComponentName << ": ";

    for (auto stateStringAndState : ComponentStateMapping)
    {
      if (currentState == stateStringAndState.second)
      {
        currentStateStr = stateStringAndState.first;
      }
    }

    stream << "Current State: " << currentStateStr;

    return stream.str();
  }

  /*!
   * \brief GetComponentType returns the type of the agent component sending the signal
   *
   * \return Type of the component
   */
  ComponentType GetComponentType() const { return componentType; }

  /**
   * \brief GetAgentComponentName returns the name of the agent component sending the signal
   *
   * \return string The name of the agent component sending the signal as a std::string
   */
  std::string GetAgentComponentName() const { return agentComponentName; }

  /*!
   * \brief GetCurrentState returns the current state of the agent component sending the signal
   *
   * \return The current state of the agent component sending the signal as a ComponentState
   */
  ComponentState GetCurrentState() const { return currentState; }

  //! \brief GetMovementDomain returns the current MovementDomain of the agent component sending the signal
  //! \return The current MovementDomain of the agent component sending the signal as a ComponentState
  MovementDomain GetMovementDomain() const { return movementDomain; }

  /*!
   * \brief GetComponentWarning returns the warning from the agent component sending the signal,
   *        if one exists
   *
   * \return The warning information from the agent component sending the signal
   */
  std::vector<ComponentWarningInformation> GetComponentWarnings() const { return warnings; }

private:
  const ComponentType componentType;                        //!< Type of the component
  const std::string agentComponentName;                     //!< Name of the component
  const ComponentState currentState;                        //!< Current component state
  const MovementDomain movementDomain;                      //!< Movement domain of the component
  const std::vector<ComponentWarningInformation> warnings;  //!< Warning Information
};

//-----------------------------------------------------------------------------
//! VehicleCompToCompCtrlSignal class
//-----------------------------------------------------------------------------
class VehicleCompToCompCtrlSignal : public AgentCompToCompCtrlSignal
{
public:
  /// Component name
  static constexpr char COMPONENTNAME[] = "VehicleCompToCompCtrlSignal";
  /**
   * @brief Construct a new Vehicle Comp To Comp Ctrl Signal object
   *
   * @param componentType         Type of the component
   * @param agentComponentName    Name of the agent component
   * @param currentState          current state of the component
   * @param movementDomain        Movement domain
   * @param adasType              Type of the ADAS component
   */
  VehicleCompToCompCtrlSignal(const ComponentType componentType,
                              const std::string& agentComponentName,
                              const ComponentState currentState,
                              const MovementDomain movementDomain,
                              const AdasType adasType)
      : AgentCompToCompCtrlSignal(componentType, agentComponentName, currentState, movementDomain, {}),
        adasType(adasType)
  {
  }

  /**
   * @brief Construct a new Vehicle Comp To Comp Ctrl Signal object
   *
   * @param componentType         Type of the component
   * @param agentComponentName    Name of the agent component
   * @param currentState          current state of the component
   * @param movementDomain        Movement domain
   * @param warnings              List of component warning informations
   * @param adasType              Type of the ADAS component
   */
  VehicleCompToCompCtrlSignal(const ComponentType componentType,
                              const std::string& agentComponentName,
                              const ComponentState currentState,
                              const MovementDomain movementDomain,
                              const std::vector<ComponentWarningInformation>& warnings,
                              const AdasType adasType)
      : AgentCompToCompCtrlSignal(componentType, agentComponentName, currentState, movementDomain, warnings),
        adasType(adasType)
  {
  }

  VehicleCompToCompCtrlSignal() = delete;
  VehicleCompToCompCtrlSignal(const VehicleCompToCompCtrlSignal&) = delete;
  VehicleCompToCompCtrlSignal(VehicleCompToCompCtrlSignal&&) = delete;
  VehicleCompToCompCtrlSignal& operator=(const VehicleCompToCompCtrlSignal&) = delete;
  VehicleCompToCompCtrlSignal& operator=(VehicleCompToCompCtrlSignal&&) = delete;

  virtual ~VehicleCompToCompCtrlSignal() {}

  /*!
   * \brief GetAdasType returns the adas type
   *
   * \return The adas type
   */
  AdasType GetAdasType() const { return adasType; }

private:
  const AdasType adasType;
};
