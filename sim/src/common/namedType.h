/********************************************************************************
 * Copyright (c) 2020 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/*******************************************************************************
The concept of named types (aka strong types) goes back to
Jonathan Boccara (https://www.fluentcpp.com/) ideas, found at:

https://github.com/joboccara/NamedType (MIT License)
Even if we do not use the full functionality, it its clearly a modification:

MIT License

Copyright (c) 2017 Jonathan Boccara

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*******************************************************************************/

#pragma once

#include <functional>

namespace openpass::type
{
/// A strong type representation
///
/// @tparam T     Base type of the named type
/// @tparam Tag   Tag giving the named type a meaning
template <typename T, typename Tag>
struct NamedType
{
  T value;  ///< Storage for the value of the named type

  /// Constructs a named type
  /// @param value   Value to store in the named type
  constexpr NamedType(T value) noexcept : value(value) {}

  /// Retrieve a stored value
  /// @return The value stored in the named type, using its unterlying base type
  constexpr operator T() const { return value; }

  /// @brief Compare two name types
  /// @param other one name type
  /// @return true, if both name types are equal
  constexpr bool operator==(NamedType<T, Tag> const &other) const noexcept { return this->value == other.value; }

  /// @brief Compare two name types
  /// @param other one name type
  /// @return true, if both name types are not equal
  constexpr bool operator!=(NamedType<T, Tag> const &other) const { return this->value != other.value; }

  /// @brief Compare two name types
  /// @param other one name type
  /// @return true, if both name types are equal
  constexpr bool operator==(T const &other) const { return this->value == other; }

  /// @brief Compare two name types
  /// @param other one name type
  /// @return true, if both name types are not equal
  constexpr bool operator!=(T const &other) const { return this->value != other; }

  //! @brief      less-than
  //! @details    compares the value of two name types
  //! @param[in]	other right-hand side name type for the comparison
  //! @returns	true IFF the value of `this` is less than the value of `other`
  constexpr bool operator<(NamedType<T, Tag> const &other) const { return this->value < other.value; }

  //! @brief      less-than
  //! @details    compares the value of two name types
  //! @param[in]	other right-hand side name type for the comparison
  //! @returns	true IFF the value of `this` is less than the value of `other`
  constexpr bool operator<(T const &other) const { return this->value < other; }

  //! @brief      greater-than
  //! @details    compares the value of two name types
  //! @param[in]	other right-hand side name type for the comparison
  //! @returns	true IFF the value of `this` is greater than the value of `other`
  constexpr bool operator>(NamedType<T, Tag> const &other) const { return this->value > other.value; }

  //! @brief      greater-than
  //! @details    compares the value of two name types
  //! @param[in]	other right-hand side name type for the comparison
  //! @returns	true IFF the value of `this` is greater than the value of `other`
  constexpr bool operator>(T const &other) const { return this->value > other; }

  //! @brief      less-than or equal
  //! @details    compares the value of two name types
  //! @param[in]	other right-hand side name type for the comparison
  //! @returns	true IFF the value of `this` is less than or equal to the value of `other`
  constexpr bool operator<=(NamedType<T, Tag> const &other) const { return this->value <= other.value; }

  //! @brief      greater-than or equal
  //! @details    compares the value of two name types
  //! @param[in]	other right-hand side name type for the comparison
  //! @returns	true IFF the value of `this` is greater than or equal to the value of `other`
  constexpr bool operator>=(T const &other) const { return this->value >= other; }
};

}  // namespace openpass::type
