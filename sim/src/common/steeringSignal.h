/********************************************************************************
 * Copyright (c) 2018 AMFD GmbH
 *               2017-2019 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @brief This file contains all functions for class
//! SteeringSignal
//!
//! This class contains all functionality of the module.
//-----------------------------------------------------------------------------

#pragma once

#include <sstream>
#include <string>
#include <units.h>

#include "include/signalInterface.h"

//-----------------------------------------------------------------------------
//! Signal class
//-----------------------------------------------------------------------------
class SteeringSignal : public ComponentStateSignalInterface
{
public:
  /// component name
  static constexpr char COMPONENTNAME[] = "SteeringSignal";

  /**
   * @brief Construct a new Steering Signal object
   *
   * @param componentState        State of the component
   * @param steeringWheelAngle    Angle of the steering wheel
   */
  SteeringSignal(ComponentState componentState, units::angle::radian_t steeringWheelAngle)
      : ComponentStateSignalInterface(componentState), steeringWheelAngle(steeringWheelAngle)
  {
  }

  /**
   * @brief Construct a new Steering Signal object
   *
   * @param componentState        State of the component
   * @param steeringWheelAngle    Angle of the steering wheel
   * @param source                Name of the source component
   */
  SteeringSignal(ComponentState componentState, units::angle::radian_t steeringWheelAngle, const std::string& source)
      : ComponentStateSignalInterface(componentState), steeringWheelAngle(steeringWheelAngle), source(source)
  {
  }

  SteeringSignal(const SteeringSignal&) = default;  ///< Copy constructor
  SteeringSignal(SteeringSignal&&) = default;       ///< Move constructor
  /// @return Copy assignment operator
  SteeringSignal& operator=(const SteeringSignal&) = default;
  /// @return Move assignment operator
  SteeringSignal& operator=(SteeringSignal&&) = default;

  virtual ~SteeringSignal() = default;

  //-----------------------------------------------------------------------------
  //! Returns the content/payload of the signal as an std::string
  //!
  //! @return                       Content/payload of the signal as an std::string
  //-----------------------------------------------------------------------------
  virtual operator std::string() const
  {
    std::ostringstream stream;
    stream << COMPONENTNAME << std::endl;
    stream << "source: " << source << std::endl;
    stream << "steeringWheelAngle: " << steeringWheelAngle << std::endl;
    return stream.str();
  }

  units::angle::radian_t steeringWheelAngle;  ///< angle of the steering wheel
  std::string source{};                       ///< name of the source component
};
