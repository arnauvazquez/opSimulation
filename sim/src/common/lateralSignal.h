/********************************************************************************
 * Copyright (c) 2018-2019 AMFD GmbH
 *               2016-2017 ITK Engineering GmbH
 *               2017-2019 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  lateralSignal.h
//! @brief This file contains all functions for class
//! LateralSignal
//!
//! This class contains all functionality of the signal.
//-----------------------------------------------------------------------------
#pragma once

#include <sstream>
#include <string>
#include <units.h>
#include <vector>

#include "common/globalDefinitions.h"
#include "include/signalInterface.h"

using namespace units::literals;

/// contains all information of the lateral signal
struct LateralInformation
{
  units::length::meter_t laneWidth{0.0};  //!< Lateral net distance that needs to be covered
  units::length::meter_t deviation{0.0};  //!< Current lateral deviation regarding trajectory
  units::angular_acceleration::radians_per_second_squared_t gainDeviation{
      0.0};                                         //!< Gain of lateral deviation controller
  units::angle::radian_t headingError{0.0};         //!< Current heading error regarding trajectory
  units::frequency::hertz_t gainHeadingError{0.0};  //!< Gain of the heading error controller
  units::curvature::inverse_meter_t kappaManoeuvre{
      0.0};  //!< Current curvature adjustment due to manoevre e.g. lane changing
};

/// class contains all functionality of the lateral signal
class LateralSignal : public ComponentStateSignalInterface
{
public:
  /// component name
  const std::string COMPONENTNAME = "LateralSignal";

  //-----------------------------------------------------------------------------
  //! Constructor
  //-----------------------------------------------------------------------------
  LateralSignal() { componentState = ComponentState::Disabled; }

  /**
   * @brief Construct a new Lateral Signal object
   *
   * @param componentState        State of the component
   * @param lateralInformation    Current lateral information
   */
  LateralSignal(ComponentState componentState, LateralInformation lateralInformation)
      : lateralInformation(lateralInformation)
  {
    this->componentState = componentState;
  }

  /**
   * @brief Construct a new Lateral Signal object
   *
   * @param componentState        State of the component
   * @param lateralInformation    Current lateral information
   * @param source                name of the source component
   */
  LateralSignal(ComponentState componentState, LateralInformation lateralInformation, const std::string& source)
      : lateralInformation(lateralInformation), source(source)
  {
    this->componentState = componentState;
  }

  /**
   * @brief Construct a new Lateral Signal object
   *
   * @param componentState                    State of the component
   * @param lateralInformation                Current lateral information
   * @param kappaRoad                         Current curvature of road
   * @param curvatureOfSegmentsToNearPoint    Curvature at the middle of each segment from reference point to the near
   * point
   * @param curvatureOfSegmentsToFarPoint     Curvature of segments between near point and far point
   */
  LateralSignal(ComponentState componentState,
                LateralInformation lateralInformation,
                units::curvature::inverse_meter_t kappaRoad,
                std::vector<units::curvature::inverse_meter_t> curvatureOfSegmentsToNearPoint,
                std::vector<units::curvature::inverse_meter_t> curvatureOfSegmentsToFarPoint)
      : ComponentStateSignalInterface{componentState},
        lateralInformation(lateralInformation),
        kappaRoad(kappaRoad),
        curvatureOfSegmentsToNearPoint(curvatureOfSegmentsToNearPoint),
        curvatureOfSegmentsToFarPoint(curvatureOfSegmentsToFarPoint)
  {
  }

  /**
   * @brief Construct a new Lateral Signal object
   *
   * @param componentState                    State of the component
   * @param lateralInformation                Current lateral information
   * @param kappaRoad                         Current curvature of road
   * @param curvatureOfSegmentsToNearPoint    Curvature at the middle of each segment from reference point to the near
   * point
   * @param curvatureOfSegmentsToFarPoint     Curvature of segments between near point and far point
   * @param source                            name of the source component
   */
  LateralSignal(ComponentState componentState,
                LateralInformation lateralInformation,
                units::curvature::inverse_meter_t kappaRoad,
                std::vector<units::curvature::inverse_meter_t> curvatureOfSegmentsToNearPoint,
                std::vector<units::curvature::inverse_meter_t> curvatureOfSegmentsToFarPoint,
                const std::string& source)
      : ComponentStateSignalInterface{componentState},
        lateralInformation(lateralInformation),
        kappaRoad(kappaRoad),
        curvatureOfSegmentsToNearPoint(curvatureOfSegmentsToNearPoint),
        curvatureOfSegmentsToFarPoint(curvatureOfSegmentsToFarPoint),
        source(source)
  {
  }

  /**
   * @brief Construct a new Lateral Signal object
   *
   * @param other another lateral signal
   */
  LateralSignal(LateralSignal& other) : LateralSignal(other.componentState, other.lateralInformation) {}

  virtual ~LateralSignal() {}

  //-----------------------------------------------------------------------------
  //! Returns the content/payload of the signal as an std::string
  //!
  //! @return                       Content/payload of the signal as an std::string
  //-----------------------------------------------------------------------------
  virtual operator std::string() const
  {
    std::ostringstream stream;
    stream << COMPONENTNAME << '\n';
    stream << "source: " << source << '\n';
    stream << "laneWidth: " << lateralInformation.laneWidth << '\n';
    stream << "deviation: " << lateralInformation.deviation << '\n';
    stream << "gainDeviation: " << lateralInformation.gainDeviation << '\n';
    stream << "headingError: " << lateralInformation.headingError << '\n';
    stream << "gainHeadingError: " << lateralInformation.gainHeadingError << '\n';
    stream << "kappaManoeuvre: " << lateralInformation.kappaManoeuvre << '\n';
    stream << "kappaRoad: " << kappaRoad << '\n';
    // stream << "curvatureOfSegmentsToNearPoint: " << curvatureOfSegmentsToNearPoint. << '\n';
    // stream << "curvatureOfSegmentsToFarPoint: " << curvatureOfSegmentsToFarPoint << '\n';
    return stream.str();
  }

  //! Information of the lateral signal
  LateralInformation lateralInformation{};
  //! Current curvature of road
  units::curvature::inverse_meter_t kappaRoad{0.};
  //! Curvature at the middle of each segment from reference point to the near point
  std::vector<units::curvature::inverse_meter_t> curvatureOfSegmentsToNearPoint{0.0_i_m};
  //! Curvature of segments between near point and far point
  std::vector<units::curvature::inverse_meter_t> curvatureOfSegmentsToFarPoint{0.0_i_m};
  /// name of the source component
  std::string source{};

  /*!
   * \brief Operator function to compare two signals
   *
   * @param other another lateral signal
   * @return this lateral signal
   */
  LateralSignal& operator=(const LateralSignal& other)
  {
    componentState = other.componentState;
    lateralInformation = other.lateralInformation;

    return *this;
  }
};
