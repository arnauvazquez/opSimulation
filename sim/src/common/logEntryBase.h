/********************************************************************************
 * Copyright (c) 2020-2021 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  logEntryBase.h
//-----------------------------------------------------------------------------
#pragma once

#include "common/openPassTypes.h"
#include "include/dataBufferInterface.h"

namespace openpass::publisher
{

/**
 * @brief Base class for logging of acyclic events in the dataBuffer
 * @see   openpass::publisher::coreDataPublisher
 * @see   openpass::publisher::LogEntry
 */
class LogEntryBase
{
public:
  LogEntryBase() = default;

  /**
   * @brief Construct a new Log Entry Base object
   *
   * @param name Name
   */
  explicit LogEntryBase(std::string name) : name{std::move(name)} {}

  /**
   * @brief Construct a new Log Entry Base object
   *
   * @param name                  Name
   * @param triggeringEntities    Triggering entities type
   * @param affectedEntities      Affected entities type
   */
  LogEntryBase(std::string name,
               openpass::type::TriggeringEntities triggeringEntities,
               openpass::type::AffectedEntities affectedEntities)
      : name{std::move(name)},
        triggeringEntities{std::move(triggeringEntities)},
        affectedEntities{std::move(affectedEntities)}
  {
  }

  /// name of the log entry
  std::string name;
  /// triggering entities
  openpass::type::TriggeringEntities triggeringEntities;
  /// affected entities
  openpass::type::AffectedEntities affectedEntities;

  /// @return An acyclic object usable in a databuffer
  virtual operator openpass::databuffer::Acyclic() const = 0;

  // We do not allow copy to force move semantics
  LogEntryBase(const LogEntryBase &) = delete;
  LogEntryBase &operator=(const LogEntryBase &) = delete;

  /// Explicitly allow move
  LogEntryBase(LogEntryBase &&) = default;

  /// @return Move assignment operator
  LogEntryBase &operator=(LogEntryBase &&) = default;

  // Neccesary evil
  virtual ~LogEntryBase() = default;
};

}  // namespace openpass::publisher
