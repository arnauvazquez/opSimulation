/********************************************************************************
 * Copyright (c) 2019 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <memory>
#include <optional>
#include <string>
#include <unordered_map>
#include <vector>

#include "MantleAPI/Execution/i_environment.h"

class ParameterInterface;
class WorldInterface;
class StochasticsInterface;
class AgentBlueprintProviderInterface;
namespace core
{
class AgentFactoryInterface;
}

enum class SpawnPointType
{
  PreRun = 0,
  Runtime
};

static const std::unordered_map<SpawnPointType, std::string> spawnPointTypeMapping{
    {SpawnPointType::PreRun, "PreRun"}, {SpawnPointType::Runtime, "Runtime"}};

/// spawn point library information
struct SpawnPointLibraryInfo
{
  /// library name
  std::string libraryName{};
  /// spawn point type
  SpawnPointType type{};
  /// priority
  int priority{};
  /// profile name
  std::optional<std::string> profileName{};
};
using SpawnPointLibraryInfoCollection = std::vector<SpawnPointLibraryInfo>;
using Vehicles = std::map<std::string, std::shared_ptr<const mantle_api::VehicleProperties>>;

/// @brief Dependencies of a given spawn point
struct SpawnPointDependencies
{
  SpawnPointDependencies() = default;

  /// @brief default constructor
  SpawnPointDependencies(const SpawnPointDependencies&) = default;

  /// @brief default constructor
  SpawnPointDependencies(SpawnPointDependencies&&) = default;

  /**
   * @brief Construct a new Spawn Point Dependencies object
   *
   * @param agentFactory              Pointer to the agent factory
   * @param world                     Pointer to the world
   * @param agentBlueprintProvider    Pointer to the agent blueprint provider
   * @param stochastics               Pointer to the stochastics
   * @param environment               Shared pointer to the environment
   * @param vehicles                  Shared pointer containing the Vehicles of the VehiclesCatalog
   */
  SpawnPointDependencies(core::AgentFactoryInterface* agentFactory,
                         WorldInterface* world,
                         const AgentBlueprintProviderInterface* agentBlueprintProvider,
                         StochasticsInterface* stochastics,
                         std::shared_ptr<mantle_api::IEnvironment> environment,
                         std::shared_ptr<Vehicles> vehicles)
      : agentFactory{agentFactory},
        world{world},
        agentBlueprintProvider{agentBlueprintProvider},
        stochastics{stochastics},
        environment{environment},
        vehicles{vehicles}
  {
  }

  /// @return dependencies of a spawn point with opertor = overload
  SpawnPointDependencies& operator=(const SpawnPointDependencies&) = default;

  /// @return dependencies of a spawn point with opertor = overload
  SpawnPointDependencies& operator=(SpawnPointDependencies&&) = default;

  /// agent factory
  core::AgentFactoryInterface* agentFactory{nullptr};
  /// world interface
  WorldInterface* world{nullptr};
  /// agent blueprint provider interface
  const AgentBlueprintProviderInterface* agentBlueprintProvider{nullptr};
  /// stochastics interface
  StochasticsInterface* stochastics{nullptr};
  /// parameter interface
  std::optional<ParameterInterface*> parameters{std::nullopt};
  //! Shared pointer to the environment
  std::shared_ptr<mantle_api::IEnvironment> environment{nullptr};
  //! Shared pointer containing the Vehicles of the VehiclesCatalog
  std::shared_ptr<Vehicles> vehicles{nullptr};
};
