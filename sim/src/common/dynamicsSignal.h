/********************************************************************************
 * Copyright (c) 2018 AMFD GmbH
 *               2017-2019 in-tech GmbH
 *               2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  dynamicsSignal.h
//! @brief This file contains all functions for class
//! DynamicsSignal
//!
//! This class contains all functionality of the module.
//-----------------------------------------------------------------------------

#pragma once

#include <sstream>
#include <string>
#include <units.h>

#include "common/globalDefinitions.h"
#include "include/signalInterface.h"

//-----------------------------------------------------------------------------
//! Signal class
//-----------------------------------------------------------------------------
class DynamicsSignal : public ComponentStateSignalInterface
{
public:
  /// component name
  static constexpr char COMPONENTNAME[] = "DynamicsSignal";

  DynamicsSignal() : ComponentStateSignalInterface{ComponentState::Disabled} {}
  /**
   * @brief Construct a new Dynamics Signal object
   *
   * @param componentState state of the component
   */
  DynamicsSignal(ComponentState componentState) : ComponentStateSignalInterface{componentState} {}

  /**
   * @brief Construct a new Dynamics Signal object
   *
   * @param componentState state of the component
   * @param dynamicsInformation calculated values
   */
  DynamicsSignal(ComponentState componentState, DynamicsInformation dynamicsInformation)
      : ComponentStateSignalInterface{componentState}, dynamicsInformation(dynamicsInformation)
  {
  }

  /**
   * @brief Construct a new Dynamics Signal object
   *
   * @param componentState state of the component
   * @param dynamicsInformation calculated values
   * @param longitudinalController active longitudinal component
   * @param lateralController active lateral component
   */
  DynamicsSignal(ComponentState componentState,
                 DynamicsInformation dynamicsInformation,
                 const std::string& longitudinalController,
                 const std::string& lateralController)
      : ComponentStateSignalInterface{componentState},
        dynamicsInformation(dynamicsInformation),
        longitudinalController(longitudinalController),
        lateralController(lateralController)
  {
  }

  //! copy constructor
  //! @param other another dynamic signal
  DynamicsSignal(DynamicsSignal& other) = default;
  DynamicsSignal(const DynamicsSignal&) = default;  //!< copy constructor
  DynamicsSignal(DynamicsSignal&&) = default;       //!< copy constructor

  //! @return copy assignment operator
  DynamicsSignal& operator=(const DynamicsSignal&) = default;

  //! @return move assignment operator
  DynamicsSignal& operator=(DynamicsSignal&&) = default;
  virtual ~DynamicsSignal() = default;

  //-----------------------------------------------------------------------------
  //! Returns the content/payload of the signal as an std::string
  //!
  //! @return                       Content/payload of the signal as an std::string
  //-----------------------------------------------------------------------------
  virtual operator std::string() const
  {
    std::ostringstream stream;
    stream << COMPONENTNAME << std::endl;
    stream << "longitudinalController: " << longitudinalController << std::endl;
    stream << "lateralController: " << lateralController << std::endl;
    stream << "acceleration: " << dynamicsInformation.acceleration << std::endl;
    stream << "velocityX: " << dynamicsInformation.velocityX << std::endl;
    stream << "velocityY: " << dynamicsInformation.velocityY << std::endl;
    stream << "positionX: " << dynamicsInformation.positionX << std::endl;
    stream << "positionY: " << dynamicsInformation.positionY << std::endl;
    stream << "yaw: " << dynamicsInformation.yaw << std::endl;
    stream << "yawRate: " << dynamicsInformation.yawRate << std::endl;
    stream << "yawAcceleration" << dynamicsInformation.yawAcceleration << std::endl;
    stream << "roll: " << dynamicsInformation.roll << std::endl;
    stream << "steeringWheelAngle: " << dynamicsInformation.steeringWheelAngle << std::endl;
    stream << "centripetalAcceleration: " << dynamicsInformation.centripetalAcceleration << std::endl;
    stream << "travelDistance: " << dynamicsInformation.travelDistance << std::endl;
    return stream.str();
  }

  DynamicsInformation dynamicsInformation{};  //!< All values regarding the dynamic calculation of the agent
  std::string longitudinalController{};       //!< Component that is active for longitudinal control
  std::string lateralController{};            //!< Component that is active for lateral control
};
