/********************************************************************************
 * Copyright (c) 2017-2020 ITK Engineering GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include "common/globalDefinitions.h"
#include "common/vector2d.h"
#include "units.h"

using namespace units::literals;

#define POSTCRASHDYNAMICID 0

/*!
 * \brief Class to store post-crash dynamics data
 * Used to store information about the dynamic after a collision.
 */
class PostCrashDynamic
{
public:
  //! Container Class to store all relevant data for a post crash dynamic
  //!
  //! \param velocity
  //! \param velocityChange
  //! \param velocityDirection
  //! \param yawVelocity
  //! \param pulse
  //! \param pulseDirection
  //! \param pulseLocal
  //! \param pointOfContactLocal
  //! \param collisionVelocity
  //! \param sliding
  //!
  PostCrashDynamic(units::velocity::meters_per_second_t velocity = 0_mps,
                   units::velocity::meters_per_second_t velocityChange = 0_mps,
                   units::angle::radian_t velocityDirection = 0_rad,
                   units::angular_velocity::radians_per_second_t yawVelocity = 0_rad_per_s,
                   Common::Vector2d<units::impulse> pulse = Common::Vector2d<units::impulse>(),
                   units::angle::radian_t pulseDirection = 0_rad,
                   Common::Vector2d<units::impulse> pulseLocal = Common::Vector2d<units::impulse>(),
                   Common::Vector2d<units::length::meter_t> pointOfContactLocal
                   = Common::Vector2d<units::length::meter_t>(),
                   units::velocity::meters_per_second_t collisionVelocity = 0_mps,
                   bool sliding = false)
      : velocity(velocity),
        velocityChange(velocityChange),
        velocityDirection(velocityDirection),
        yawVelocity(yawVelocity),
        pulse(pulse),
        pulseDirection(pulseDirection),
        pulseLocal(pulseLocal),
        pointOfContactLocal(pointOfContactLocal),
        collisionVelocity(collisionVelocity),
        sliding(sliding)
  {
  }
  virtual ~PostCrashDynamic() = default;

  /** @brief Function to get post crash velocity
   *
   * \return double returns post crash velocity
   */
  units::velocity::meters_per_second_t GetVelocity() const { return velocity; }

  /** @brief Function to set post crash velocity
   *
   * \param[in] value post crash velocity
   */
  void SetVelocity(units::velocity::meters_per_second_t value) { velocity = value; }

  /** @brief Function to get delta-V: collision induced velocity change [m/s]
   *
   * \return double returns delta-V: collision induced velocity change [m/s]
   */
  units::velocity::meters_per_second_t GetVelocityChange() const { return velocityChange; }

  /** @brief Function to set delta-V: collision induced velocity change [m/s]
   *
   * \param[in] value delta-V: collision induced velocity change [m/s]
   */
  void SetVelocityChange(units::velocity::meters_per_second_t value) { velocityChange = value; }

  /** @brief Function to get post crash velocity direction [rad]
   *
   * \return double returns post crash velocity direction [rad]
   */
  units::angle::radian_t GetVelocityDirection() const { return velocityDirection; }

  /** @brief Function to set post crash velocity direction [rad]
   *
   * \param[in] value post crash velocity direction [rad]
   */
  void SetVelocityDirection(units::angle::radian_t value) { velocityDirection = value; }

  /** @brief Function to get post crash yaw velocity [rad/s]
   *
   * \return double returns post crash yaw velocity [rad/s]
   */
  units::angular_velocity::radians_per_second_t GetYawVelocity() const { return yawVelocity; }

  /** @brief Function to set post crash yaw velocity [rad/s]
   *
   * \param[in] value post crash yaw velocity [rad/s]
   */
  void SetYawVelocity(units::angular_velocity::radians_per_second_t value) { yawVelocity = value; }

  /** @brief Function to get pulse vector [two-dimensional vector: kg*m/s,kg*m/s]
   *
   * \return vector returns pulse vector [two-dimensional vector: kg*m/s,kg*m/s]
   */
  Common::Vector2d<units::impulse> GetPulse() const { return pulse; }

  /** @brief Function to set pulse vector [two-dimensional vector: kg*m/s,kg*m/s]
   *
   * \param[in] value pulse vector [two-dimensional vector: kg*m/s,kg*m/s]
   */
  void SetPulse(const Common::Vector2d<units::impulse> &value) { pulse = value; }

  /** @brief Function to get pulse direction [rad]
   *
   * \return double returns pulse direction [rad]
   */
  units::angle::radian_t GetPulseDirection() const { return pulseDirection; }

  /** @brief Function to set pulse direction [rad]
   *
   * \param[in] value pulse direction [rad]
   */
  void SetPulseDirection(units::angle::radian_t value) { pulseDirection = value; }

  /** @brief Function to get crash pulse in local vehicle coordinate system
   *
   * \return vector2d returns crash pulse in local vehicle coordinate system
   */
  Common::Vector2d<units::impulse> GetPulseLocal() const { return pulseLocal; }

  /** @brief Function to crash pulse in local vehicle coordinate system
   *
   * \param[in] value crash pulse in local vehicle coordinate system
   */
  void SetPulseLocal(Common::Vector2d<units::impulse> value) { pulseLocal = value; }

  /** @brief Function to get point of impact in local vehicle coordinate system
   *
   * \return vector2d returns point of impact in local vehicle coordinate system
   */
  Common::Vector2d<units::length::meter_t> GetPointOfContactLocal() const { return pointOfContactLocal; }

  /** @brief Function to set point of impact in local vehicle coordinate system
   *
   * \param[in] value point of impact in local vehicle coordinate system
   */
  void SetPointOfContactLocal(const Common::Vector2d<units::length::meter_t> &value) { pointOfContactLocal = value; }

  /** @brief Function to get collision velocity
   *
   * \return double returns collision velocity
   */
  units::velocity::meters_per_second_t GetCollisionVelocity() const { return collisionVelocity; }

  /** @brief Function to set collision velocity
   *
   * \param[in] value collision velocity
   */
  void SetCollisionVelocity(units::velocity::meters_per_second_t value) { collisionVelocity = value; }

  /** @brief Function to get sliding flag
   *
   * \return bool returns sliding flag
   */
  bool GetSliding() const { return sliding; }

  /** @brief Function to set sliding flag
   *
   * \param[in] value sliding flag
   */
  void SetSliding(bool value) { sliding = value; }

private:
  //! post crash velocity, absolute [m/s]
  units::velocity::meters_per_second_t velocity{0};
  //! delta-V: collision induced velocity change [m/s]
  units::velocity::meters_per_second_t velocityChange{0};
  //! post crash velocity direction [rad]
  units::angle::radian_t velocityDirection{0};
  //! post crash yaw velocity [rad/s]
  units::angular_velocity::radians_per_second_t yawVelocity{0};
  //! pulse vector [two-dimensional vector: kg*m/s,kg*m/s]
  Common::Vector2d<units::impulse> pulse = Common::Vector2d(units::impulse(0), units::impulse(0));
  //! pulse direction [rad]
  units::angle::radian_t pulseDirection{0};
  //! crash pulse in local vehicle coordinate system
  Common::Vector2d<units::impulse> pulseLocal = Common::Vector2d(units::impulse(0), units::impulse(0));
  //! point of impact in local vehicle coordinate system
  Common::Vector2d<units::length::meter_t> pointOfContactLocal = Common::Vector2d<units::length::meter_t>(0_m, 0_m);
  //! collision velocity
  units::velocity::meters_per_second_t collisionVelocity{0};
  /*!
   * \brief flag for collision type
   *
   * true: sliding collision, false: no sliding collision
   *
   * Note that the collision model is not valid for sliding collisions.
   */
  bool sliding = false;
};
