/********************************************************************************
 * Copyright (c) 2019-2020 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <sstream>
#include <string>

namespace openpass::common
{

//! Struct for defining the version of the simulator build
//! The version can either be given by using semantic versioning with three integers
//! or as some generic string
//! @return simulator build version
struct Version
{
  ///version of thhe simulator build
  std::string tag;
  ///major release number
  unsigned int major{0};
  ///minor release number
  unsigned int minor{0};
  ///patch number
  unsigned int patch{0};

  /**
   * @brief Construct a new Version object
   *
   * @param tag generic string
   */
  explicit Version(const std::string& tag) : tag{tag} {}

  /**
   * @brief Constructor using semantic versioning
   *
   * @param major     Major number
   * @param minor     Minor version number
   * @param patch     Patch number
   */
  explicit Version(unsigned int major, unsigned int minor, unsigned int patch) noexcept
      : major{major}, minor{minor}, patch{patch}
  {
  }

  /**
   * @brief Returns version as string
   *
   * @return tag as string
   */
  std::string str() const noexcept
  {
    if (tag.empty())
    {
      std::ostringstream oss;
      oss << major << '.' << minor << '.' << patch;
      return oss.str();
    }
    else
    {
      return tag;
    }
  }
};

}  // namespace openpass::common
