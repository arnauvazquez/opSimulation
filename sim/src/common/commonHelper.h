/********************************************************************************
 * Copyright (c) 2020 in-tech GmbH
 *               2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <map>
#include <units.h>
#include <unordered_map>
#include <vector>

#include "math.h"

/* std::abs is not constexpr (at least up to C++20) other than gcc, MSVC sticks to this standard.
   this is a constexpr version of abs which should work with all compilers*/
inline constexpr double constAbs(double a) noexcept
{
    return (a >= 0) ? a : -a;
}

//-----------------------------------------------------------------------------
//! @brief defines common helper functions like conversion from and to enums.
//-----------------------------------------------------------------------------
namespace CommonHelper
{
static constexpr double EPSILON = 0.001;  //!Treat values smaller than epsilon as zero for geometric calculations

[[maybe_unused]] static inline constexpr bool DoubleEquality(double value1, double value2, double epsilon = EPSILON)
{
  return std::abs(value1 - value2) <= epsilon;
}

//! Returns the same angle but within the range [-PI, PI]
[[maybe_unused]] static inline units::angle::radian_t SetAngleToValidRange(units::angle::radian_t angle)
{
  units::angle::radian_t pi{M_PI};

  return (angle >= -pi) ? units::math::fmod(angle + pi, 2 * pi) - pi : units::math::fmod(angle + pi, 2 * pi) + pi;
}

//-----------------------------------------------------------------------------
//! @brief Calculate linear interpolated points with constant spacing.
//!
//! @param[in] start            Start of interval
//! @param[in] end              End of interval
//! @param[in] totalPoints      Total number of points returned (includes start
//!                             and end point)
//!
//! @return                     Vector of interpolated points.
//-----------------------------------------------------------------------------
[[maybe_unused]] static std::vector<double> InterpolateLinear(const double start,
                                                              const double end,
                                                              const int totalPoints)
{
  std::vector<double> elements;

  for (int i = 0; i < totalPoints; ++i)
  {
    elements.push_back(start + (i * ((end - start) / (totalPoints - 1))));
    //elements.push_back(start + (i * ((end-start+1) / (totalPoints))));  //not correct implemented yet
  }
  return elements;
}

//-----------------------------------------------------------------------------
//! @brief Calculate linear interpolated points with constant spacing for unit types.
//!
//! @param[in] start            Start of interval
//! @param[in] end              End of interval
//! @param[in] totalPoints      Total number of points returned (includes start
//!                             and end point)
//!
//! @return                     Vector of interpolated points.
//-----------------------------------------------------------------------------
template <typename T, class = typename std::enable_if_t<units::traits::is_unit_t<T>::value>>
[[maybe_unused]] static std::vector<T> InterpolateLinear(const T start, const T end, const int totalPoints)
{
  std::vector<T> elements;

  for (int i = 0; i < totalPoints; ++i)
  {
    elements.push_back(start + (i * ((end - start) / (totalPoints - 1))));
    //elements.push_back(start + (i * ((end-start+1) / (totalPoints))));  //not correct implemented yet
  }
  return elements;
}

template <typename T>
[[maybe_unused]] static inline constexpr T PerHourToPerSecond(T value) noexcept
{
  static_assert(std::is_floating_point_v<T>, "the value must be a floating point data type");
  return value / static_cast<T>(3600.0);
}

template <typename T>
[[maybe_unused]] static inline constexpr T KilometerPerHourToMeterPerSecond(T kmH) noexcept
{
  static_assert(std::is_floating_point_v<T>, "kmH must be a floating point data type");
  return kmH / static_cast<T>(3.6);
}
};  // end namespace CommonHelper
